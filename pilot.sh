#!/bin/bash

PROGRAM="Pthread"

export AUTOPILOT_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Каталог в котором будем искать файл
WATCH_DIR="${AUTOPILOT_HOME}/tmp/"

# Имя файла по которому будем определять запущен ли уже Родитель
LOCK_FILE="${WATCH_DIR}/monitor_file.lock"

# Файл где будем хранить номер работающего процесса Родителя
PID_FILE="${WATCH_DIR}/monitor_file.pid"

PID_FILE_CHILD="${WATCH_DIR}/monitor_file_child.pid"

# Имя файла по которому будем определять запущен ли Потомок
JOB_LOCK_FILE="${WATCH_DIR}/job_monitor_file.lock"

# В этот файл будем писать ход выполнения скрипта
LOG="${WATCH_DIR}/monitor_file_work.log"

# В этот файл будут попадать ошибки при работе скрипта
ERR_LOG="${WATCH_DIR}/monitor_file_error.log"

# Выводим помощь
usage()
{
        echo "$0 (start|stop)"
        echo "Autopilot"
        echo "this programm for Raspberry Pi3 and Navio2"
}

# Функция логирования
_log()
{
        process=$1
        shift
        echo "${process}[$$]: $*"
}

# Функция остановки демона
stop()
{
        # Если существует pid файл, то убиваем процесс с номером из pid файла
        if [ -e ${PID_FILE} ]
        then
                _pid=$(cat ${PID_FILE})
                sudo kill $_pid
                rt=$?
                if [ "$rt" == "0" ]
                then
                        echo "Daemon parent stop"
                else
                        echo "Error stop parent daemon"
                fi
        else
                echo "Daemon parent is't running"
        fi

        if [ -e ${PID_FILE_CHILD} ]
        then
                _pid=$(cat ${PID_FILE_CHILD})
                sudo kill $_pid
                rt=$?
                if [ "$rt" == "0" ]
                then
                        echo "Daemon stop"
                else
                        echo "Error stop daemon"
                fi
        else
                echo "Daemon is't running"
        fi

        
	    for PID in `ps -aef | grep $PROGRAM | awk '{print $2}'`
	    do
			sudo kill $PID
		done
		
}

# Функция запуска демона
start()
{
	# Если существует файл с pid процесса не запускаем еще одну копию демона
	if [ -e $PID_FILE ]
	then
		_pid=$(cat ${PID_FILE})
		if [ -e /proc/${_pid} ]
		then
			echo "Daemon already running with pid = $_pid"
			exit 0
		fi
	fi
	rm -f ${LOG}
	rm -f ${ERR_LOG}
	rm -f ${PID_FILE_CHILD}
	# Создаем файлы логов
	touch ${LOG}
	touch ${ERR_LOG}

	# переходим в корень, что бы не блокировать фс
	cd /

	# Перенаправляем стандартный вывод, вывод ошибок и стандартный ввод
	exec > $LOG
	exec 2> $ERR_LOG
	exec < /dev/null
    
    

	# Запускаем подготовленную копию процесса, вообщем форкаемся. Здесь происходит вся работа скрипта
	(
		# Не забываем удалять файл с номером процесса и файл очереди при выходе 
		trap  "{ rm -f ${PID_FILE}; exit 255; }" TERM INT EXIT 
		# Основной цикл работы скрипта
		while [ 1 ]
		do
			
			if [ ! -e ${JOB_LOCK_FILE} ]
			then
				run_job
				_log "parent" "Running job with pid $!"
			fi
						
			# Дадим процессору отдохнуть
			sleep 1

		done

		exit 0
	)&

	# Пишем pid потомка в файл, и заканчиваем работу
	echo $! > ${PID_FILE}
}

# Функция запуска Потомка. Потомок берет первый элемент из массива JOBS, и работает
run_job()
{
	# Здесь происходит порождение Потомка. Здесь уже ничего не подготавливаем и не переопределяем стандартный ввод/вывод, т.к. это уже сделано в Родителе
	(
		
		# Не забываем удалять после окончания работы файл
		trap "{ rm -f ${JOB_LOCK_FILE}; exit 255; }" TERM INT EXIT
		# Дополнительная проверка что бы убедиться что Потомок один
		if [ ! -e ${JOB_LOCK_FILE} ]
		then
			# Пишем номер pid процесса в файл, на всякий случай
			echo "$$" > ${JOB_LOCK_FILE}
			_log "child" "Job with pid $$"
			sudo ${AUTOPILOT_HOME}/src/${PROGRAM}
			_log "child" "Start Pthread"
			
		else
			_log "child" "Lock file is exists"
		fi
		# Выходим
		exit 0
	)&
	echo $! > ${PID_FILE_CHILD}
}

case $1 in
        "start")
                start
                ;;
        "stop")
                stop
                ;;
        *)
                usage
                ;;
esac
exit

