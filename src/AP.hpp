#ifndef AP_HPP
#define AP_HPP

#define DEBUG_THROTTLE false
#define DEBUG_GPS false
#define DEBUG_ACCEL false
#define DEBUG_GYRO false
#define DEBUG_ROLL_PICH_YAW false
#define DEBUG_ZAD false

#define QUADRODRONE_SIMPLEPID_LOGIC true
#define AHRS_GIR true


#define FREQ_OSN   800                 // основная частота
#define FREQ_GIR   (FREQ_OSN)          // Частота опроса датчиков 
#define DT_GIR     (1.0/FREQ_GIR)
#define FREQ_PWM   200

#define RZ  6380.// радиус земли возможно 6371.
#define G 9.8065
#define ToGrad 57.2957795130823
#define M_PI   3.14159265358979323846
#define RO 0.125

#define NBFM    50

#include "x51.h"
#include <float.h>
#include <math.h>
#include <stdio.h>

#include <vector>

#include <iostream>
#include <cstdlib>
#include <pthread.h>

#include "PID.hpp"

pthread_mutex_t mutex_ap = PTHREAD_MUTEX_INITIALIZER;


class AP
{
    private:

    float m_pitch; //tang
    float m_roll;  //kren 
    float m_yaw;   //rysk

    PID pitchPID;
    PID rollPID;
    PID yawPID;

    PID pitchAnglePID;
    PID rollAnglePID;
    PID yawForcePID;

    //From Old Autopilot
    //----------------------------------------------------------------------------------------
    struct vektor
    {
        float x, y, z;
    };

    //------------------------------------------------------------------------------------------------
    float V_bum;

    //-------------------------------------------------------------------------------------

    int H_Mar[128];
    unsigned char Vz_Mar[128], n_, i_mar;
    float cos_Lat0;

    //--------------------------------------------------------------------------------------------

    float H_dat, Vy_dat, H_filtr, H_filtr_pr, Vy_filtr, H_start_landing, delta_RO = 1, Vz, Vz_tmp, V_dat, kV, q_dat;
    float koors_zad, koors, koors_tmp, koors_zad1;
    float  H_zad, H_max;
    float V_zad = 15, ax_zad, ax_filtr, Vv_dat, Vxv_dat, Vzv_dat;
    signed char Vz_zad = 1;
    float kren_zad, kren_zad_filtr;

    char KrenKam, UgolKam, KrenKam_zad, UgolKam_zad;
    float wx0, wy0, wz0;

    float delta_g_p1, delta_g_l1, delta_g_p2, delta_g_l2, delta_u_p1, delta_u_l1, delta_u_p2, delta_u_l2, delta_u;
    float g_p1, g_l1, g_p2, g_l2, u_p1, u_l1, u_p2, u_l2;
    float delta_g, delta_u_upor;
    float rsk_gir, tang_gir, kren_gir, teta_and_alfa, rsk_m, rsk_gps, alfa, kren_dat;
    float kren_din, tang_din;
    float cos_kren_gir, sin_kren_gir, sin_tang_gir, cos_tang_gir;
    float drsk_gir, dtang_gir, dkren_gir, int_dtang, int_dtang0, int_dkoors;
    float int_dV, int_dkren, int_dVy;
    float kren_zad_effect, slip, wx_zad, wz_zad, Vy_zad;

    unsigned char flAnswer;
    bool flRun   //сработал таймер основного цикла
        , flOtkazRK
            , flKoord
                , flTele
                    , flstopThrottleAndConversion
                        , flCommandstopThrottleAndConversion
                            , flAutostopThrottleAndConversion
                                , flStoika
                                    , flOutTang
                                        , flOutKren
                                            , flOutH
                                                , flOutU
                                                    , flFaultH
                                                        , flFaultV
                                                            , flFaultWx
                                                                , flFaultWy
                                                                    , flFaultWz
                                                                        , flOutLand
                                                                            , flOtkazRK05;

    signed char erWx, erWy, erWz;


    float rasst_toch_mar;

    #define SIZE_BUFFER0  80
    unsigned char BufferInModem[SIZE_BUFFER0]; // Для отправки в последовательный порт

    float dt0_p1 = 2.0e-3, dt90_p1 = 1.1e-3, dt0_l1 = 1.95e-3, dt90_l1 = 1.15e-3
        , dt0_p2 = 1.2e-3, dt90_p2 = 2.0e-3, dt0_l2 = 1.05e-3, dt90_l2 = 1.95e-3;//пока


    float alfa0, i_tang, p_tang, d_tang, d2_tang, k1_tang, k2_tang, g_tang, v_tang, tang_zad, tang_zad_filtr;
    float kren0, i_kren, p_kren, d_kren, d2_kren, k1_kren, k2_kren, kren_zad_wy;
    float i_rsk, p_rsk, d_rsk, d2_rsk, k1_rsk, k2_rsk;
    float i_ay, p_ay, d_ay, k1_ay, dH_max;
    float i_ax, p_ax, d_ax, k1_ax, U_end;
    float delta_ax, delta_ay, delta_u_zad, delta_g_zad, delta_tang, delta_rsk, delta_kren;
    float v_rsk, g_kren, g_rsk, v_kren, kl, gaz0, tdelta_g_max = 1.0e-3, tdelta_g_min_infly = 1.0e-3, tdelta_g_min = 1.0e-3, w_max, delta_max;

    double delta_time_gir = 0.0;
    //---------------------------------------------------------------------------
    unsigned int fromEarth2(unsigned char mess[], unsigned char i)
    {
        return((unsigned int)mess[i]& 0x7f)+((mess[i+1]& 0x7f)<< 7);
    }

    struct vektor w_dat, w_dat_pr, e_dat, e_zad, a_dat, a_dat1, a_gir, V_gir, S_gir, l, v1, v2, v3, a_dat_ct, vVz;

    //Векторное прои-ние----------------------------------------------------------
    struct vektor vp (struct vektor u, struct vektor v)
    {
        struct vektor a;
        a.x = u.y*v.z-u.z*v.y;
        a.y = u.z*v.x-u.x*v.z;
        a.z = u.x*v.y-u.y*v.x;
        return a;
    }
    //------------------------------------------------------------------------------
    void outModem1(unsigned char Data, unsigned char i)
    {
        BufferInModem[i] = Data | 0x80;
    }

    //------------------------------------------------------------------------------
    void outModem2(unsigned int Data, unsigned char i)
    {
        BufferInModem[i] = (Data & 0x007f)| 0x80;
        BufferInModem[i+1] = ((Data & 0x3f80) >> 7)| 0x80;
    }

    //------------------------------------------------------------------------------
    void outModem4(unsigned long int Data, unsigned char i)
    {
        BufferInModem[i] = (Data & 0x0000007f)| 0x80;
        BufferInModem[i+1] = ((Data & 0x3f80) >> 7) | 0x80;
        BufferInModem[i+2] = ((Data & 0x1fc000) >> 14) | 0x80;
        BufferInModem[i+3] = ((Data & 0xfe00000)>> 21) | 0x80;
    }

    unsigned char last_reset_source, count_reset_source = 0;

    //------------------------------------------------------------------------------
    float U_AKB, I_AKB, C_AKB;
    unsigned char fl_AKB, last_reset_source_AKB, count_reset_source_AKB = 0, command_AKB;

    //----------------------------------------------------------------------------
    unsigned long decodeLatOrLon(unsigned char Array[], unsigned char n)
    {
        unsigned long koord, tmp;
        koord = Array[n] & 0x7f;
        tmp = Array[n+1] & 0x7f;
        koord = koord+(tmp << 7);
        tmp = Array[n+2] & 0x7f;
        koord = koord+(tmp << 14);
        tmp = Array[n+3] & 0x7f;
        koord = koord+(tmp << 21);
        return koord;
    }

    //-------------------------------------------------------------------------------------------------
    float calcTangZad(void)
    {
        float tang_zad = alfa0;
        if(V_dat < V_bum)
        {
            tang_zad = alfa0+(ALFA_MAX-alfa0)/V_bum*V_dat; 
            if(tang_zad > ALFA_MAX) tang_zad = ALFA_MAX;
            if(tang_zad < ALFA_MIN) tang_zad = ALFA_MIN;
            tang_zad += V_dat/V_bum*Vy_zad/V_bum;
        }
        else
        {
            float tmp_Cy_potr = 2.0*GLA/(RO*delta_RO*SLA*V_dat*V_dat);  
            tang_zad = (-6.738+6.67*tmp_Cy_potr)/ToGrad;
            if(tang_zad > ALFA_MAX) tang_zad = ALFA_MAX;
            if(tang_zad < ALFA_MIN) tang_zad = ALFA_MIN;
            tang_zad += Vy_zad/V_dat;
        }
        if (tang_zad > TANG_MAX)    tang_zad = TANG_MAX;
        else if (tang_zad < -TANG_MAX)  tang_zad = -TANG_MAX;
        return tang_zad;
    }

    //-------------------------------------------------------------------------------------------------
    void landing(void)
    {
        rgFly = 5;
        rgAnswer = 5;
        liTimer_tick_start = liTimer_tick;
        H_start_landing = H_filtr;
        if (H_start_landing < 10) H_start_landing = 10;
    }
    
    float rsk_zad_, wy_zad; 
    float angle, l_km,/*rsk_gps, */napr_vetv_mar, napr_toch_mar, dz, dx, dz_pr, dx_pr; //Для автоуправления

    //Сбор данных с датчиков-----------------------------------------------------------------------------
    void gpsCheck(void) //Проверяем время последней посылки GPS
    {
        if (liTimer_tick-liTimer_tick_GPS > 2*FREQ_GIR) //Отсутствие координат
        {
            flKoord = 0;
            liTimer_tick_GPS = liTimer_tick;
            flTele = 1;
        }
    }

    void getAltitudeAndVelocityY(void) //Находим высоту и вертикальную скорость  
    {
        H_filtr = H_filtr+(altitude-H_filtr)/FREQ_GIR*0.8;
        Vy_filtr = Vy_filtr+((H_filtr-H_filtr_pr)*FREQ_GIR-Vy_filtr)/FREQ_GIR*0.8;
        H_filtr_pr = H_filtr;

        Vy_dat = Vy_dat+((altitude -H_dat)*FREQ_GIR-Vy_dat)/FREQ_GIR/0.3;
        H_dat = altitude;
    }

    void getDensityOfAir(void) //Находим плотность воздуха
    {
        delta_RO = 1-0.000095*H_filtr+3.05e-09*H_filtr*H_filtr;
        if (delta_RO < 0.5)
            delta_RO = 0.5;
    }

    void getStallVelocity(void) //Находим скорость срыва потока
    {
        V_bum = sqrt(2.0*GLA/(CY_MAX*RO*delta_RO*SLA));
    }

    void getVelocityAirspeedSensor(void) //Находим воздушную скорость
    {
         // TODO speed
        if(flStoika)
            ;
        else
            V_dat = 0;
    }

    //Инициализация -------------------------------------------------------------------
    void initSystem(void) //первоначальная инициализация
    {
        timer_tick = 0;
        rgStart = 1;
        rgFly = 1;
        rgAnswer = 5;
        S_gir.y = V_gir.y = H_filtr = H_filtr_pr = Vy_filtr = H_dat = Vy_dat = ax_filtr = 0;

        rsk_gir = rsk_m;
        if(fabs(a_dat.y) > FLT_EPSILON)
            kren_dat = kren_din = kren_gir = -atan(a_dat.z/a_dat.y);

        float a_dat_yz = a_dat.y*cos(kren_gir)-a_dat.z*sin(kren_gir);
        if(fabs(a_dat_yz) > FLT_EPSILON)
            teta_and_alfa = tang_din = tang_gir = atan(a_dat.x/a_dat_yz);

        V_bum = 25;
    }

    void testDeltaU(void) //тест рулей
    {
        if (timer_tick < FREQ_GIR)
            delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = 0.5*M_PI;
        else if (timer_tick < 2.0*FREQ_GIR)
            delta_u_p1 = DELTA_U_MAX;
        else if (timer_tick < 3.0*FREQ_GIR)
            delta_u_p1 = DELTA_U_MIN;
        else if (timer_tick < 4.0*FREQ_GIR)
        {
            delta_u_p1 = 0.5*M_PI;
            delta_u_l1 = DELTA_U_MAX;
        }
        else if (timer_tick < 5.0*FREQ_GIR)
            delta_u_l1 = DELTA_U_MIN;
        else if (timer_tick < 6.0*FREQ_GIR)
        {
            delta_u_l1 = 0.5*M_PI;
            delta_u_p2 = DELTA_U_MAX;
        }
        else if (timer_tick < 7.0*FREQ_GIR)
            delta_u_p2 = DELTA_U_MIN;
        else if (timer_tick < 8.0*FREQ_GIR)
        {
            delta_u_p2 = 0.5*M_PI;
            delta_u_l2 = DELTA_U_MAX;
        }
        else if (timer_tick < 9.0*FREQ_GIR)
            delta_u_l2 = DELTA_U_MIN;
        else
        {
            delta_u_l2 = 0.5*M_PI;
            timer_tick = 0;

            if (flKoord)
                rgStart = 4;        //системная готовность
            else
                rgStart = 3;        //ждем кординаты
            rgAnswer = 5;
        }
    }

    void testDeltaG(void) //тест силовых установок 
    {
        #ifdef ERA51
        float tmp_delta_g_p_max = 80;
        #else
        float tmp_delta_g_p_max = 100;
        #endif
        if (rgTest == 1)
        {
            delta_g_p1 = delta_g_l1 = delta_g_p2 = delta_g_l2 = DELTA_G_MIN;
            delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = 0.5*M_PI;
        }
        else if (rgTest == 2)
        {
            delta_g_p1 += D_DELTA_G_MAX;
            if(delta_g_p1 > tmp_delta_g_p_max)    delta_g_p1 = tmp_delta_g_p_max;
        }
        else if (rgTest == 3)
            delta_g_p1 = DELTA_G_MIN;
        else if (rgTest == 4)
        {
            /*  
            if(delta_g_l1 < 0.6*DELTA_G_MAX)    delta_g_l1 += D_DELTA_G_MAX;
            else                            delta_g_l1 = DELTA_G_MAX;
            delta_g_l1 += D_DELTA_G_MAX;
            if(delta_g_l1 > DELTA_G_MAX)    delta_g_l1 = DELTA_G_MAX;
            */
            delta_g_l1 += D_DELTA_G_MAX;
            if(delta_g_l1 > tmp_delta_g_p_max)    delta_g_l1 = tmp_delta_g_p_max;
        }
        else if (rgTest == 5)
            delta_g_l1 = DELTA_G_MIN;
        else if (rgTest == 6)
        {
            delta_g_p2 += D_DELTA_G_MAX;
            if(delta_g_p2 > tmp_delta_g_p_max)    delta_g_p2 = tmp_delta_g_p_max;
        }
        else if (rgTest == 7)
            delta_g_p2 = DELTA_G_MIN;
        else if (rgTest == 8)
        {
            delta_g_l2 += D_DELTA_G_MAX;
            if(delta_g_l2 > tmp_delta_g_p_max)    delta_g_l2 = tmp_delta_g_p_max;
        }
        else
        {
            delta_g_l2 = DELTA_G_MIN;
            rgTest = 0;

            if (flKoord)
                rgStart = 4;        //системная готовность
            else
                rgStart = 3;        //ждем кординаты
            rgAnswer = 5;
        }
    }

    void testDeltaU_slow(void) //Замедленный тест рулей 
    {
        if (rgTest == 1)
            delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = 0.5*M_PI;
        else if (rgTest == 2)
            delta_u_p1 = 0;
        else if (rgTest == 3)
            delta_u_l1 = 0;
        else if (rgTest == 4)
            delta_u_p2 = 0;
        else if (rgTest == 5)
            delta_u_l2 = 0;
        else if (rgTest == 6)
            delta_u_p1 = 0.5*M_PI;
        else if (rgTest == 7)
            delta_u_l1 = 0.5*M_PI;
        else if (rgTest == 8)
            delta_u_p2 = 0.5*M_PI;
        else
        {
            delta_u_l2 = 0.5*M_PI;
            rgTest = 0;

            if (flKoord)
                rgStart = 4;        //системная готовность
            else
                rgStart = 3;        //ждем кординаты
            rgAnswer = 5;
        }
    }

    void autoLanding(void) //автоматический перевод в режим спасения
    {
        tang_zad_filtr = tang_zad_filtr+(tang_zad-tang_zad_filtr)/FREQ_GIR*0.8;
        if((fabs(tang_gir) > 30.0/ToGrad) || (fabs(tang_gir-tang_zad_filtr) > 12.0/ToGrad))
        {
            landing();
            flOutTang = 1;
        }
        kren_zad_filtr = kren_zad_filtr+(kren_zad_effect-kren_zad_filtr)/FREQ_GIR*0.8;
        if(fabs(kren_gir-kren_zad_filtr) > 25.0/ToGrad)
        {
            landing();
            flOutKren = 1;
        }

        if(rgFly != 1)//срабатывает при просадке по высоте
        {
            float tmp_deltaH = (H_filtr >= H_AVAR) ? 100 : 50;
            if ((H_filtr > H_max) && (H_filtr < H_zad))  H_max = H_filtr; //Если летим снизу
            if(H_filtr < (H_max-tmp_deltaH )) //просадка
            {
                landing();
                flOutH = 1;
            }
        }
        if(NoCommand > 15*FREQ_GIR) //по отказу радиоканала
        {
            landing();
            flOtkazRK = 1;
        }
        /* TODO AKB
        if(U_AKB < U_end)
        {
            landing();
            flOutU = 1;
        }
        */
        //добавить защиту при полете самолетом вблизи земли
    }

    void autoOff(void) //выключение в экстренной ситуации
    {
        //выключение аппарата при ударе
        if((rgStart > 5) && ((a_dat.x*a_dat.x+a_dat.y*a_dat.y+a_dat.z*a_dat.z) > 2500) && (H_dat < 10))
        {
            flstopThrottleAndConversion = 1;
            rgAnswer = 5;
        }
        //выключение по истечении времени аварийной посадки
        if((flstopThrottleAndConversion == 0) && (rgFly == 5) && ((liTimer_tick-liTimer_tick_start)/FREQ_GIR > 1.3*H_start_landing))
        {
            if (Vy_filtr > -1)
            {
                flstopThrottleAndConversion = 1;
                rgAnswer = 5;
            }
        }

        //вылючение на стойке по отказу связи
        if(true == flStoika && (NoCommand > 2*FREQ_GIR))
        {
            flstopThrottleAndConversion = true;
            rgAnswer = 5;
        }
     
    }

    void initPrelaunch(void) //инициализация до старта
    {
        H_max = int_dV = int_dkoors = int_dkren = 0;
        int_dtang = int_dtang0;
        delta_u_upor = asin(0.01*(gaz0+5));
    }

    void stopThrottleAndConversion(void)
    {
        g_p1 = g_l1 = g_p2 = g_l2 = delta_g_p1 = delta_g_l1 = delta_g_p2 = delta_g_l2 = 0;
        u_p1 = u_l1 = u_p2 = u_l2 = delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = (90.0/ToGrad);
    }

    //IMU and AHRS -------------------------------------------------------------------

    void getAngularAccel(void) //угловые ускорения
    {
        e_dat.x = (w_dat.x-w_dat_pr.x)/delta_time_gir;
        e_dat.y = (w_dat.y-w_dat_pr.y)/delta_time_gir;
        e_dat.z = (w_dat.z-w_dat_pr.z)/delta_time_gir;

        w_dat_pr.x = w_dat.x;
        w_dat_pr.y = w_dat.y;
        w_dat_pr.z = w_dat.z;
    }

    void getAccelCPoint(void) //показания акселей в ЦТ
    {
        //векторное произведение угловых ускорений на расстояние от ЦТ
        v1 = vp(e_dat, l);

        //векторное произведение угловых скоростей на расстояние от ЦТ
        v2 = vp(w_dat, l);

        //векторное произведение угловых скоростей на векторное произведение (угловых скоростей на расстояние от ЦТ)
        v2 = vp(w_dat, v2);

        a_dat1.x = a_dat.x-v1.x-v2.x;
        a_dat1.y = a_dat.y-v1.y-v2.y;
        a_dat1.z = a_dat.z-v1.z-v2.z;

        if(flStoika || (rgFly == 5))
        {
            v3.x = 0;
            v3.y = 0;
            v3.z = 0;
        }
        else if(flKoord)
        {
            vVz.x = Vz;
            vVz.y = 0;
            vVz.z = 0;

            v3 = vp(w_dat, vVz);
        }
        else
        {
            vVz.x = V_dat;
            vVz.y = 0;
            vVz.z = 0;

            v3 = vp(w_dat, vVz);
        }

        //показания акселей в цт очищенные от вращательных перегрузок
        a_dat_ct.x = a_dat1.x+2.0*v3.x;
        a_dat_ct.y = a_dat1.y+2.0*v3.y;
        a_dat_ct.z = a_dat1.z+2.0*v3.z;

        //ускорение цт относительно инерциальной СК паралельной связанным
        a_gir.x = -a_dat_ct.x-G*sin_tang_gir; 
        a_gir.y = -a_dat_ct.y-G*cos_tang_gir*cos_kren_gir;
        a_gir.z = -a_dat_ct.z+G*cos_tang_gir*sin_kren_gir;
        if(AHRS_GIR)
        {
            ax_filtr = a_gir.x;
        }
        else
        {
            ax_filtr = ax_filtr+(a_gir.x-ax_filtr)/FREQ_GIR*0.8;
        }
    }
    void getSlip(void) //указатель скольжения
    {
        if ((fabs(a_dat1.y) > FLT_EPSILON) || (fabs(a_dat1.z) > FLT_EPSILON))
        {
            float slip_tmp = atan2(-a_dat1.z, -a_dat1.y);
            slip = slip+((kren_zad+kren0+slip_tmp)-slip)*kren_zad_wy*delta_time_gir;
        }   
    }

    void getVelocityCPoint(void) //скорость ЦТ относительно инерциальной системы координат
    {
        V_gir.x = V_gir.x+a_gir.x*delta_time_gir;
        V_gir.y = V_gir.y+a_gir.y*delta_time_gir;
        V_gir.z = V_gir.z+a_gir.z*delta_time_gir;


        if(AHRS_GIR)
        {
            ;
        }
        else
        {
            if(flKoord) V_gir.x = 0.999*V_gir.x+0.001*Vz;
            else        V_gir.x = 0.999*V_gir.x+0.001*V_dat;

            float tmp_cos_kren_tang_gir = cos_tang_gir*cos_kren_gir;
            if(tmp_cos_kren_tang_gir > 10.0*FLT_EPSILON)
            {
                V_gir.y = 0.999*V_gir.y+0.001*Vy_filtr/tmp_cos_kren_tang_gir;
            }
            V_gir.z = 0.999*V_gir.z;
        }
    }

    void getYawGir(void) //Рыск гироскопический (Yaw)
    {
        if(AHRS_GIR)
        {
            pthread_mutex_lock( &mutex_ap );
            rsk_gir = m_yaw;
            pthread_mutex_unlock( &mutex_ap );
        }
        else
        {
            drsk_gir = (w_dat.y*cos_kren_gir-w_dat.z*sin_kren_gir)/cos_tang_gir;
            rsk_gir = rsk_gir+drsk_gir*delta_time_gir;

            while (rsk_gir > 2.0*M_PI)  
                rsk_gir -= 2.0*M_PI;//загоняем в диапазон от 0 до 2*M_PI

            while (rsk_gir < 0)         
                rsk_gir += 2.0*M_PI;
        }	
    }

    void getPitchGir(void) //Тангаж гироскопический (pitch)
    {
        if(AHRS_GIR)
        {
            pthread_mutex_lock( &mutex_ap );
            tang_gir = m_pitch;
            pthread_mutex_unlock( &mutex_ap );
        }
        else
        {
            dtang_gir = w_dat.y*sin_kren_gir+w_dat.z*cos_kren_gir;
            tang_gir = tang_gir+dtang_gir*delta_time_gir;

            while (tang_gir > M_PI) 
                tang_gir -= 2.0*M_PI;  //загоняем в диапазон от -M_PI до M_PI

            while (tang_gir < -M_PI)
                tang_gir += 2.0*M_PI;
        }
    }

    void getRollGir(void) //Крен гироскопический (Roll)
    {
        if(AHRS_GIR)
        {
            pthread_mutex_lock( &mutex_ap );
            kren_gir = m_roll;
            pthread_mutex_unlock( &mutex_ap );
        }
        else
        {
            dkren_gir = w_dat.x-tan(tang_gir)*(w_dat.y*cos_kren_gir-w_dat.z*sin_kren_gir);
            kren_gir = kren_gir+dkren_gir*delta_time_gir;

            while (kren_gir > M_PI)  
                kren_gir -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI

            while (kren_gir < -M_PI) 
                kren_gir += 2.0*M_PI;
        }
    }
    //-------------------------------------------------------------------

    void correctPitchRoll(void) 
    {
      	if (V_dat < 0)
		{
			teta_and_alfa = alfa = alfa0;
			kV = 0;
        }
        else if(V_dat < V_bum)
        {
            if(cos(kren_gir) > FLT_EPSILON)
			{
                alfa = alfa0+(ALFA_MAX-alfa0)/V_bum*V_dat/cos(kren_gir);
                if(alfa > ALFA_MAX) alfa = ALFA_MAX;
                if(alfa < ALFA_MIN) alfa = ALFA_MIN;

                teta_and_alfa = alfa+V_dat/V_bum*asin(((fabs(V_dat)>FLT_EPSILON) ? (((Vy_dat/V_dat)>1) ? 1 : ((Vy_dat/V_dat)<-1)? -1 : (Vy_dat/V_dat)) : 1));

                kV = ((CY_ALFA*alfa+CY0)*RO*delta_RO*V_dat*V_dat*0.5*SLA)/GLA;
                if(kV > 1) kV = 1;
                if (kV < 0) kV = 0;
            }
        }
        else
        {
            if(cos(kren_gir) > FLT_EPSILON)
			{
                float tmp_Cy_potr = 2.0*GLA/(RO*delta_RO*SLA*V_dat*V_dat)/cos(kren_gir);    //tmp = Cy_potr
                alfa = (-6.738+6.67*tmp_Cy_potr)/ToGrad;
                if(alfa > ALFA_MAX) alfa = ALFA_MAX;
                if(alfa < ALFA_MIN) alfa = ALFA_MIN;

                teta_and_alfa = alfa+asin((fabs(V_dat)>FLT_EPSILON) ? (((Vy_dat/V_dat)>1) ? 1 : ((Vy_dat/V_dat)<-1)? -1 : (Vy_dat/V_dat)) : 1);

                kV = 1;
            }
        }

        //float tmp_V = flKoord ? Vz : V_dat; TODO add V
        float tmp_V = 0;

        if(tmp_V > 2)
            kren_dat = kren_dat+(dkren_gir-drsk_gir-1.0*G/tmp_V*tan(kren_dat))*delta_time_gir;
        else if ((fabs(a_dat_ct.y) > FLT_EPSILON) || (fabs(a_dat_ct.z) > FLT_EPSILON))
            kren_dat = kren_dat+(atan2(a_dat_ct.z, -a_dat_ct.y)-kren_dat)*delta_time_gir*0.8;

        while (kren_dat > M_PI)  
            kren_dat -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI

        while (kren_dat < -M_PI) 
            kren_dat += 2.0*M_PI;


        if ((fabs(kren_gir) < 70.0/ToGrad) && (fabs(tang_gir) < 70.0/ToGrad))
        {
            //поперечная компенсация
            if ((fabs(a_dat_ct.y) > FLT_EPSILON) || (fabs(a_dat_ct.z) > FLT_EPSILON))
            {
                kren_din = atan2(a_dat_ct.z, -a_dat_ct.y);
            }


            if(flStoika || (rgFly == 5))
                kren_gir = 0.998*kren_gir+0.002*kren_din;
            else if (fabs(tmp_V) < 2)//пока
                kren_gir = 0.998*kren_gir+0.002*((1-tmp_V/2)*kren_din+tmp_V/2*kren_dat);
            else
                kren_gir = 0.998*kren_gir+0.002*kren_dat;


            //продольная компенсация
            angle = a_dat_ct.y*cos(kren_gir)-a_dat_ct.z*sin(kren_gir);
            float tmp_ay = (-a_dat_ct.x*sin(delta_u)+angle*cos(delta_u))/-G; //tmp = ay
            if(tmp_ay > 1) tmp_ay = 1;
            if(tmp_ay < -1)    tmp_ay = -1;
            tang_din = acos(tmp_ay)-delta_u;   //tmp = tang

            if(flStoika)            tang_gir = 0.998*tang_gir+0.002*tang_din;
            else if(V_dat < V_bum)  tang_gir = 0.998*tang_gir+0.002*((1-kV)*tang_din+kV*teta_and_alfa);
            else                    tang_gir = 0.998*tang_gir+0.002*teta_and_alfa;
        }
    }
    //Управление полетом -------------------------------------------------------------------

    void getDeltaUpor(void) // Угол наклона винтов
    {
        float tmp_delta_u_upor = DELTA_U_MIN;
        if(V_dat < V_bum)
        {
            if ((flStoika == 0) && (H_filtr < 10))  tmp_delta_u_upor = DELTA_U_UPOR_MAX; //запрет вблизи земли летать самолетом
            else   tmp_delta_u_upor = asin((1-kV)*0.01*(gaz0+5));//запрет опускать мотогондолы ниже несущих возможностей планера
            //5 - это запас на случай крена и т.п.
        }
        else
            tmp_delta_u_upor = DELTA_U_MIN;

        //ограничение по критической скорости винта
        if((flStoika == 0) && (V_dat > 10))
        {
            float tmp_cos_alfa = 0.14*0.01*delta_g*NS_MAX*0.8/V_dat; //tmp1 = cos(alfa); 0.8 - коэф-т скольжения
            if (tmp_cos_alfa < 0)   tmp_cos_alfa = 0;
            else if(tmp_cos_alfa > 1) tmp_cos_alfa = 1;
            tmp_cos_alfa = acos(tmp_cos_alfa);  //tmp1 = alfa;

            if(tmp_delta_u_upor < tmp_cos_alfa) tmp_delta_u_upor = tmp_cos_alfa;
        }
        if (tmp_delta_u_upor > DELTA_U_UPOR_MAX) tmp_delta_u_upor = DELTA_U_UPOR_MAX;
        if (tmp_delta_u_upor < DELTA_U_MIN) tmp_delta_u_upor = DELTA_U_MIN;

        delta_u_upor = delta_u_upor+(tmp_delta_u_upor-delta_u_upor)*delta_time_gir*0.8;//0.4;
        if (delta_u_upor > DELTA_U_UPOR_MAX) delta_u_upor = DELTA_U_UPOR_MAX;
        else if (delta_u_upor < DELTA_U_MIN) delta_u_upor = DELTA_U_MIN;
    }

    //Управление полетом -------------------------------------------------------------------

    void flyAutoControll(void) // if((rgFly == 0) || (rgFly == 2) || (rgFly == 4)) //неручное управление
    {
        //стабилизация скорости
        if (rgStart > 5)
        {
            ax_zad = V_zad-V_dat;   //ax_zad - сектор
            if (ax_zad > 10)  ax_zad = 10;
            if (ax_zad < -10) ax_zad = -10;

            int_dV = int_dV+i_ax*ax_zad*delta_time_gir;
            if (int_dV > DELTA_AX_MAX) int_dV = DELTA_AX_MAX;
            if (int_dV < DELTA_AX_MIN) int_dV = DELTA_AX_MIN;

            delta_ax = p_ax*ax_zad-d_ax*ax_filtr+int_dV;
            if (delta_ax > DELTA_AX_MAX) delta_ax = DELTA_AX_MAX;
            if (delta_ax < DELTA_AX_MIN) delta_ax = DELTA_AX_MIN;
            ax_zad = ax_zad*k1_ax;
        }
        else
        {
            delta_ax = 0;
        }

        Vy_zad = H_zad-H_dat;
        if (Vy_zad > dH_max) Vy_zad = dH_max;
        else if (Vy_zad < -dH_max) Vy_zad = -dH_max;

        if (rgStart == 5)
        {
            int_dVy = int_dVy+D_DELTA_G_MAX;
            if (int_dVy > gaz0)
            {
                rgStart = 6;
                rgAnswer = 5;
            }
        }
        else if (H_filtr < 10)
        {
            float Vy_zad_landing = -0.25-H_filtr*0.1;
            if (Vy_zad_landing < -0.25)    Vy_zad_landing = -0.25;
            if (Vy_zad < Vy_zad_landing)   Vy_zad = Vy_zad_landing;

            int_dVy = int_dVy+i_ay*Vy_zad*delta_time_gir;
            if (int_dVy < (gaz0-d_ay*dH_max))  int_dVy = gaz0-d_ay*dH_max;
        }
        else
        {
            int_dVy = int_dVy+i_ay*Vy_zad*delta_time_gir;
            if (int_dVy < (gaz0-d_ay*dH_max))  int_dVy = gaz0-d_ay*dH_max;//не p_ay, a d_ay
        }
        if (int_dVy > DELTA_AY_MAX)  int_dVy = DELTA_AY_MAX;

        delta_ay = p_ay*Vy_zad-d_ay*Vy_dat+int_dVy;//проверить d_ay было d_ay = p_ay сейчас стало 0,02...
        if (delta_ay > DELTA_AY_MAX)  delta_ay = DELTA_AY_MAX;
        if (delta_ay < DELTA_AY_MIN) delta_ay = DELTA_AY_MIN;
        Vy_zad = k1_ay*Vy_zad;

        //tang_zad----------------------------------------------------
        tang_zad = calcTangZad();

        //смеситель----------------------------------------------------
        if(V_dat < V_bum)
        {
            float tmp_delta_ay_cos_kren_zad_effect = (1-kV)*delta_ay/cos(kren_zad_effect);
            if(fabs(delta_ax) > FLT_EPSILON || fabs(tmp_delta_ay_cos_kren_zad_effect) > FLT_EPSILON)
                delta_u = atan2(tmp_delta_ay_cos_kren_zad_effect, delta_ax)+DELTA_U_MIN-tang_zad;//+delta_u0 = 0, т.к. при повороте вперед еффективность задних двигателей отстает

            delta_g = sqrt(delta_ax*delta_ax+tmp_delta_ay_cos_kren_zad_effect*tmp_delta_ay_cos_kren_zad_effect);
        }
        else
        {
            delta_u = DELTA_U_MIN;//-tang_zad;  //???
            delta_g = delta_ax;
        }

        if (delta_g > DELTA_G_MAX) delta_g = DELTA_G_MAX;
        if (delta_g < DELTA_G_MIN) delta_g = DELTA_G_MIN;
        g_p1 = g_l1 = g_p2 = g_l2 = delta_g;

        if (delta_u > DELTA_U_MAX)  delta_u = DELTA_U_MAX;
        if (delta_u < delta_u_upor) delta_u = delta_u_upor;
        if (delta_u < DELTA_U_MIN)  delta_u = DELTA_U_MIN;
        u_p1 = u_l1 = u_p2 = u_l2 = delta_u;
    }

    void flyHandControll(void) //if(rgFly == 1)     //ручное управление
    {
        if(rgStart == 5)
        {
            if(flStoika && (delta_g_zad > 5))
            {
                rgStart = 6;
                rgAnswer = 5;
            }
            else if ((flStoika == 0) && (delta_g_zad > gaz0))
            {
                rgStart = 6;
                rgAnswer = 5;
            }
        }
        if(QUADRODRONE_SIMPLEPID_LOGIC)
        {
            delta_g = delta_g_zad;

        }
        else
        {
            delta_g = delta_g_zad/cos(kren_zad_effect);
            if((flStoika == 0)&&(rgStart > 5) && (H_filtr < 10) && (delta_g < (gaz0-3)))
                delta_g = (gaz0-3); //защита от дурака
            if (delta_g_zad < 1)    delta_g = DELTA_G_MIN;    
        }
        

        if(delta_g > DELTA_G_MAX) delta_g = DELTA_G_MAX;
        if(delta_g < DELTA_G_MIN) delta_g = DELTA_G_MIN;
        g_p1 = g_l1 = g_p2 = g_l2 = delta_g;

        Vy_zad = (delta_g-gaz0)/(DELTA_G_MAX-gaz0)*dH_max*k1_ay;    //Vy_max = dH_max*k1_ay
        if(Vy_zad > k1_ay*dH_max) Vy_zad = k1_ay*dH_max;
        if(Vy_zad < -k1_ay*dH_max) Vy_zad = -k1_ay*dH_max;

        tang_zad = calcTangZad();
        delta_u = delta_u_zad-tang_zad;
        
        if((H_dat < 10) && flOutLand)
        {
            if (delta_u > (0.5*M_PI+5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI+5.0/ToGrad-tang_zad;
            if (delta_u < (0.5*M_PI-5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI-5.0/ToGrad-tang_zad;
        }
        else
        {
            if (delta_u > DELTA_U_MAX)  delta_u = DELTA_U_MAX;
            if (delta_u < delta_u_upor) delta_u = delta_u_upor;
            if (delta_u < DELTA_U_MIN)  delta_u = DELTA_U_MIN;
        }
        u_p1 = u_l1 = u_p2 = u_l2 = delta_u;
    }

    void flyRescue(void) //if(rgFly == 5)     //спасение
    {
        if(delta_g_zad > 1) delta_g = gaz0;
        if (delta_g_zad < 1)delta_g = DELTA_G_MIN;
        g_p1 = g_l1 = g_p2 = g_l2 = delta_g;

        tang_zad = alfa0;//calcTangZad();
        if (flOtkazRK)
            delta_u = 0.5*M_PI-tang_zad;
        else
        {
            delta_u = delta_u_zad-tang_zad;
            if (delta_u > (0.5*M_PI+5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI+5.0/ToGrad-tang_zad;
            if (delta_u < (0.5*M_PI-5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI-5.0/ToGrad-tang_zad;
        }
        u_p1 = u_l1 = u_p2 = u_l2 = delta_u;
    }

    void flyStabilize(void) // находим значения газа и поворота винтов
    {
        

        if(QUADRODRONE_SIMPLEPID_LOGIC)
        {
            float throttle = delta_g;


            if(throttle < gaz0)
            {
                pitchPID.resetIntError();
                rollPID.resetIntError();
                yawPID.resetIntError();

                pitchAnglePID.resetIntError();
                rollAnglePID.resetIntError();
                yawForcePID.resetIntError();
            }

            pitchPID.setPID(p_tang, i_tang, d_tang); //tang 18.0, 1.0, 9.0
            pitchPID.setMaxMin(20.0 * FREQ_OSN);
            pitchPID.setFCut(FREQ_PWM);

            rollPID.setPID(p_kren, i_kren, d_kren); //kren 18.0, 1.0, 9.0
            rollPID.setMaxMin(15.0 * FREQ_OSN);
            rollPID.setFCut(FREQ_PWM);

            yawPID.setPID(p_rsk, i_rsk, d_rsk);  //rysk 7.0, 0.0, 5.0
            yawPID.setMaxMin(12.0/ToGrad * FREQ_OSN);
            yawPID.setFCut(FREQ_PWM/40.0);

            pitchAnglePID.setPID(1.8, 0.8, 0.08); //tang 7.0, 0.0, 5.0
            pitchAnglePID.setMaxMin(12.0/ToGrad * FREQ_OSN);
            pitchAnglePID.setFCut(FREQ_PWM/40.0);

            rollAnglePID.setPID(0.4, 0.7, 0.17); //kren 7.0, 0.0, 5.0
            rollAnglePID.setMaxMin(20.0/ToGrad * FREQ_OSN);
            rollAnglePID.setFCut(FREQ_PWM/40.0);

            yawForcePID.setPID(4.0, 0.1, 4.0);  //rysk 9.0, 0.0, 7.0
            yawForcePID.setMaxMin(4.0 * FREQ_OSN);
            yawForcePID.setFCut(FREQ_PWM);

            pthread_mutex_lock( &mutex_ap );
            
            float dPitch = tang_gir - tang_zad;    
            //dPitch = fabs(dPitch) > M_PI ? (dPitch < 0 ? 1.0 : -1.0)*(fabs(dPitch)-M_PI): dPitch;

            float dRoll = kren_gir - kren_zad;
            //dRoll = fabs(dRoll) > M_PI ? (dRoll < 0 ? 1.0 : -1.0)*(fabs(dRoll)-M_PI) : dRoll;

            float rsk_zad =(360-koors_zad)/ToGrad - M_PI;
            float dYaw = rsk_gir - rsk_zad;
            dYaw = fabs(dYaw) > M_PI ? (dYaw < 0 ? 1.0 : -1.0)*(fabs(dYaw)-M_PI) : dYaw;

            pthread_mutex_unlock( &mutex_ap );
            
            if((fabs(dPitch)>(M_PI)) || (fabs(dRoll)>(M_PI)) || fabs(dYaw) > M_PI)
            {
                printf(" tang_zad %f, kren_zad %f, rsk_zad %f\n", tang_zad, kren_zad, rsk_zad);
                printf(" tang_gir %f, kren_gir %f, rsk_gir %f\n", tang_gir, kren_gir, rsk_gir);
                printf(" dPitch %f, dRoll %f, dYaw %f\n", dPitch, dRoll, dYaw);
                dPitch = dRoll = dYaw = 0;
                printf("PID QUADRODRONE_SIMPLEPID_LOGIC Error \n");
            }

            
            float throttleLimit = throttle > 5 ? 5 : throttle;

            float powerLimit =  throttleLimit * sin(delta_u);
            float angleLimit =  25.0/ToGrad * cos(delta_u);

            if(delta_u < 45/ToGrad)
            {
                pitchPID.resetIntError();
                rollPID.resetIntError();
                yawPID.resetIntError();
            }
            else
            {
                pitchAnglePID.resetIntError();
                rollAnglePID.resetIntError();
                yawForcePID.resetIntError();
            }

            //Pitch
            float pitchForce = pitchPID.calc(0, dPitch);
           
            pitchForce = pitchForce > powerLimit*4 ? powerLimit*4 : pitchForce;
            pitchForce = pitchForce < -powerLimit*4 ? -powerLimit*4 : pitchForce;

            g_p1 -= pitchForce;
            g_l1 -= pitchForce;
            g_p2 += pitchForce;
            g_l2 += pitchForce;

            float pitchAngle = pitchAnglePID.calc(0, dPitch);

            pitchAngle = pitchAngle > angleLimit ? angleLimit : pitchAngle;
            pitchAngle = pitchAngle < -angleLimit ? -angleLimit : pitchAngle;

            u_p1 -= pitchAngle;
            u_l1 -= pitchAngle;
            u_p2 += pitchAngle;
            u_l2 += pitchAngle;

            //Roll
            float rollForce = rollPID.calc(0, dRoll);
            
            rollForce = rollForce > powerLimit*2 ? powerLimit*2 : rollForce;
            rollForce = rollForce < -powerLimit*2 ? -powerLimit*2 : rollForce;

            g_p1 += rollForce; 
            g_l1 -= rollForce;
            g_p2 += rollForce;
            g_l2 -= rollForce;

            float rollAngle = rollAnglePID.calc(0, dRoll);

            rollAngle = rollAngle > angleLimit ? angleLimit : rollAngle;
            rollAngle = rollAngle < -angleLimit ? -angleLimit : rollAngle;

            u_p1 += rollAngle;
            u_l1 -= rollAngle;
            u_p2 += rollAngle;
            u_l2 -= rollAngle;

            //Yaw
            float yawForce = yawForcePID.calc(0, dYaw);
           
            float yawAngle =  yawPID.calc(0, dYaw)/ToGrad;

            float yawAngleLumit = (1 - cos(delta_u)) * 15.0/ToGrad;
            yawAngle = yawAngle > yawAngleLumit ? yawAngleLumit : yawAngle;
            yawAngle = yawAngle < -yawAngleLumit ? -yawAngleLumit : yawAngle;

            u_p1 += yawAngle;
            u_l1 -= yawAngle;
            u_p2 += yawAngle;
            u_l2 -= yawAngle;

            float yawForceLimit = 2*throttleLimit * (1 - sin(delta_u));
            yawForce = yawForce >  yawForceLimit ? yawForceLimit : yawForce;
            yawForce = yawForce < -yawForceLimit ? -yawForceLimit : yawForce;

            g_p1 -= yawForce;
            g_l1 += yawForce;
            g_p2 -= yawForce;
            g_l2 += yawForce;
    

            if(DEBUG_ZAD)
            {
                if(flNewGPS)
                {
                    printf(" throttle %f, pitchForce %f, rollForce %f, yawForce %f\n", throttle, pitchForce, rollForce, yawForce);
                    printf(" p_tang %f, i_tang %f, d_tang %f\n", p_tang, i_tang, d_tang);
                    printf(" p_kren %f, i_kren %f, d_kren %f\n", p_kren, i_kren, d_kren);
                    printf(" p_rsk %f, i_rsk %f, d_rsk %f\n", p_rsk, i_rsk, d_rsk);
                    printf(" tang_zad %f, kren_zad %f, rsk_zad %f\n", tang_zad, kren_zad, rsk_zad);
                    printf(" tang_gir %f, kren_gir %f, rsk_gir %f\n", tang_gir, kren_gir, rsk_gir);
                    printf(" dPitch %f, dRoll %f, dYaw %f\n", dPitch, dRoll, dYaw);
                }
            }

            pthread_mutex_lock( &mutex_ap );

            float k_pwm_osn = float(FREQ_PWM)/float(FREQ_OSN);
            delta_g_p1 = delta_g_p1 + (g_p1 - delta_g_p1)*k_pwm_osn;
            delta_g_l1 = delta_g_l1 + (g_l1 - delta_g_l1)*k_pwm_osn;
            delta_g_p2 = delta_g_p2 + (g_p2 - delta_g_p2)*k_pwm_osn;
            delta_g_l2 = delta_g_l2 + (g_l2 - delta_g_l2)*k_pwm_osn;

            delta_u_p1 = delta_u_p1 + (u_p1 - delta_u_p1)*k_pwm_osn;
            delta_u_l1 = delta_u_l1 + (u_l1 - delta_u_l1)*k_pwm_osn;
            delta_u_p2 = delta_u_p2 + (u_p2 - delta_u_p2)*k_pwm_osn;
            delta_u_l2 = delta_u_l2 + (u_l2 - delta_u_l2)*k_pwm_osn;

            pthread_mutex_unlock( &mutex_ap );
        }
        else
        {
            //продольный канал-----------------------------------------------------------------
            wz_zad = tang_zad-tang_gir;     //сектор
            if (wz_zad > w_max)             wz_zad = w_max;
            else if (wz_zad < -w_max)       wz_zad = -w_max;

            int_dtang = int_dtang+i_tang*wz_zad*delta_time_gir;
            if (int_dtang > delta_max)  int_dtang = delta_max;
            if (int_dtang < -delta_max) int_dtang = -delta_max;

            delta_tang = -(p_tang*wz_zad-d_tang*w_dat.z+d2_tang*e_dat.z+int_dtang);
            if (delta_tang > delta_max) delta_tang = delta_max;
            if (delta_tang < -delta_max) delta_tang = -delta_max;

            float tmp_dg = delta_tang*v_tang*sin(delta_u);   //tmp = dg
            g_p1 -= tmp_dg;
            g_l1 -= tmp_dg;
            g_p2 += tmp_dg;
            g_l2 += tmp_dg;

            float tmp_du = delta_tang*g_tang*cos(delta_u)/ToGrad;    //tmp = du
            u_p1 -= tmp_du;
            u_l1 -= tmp_du;
            u_p2 += tmp_du;
            u_l2 += tmp_du;

            wz_zad = k1_tang*wz_zad;
            e_zad.z = -k2_tang*(wz_zad-w_dat.z);

            //Управляем влево-вправо--------------------------------------
            rsk_zad_ = (360-koors_zad)/ToGrad;
            while (rsk_zad_ >= 2.0*M_PI)    //загоняем в диапазон от 0 до 2*M_PI
                rsk_zad_ = rsk_zad_-2.0*M_PI;
            while (rsk_zad_ <= 0)
                rsk_zad_ = rsk_zad_+2.0*M_PI;
            wy_zad = rsk_zad_-rsk_gir;

            while (rsk_zad_ > M_PI)    //загоняем в диапазон от -M_PI до M_PI
                rsk_zad_ = rsk_zad_-2.0*M_PI;
            while (rsk_zad_ < -M_PI)
                rsk_zad_ = rsk_zad_+2.0*M_PI;

            float tmp_rsk_gir = rsk_gir;  //tmp = rsk_gir_
            while (tmp_rsk_gir > M_PI)    //загоняем в диапазон от -M_PI до M_PI
                tmp_rsk_gir = tmp_rsk_gir-2.0*M_PI;
            while (tmp_rsk_gir < -M_PI)
                tmp_rsk_gir = tmp_rsk_gir+2.0*M_PI;

            float tmp_wy_zad = rsk_zad_-tmp_rsk_gir; //tmp = wy_zad_
            if(fabs(wy_zad) > fabs(tmp_wy_zad))    wy_zad = tmp_wy_zad;
            if (wy_zad > w_max)             wy_zad = w_max; //сектор
            else if (wy_zad < -w_max)       wy_zad = -w_max;

            float V_tmp = ((( flKoord ? Vz : V_dat ) < 1 ) ? 1 : ( flKoord ? Vz : V_dat )); //ограничение по скорости

            float tmp_drsk_max = G*tan(KREN_MAX)/k1_rsk/cos(tang_zad)/cos(KREN_MAX)/V_tmp;//tmp = drsk_max
            if (wy_zad > tmp_drsk_max)               wy_zad = tmp_drsk_max;   //сектор
            else if (wy_zad < -tmp_drsk_max)         wy_zad = -tmp_drsk_max;

            int_dkoors = int_dkoors+i_rsk*wy_zad*delta_time_gir;
            if(int_dkoors > 0.2*delta_max)       int_dkoors = 0.2*delta_max;
            else if(int_dkoors < -0.2*delta_max) int_dkoors = -0.2*delta_max;

            delta_rsk = -(p_rsk*wy_zad-d_rsk*w_dat.y+d2_rsk*e_dat.y+int_dkoors);
            if (delta_rsk > delta_max) delta_rsk = delta_max;
            if (delta_rsk < -delta_max) delta_rsk = -delta_max;
            wy_zad = k1_rsk*wy_zad;                             //это скорость

            //демпфируем боковой канал-------------------------------------
            if(NoCommand > 2.0*FREQ_GIR)    //по отказу радиоканала
            {
                kren_zad = 0;
                flOtkazRK05 = 1;
            }

            if (rgFly == 5)
            {
                kren_zad_effect = kren_zad+kren0;
            }
            else
            {
                kren_zad_effect = /*kren_zad+kren0+*/slip;
                /*                  if(flKoord) tmp = Vz;
                                    else        tmp = V_dat;
                                    if(tmp < 0.1) tmp = 0.1;
                                    tmp = -wy_zad*cos(tang_zad)*tmp/G;  //tmp = a

                                    kren_zad_effect = kren_zad+kren0+kren_zad_wy*asin((-1.0+sqrt(1.0+4.0*tmp*tmp))/(2.0*tmp));*/
            }
            e_zad.y = -k2_rsk*(wy_zad-w_dat.y);

            if (kren_zad_effect > KREN_MAX)  kren_zad_effect = KREN_MAX;
            if (kren_zad_effect < -KREN_MAX) kren_zad_effect = -KREN_MAX;

            wx_zad = kren_zad_effect-kren_gir;       //сектор
            if (wx_zad > w_max)          wx_zad = w_max;
            else if (wx_zad < -w_max)      wx_zad = -w_max;

            int_dkren = int_dkren+i_kren*wx_zad*delta_time_gir;
            if (int_dkren > delta_max)  int_dkren = delta_max;
            if (int_dkren < -delta_max) int_dkren = -delta_max;

            delta_kren = -(p_kren*wx_zad-d_kren*w_dat.x+d2_kren*e_dat.x+int_dkren);
            if (delta_kren > delta_max) delta_kren = delta_max;
            if (delta_kren < -delta_max) delta_kren = -delta_max;

            wx_zad = k1_kren*wx_zad;
            e_zad.x = -k2_kren*(wx_zad-w_dat.x);

            //смеситель-----------------------------------------------------
            printf("v_rsk %f, delta_rsk %f, delta_u %f \n", v_rsk, delta_rsk, delta_u);

            tmp_du = (v_rsk*delta_rsk*sin(delta_u)+g_kren*delta_kren*cos(delta_u))/ToGrad; //tmp = du
            u_p1 += kl*tmp_du;
            u_l1 -= kl*tmp_du;
            u_p2 += tmp_du;
            u_l2 -= tmp_du;

            tmp_du = u_p1;
            if(tmp_du < u_l1) tmp_du = u_l1;
            if(tmp_du < u_p2) tmp_du = u_p2;
            if(tmp_du < u_l2) tmp_du = u_l2;
            if (tmp_du > DELTA_U_MAX)
            {
                tmp_du = tmp_du-DELTA_U_MAX;
                u_p1 = u_p1-tmp_du;
                u_l1 = u_l1-tmp_du;
                u_p2 = u_p2-tmp_du;
                u_l2 = u_l2-tmp_du;
            }

            tmp_du = u_p1;
            if(tmp_du > u_l1) tmp_du = u_l1;
            if(tmp_du > u_p2) tmp_du = u_p2;
            if(tmp_du > u_l2) tmp_du = u_l2;

            if (tmp_du < delta_u_upor)
            {
                tmp_du = delta_u_upor-tmp_du;
                u_p1 = u_p1+tmp_du;
                u_l1 = u_l1+tmp_du;
                u_p2 = u_p2+tmp_du;
                u_l2 = u_l2+tmp_du;
            }

            delta_u_p1 = u_p1;
            delta_u_l1 = u_l1;
            delta_u_p2 = u_p2;
            delta_u_l2 = u_l2;

            g_p1 += (-g_rsk*delta_rsk*cos(delta_u_p1)+v_kren*delta_kren*sin(delta_u_p1))*kl;
            g_l1 -= (-g_rsk*delta_rsk*cos(delta_u_l1)+v_kren*delta_kren*sin(delta_u_l1))*kl;
            g_p2 +=  -g_rsk*delta_rsk*cos(delta_u_p2)+v_kren*delta_kren*sin(delta_u_p2);
            g_l2 -=  -g_rsk*delta_rsk*cos(delta_u_l2)+v_kren*delta_kren*sin(delta_u_l2);

            //-------------------------------------------------------------

            tmp_du = g_p1;
            if(tmp_du < g_l1) tmp_du = g_l1;
            if(tmp_du < g_p2) tmp_du = g_p2;
            if(tmp_du < g_l2) tmp_du = g_l2;

            #ifdef ERA51
            float tmp1_V_dat;
            if (V_dat > 0)  tmp1_V_dat = 80+V_dat;
            else tmp1_V_dat = 80;
            if(tmp1_V_dat > DELTA_G_MAX) tmp1_V_dat = DELTA_G_MAX;
            if (tmp_du > tmp1_V_dat)
            {
                tmp_du = tmp_du-tmp1_V_dat;
                g_p1 = g_p1-tmp_du;
                g_l1 = g_l1-tmp_du;
                g_p2 = g_p2-tmp_du;
                g_l2 = g_l2-tmp_du;
            }
            #else
            if (tmp_du > DELTA_G_MAX)
            {
                tmp_du = tmp_du-DELTA_G_MAX;
                g_p1 = g_p1-tmp_du;
                g_l1 = g_l1-tmp_du;
                g_p2 = g_p2-tmp_du;
                g_l2 = g_l2-tmp_du;
            }
            #endif
            tmp_du = g_p1;
            if(tmp_du > g_l1) tmp_du = g_l1;
            if(tmp_du > g_p2) tmp_du = g_p2;
            if(tmp_du > g_l2) tmp_du = g_l2;
            if (tmp_du < DELTA_G_MIN)
            {
                tmp_du = DELTA_G_MIN-tmp_du;
                g_p1 = g_p1+tmp_du;
                g_l1 = g_l1+tmp_du;
                g_p2 = g_p2+tmp_du;
                g_l2 = g_l2+tmp_du;
            }
            delta_g_p1 = g_p1;
            delta_g_l1 = g_l1;
            delta_g_p2 = g_p2;
            delta_g_l2 = g_l2;
        }
    }

    //-------------------------------------------------------------------

    AP()
    {
        // Old init
        delta_u_zad = u_p1 = u_l1 = u_p2 = u_l2 = delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = delta_u = 0.5*M_PI;
        delta_u_upor = DELTA_U_UPOR_MAX;

        a_dat.y = -G;

        l.x = XCT0;
        l.y = YCT0;
        l.z = ZCT0;

        rgTest=0;
        rgStart=0;
        rgFly=0;
        rgAnswer=0;

        rsk_gir = rsk_m = 0;
        //------------
        ggg = 1;
        //------------

    };
    ~AP(){};
    AP& operator=(const AP& item);

    protected:

    static AP* GetPointer()
    {
        AP* pTmp = NULL;
        try
        {
            static AP var;
            pTmp = &var;
        }
        catch(...)
        {
            //assert(false);
            pTmp = NULL;
        }
        return pTmp;
    }

    public:

    static AP* Get(void)
    {
        while(pthread_mutex_lock(&mutex_ap)!=0)
            ;

        AP* pTmp = GetPointer();

        while(pthread_mutex_unlock(&mutex_ap)!=0)
            ;

        return pTmp;
    }

    //время(сек) = timer_tick*FREQ
    unsigned int timer_tick;   //относительное (счётчик)
    long int liTimer_tick, liTimer_tick_start, liTimer_tick_rsk_m;     //абсолютное
    long int liTimer_tick_GPS; //прихода последней GPS посылки
    unsigned long time_t_posix;       //POSIX - время

    unsigned int NoCommand;

    unsigned long int LatFly, LonFly, LatFly_tmp, LonFly_tmp, LatMar[128], LonMar[128];

    char rgAnswer;

    signed char rgFly;  /*  0 - полуавтомат(крен, угол рыска, высота, скорость)
                                  1 - управление джойстиком (крен, угол рыска, газ, угол наклона мотогондол)
                                  2 - автономный полет (с автоматическим взлет-посадкой)
                                  3 - прошивка маршрута
                                  4 - возврат
                                5 - спасение */

    signed char rgStart;          /* -1 - тест силовых установок
                              0 - стоим
                              1 - тест
                              2 - замедленный тест
                              3 - ждем координаты
                              4 - системная готовность
                                                5 - старт!
                                              6 - летим       */

    signed char rgTest;         /*  0 - нет теста
                                1 - 255 этапы теста     */

    float altitude;

    unsigned char ggg, ggg_save;

    bool  flNewGPS;

    void run()
    {
        //ggg = 18;

        flRun = 0;

        gpsCheck();                     //Проверяем время последней посылки GPS

        getAltitudeAndVelocityY();      //Находим высоту и вертикальную скорость  

        getDensityOfAir();              //Находим плотность воздуха

        getStallVelocity();             //Находим скорость срыва потока

        getVelocityAirspeedSensor();    //Находим воздушную скорость         


        if((rgStart == 0) && (timer_tick > 10.0*FREQ_GIR))      
        {
            initSystem();           //первоначальная инициализация
        }
        if (rgStart == 1)                                       
        {
            testDeltaU();         //тест рулей
        }
        else if (rgStart == -1)                                 
        {
            testDeltaG();         //тест силовых установок 
        }
        else if (rgStart == 2)                                  
        {
            testDeltaU_slow();    //Замедленный тест рулей 
        }
        else if (rgStart > 2)
        {
            //------------
            ggg = 20;
            //------------

            //гироскоп----------------------------------------------------------

            cos_kren_gir = cos(kren_gir); 
            sin_kren_gir = sin(kren_gir);
            sin_tang_gir = sin(tang_gir);
            cos_tang_gir = cos(tang_gir);

            S_gir.y = 0.999*(S_gir.y+(V_gir.x*sin_tang_gir+V_gir.y*cos_tang_gir*cos_kren_gir-V_gir.z*cos_tang_gir*sin_kren_gir)*delta_time_gir)+0.001*H_filtr;

            getAngularAccel();      //угловые ускорения

            getAccelCPoint();       //показания акселей в цт

            getSlip();              //указатель скольжения

            getVelocityCPoint();    //скорость ЦТ относительно инерциальной системы координат

            getYawGir(); //Рыск гироскопический (Yaw) My

            getPitchGir(); //Тангаж гироскопический (pitch) Mz

            getRollGir(); //Крен гироскопический (Roll) Mx

            if(AHRS_GIR)
            {
                ;
            }
            else
            {
                correctPitchRoll(); // поправки крена и тангажа 
            }

            if(DEBUG_ZAD)
            {
                if(flNewGPS)
                {
                    
                    printf(" tang_zad %f, kren_zad %f, koors_zad %f\n", tang_zad, kren_zad, ((360-koors_zad)/ToGrad - M_PI));
                    printf(" NoCommand %lu \n", NoCommand );

                }
            }

        }
        if(rgStart == 5)    //инициализация до старта
        {
            initPrelaunch();
        }

        //возвращение в режим старта
        if((rgStart > 5) && (H_filtr < 10) && (delta_g_zad < 5.0))  //на период отладки
        {
            rgStart = 5;
            rgAnswer = 5;
            flOutLand = 0;
        }

        if ((H_filtr > 20) && (rgStart > 5))
        {
            flOutLand = 1;
        }

        //отсечение нижнего порога у шима
        if(flstopThrottleAndConversion)              tdelta_g_min = 1.0e-3;
        else if(rgStart > 5)    tdelta_g_min = tdelta_g_min_infly;
        else                    tdelta_g_min = 1.0e-3;


        if((flStoika == 0) && (rgStart == 6) && (rgFly != 5)) //автоматический перевод в режим спасения
        {
            autoLanding();
        }

        autoOff(); //выключение в экстренной ситуации
        
        if(flstopThrottleAndConversion)
        {
            stopThrottleAndConversion();
        }
        else if(rgStart > 4)
        {
            //------------
            ggg = 21;
            //------------

            getDeltaUpor(); // Угол наклона винтов

            if((rgFly == 0) || (rgFly == 2) || (rgFly == 4)) 
            {
                flyAutoControll();          //неручное управление
            }
            else if(rgFly == 1)     
            {
                flyHandControll();          //ручное управление
            }
            else if(rgFly == 5)     
            {
                flyRescue();        //спасение
            }

            flyStabilize(); // находим значения газа и поворота винтов 
        }
    }// Run

    void pushRxData(unsigned char rx_buffer[], int rx_length)
    {
        unsigned char BuferFromModem [NBFM]; // Для анализа с последовательного порта
        unsigned char wBFM, rBFM, marBFM;

        for(int i=0; i< (rx_length); i++)
        {
            BuferFromModem [wBFM++] =  rx_buffer[i]; 
            if(wBFM >= NBFM)
            {
                wBFM = 0;
                marBFM = 1;
            }
        }

        unsigned char RK_code[66], nByte = 0, KontrSumma = 0, NPackage;  //Для работы с последовательным портом "Модем"

        pthread_mutex_lock( &mutex_ap );
            
        for(int i = 0; i < rx_length; i++)
        {
            if(rBFM < (wBFM+marBFM*NBFM))//связь с землей
            {
                if ((BuferFromModem[rBFM] & 0xC0) == 0x40)
                {
                    nByte = 0;
                    KontrSumma = 0;
                    NPackage = BuferFromModem[rBFM] & 0x3f;
                    NoCommand = 0;
                    flOtkazRK = 0;
                    flOtkazRK05 = 0;
                }

                if (nByte > 65)
                    nByte = 65;
                RK_code[nByte] = BuferFromModem[rBFM] & 0x7f;
                KontrSumma = KontrSumma^RK_code[nByte++];

                if ( (nByte == 3) && (KontrSumma == 0) )
                {
                    //------------
                    ggg = 3;
                    //------------
                    if (NPackage == 2)
                    {
                        if(RK_code[1] == 1)  //Телеметрия
                        {
                            //                      flCommand = 1;
                        }
                        else if((RK_code[1] == 2) && (rgFly != 5))  // ручное управление
                        {
                            rgAnswer = 5;
                            rgFly = 1;
                            if(rgStart > 5)
                            {
                                delta_g_zad = gaz0;           //переводим в спасение
                                delta_u_zad = 0.5*M_PI;
                                //                          kren_zad = 0;
                            }
                        }
                        if(RK_code[1] == 3)  //stopThrottleAndConversion
                        {
                            if(flStoika)
                            {
                                flCommandstopThrottleAndConversion = 1;
                                flstopThrottleAndConversion = 1;
                            }
                            else
                            {
                                flCommandstopThrottleAndConversion = 1;
                                landing();
                            }
                            rgAnswer = 5;
                        }
                        //                  else if ((RK_code[1] == 4) && (rgStart == 4))  //Старт
                        else if ((RK_code[1] == 4) && (rgStart > 2) && (rgStart < 6))  //Старт (условие rgStart > 3) на время отладки 
                        {
                            rgAnswer = 3;
                            rgStart = 5;
                            n_ = 1;
                        }
                        else if((RK_code[1] == 5) && (flStoika == 0) &&  (rgFly != 5))  //автомат
                        {
                            if(rgFly == 1)
                            {
                                if (H_dat < 100)    H_zad = 100;
                                else                H_zad = H_dat+Vy_filtr;
                                if(H_filtr < H_zad) H_max = H_filtr;//летим снизу
                                else                H_max = H_zad;

                                //V_zad = 15;
                                V_zad = V_dat;
                                int_dV = delta_g*cos(delta_u);

                                if(fabs(1-kV) > FLT_EPSILON)    int_dVy = delta_g*sin(delta_u)/(1-kV);
                                else                            int_dVy = delta_g*sin(delta_u);
                                if (int_dVy < (gaz0-d_ay*dH_max))   int_dVy = gaz0-d_ay*dH_max;
                                if (int_dVy > DELTA_AY_MAX)         int_dVy = DELTA_AY_MAX;
                            }
                            rgAnswer = 5;
                            rgFly = 0;
                        }
                        else if ((RK_code[1] == 8) && (rgStart < 5))  //Замедленный тест рулей + Инициализация
                        {
                            rgStart = 2;
                            rgTest++;
                            rgAnswer = 5;
                        }
                        else if ((RK_code[1] == 10) && (flStoika == 0) &&  (rgFly != 5))  //Возврат
                        {
                            rgAnswer = 5;
                            rgFly = 4;
                            n_ = i_mar-1;

                            if (H_dat < 100)    H_zad = 100;
                            else                H_zad = H_dat+Vy_filtr;
                            if(H_filtr < H_zad) H_max = H_filtr;//летим снизу
                            else                H_max = H_zad;

                            Vz_zad = 25;
                            if(rgFly == 1)
                            {
                                int_dV = delta_g*cos(delta_u);
                                if(fabs(1-kV) > FLT_EPSILON)        int_dVy = delta_g*sin(delta_u)/(1-kV);
                                else                                int_dVy = delta_g*sin(delta_u);
                                if (int_dVy < (gaz0-d_ay*dH_max))   int_dVy = gaz0-d_ay*dH_max;
                                if (int_dVy > DELTA_AY_MAX)         int_dVy = DELTA_AY_MAX;
                            }
                        }
                        else if ((RK_code[1] == 11) && (rgStart < 5))  //Тест силовых установок
                        {
                            rgStart = -1;
                            rgTest++;
                            rgAnswer = 5;
                        }
                        else if ((RK_code[1] == 12) && (rgStart < 5))
                        {
                            flStoika = 1;
                            rgAnswer = 5;
                        }
                        else if (RK_code[1] == 13)
                        {
                            flStoika = 0;
                            rgAnswer = 5;
                        }
                        else if (RK_code[1] == 14)
                        {
                            command_AKB = 2;
                        }
                        else if (RK_code[1] == 15)
                        {
                            command_AKB = 3;
                        }
                    }
                    else if((NPackage == 4) && (flStoika == 0) &&  (rgFly != 5))             //автономный полет
                    {
                        //------------
                        ggg = 5;
                        //------------

                        rgFly = 2;
                        rgAnswer = 5;
                        n_ = RK_code[1];
                        H_zad = H_Mar[n_];
                        V_zad = Vz_Mar[n_];
                        if(H_filtr < H_zad) H_max = H_filtr;    //летим снизу
                        else                H_max = H_zad;

                        if(rgFly == 1)
                        {
                            int_dV = delta_g*cos(delta_u);
                            if(fabs(1-kV) > FLT_EPSILON)    int_dVy = delta_g*sin(delta_u)/(1-kV);
                            else                    int_dVy = delta_g*sin(delta_u);
                            if (int_dVy < (gaz0-d_ay*dH_max))   int_dVy = gaz0-d_ay*dH_max;
                            if (int_dVy > DELTA_AY_MAX)         int_dVy = DELTA_AY_MAX;
                        }
                    }
                    else if(NPackage == 8)  //Крен камеры зад.
                    {
                        rgAnswer = 5;
                        KrenKam_zad = RK_code[1];
                        KrenKam_zad = KrenKam_zad-60;
                    }
                    else if(NPackage == 9)  //Угол камеры к горизонту зад.UgolKam_zad
                    {
                        rgAnswer = 5;
                        UgolKam_zad = RK_code[1];
                    }
                }   //if ( nByte == 3 )
                else if(NPackage == 1 && (nByte == 4) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 6;
                    //------------

                    if (rgFly == 0)
                    {
                        i = RK_code[2];
                        H_zad = -100+0.1*(RK_code[1]+(i << 7));
                        if(H_zad < H_ZAD_MIN)       H_zad = H_ZAD_MIN;
                        else if(H_zad > H_ZAD_MAX)  H_zad = H_ZAD_MAX;
                        if(H_filtr < H_zad)         H_max = H_filtr;//летим снизу
                        else                        H_max = H_zad;
                    }
                    rgAnswer = 5;
                }
                else if((NPackage == 3) && (nByte == 4) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 7;
                    //------------

                    rgAnswer = 5;
                    if  ((rgFly == 0) || (rgFly == 1) || (rgFly == 5))
                    {
                        i = RK_code[2];
                        koors_zad = RK_code[1]+(i << 7);
                    }
                }
                else if((NPackage == 5) && (nByte == 4) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 8;
                    //------------

                    if ((rgFly == 0) || (rgFly == 1) || (rgFly == 5))
                    {
                        i = RK_code[2];
                        i = RK_code[1]+(i << 7);
                        kren_zad = ((float)i-8000)/2500;
                        if(kren_zad > KREN_MAX) kren_zad = KREN_MAX;
                        if(kren_zad < -KREN_MAX)kren_zad = -KREN_MAX;
                    }
                }
                else if((NPackage == 6) && (nByte == 4) && (KontrSumma == 0))   //V_zad
                {
                    //------------
                    ggg = 9;
                    //------------

                    if (rgFly == 0)
                    {
                        i = RK_code[2];
                        i = RK_code[1]+(i << 7);
                        V_zad = 0.1*i-10;
                        if (V_zad > V_MAX) V_zad = V_MAX;
                        if (V_zad < V_MIN) V_zad = V_MIN;
                    }
                    else if(flStoika)
                    {
                        i = RK_code[2];
                        i = RK_code[1]+(i << 7);
                        V_dat = 0.1*i-10;
                        if (V_dat > V_MAX) V_dat = V_MAX;
                        if (V_dat < V_MIN) V_dat = V_MIN;
                    }
                }
                else if((NPackage == 7) && (nByte == 4) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 10;
                    //------------

                    if ((rgFly == 1) || (rgFly == 5))
                    {
                        i = RK_code[2];
                        i = RK_code[1]+(i << 7);
                        delta_g_zad = 0.1*i; 
                        if (delta_g_zad < DELTA_G_MIN) delta_g_zad = DELTA_G_MIN;
                        if (delta_g_zad > DELTA_G_MAX) delta_g_zad = DELTA_G_MAX;
                    }
                }
                else if((NPackage == 10) && (nByte == 7) && (time_t_posix == 0) && (KontrSumma == 0)) //POSIX время
                {
                    //------------
                    ggg = 11;
                    //------------

                    time_t_posix = RK_code[5];
                    time_t_posix = (time_t_posix << 7)+RK_code[4];
                    time_t_posix = (time_t_posix << 7)+RK_code[3];
                    time_t_posix = (time_t_posix << 7)+RK_code[2];
                    time_t_posix = (time_t_posix << 7)+RK_code[1];
                }
                else if((NPackage == 12) && (nByte == 4) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 12;
                    //------------

                    if(flStoika)
                    {
                        i = RK_code[2];
                        i = RK_code[1]+(i << 7);
                        tang_zad = ((float)i-8168)/1300;
                        if (tang_zad < -TANG_MAX) tang_zad = -TANG_MAX;
                        if (tang_zad > TANG_MAX) tang_zad = TANG_MAX;
                    }
                }
                else if((NPackage == 13) && (nByte == 4) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 13;
                    //------------

                    if ((rgFly == 1) || (rgFly == 5))
                    {
                        i = RK_code[2];
                        i = (i << 7)+RK_code[1];
                        delta_u_zad = 0.1*i/ToGrad;
                        if (delta_u_zad < DELTA_U_MIN) delta_u_zad = DELTA_U_MIN;
                        if (delta_u_zad > DELTA_U_MAX) delta_u_zad = DELTA_U_MAX;
                    }
                }
                //Координаты ППМ------------------------------------------------------
                else if ((NPackage == 11) && (nByte == 13) && (rgStart < 5) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 14;
                    //------------

                    n_ = RK_code[11];
                    if(n_ > i_mar+1) i_mar = n_+1;
                    LatMar[n_] = decodeLatOrLon(RK_code, 1);
                    LonMar[n_] = decodeLatOrLon(RK_code, 5);
                    if (n_ == 0)
                    {
                        cos_Lat0 = LatMar[0];
                        cos_Lat0 = cos((cos_Lat0/60/10000-90)/ToGrad);
                    }

                    H_Mar[n_] = (int)RK_code[9]*50-1000;
                    if(H_Mar[n_] < H_ZAD_MIN)       H_Mar[n_]= H_ZAD_MIN;
                    else if(H_Mar[n_] > H_ZAD_MAX)  H_Mar[n_] = H_ZAD_MAX;
                    Vz_Mar[n_] = (int)RK_code[10];

                    rgAnswer = 4;
                }
                else if((NPackage == 14) && (nByte == 50) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 15;
                    //------------

                    dt0_p1 = 1e-5*fromEarth2(RK_code, 1);
                    dt90_p1 = 1e-5*fromEarth2(RK_code, 3);
                    dt0_l1 = 1e-5*fromEarth2(RK_code, 5);
                    dt90_l1 = 1e-5*fromEarth2(RK_code, 7);
                    dt0_p2 = 1e-5*fromEarth2(RK_code, 9);
                    dt90_p2 = 1e-5*fromEarth2(RK_code, 11);
                    dt0_l2 = 1e-5*fromEarth2(RK_code, 13);
                    dt90_l2 = 1e-5*fromEarth2(RK_code, 15);

                    alfa0 = ((float)fromEarth2(RK_code, 17)-8168)/1300;
                    i_tang = 1e-2*fromEarth2(RK_code, 19);
                    p_tang = 1e-2*fromEarth2(RK_code, 23);
                    d_tang = 1e-2*fromEarth2(RK_code, 25);
                    kl = 1e-2*fromEarth2(RK_code, 27);

                    kren0 = ((float)fromEarth2(RK_code, 29)-8168)/1300;
                    i_kren = 1e-2*fromEarth2(RK_code, 31);
                    p_kren = 1e-2*fromEarth2(RK_code, 33);
                    d_kren = 1e-2*fromEarth2(RK_code, 35);
                    gaz0 = 1e-2*fromEarth2(RK_code, 37);

                    i_rsk = 1e-2*fromEarth2(RK_code, 39);
                    p_rsk = 1e-2*fromEarth2(RK_code, 41);
                    d_rsk = 1e-2*fromEarth2(RK_code, 43);
                    w_max = 1e-2*fromEarth2(RK_code, 45);
                    delta_max = 1e-2*fromEarth2(RK_code, 47);
                    rgAnswer = 1;
                }
                else if((NPackage == 15) && (nByte == 50) && (KontrSumma == 0))
                {
                    //------------
                    ggg = 16;
                    //------------

                    i_ay = 1e-2*fromEarth2(RK_code, 1);
                    p_ay = 1e-2*fromEarth2(RK_code, 3);

                    tdelta_g_min_infly = 1e-5*fromEarth2(RK_code, 7);
                    i_ax = 1e-2*fromEarth2(RK_code, 9);
                    p_ax = 1e-2*fromEarth2(RK_code, 11);
                    tdelta_g_max = 1e-5*fromEarth2(RK_code, 13);

                    v_rsk = 1e-2*fromEarth2(RK_code, 17);
                    g_kren = 1e-2*fromEarth2(RK_code, 19);
                    g_rsk = 1e-2*fromEarth2(RK_code, 21);
                    v_kren = 1e-2*fromEarth2(RK_code, 23);
                    g_tang = 1e-2*fromEarth2(RK_code, 25);
                    v_tang = 1e-2*fromEarth2(RK_code, 27);
                    int_dtang0 = -80.0+1e-2*fromEarth2(RK_code, 29);
                    dH_max = 1e-2*fromEarth2(RK_code, 31);
                    d2_tang = 1e-2*fromEarth2(RK_code, 33)-80;
                    d2_kren = 1e-2*fromEarth2(RK_code, 35)-80;
                    d2_rsk = 1e-2*fromEarth2(RK_code, 37)-80;

                    if (d_tang > 0.01)  k1_tang = p_tang/d_tang;
                    else                k1_tang = p_tang/0.01;

                    if (d_kren > 0.01)  k1_kren = p_kren/d_kren;
                    else                k1_kren = p_kren/0.01;

                    if (d_rsk > 0.01)   k1_rsk = p_rsk/d_rsk;
                    else                k1_rsk = p_rsk/0.01;

                    if (d2_tang > 0.01) k2_tang = d_tang/d2_tang;
                    else                k2_tang = d_tang/0.01;

                    if (d2_kren > 0.01) k2_kren = d_kren/d2_kren;
                    else                k2_kren = d_kren/0.01;

                    if (d2_rsk > 0.01)  k2_rsk = d_rsk/d2_rsk;
                    else                k2_rsk = d_rsk/0.01;

                    kren_zad_wy = 1e-2*fromEarth2(RK_code, 39);
                    d_ay = 1e-2*fromEarth2(RK_code, 41);
                    d_ax = 1e-2*fromEarth2(RK_code, 43);
                    U_end = 1e-2*fromEarth2(RK_code, 45);

                    if (d_ay > 0.01)    k1_ay = p_ay/d_ay;
                    else                k1_ay = p_ay/0.01;

                    if (d_ax > 0.01)    k1_ax = p_ax/d_ax;
                    else                k1_ax = p_ax/0.01;
                    rgAnswer = 2;
                }
                rBFM++;
                if(rBFM >= NBFM)
                {
                    rBFM = 0;
                    marBFM = 0;
                }
            }
        }
        pthread_mutex_unlock( &mutex_ap );
    }

    unsigned char *pullTxData( void)
    {
        if(rgAnswer == 1)
        {
            rgAnswer = 0;

            BufferInModem[0] = 26 | 0x40;

            outModem2(1e+5*dt0_p1+0.5, 1);
            outModem2(1e+5*dt90_p1+0.5, 3);
            outModem2(1e+5*dt0_l1+0.5, 5);
            outModem2(1e+5*dt90_l1+0.5, 7);
            outModem2(1e+5*dt0_p2+0.5, 9);
            outModem2(1e+5*dt90_p2+0.5, 11);
            outModem2(1e+5*dt0_l2+0.5, 13);
            outModem2(1e+5*dt90_l2+0.5, 15);

            outModem2(alfa0*1300+8168.5, 17);
            outModem2(1e+2*i_tang+0.5, 19);
            outModem2(0, 21);
            outModem2(1e+2*p_tang+0.5, 23);
            outModem2(1e+2*d_tang+0.5, 25);
            outModem2(1e+2*kl+0.5, 27);

            outModem2(kren0*1300+8168.5, 29);
            outModem2(1e+2*i_kren+0.5, 31);
            outModem2(1e+2*p_kren+0.5, 33);
            outModem2(1e+2*d_kren+0.5, 35);
            outModem2(1e+2*gaz0+0.5, 37);

            outModem2(1e+2*i_rsk+0.5, 39);
            outModem2(1e+2*p_rsk+0.5, 41);
            outModem2(1e+2*d_rsk+0.5, 43);
            outModem2(1e+2*w_max+0.5, 45);

            outModem2(1e+2*delta_max+0.5, 47);

            BufferInModem[49] = 0;
            for (int i = 0; i < 49; i++ )
                BufferInModem[49] = BufferInModem[49] ^ BufferInModem[i];
            outModem1(BufferInModem[49], 49);
            BufferInModem[50] = 0;
            return &BufferInModem[0];
        }
        else if(rgAnswer == 2)
        {
            rgAnswer = 0;

            BufferInModem[0] = 27 | 0x40;

            outModem2(1e+2*i_ay+0.5, 1);
            outModem2(1e+2*p_ay+0.5, 3);
            outModem2(0, 5);
            outModem2(1e+5*tdelta_g_min_infly+0.5, 7);
            outModem2(1e+2*i_ax+0.5, 9);
            outModem2(1e+2*p_ax+0.5, 11);
            outModem2(1e+5*tdelta_g_max+0.5, 13);
            outModem2(0, 15);
            outModem2(1e+2*v_rsk+0.5, 17);
            outModem2(1e+2*g_kren+0.5, 19);
            outModem2(1e+2*g_rsk+0.5, 21);
            outModem2(1e+2*v_kren+0.5, 23);
            outModem2(1e+2*g_tang+0.5, 25);
            outModem2(1e+2*v_tang+0.5, 27);
            outModem2(1e+2*int_dtang0+8000.5, 29);
            outModem2(1e+2*dH_max+0.5, 31);

            outModem2(1e+2*d2_tang+8000.5, 33);
            outModem2(1e+2*d2_kren+8000.5, 35);
            outModem2(1e+2*d2_rsk+8000.5, 37);
            outModem2(1e+2*kren_zad_wy+0.5, 39);
            outModem2(1e+2*d_ay+0.5, 41);
            outModem2(1e+2*d_ax+0.5, 43);
            outModem2(1e+2*U_end+0.5, 45);
            outModem2(0, 47);

            BufferInModem[49] = 0;
            for (int i = 0; i < 49; i++ )
                BufferInModem[49] = BufferInModem[49] ^ BufferInModem[i];
            outModem1(BufferInModem[49], 49);
            BufferInModem[50] = 0;
            return &BufferInModem[0];
        }
        else if(rgAnswer == 3)
        {
            rgAnswer = 5;

            BufferInModem[0] = 0x40 | 23;
            BufferInModem[1] = 0x80;
            BufferInModem[2] = (BufferInModem[0]^BufferInModem[1]) | 0x80;
            BufferInModem[3] = 0;

            BufferInModem[3] = 0;
            return &BufferInModem[0];
        }
        else if(rgAnswer == 4)
        {
            rgAnswer = 0;
            BufferInModem[0] = 22 | 0x40;
            outModem4(LatMar[n_], 1);
            outModem4(LonMar[n_], 5);
            BufferInModem[9] = (H_Mar[n_]+1000)/50 | 0x80;
            BufferInModem[10] = Vz_Mar[n_] | 0x80;
            BufferInModem[11] = n_ | 0x80;

            BufferInModem[12] = 0;
            for (int i = 0; i < 12; i++ )
                BufferInModem[12] = BufferInModem[12] ^ BufferInModem[i];
            BufferInModem[12] = 0x80|BufferInModem[12];

            BufferInModem[13] = 0;
            return &BufferInModem[0];        
        }
        else if (rgAnswer == 5)
        {
            rgAnswer = 0;
            BufferInModem[0] = 21 | 0x40;
            outModem2(10*H_zad+1000.5, 1);
            outModem2(koors_zad, 3);
            outModem1(n_, 5);
            outModem1(KrenKam_zad+60, 6);
            outModem1(UgolKam_zad, 7);
            BufferInModem[8] = 0x80;
            if(rgStart < 0)     BufferInModem[9] = 0x80 | (0x07 << 2);
            else                BufferInModem[9] = 0x80 | ((rgStart & 0x07) << 2);

            if(flStoika)        BufferInModem[10] = 0x88 | (rgFly & 0x07);
            else                BufferInModem[10] = 0x80 | (rgFly & 0x07);

            BufferInModem[11] = 0;
            for (int i = 0; i < 11; i++ )
                BufferInModem[11] = BufferInModem[11] ^ BufferInModem[i];
            BufferInModem[11] =  0x80 | BufferInModem[11];
            BufferInModem[12] = 0;
            return &BufferInModem[0];
        }
        else if (flTele )
        {
            flTele = 0;

            BufferInModem[0] = 20 | 0x40;
            if(flKoord)
            {
                outModem4(LatFly, 1);
                outModem4(LonFly, 5);
                outModem2(koors*ToGrad, 9);
                outModem1(Vz, 11);     //Vзем
            }
            else
            {
                BufferInModem[1] = 0x80;
                BufferInModem[2] = 0x80;
                BufferInModem[3] = 0x80;
                BufferInModem[4] = 0xff;
                BufferInModem[5] = 0x80;
                BufferInModem[6] = 0x80;
                BufferInModem[7] = 0x80;
                BufferInModem[8] = 0x80;
                BufferInModem[9] = 0x80;
                BufferInModem[10] = 0x80;
                BufferInModem[11] = 0x80;
            }
            BufferInModem[12] = 0;
            for (int i = 0; i < 12; i++ )
                BufferInModem[12] = BufferInModem[12] ^ BufferInModem[i];
            BufferInModem[12] =  0x80 | BufferInModem[12];

            BufferInModem[13] = 0;
            return &BufferInModem[0];
        }
        else if (flAnswer == 0)
        {
            flAnswer = 1;
            BufferInModem[0] = 24 | 0x40;
            outModem4(liTimer_tick, 1); //отправить в MK "Modem"
            outModem2(kren_din*1300+8168.5, 5);
            outModem2(tang_din*1300+8168.5, 7);
            outModem2(10*delta_g_zad+0.5, 9);

            outModem2(teta_and_alfa*1300+8168.5, 11);
            outModem1(last_reset_source, 13);
            outModem2((V_gir.x+10)*10+0.5, 14);
            outModem2((V_gir.y+10)*10, 16);
            outModem2((V_gir.z+10)*10, 18);

            outModem2((a_gir.x+800)*10, 20);
            outModem2((a_gir.y+800)*10, 22);
            outModem2((a_gir.z+800)*10, 24);
            outModem2(kren_gir*1300+8168.5, 26);
            outModem2(tang_gir*1300+8168.5, 28);
            outModem2(rsk_gir*1300+8168.5, 30);
            outModem2(w_dat.x*2500+8000.5, 32);
            outModem2(w_dat.y*2500+8000.5, 34);
            outModem2(w_dat.z*2500+8000.5, 36);

            outModem2(kren_zad*2500+8000.5, 38);
            outModem2(10*(V_zad+10)+0.5, 40);
            outModem1(count_reset_source, 42);
            outModem2(delta_g*10+0.5, 43);
            outModem2((H_filtr+100)*10, 45);
            outModem2((Vy_filtr+800)*10, 47);
            outModem2((V_dat+10)*10, 49);
            outModem2((Vy_zad+800)*10, 51);
            outModem2(tang_zad*1300+8168.5, 53);
            outModem2(kren_zad_effect*1300+8168.5, 55);
            outModem2(10.0*delta_u*ToGrad+0.5, 57);

            BufferInModem[59] =  0x80 | fl_AKB;
            BufferInModem[60] =  0x80 | last_reset_source_AKB;
            BufferInModem[61] =  0x80 | count_reset_source_AKB;
            outModem2((ax_zad+800)*10, 62);
            outModem2((ax_filtr+800)*10, 64);

            BufferInModem[66] =  0x80|erWx;
            BufferInModem[67] =  0x80|erWy;
            BufferInModem[68] =  0x80|erWz;
            erWx = erWy = erWz = 0;
            BufferInModem[69] = 0;
            for (int i = 0; i < 69; i++ )
                BufferInModem[69] = BufferInModem[69] ^ BufferInModem[i];
            BufferInModem[69] =  0x80 | BufferInModem[69];
            BufferInModem[70] = 0;
            return &BufferInModem[0];
        }
        else
        {
            flAnswer = 0;
            BufferInModem[0] = 25 | 0x40;
            outModem2(10.0*delta_u_upor*ToGrad+0.5, 1);
            outModem2(10*delta_u_zad*ToGrad+0.5, 3);

            outModem2((int_dV+30)*10, 5);

            int i = 10*(delta_ax+30);
            BufferInModem[7] = (i & 0x007f)| 0x80;
            BufferInModem[14] = ((i & 0x3f80) >> 7)| 0x80;

            outModem2((H_dat+100)*10, 8);
            outModem2((Vy_dat+800)*10, 10);
            outModem2((int_dVy+30)*10, 12);
            //          BufferInModem[14] = delta_ax;

            outModem2(100*U_AKB, 15);
            outModem2(wy_zad*2500+8000, 17);
            outModem2((int_dkoors+20)*10, 19);
            outModem1(delta_rsk+60, 21);

            outModem2(10*delta_ay+0.5, 22);
            outModem2((int_dkren+20)*10, 24);
            outModem1(delta_kren+60, 26);

            outModem2(10*I_AKB, 27);
            outModem2((int_dtang+20)*10, 29);
            outModem1(delta_tang+60, 31);

            outModem1(delta_g_p1, 32);
            outModem1(delta_g_l1, 33);
            outModem1(delta_g_p2, 34);
            outModem1(delta_g_l2, 35);
            //          outModem1(I2C_RUN, 35);

            outModem1(delta_u_p1*ToGrad+10, 36);
            outModem1(delta_u_l1*ToGrad+10, 37);
            outModem1(delta_u_p2*ToGrad+10, 38);
            outModem1(delta_u_l2*ToGrad+10, 39);
            outModem2(rsk_m*1300+8168.5, 40);
            outModem2(10*C_AKB, 42);

            outModem2((S_gir.y+100)*10, 44);
            outModem1(0.5*koors_zad1, 46);
            //BufferInModem[46] = 0x80;

            BufferInModem[47] = 0x80;
            if(flstopThrottleAndConversion)
                BufferInModem[47] = BufferInModem[47] | 0x01;
            if (flFaultH)
            {
                BufferInModem[47] = BufferInModem[47] | 0x02;
                flFaultH = 0;
            }
            if (flFaultV)
            {
                BufferInModem[47] = BufferInModem[47] | 0x04;
                flFaultV = 0;
            }
            if(flCommandstopThrottleAndConversion)
                BufferInModem[47] = BufferInModem[47] | 0x08;
            if(flOutTang)
                BufferInModem[47] = BufferInModem[47] | 0x10;
            if(flOutKren)
                BufferInModem[47] = BufferInModem[47] | 0x20;
            if(flOutH)
                BufferInModem[47] = BufferInModem[47] | 0x40;

            BufferInModem[48] = 0x80;
            if(flOutU)
                BufferInModem[48] = BufferInModem[48] | 0x01;
            if(flAutostopThrottleAndConversion)
                BufferInModem[48] = BufferInModem[48] | 0x02;
            if(flOutLand)
                BufferInModem[48] = BufferInModem[48] | 0x04;
            if(flOtkazRK)
                BufferInModem[48] = BufferInModem[48] | 0x08;
            if (flFaultWx)
            {
                BufferInModem[48] = BufferInModem[48] | 0x10;
                flFaultWx = 0;
            }
            if (flFaultWy)
            {
                BufferInModem[48] = BufferInModem[48] | 0x20;
                flFaultWy = 0;
            }
            if (flFaultWz)
            {
                BufferInModem[48] = BufferInModem[48] | 0x40;
                flFaultWz = 0;
            }
            outModem2(wx_zad*2500+8000.5, 49);
            outModem2(wz_zad*2500+8000.5, 51);
            outModem2((a_dat.x+800)*10, 53);
            outModem2((a_dat.y+800)*10, 55);
            outModem2((a_dat.z+800)*10, 57);
            outModem2(kren_dat*1300+8168.5, 59);
            outModem2((/*H_son+*/100)*10, 61);
            ggg_save = ggg;
            BufferInModem[63] = 0x80+ggg_save;
            outModem2(e_dat.x*2500+8000.5, 64);
            outModem2(e_zad.x*2500+8000.5, 66);

            BufferInModem[68] = 0x80;
            if (flOtkazRK05)
                BufferInModem[68] = BufferInModem[68] | 0x01;

            BufferInModem[69] = 0;
            for (int i = 0; i < 69; i++ )
                BufferInModem[69] = BufferInModem[69] ^ BufferInModem[i];
            BufferInModem[69] =  0x80 | BufferInModem[69];

            BufferInModem[70] = 0;
            return &BufferInModem[0];
        }         
    }

    void pullThrottleAndConversionAngle(double &T1, double &T2, double &T3, double &T4, double &A1, double &A2, double &A3, double &A4)
    {
        pthread_mutex_lock( &mutex_ap );

        if(delta_g_p1 > DELTA_G_MAX) delta_g_p1 = DELTA_G_MAX; 
        if(delta_g_p1 < DELTA_G_MIN) delta_g_p1 = DELTA_G_MIN;
        T1 = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(DELTA_G_MAX-DELTA_G_MIN)*delta_g_p1;

        if(delta_g_l1 > DELTA_G_MAX) delta_g_l1 = DELTA_G_MAX;
        if(delta_g_l1 < DELTA_G_MIN) delta_g_l1 = DELTA_G_MIN;
        T2 = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(DELTA_G_MAX-DELTA_G_MIN)*delta_g_l1;

        if(delta_g_p2 > DELTA_G_MAX) delta_g_p2 = DELTA_G_MAX;
        if(delta_g_p2 < DELTA_G_MIN) delta_g_p2 = DELTA_G_MIN;
        T3 = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(DELTA_G_MAX-DELTA_G_MIN)*delta_g_p2;

        if(delta_g_l2 > DELTA_G_MAX) delta_g_l2 = DELTA_G_MAX;
        if(delta_g_l2 < DELTA_G_MIN) delta_g_l2 = DELTA_G_MIN;
        T4 = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(DELTA_G_MAX-DELTA_G_MIN)*delta_g_l2;

        if(delta_u_p1 > DELTA_U_MAX) delta_u_p1 = DELTA_U_MAX;
        if(delta_u_p1 < DELTA_U_MIN) delta_u_p1 = DELTA_U_MIN;
        A1 = dt0_p1+(dt90_p1-dt0_p1)/(0.5*M_PI)*delta_u_p1;

        if(delta_u_l1 > DELTA_U_MAX) delta_u_l1 = DELTA_U_MAX;
        if(delta_u_l1 < DELTA_U_MIN) delta_u_l1 = DELTA_U_MIN;
        A2 = dt0_l1+(dt90_l1-dt0_l1)/(0.5*M_PI)*delta_u_l1;

        if(delta_u_p2 > DELTA_U_MAX) delta_u_p2 = DELTA_U_MAX;
        if(delta_u_p2 < DELTA_U_MIN) delta_u_p2 = DELTA_U_MIN;
        A3 = dt0_p2+(dt90_p2-dt0_p2)/(0.5*M_PI)*delta_u_p2;

        if(delta_u_l2 > DELTA_U_MAX) delta_u_l2 = DELTA_U_MAX;
        if(delta_u_l2 < DELTA_U_MIN) delta_u_l2 = DELTA_U_MIN;
        A4 = dt0_l2+(dt90_l2-dt0_l2)/(0.5*M_PI)*delta_u_l2;

        pthread_mutex_unlock( &mutex_ap );

        T1 *= 1000.;
        T2 *= 1000.;
        T3 *= 1000.;
        T4 *= 1000.;

        A1 *= 1000.;
        A2 *= 1000.;
        A3 *= 1000.;
        A4 *= 1000.;
    }

    void pushPitchRollYawData(float pitch, float roll, float yaw)
    {
        pthread_mutex_lock( &mutex_ap );

        m_pitch = pitch / ToGrad; //tang
        m_roll = roll / ToGrad; //kren 
        m_yaw = yaw / ToGrad; //rysk

        pthread_mutex_unlock( &mutex_ap );
    }

    void pushAccelGyroData(float ax, float ay, float az, float gx, float gy, float gz, float rsc_mag, double _dt)
    {
        //kren = Roll MX
        //rysk = yaw My
        //tang = pitch Mz
        delta_time_gir = _dt;


        if(DEBUG_ACCEL)
        {
            if(flNewGPS)
                printf(" ax %f, ay %f, az %f\n", a_dat.x, a_dat.y, a_dat.z);
            
        }
        

        if(AHRS_GIR)
        {
        	a_dat.x = -ay;
        	a_dat.y = -az;
        	a_dat.z = -ax;

            w_dat.x = gy;
            w_dat.y = -gz;
            w_dat.z = gx;

            rsk_m = rsc_mag;
        }
        else
        {
        	a_dat.x = -ay*G;
        	a_dat.y = -az*G;
        	a_dat.z = -ax*G;

            if(rgStart)     w_dat.x = gy-wx0;
            else            wx0 = wx0+(gy-wx0)/FREQ_GIR*0.5;

            if(rgStart)     w_dat.y = gz-wy0;
            else            wy0 = wy0+(-gz-wy0)/FREQ_GIR*0.5;

            if(rgStart)     w_dat.z = gx-wz0;
            else            wz0 = wz0+(gx-wz0)/FREQ_GIR*0.5;

            rsk_m = 2.0*M_PI - M_PI/2 - rsc_mag; 

            while (rsk_m > 2.0*M_PI)  rsk_m -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
            while (rsk_m < 0) rsk_m += 2.0*M_PI;

            if((rgFly != 5) /*&& (flStoika == 0)*/)
            {
                float tmp_rsk_gir = rsk_gir;
                while (tmp_rsk_gir > M_PI)  tmp_rsk_gir -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
                while (tmp_rsk_gir < -M_PI) tmp_rsk_gir += 2.0*M_PI;

                float tmp_rsk_m = rsk_m;
                while (tmp_rsk_m > M_PI) tmp_rsk_m -= 2.0*M_PI; //загоняем в диапазон от -M_PI до M_PI
                while (tmp_rsk_m < -M_PI)tmp_rsk_m += 2.0*M_PI;

                if(fabs(rsk_m-rsk_gir) < fabs(tmp_rsk_m-tmp_rsk_gir))  rsk_gir = 0.9*rsk_gir+0.1*rsk_m;
                else                          rsk_gir = 0.9*tmp_rsk_gir+0.1*tmp_rsk_m;
                while (rsk_gir > 2.0*M_PI)  rsk_gir -= 2.0*M_PI;//загоняем в диапазон от 0 до 2*M_PI
                while (rsk_gir < 0)     rsk_gir += 2.0*M_PI;
            }
        }

        if(DEBUG_GYRO)
        {
            if(flNewGPS)
                printf(" w_dat.x %f, w_dat.y %f, w_dat.z %f  \n",  w_dat.x, w_dat.y, w_dat.z);
        }

    }

    void pushGpsData(std::vector<double> pos_data)
    {
        //-----------------------------------------------------------------------------------
        if(flNewGPS)    //автономное управление
        {
            //------------
            //ggg = 17;
            //------------

            LatFly = (unsigned long int)(60UL*10000.*(pos_data[2]/10000000+90));
            LonFly = (unsigned long int)(60UL*10000.*(pos_data[1]/10000000+180));
            Vz = 0; //в интервейса Navio нет скорости из GPS

            if(DEBUG_GPS)
            {

            }
            flTele = flKoord = 1;
            liTimer_tick_GPS = liTimer_tick;

            if (rgStart == 3)
            {
                rgStart = 4;
                rgAnswer = 5;
            }

            flNewGPS = 0;
            if((rgStart > 4)/* && ((rgFly == 2) || (rgFly == 4))*/)
            {
                dz = LonMar[n_];
                dz = 0.1856*(dz - LonFly)*cos_Lat0;
                dx = LatMar[n_];
                dx = 0.1856*(dx-LatFly);

                if ( (fabs(dx) > FLT_EPSILON) || (fabs(dz) > FLT_EPSILON) )
                    napr_toch_mar = atan2(dz, dx);

                rasst_toch_mar = sqrt(dz*dz+dx*dx);
                if ((rgFly == 2) || (rgFly == 4)) //пока входе не будет стоять условие if(rgFly == 2) || (rgFly == 4)
                {
                    if (rasst_toch_mar < 100)
                    {
                        if (n_ < (i_mar-1))
                        {
                            n_++;
                            rgAnswer = 5;
                            V_zad = Vz_Mar[n_];
                            H_zad = H_Mar[n_];

                            if(H_filtr < H_zad) H_max = H_filtr; //летим снизу
                            else                H_max = H_zad;
                        }
                        else
                        {
                            flAutostopThrottleAndConversion = 1;
                            landing();
                        }
                    }
                }//пока
                dz_pr = 0.1856*cos_Lat0*LonMar[n_]-0.1856*cos_Lat0*LonMar[n_-1];
                dx_pr = 0.1856*LatMar[n_]-0.1856*LatMar[n_-1];
                if ( (fabs(dx_pr) > FLT_EPSILON) || (fabs(dz_pr) > FLT_EPSILON) )
                    napr_vetv_mar = atan2(dz_pr, dx_pr);

                angle = napr_toch_mar-napr_vetv_mar;
                if (angle > M_PI) angle = angle-2.0*M_PI;
                if(angle < -M_PI) angle = 2.0*M_PI+angle;

                l_km = 10*V_dat;
                if (l_km < 80) l_km = 80;
                if(fabs(angle) > M_PI/2) l_km = -l_km;
                float tmp_otkl_ot_mar = rasst_toch_mar*sin(angle);   //tmp = otkl_ot_mar
                dz = tmp_otkl_ot_mar*cos(napr_vetv_mar)+l_km*sin(napr_vetv_mar);    //dz = z_toch_pricel
                dx = -tmp_otkl_ot_mar*sin(napr_vetv_mar)+l_km*cos(napr_vetv_mar);   //dx = x_toch_pricel
                if ((rgFly == 2) || (rgFly == 4))
                {
                    if ((fabs(dx) > FLT_EPSILON) || (fabs(dz) > FLT_EPSILON))
                        koors_zad = ToGrad*atan2(dz, dx);
                }
                else
                {
                    if ((fabs(dx) > FLT_EPSILON) || (fabs(dz) > FLT_EPSILON))
                        koors_zad1 = ToGrad*atan2(dz, dx);
                }
            }
        }
        else 
        {
            gpsCheck();
        }
    }
};

#endif // AP_hpp
