#include "UDP.hpp"
#include "PID.hpp"

class MyTest : public CxxTest::TestSuite
{
public:
    void testMethod( void )
    {
        TS_ASSERT( 1 + 1 > 1 );
        TS_ASSERT_EQUALS( 1 + 1, 2 );
    }
}; 

class UdpTest : public CxxTest::TestSuite
{
public:

	void testUdpSockets(void)
	{
		printf("UDP sockets create \n");
		unsigned char Buffer1[80];
		unsigned char Buffer2[80];
		UdpSocket tx_socket(7001);
		UdpServer rx_socket(7001);
		unsigned char* tx_bufer = 0;
		unsigned char* rx_bufer = 0;
		TS_ASSERT_EQUALS(rx_socket.checkStatus(), true);
		TS_ASSERT_EQUALS(tx_socket.checkStatus(), true);
		printf("UDP Server started \n");
		for(int i=0; i<80; i++)
		{
			Buffer1[i] = 0x00;
			Buffer2[i] = 0x00;
		}
		for(int i=0; i<40; i++)
		{
			Buffer1[i] = 0xff;
		}
		Buffer1[20] = 0x00;

		tx_bufer = &Buffer1[0];
		rx_bufer = &Buffer2[0];

		printf("UDP send \n");

		int status = tx_socket.send(tx_bufer);
		TS_ASSERT( status > 0 );
		
		bool success = true;
		for(int i=0; i<80; i++)
		{
			if(Buffer2[i] != Buffer1[i])
				success = false;
		}
		TS_ASSERT( !success );

		int rec_size = 0;
		
		if(rx_socket.checkStatus())
		{
			printf("UDP recieve \n");
			rec_size = rx_socket.recieve(rx_bufer);
		}
		TS_ASSERT_EQUALS( rec_size , 20 );
		
		success = true;
		for(int i=0; i<rec_size; i++)
		{
			if(Buffer2[i] != Buffer1[i])
				success = false;
		}

		TS_ASSERT( success );

		for(int i=0; i<80; i++)
		{
			Buffer2[i] = 0x00;
		}

		status = rx_socket.send(tx_bufer);
		TS_ASSERT( status > 0 );

		rec_size = rx_socket.recieve(rx_bufer);
		TS_ASSERT_EQUALS(status, rec_size);

		success = true;
		for(int i=0; i<rec_size; i++)
		{
			if(Buffer2[i] != Buffer1[i])
				success = false;
		}

		TS_ASSERT( success );
		
	}

};

class PidTest : public CxxTest::TestSuite
{
public:	
	void testP(void)
	{
		printf("P \n");		
		PID pid_test(1, 0.0, 0.0);
		double current = 0.0;
		double target = 10.0;
		double dt  = 0.1;

		double diff_before =target - current;
		//current = pid_test.calc(current, target, dt);
		double diff_after =target - current;

		double diff_to_zero = 0;

		for(int i=0; i<1; i++)
		{
			diff_to_zero = pid_test.calc(current, target, dt);
			//diff_to_zero = pid_test.calc(current, target);
			diff_after = target - current - diff_to_zero;
			printf("current: %5.2f, target: %5.2f, diff_before: %5.2f, diff_after: %5.2f \n", current, target, diff_before, diff_after);
		}

		double accuracy = 0.1;

		TS_ASSERT(fabs(diff_after) <  fabs(diff_before));
		printf("%5.2f, %5.2f \n", fabs(diff_after), accuracy);
		TS_ASSERT(fabs(diff_after) < accuracy)
	}

	void testI(void)
	{
		printf("I \n");
		PID pid_test(0.0, 1.0, 0.0);
		double current = 0.0;
		double target = 10.0;
		double dt  = 0.1;
		pid_test.setMaxMin(target/dt);

		double diff_before =target - current;
		//current = pid_test.calc(current, target, dt);
		double diff_after =target - current;

		double diff_to_zero = 0;

		for(int i=0; i<10; i++)
		{
			diff_to_zero = pid_test.calc(current, target, dt);
			diff_after = target - current - diff_to_zero;
			printf("current: %5.2f, target: %5.2f, diff_before: %5.2f, diff_after: %5.2f \n", current, target, diff_before, diff_after);
		}

		double accuracy = 0.1;

		TS_ASSERT(fabs(diff_after) <  fabs(diff_before));
		printf("%5.2f, %5.2f \n", fabs(diff_after), accuracy);
		TS_ASSERT(fabs(diff_after) < accuracy)
	}

	void testD(void)
	{
		printf("D \n");
		PID pid_test(0.0, 0.0, 1.0);
		double current = 0.0;
		double target = 0.0;
		double dt  = 0.1;
		pid_test.setFCut(1.0);

		double diff_before =target - current;
		//current = pid_test.calc(current, target, dt);
		double diff_after =target - current;

		double diff_to_zero = 0;

		for(int i=0; i<10; i++)
		{
			diff_to_zero = pid_test.calc(current, target, dt);
			diff_after = target - current - diff_to_zero;
			
			printf("current: %5.2f, target: %5.2f, diff_before: %5.2f, diff_after: %5.2f \n", current, target, diff_before, diff_after);
			target++;
		}

		double accuracy = 0.1;
		printf("%5.2f, %5.2f \n", fabs(diff_after), accuracy);
		TS_ASSERT(fabs(diff_after) < accuracy)
	}

};
