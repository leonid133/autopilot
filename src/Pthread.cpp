#include "AP.hpp"
#include "UDP.hpp"

#include "Navio/RGBled.h"

#include "Navio/MPU9250.h"
#include "Navio/LSM9DS1.h"

#include "Navio/MS5611.h"

#include "Navio/Ublox.h"

#include "Navio/PWM.h"

#include "Navio/Util.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <cstdlib>
#include <pthread.h>

#include <time.h>

#include "AHRS.hpp"
#include <sys/time.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>
#include <fcntl.h>
#include <termios.h>

#define G_SI 9.80665
#define PI   3.14159

//Thread data struct
enum class Status {
    Started,
    Initialized,
    Running,
    Stopped,
    Aborted,
    Failed
};

struct thread_data{
    int  thread_id;
    int  thread_task;
    char *message;
    Status thread_status;
    int fd;
    AP *ap;
    UdpServer *udpServer;
};

//LED
void *aThreadLED(void *threading);
pthread_mutex_t mutexLED = PTHREAD_MUTEX_INITIALIZER;

//AccelGyroMag
void *aThreadAccelGyroMag(void *threading);
pthread_mutex_t mutexAccelGyroMag = PTHREAD_MUTEX_INITIALIZER;

//Barometer
void *aThreadBarometer(void *threading);
pthread_mutex_t mutexBarometer = PTHREAD_MUTEX_INITIALIZER;

//GPS
void *aThreadGPS(void * threading);
pthread_mutex_t mutexGPS = PTHREAD_MUTEX_INITIALIZER;

//TxRx
void *aThreadRx(void *threading);
void *aThreadTx(void *threading);

pthread_mutex_t mutexUart = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexUdp = PTHREAD_MUTEX_INITIALIZER;

struct termios serial;
const char* device = "/dev/ttyAMA0";

//Servo
void *aThreadServo(void *threading);
pthread_mutex_t mutexServo = PTHREAD_MUTEX_INITIALIZER;

#define PWM_OUTPUT_T1 0
#define PWM_OUTPUT_T2 1
#define PWM_OUTPUT_T3 2
#define PWM_OUTPUT_T4 3
#define PWM_OUTPUT_A1 5
#define PWM_OUTPUT_A2 6
#define PWM_OUTPUT_A3 7
#define PWM_OUTPUT_A4 8

#define SERVO_MIN 0.570 /*mS*/
#define SERVO_MAX 2.630 /*mS*/
#define THROTTLE_MIN 1.000 /*ms*/
#define THROTTLE_MAX 1.900 /*ms*/

int main ()
{
    if (check_apm()) {
        return 1;
    }

    printf("Let`s Go \n");
    AP *ap = AP::Get(); //Old autopilot

    //LED
    int rcLED;
    pthread_t threadLED;
    struct thread_data tdLED;

    tdLED.thread_id = 1;
    tdLED.thread_task = 8;
    tdLED.thread_status = Status::Started;
    tdLED.ap = AP::Get();

    if( (rcLED = pthread_create(&threadLED, NULL, aThreadLED, (void *)&tdLED) ) )
    {
        printf("Thread creation failed: %d\n", rcLED);
        tdLED.thread_status = Status::Failed;
    }

    //AccelGyroMag
    int rcAccelGyroMag;
    pthread_t threadAccelGyroMag;
    struct thread_data tdAccelGyroMag;

    tdAccelGyroMag.thread_id = 2;
    tdAccelGyroMag.thread_status = Status::Started;
    tdAccelGyroMag.thread_task = 0;
    tdAccelGyroMag.ap = AP::Get();

    if( (rcAccelGyroMag = pthread_create(&threadAccelGyroMag, NULL, aThreadAccelGyroMag, (void *)&tdAccelGyroMag) ) )
    {
        printf("Thread creation failed: %d\n", rcAccelGyroMag);
        tdAccelGyroMag.thread_status = Status::Failed;
    }

    //Barometer
    int rcBarometer;
    pthread_t threadBarometer;
    struct thread_data tdBarometer;

    tdBarometer.thread_id = 3;
    tdBarometer.thread_status = Status::Started;
    tdBarometer.thread_task = 0;
    tdBarometer.ap = AP::Get();


    if( (rcBarometer = pthread_create(&threadBarometer, NULL, aThreadBarometer, (void *)&tdBarometer) ) )
    {
        printf("Thread creation failed: %d\n", rcBarometer);
        tdBarometer.thread_status = Status::Failed;
    }

    //GPS
    int rcGPS;
    pthread_t threadGPS;
    struct thread_data tdGPS;

    tdGPS.thread_id = 4;
    tdGPS.thread_task = 0;
    tdGPS.thread_status = Status::Started;
    tdGPS.ap = AP::Get();

    if( (rcGPS = pthread_create(&threadGPS, NULL, aThreadGPS, (void *)&tdGPS) ) )
    {
        printf("Thread creation failed: %d\n", rcGPS);
        tdGPS.thread_status = Status::Failed;
    }

    //RxTx
    printf("Opening %s\n", device);

    int fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY);

    if (fd == -1) {
        perror(device);
        return -1;
    }

    if (tcgetattr(fd, &serial) < 0) {
        perror("Getting configuration");
        return -1;
    }

    // Set up Serial Configuration
    serial.c_iflag = 0;
    serial.c_oflag = 0;
    serial.c_lflag = 0;
    serial.c_cflag = 0;

    serial.c_cc[VMIN] = 0;
    serial.c_cc[VTIME] = 0;

    //8N1
    serial.c_cflag = B57600 | (CLOCAL | CREAD);
    serial.c_cflag &= ~PARENB;
    serial.c_cflag &= ~CSTOPB;
    serial.c_cflag &= ~CSIZE;
    serial.c_cflag |= CS8;

    tcsetattr(fd, TCSANOW, &serial); // Apply configuration

    int rcRx;
    pthread_t threadRx;
    struct thread_data tdRx;

    tdRx.thread_id = 5;
    tdRx.fd = fd;
    tdRx.ap = AP::Get();
    tdRx.thread_status = Status::Started;
    if( (rcRx = pthread_create(&threadRx, NULL, aThreadRx, (void *)&tdRx) ) )
    {
        printf("Thread creation failed: %d\n", rcRx);
    }

    int rcTx;
    pthread_t threadTx;
    struct thread_data tdTx;

    tdTx.thread_id = 6;
    tdTx.thread_task = 0;
    tdTx.fd = fd;
    tdTx.ap = AP::Get();
    tdTx.thread_status = Status::Started;
    if( (rcTx = pthread_create(&threadTx, NULL, aThreadTx, (void *)&tdTx) ) )
    {
        printf("Thread creation failed: %d\n", rcTx);
    }

    //Servo
    int rcServo;
    pthread_t threadServo;
    struct thread_data tdServo;

    tdServo.thread_id = 7;
    tdServo.thread_task = 0;
    tdServo.thread_status = Status::Started;
    tdServo.ap = AP::Get();

    if( (rcServo = pthread_create(&threadServo, NULL, aThreadServo, (void *)&tdServo) ) )
    {
        printf("Thread creation failed: %d\n", rcServo);
        tdServo.thread_status = Status::Failed;
    }

    printf("Main process ID: %d\n", (int)getpid());

    long clockPrint= clock();

    bool flRun = false;
    bool flTick = false;

    //timing flRun
    struct timeval tv_run;
    unsigned long previoustime_fl, currenttime_fl;

    double dt_run = 0;

    //Main Loop
    while(true)
    {
        if(flRun)
        {
            gettimeofday(&tv_run,NULL);
            previoustime_fl = currenttime_fl;
            currenttime_fl = 1000000 * tv_run.tv_sec + tv_run.tv_usec;
            dt_run = (currenttime_fl - previoustime_fl) / 1000000.0;
            if(dt_run < 1./FREQ_OSN) usleep((1./FREQ_OSN-dt_run)*1000000);
            gettimeofday(&tv_run,NULL);
            currenttime_fl = 1000000 * tv_run.tv_sec + tv_run.tv_usec;
            dt_run = (currenttime_fl - previoustime_fl) / 1000000.0;

            ap->run();
            flRun = 0;
        }

        long current_clock = clock();

        //Работа---------------------------------------------------------------

        if( Status::Running == tdAccelGyroMag.thread_status &&
           Status::Running == tdBarometer.thread_status &&
           Status::Running == tdGPS.thread_status &&
           Status::Running == tdTx.thread_status &&
           Status::Running == tdRx.thread_status &&
           Status::Running == tdServo.thread_status)
        {            
            flRun = 1;
        }
        else
        {
            sleep(5);
            if( Status::Stopped == tdAccelGyroMag.thread_status || 
               Status::Aborted == tdAccelGyroMag.thread_status || 
               Status::Failed  == tdAccelGyroMag.thread_status ||
               Status::Stopped == tdBarometer.thread_status || 
               Status::Aborted == tdBarometer.thread_status || 
               Status::Failed  == tdBarometer.thread_status ||
               Status::Stopped == tdGPS.thread_status || 
               Status::Aborted == tdGPS.thread_status || 
               Status::Failed  == tdGPS.thread_status ||
               Status::Stopped == tdTx.thread_status || 
               Status::Aborted == tdTx.thread_status || 
               Status::Failed  == tdTx.thread_status ||
               Status::Stopped == tdRx.thread_status || 
               Status::Aborted == tdRx.thread_status || 
               Status::Failed  == tdRx.thread_status ||
               Status::Stopped == tdServo.thread_status || 
               Status::Aborted == tdServo.thread_status || 
               Status::Failed  == tdServo.thread_status 
              )
                exit( EXIT_FAILURE);
        }

        if( (flRun) && (current_clock - clockPrint)> 5000000 && dt_run > 0)
        {
            clockPrint = current_clock;

            printf("\n");
            printf("rgFly: %d rgStart: %d  rgTest: %d rgAnswer: %d \n", ap->rgFly, ap->rgStart, ap->rgTest, ap->rgAnswer);

            printf("PERIOD_Run %.4fs  RATE_Run %.4f Hz\n", dt_run, (1./dt_run));

            printf("Altitude: %+3.3f \n", ap->altitude);

            if(tdGPS.thread_task != 0)//&& (int)pos_data[0]!=0x00)
            {
                printf("Longitude: %lu\n", ap->LonFly);
                printf("Latitude: %lu\n", ap->LatFly);

                printf("\n");
            }
        }
    }

    pthread_join( threadLED, NULL);
    pthread_join( threadAccelGyroMag, NULL);
    pthread_join( threadBarometer, NULL);
    pthread_join( threadGPS, NULL);
    pthread_join( threadRx, NULL );
    pthread_join( threadTx, NULL );
    pthread_join( threadServo, NULL );

    close(fd);

    printf("From main process ID: %d\n", (int)getpid());

    exit(EXIT_SUCCESS);
    return 0;
}

void *aThreadLED(void *threading)
{
    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("Led Thread ID :  %d\n", my_data->thread_id);
    RGBled led;

    if(!led.initialize())
    {
        my_data->thread_status = Status::Failed;
        printf("Failed Thread ID :  %d\n", my_data->thread_id);
        pthread_exit(NULL);
    }

    my_data->thread_status = Status::Initialized;
    printf("Initialized Thread ID :  %d\n", my_data->thread_id);

    while(true)
    {
        my_data->thread_status = Status::Running;

        switch(my_data->thread_task)
        {
            case 0: //0 - стоим
                led.setColor(Colors::Cyan);
                break;
            case 1: //1 - тест силовых установок
                led.setColor(Colors::Red);
                break;
            case 2: //2 - замедленный тест
                led.setColor(Colors::Yellow);
                break;
            case 3: //3 - ждем координаты
                led.setColor(Colors::Blue);
                break;
            case 4: //4 - системная готовность
                led.setColor(Colors::Green);
                break;
            case 5: //5 - старт!
                led.setColor(Colors::Magneta);
                break;
            case 6: //6 - летим
                led.setColor(Colors::White);
                break;
            case 7:
                led.setColor(Colors::White);
                break;
            case 8:
                led.setColor(Colors::White);
                sleep(1);
                led.setColor(Colors::Black);
                break;
            case 255:
                led.setColor(Colors::Black);
                my_data->thread_status = Status::Stopped;
                printf("Stopped Thread ID :  %d\n", my_data->thread_id);
                pthread_exit(NULL);
                break;
            default:
                led.setColor(Colors::Black);
                break;
        }
        sleep(2);
        my_data->thread_task = my_data->ap->rgStart;
    }
    my_data->thread_status = Status::Stopped;
    printf("Stopped Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}


void *aThreadAccelGyroMag(void *threading)
{
    // Timing data

    float offset[3];
    float offsetMpu[3];
    struct timeval tv;
    float dt, maxdt;
    float mindt = 0.01;

    float accel_summ = 0;
    int isFirst = 1;
    unsigned long previoustime, currenttime;

    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("AccelGyroMag Thread ID :  %d\n", my_data->thread_id);

    // variables
    AHRS    ahrs, ahrsMpu;   // Mahony AHRS

    // Orientation data
    float roll, pitch, yaw;

    float rollM, pitchM, yawM;

    float ax, ay, az;
    float gx, gy, gz;
    float mx, my, mz;

    float axM, ayM, azM;
    float gxM, gyM, gzM;
    float mxM, myM, mzM;

    InertialSensor *sensor;
    InertialSensor *sensorMpu;
    sensor = new LSM9DS1();
    sensorMpu = new MPU9250();

    if (!sensor->probe() || !sensorMpu->probe())
    {
        printf("Sensor not enabled\n");
        my_data->thread_status = Status::Failed;
        printf("Failed Thread ID :  %d\n", my_data->thread_id);
        pthread_exit(NULL);
    }

    sensor->initialize();
    sensorMpu->initialize();

    my_data->thread_status = Status::Initialized;
    printf("Initialized Thread ID :  %d\n", my_data->thread_id);

    printf("Beginning Gyro calibration...\n");
    float calibration_i = 1000.0;
    for(int i = 0; i<(int)calibration_i; i++)
    {
        sensor->update();
        sensor->read_gyroscope(&gx, &gy, &gz);

        gx *= 180 / PI;
        gy *= 180 / PI;
        gz *= 180 / PI;

        offset[0] += (-gx*0.0175);
        offset[1] += (-gy*0.0175);
        offset[2] += (-gz*0.0175);
        usleep(10000);
    }
    offset[0]/=calibration_i;
    offset[1]/=calibration_i;
    offset[2]/=calibration_i;

    printf("Offsets are: %f %f %f\n", offset[0], offset[1], offset[2]);
    ahrs.setGyroOffset(offset[0], offset[1], offset[2]);

    for(int i = 0; i<(int)calibration_i; i++)
    {
        sensorMpu->update();
        sensorMpu->read_gyroscope(&gxM, &gyM, &gzM);

        gxM *= 180 / PI;
        gyM *= 180 / PI;
        gzM *= 180 / PI;

        offsetMpu[0] += (-gxM*0.0175);
        offsetMpu[1] += (-gyM*0.0175);
        offsetMpu[2] += (-gzM*0.0175);
        usleep(10000);
    }
    offsetMpu[0]/=calibration_i;
    offsetMpu[1]/=calibration_i;
    offsetMpu[2]/=calibration_i;

    printf("Offsets are: %f %f %f\n", offsetMpu[0], offsetMpu[1], offsetMpu[2]);
    ahrsMpu.setGyroOffset(offsetMpu[0], offsetMpu[1], offsetMpu[2]);

    //DEBUG_ROLL_PICH_YAW
    float dtsumm = 0;
    // Network data
    int sockfd;
    struct sockaddr_in servaddr = {0};
    char sendline[80];
    if(DEBUG_ROLL_PICH_YAW)
    { 
        //--------------------------- Network setup -------------------------------
        sockfd = socket(AF_INET,SOCK_DGRAM,0);
        servaddr.sin_family = AF_INET;

        servaddr.sin_addr.s_addr = inet_addr("192.168.1.113");
        servaddr.sin_port = htons(7000);
        //
    }

    while(true)
    {
        my_data->thread_status = Status::Running;
        //----------------------- Calculate delta time ----------------------------

        gettimeofday(&tv,NULL);
        previoustime = currenttime;
        currenttime = 1000000 * tv.tv_sec + tv.tv_usec;
        dt = (currenttime - previoustime) / 1000000.0;
        if(dt < 1./FREQ_GIR) usleep((1./FREQ_GIR-dt)*1000000);

        gettimeofday(&tv,NULL);
        currenttime = 1000000 * tv.tv_sec + tv.tv_usec;
        dt = (currenttime - previoustime) / 1000000.0;

        //--------------------------Sensor update---------------
        sensor->update();

        sensor->read_accelerometer(&ax, &ay, &az);
        sensor->read_gyroscope(&gx, &gy, &gz);
        //sensor->read_magnetometer(&mx, &my, &mz);

        ax /= G_SI;
        ay /= G_SI;
        az /= G_SI;
        gx *= 180 / PI;
        gy *= 180 / PI;
        gz *= 180 / PI;

        //ahrs.update(ax, ay, az, gx*0.0175, gy*0.0175, gz*0.0175, my, mx, -mz, dt);
        ahrs.updateIMU(ax, ay, az, gx*0.0175, gy*0.0175, gz*0.0175, dt);

        //------------------------ Read Euler angles ------------------------------
        ahrs.getEuler(&roll, &pitch, &yaw);

        //--------------------------Sensor update---------------
        sensorMpu->update();

        sensorMpu->read_accelerometer(&axM, &ayM, &azM);
        sensorMpu->read_gyroscope(&gxM, &gyM, &gzM);
        sensorMpu->read_magnetometer(&mxM, &myM, &mzM);

        axM /= G_SI;
        ayM /= G_SI;
        azM /= G_SI;
        gxM *= 180 / PI;
        gyM *= 180 / PI;
        gzM *= 180 / PI;

        ahrsMpu.update(axM, ayM, azM, gxM*0.0175, gyM*0.0175, gzM*0.0175, myM, mxM, -mzM, dt);
        //ahrs.updateIMU(ax, ay, az, gx*0.0175, gy*0.0175, gz*0.0175, dt);

        //------------------------ Read Euler angles ------------------------------
        ahrsMpu.getEuler(&rollM, &pitchM, &yawM);

        //------------------- Discard the time of the first cycle -----------------

        if (!isFirst)
        {
            if (dt > maxdt) maxdt = dt;
            if (dt < mindt) mindt = dt;
        }
        isFirst = 0;

        ////-------------Send data to AP//-------------
        my_data->ap->pushAccelGyroData(ax, ay, az, gx/ToGrad, gy/ToGrad, gz/ToGrad, yawM/ToGrad, dt);
        my_data->ap->pushPitchRollYawData(roll, pitch, yaw);


        my_data->ap->timer_tick++; 
        my_data->ap->liTimer_tick++;
        if (my_data->ap->NoCommand++ > 21*FREQ_GIR)  my_data->ap->NoCommand = 21*FREQ_GIR;

        //------------- Console and network output with a lowered rate ------------
        if(DEBUG_ROLL_PICH_YAW)
        { 
            dtsumm += dt;
            if(dtsumm >= 0.05) //20 Hz
            {       
                printf("ROLL: %+05.2f PITCH: %+05.2f YAW: %+05.2f PERIOD %.4fs RATE %d Hz\n", roll, pitch, yawM * -1, dt, int(1/dt));
                // Network output
                sprintf(sendline,"%10f %10f %10f %10f %dHz\n", ahrs.getW(), ahrs.getX(), ahrs.getY(), ahrs.getZ(), int(1/dt));
                sendto(sockfd, sendline, strlen(sendline), 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
                dtsumm = 0;
            }
        }
    }
    my_data->thread_status = Status::Stopped;
    printf("Stopped Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}


void *aThreadBarometer(void *threading)
{
    struct timeval tv_bar;
    unsigned long previoustime_ba, currenttime_ba;
    float dt_ba;

    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("Barometer Thread ID :  %d\n", my_data->thread_id);

    MS5611 barometer;
    barometer.initialize();

    if(!barometer.testConnection())
    {
        my_data->thread_status = Status::Failed;
        printf("Failed Thread ID :  %d\n", my_data->thread_id);
        pthread_exit(NULL);
    }

    my_data->thread_status = Status::Initialized;
    printf("Initialized Thread ID :  %d\n", my_data->thread_id);

    float temperature, pressure;
    float temperature_start, pressure_start;
    for(int i=0; i<1000; i++)
    {
        barometer.refreshPressure();
        usleep(10000); // Waiting for pressure data ready
        barometer.readPressure();
        barometer.refreshTemperature();
        usleep(10000); // Waiting for temperature data ready
        barometer.readTemperature();
        barometer.calculatePressureAndTemperature();
        temperature = temperature_start = temperature_start+(barometer.getTemperature()-temperature_start)*0.1;
        pressure = pressure_start = pressure_start+(barometer.getPressure()-pressure_start)*0.1;
        my_data->ap->altitude = 0;
    }
    sleep(1);
    while(true)
    {
        my_data->thread_status = Status::Running;
        gettimeofday(&tv_bar,NULL);
        previoustime_ba = currenttime_ba;
        currenttime_ba = 1000000 * tv_bar.tv_sec + tv_bar.tv_usec;
        dt_ba = (currenttime_ba - previoustime_ba) / 1000000.0;
        switch(my_data->thread_task)
        {
            case 0:
                for(int i=0; i<1000; i++)
                {
                    barometer.refreshPressure();
                    usleep(10000); // Waiting for pressure data ready
                    barometer.readPressure();
                    barometer.refreshTemperature();
                    usleep(10000); // Waiting for temperature data ready
                    barometer.readTemperature();
                    barometer.calculatePressureAndTemperature();
                    temperature = temperature_start = temperature_start+(barometer.getTemperature()-temperature_start)*0.1;
                    pressure = pressure_start = pressure_start+(barometer.getPressure()-pressure_start)*0.1;
                }
                my_data->thread_task = 1;
                break;
            case 1:
                barometer.refreshPressure();
                usleep(10000); // Waiting for pressure data ready
                barometer.readPressure();
                barometer.refreshTemperature();
                usleep(10000); // Waiting for temperature data ready
                barometer.readTemperature();
                barometer.calculatePressureAndTemperature();
                temperature = temperature+(barometer.getTemperature()-temperature)*0.1;
                pressure = pressure +(barometer.getPressure()-pressure)*0.3;
                my_data->ap->altitude = 18400*(1+0.003665*(temperature_start + temperature)*0.5 )*log(pressure_start/pressure);
                break;
            case 255:
                my_data->thread_status = Status::Stopped;
                pthread_exit(NULL);
                break;
            default:
                break;
        }
    }
    my_data->thread_status = Status::Stopped;
    printf("Stopped Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}

void * aThreadGPS(void *threading)
{
    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("GPS Thread ID :  %d\n", my_data->thread_id);

    Ublox gps;
    std::vector<double> pos_data;

    sleep(1);
    if(gps.testConnection())
    {
        my_data->thread_status = Status::Initialized;
        printf("Initialized Thread ID :  %d\n", my_data->thread_id);

        while(true)
        {
            my_data->thread_status = Status::Running;

            int stat_i = gps.decodeSingleMessage(Ublox::NAV_STATUS, pos_data);
            if ( stat_i == 1)
            {
                my_data->thread_task =1;

                if(DEBUG_GPS)
                {
                    printf("Current GPS status:\n");
                    printf("gpsFixOk: %d\n", ((int)pos_data[1] & 0x01));
                    printf("gps Fix status: ");
                }

                switch((int)pos_data[0])
                {
                    case 0x00:
                        //my_data->ap->flNewGPS = true;
                        if(DEBUG_GPS)printf("no fix\n");
                        break;

                    case 0x01:
                        my_data->ap->flNewGPS = true;
                        if(DEBUG_GPS) printf("dead reckoning only\n");
                        break;

                    case 0x02:
                        my_data->ap->flNewGPS = true;
                        if(DEBUG_GPS) printf("2D-fix\n");
                        break;

                    case 0x03:
                        my_data->ap->flNewGPS = true;
                        if(DEBUG_GPS) printf("3D-fix\n");
                        break;

                    case 0x04:
                        my_data->ap->flNewGPS = true;
                        if(DEBUG_GPS) printf("GPS + dead reckoning combined\n");
                        break;

                    case 0x05:
                        if(DEBUG_GPS) printf("Time only fix\n");
                        break;

                    default:
                        if(DEBUG_GPS) printf("Reserved value. Current state unknown\n");
                        break;
                }
            }
            else
            {
                if(DEBUG_GPS) printf("Status Message not captured\n");
            }

            int pos_i = gps.decodeSingleMessage(Ublox::NAV_POSLLH, pos_data);
            if ( pos_i == 1)
            {
                // after desired message is successfully decoded, we can use the information stored in pos_data vector
                // right here, or we can do something with it from inside decodeSingleMessage() function(see ublox.h).
                // the way, data is stored in pos_data vector is specified in decodeMessage() function of class UBXParser(see ublox.h)
                if(DEBUG_GPS){
                    printf("GPS Millisecond Time of Week: %.0lf s\n", pos_data[0]/1000);
                    printf("Longitude: %lf\n", pos_data[1]/10000000);
                    printf("Latitude: %lf\n", pos_data[2]/10000000);
                    printf("Height above Ellipsoid: %.3lf m\n", pos_data[3]/1000);
                    printf("Height above mean sea level: %.3lf m\n", pos_data[4]/1000);
                    printf("Horizontal Accuracy Estateimate: %.3lf m\n", pos_data[5]/1000);
                    printf("Vertical Accuracy Estateimate: %.3lf m\n", pos_data[6]/1000);
                }
            }

            if(my_data->ap->flNewGPS)
                my_data->ap->pushGpsData(pos_data);

            usleep(500000);
        }
    }
    my_data->thread_status = Status::Failed;
    printf("Failed Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}

void *aThreadRx(void *threading)
{
    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("Rx Thread ID :  %d\n", my_data->thread_id);

    UdpServer rx_socket(7654);

    unsigned char rx_bufer[80];
    unsigned char* p_rx_bufer;
    p_rx_bufer = &rx_bufer[0];

    my_data->thread_status = Status::Initialized;
    printf("Initialized Thread ID :  %d\n", my_data->thread_id);

    while(true)
    {
        my_data->thread_status = Status::Running;
        if ( my_data->fd != -1 )
        {
            // UART read
            pthread_mutex_lock(&mutexUart);
            int rx_length = read(my_data->fd, (void*)p_rx_bufer, 80);    //Filestream, buffer to store in, number of bytes to read (max)
            pthread_mutex_unlock(&mutexUart);

            if (rx_length < 0)
            {
                //An error occured (will occur if there are no bytes)
            }
            else if (rx_length == 0)
            {
                //No data waiting
                //printf("No data");
            }
            else
            {
                //Bytes received
                my_data->ap->pushRxData(rx_bufer, rx_length);
                //rx_bufer[rx_length] = '\0';
                //printf("%i bytes read : %s\n", rx_length, rx_bufer);
            }
        }

        if ( rx_socket.checkStatus() )
        {
            //UDP Read
            pthread_mutex_lock(&mutexUdp);
            int rx_length = rx_socket.recieve(p_rx_bufer);
            pthread_mutex_unlock(&mutexUdp);
            //printf("%i bytes read : %s\n", rx_length, rx_bufer);
            if(rx_length>0)
                my_data->ap->pushRxData(rx_bufer, rx_length);
            //printf("udp recSize Status = %d\n", rx_socket.recSize());
        }
        usleep(35000);
    }

    my_data->thread_status = Status::Stopped;
    printf("Stopped Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}

void *aThreadTx(void *threading)
{
    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("Tx Thread ID :  %d\n", my_data->thread_id);

    UdpSocket tx_socket(7001);

    my_data->thread_task = 0;

    unsigned char *p_tx_bufer;
    unsigned char tx_bufer[80];
    p_tx_bufer = &tx_bufer[0];

    my_data->thread_status = Status::Initialized;
    printf("Initialized Thread ID :  %d\n", my_data->thread_id);

    while(true)
    {
        my_data->thread_status = Status::Running;

        p_tx_bufer = my_data->ap->pullTxData();

        for(int i = 0; i < 80; i++ )
        {
            tx_bufer[i] = p_tx_bufer[i];
        }

        //UART send
        pthread_mutex_lock(&mutexUart);
        int wcount = write(my_data->fd, tx_bufer, sizeof tx_bufer/ sizeof tx_bufer[0]);
        pthread_mutex_unlock(&mutexUart);
        if (wcount < 0) {
            //perror("Write");
        }
        else {
            //printf("Sent %d characters\n", wcount);
        }

        //UDP send
        if ( tx_socket.checkStatus() )
        {
            pthread_mutex_lock(&mutexUdp);
            int udp_send_status = tx_socket.send(tx_bufer);
            pthread_mutex_unlock(&mutexUdp);
        }

        usleep(35000);
    }
    my_data->thread_status = Status::Stopped;
    printf("Stopped Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}

void *aThreadServo(void *threading)
{
    double T1, T2, T3, T4; // Throttle
    double A1, A2, A3, A4; //Angle

 // Timing data

    struct timeval tv;
    double dt;
 
    unsigned long previoustime, currenttime;
    
    struct thread_data *my_data;
    my_data = (struct thread_data *) threading;
    printf("Servo Thread ID :  %d\n", my_data->thread_id);


    PWM pwm;

    if ( !pwm.init(PWM_OUTPUT_T1) || 
        !pwm.init(PWM_OUTPUT_T2) || 
        !pwm.init(PWM_OUTPUT_T3) || 
        !pwm.init(PWM_OUTPUT_T4) || 
        !pwm.init(PWM_OUTPUT_A1) || 
        !pwm.init(PWM_OUTPUT_A2) || 
        !pwm.init(PWM_OUTPUT_A3) || 
        !pwm.init(PWM_OUTPUT_A4) )
    {
        fprintf(stderr, "Output Enable not set. Are you root?\n");
        my_data->thread_status = Status::Failed;
        printf("Failed Thread ID :  %d\n", my_data->thread_id);
        pthread_exit(NULL);
    }

    pwm.enable(PWM_OUTPUT_T1);
    pwm.set_period(PWM_OUTPUT_T1, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_T2);
    pwm.set_period(PWM_OUTPUT_T2, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_T3);
    pwm.set_period(PWM_OUTPUT_T3, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_T4);
    pwm.set_period(PWM_OUTPUT_T4, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_A1);
    pwm.set_period(PWM_OUTPUT_A1, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_A2);
    pwm.set_period(PWM_OUTPUT_A2, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_A3);
    pwm.set_period(PWM_OUTPUT_A3, FREQ_PWM);

    pwm.enable(PWM_OUTPUT_A4);
    pwm.set_period(PWM_OUTPUT_A4, FREQ_PWM);

    pthread_mutex_lock( &mutexServo );
    T1 = T2 = T3 = T4 = A1 = A2 = A3 = A4 =0.0;
    pthread_mutex_unlock( &mutexServo );

    my_data->thread_status = Status::Initialized;
    printf("Initialized Thread ID :  %d\n", my_data->thread_id);

    unsigned int timer_tick_servo = 0;
    unsigned int timer_tick_run = 0;

    while(true)
    {
        //----------------------- Calculate delta time ----------------------------

        gettimeofday(&tv,NULL);
        previoustime = currenttime;
        currenttime = 1000000 * tv.tv_sec + tv.tv_usec;
        dt = (currenttime - previoustime) / 1000000.0;
        if(dt < 1./FREQ_PWM) usleep((1./FREQ_PWM-dt)*1000000);

        gettimeofday(&tv,NULL);
        currenttime = 1000000 * tv.tv_sec + tv.tv_usec; 
        dt = (currenttime - previoustime) / 1000000.0;

        my_data->thread_status = Status::Running;

        switch(my_data->thread_task)
        {
            case 0:
                pwm.set_duty_cycle(PWM_OUTPUT_T1, THROTTLE_MIN);
                pwm.set_duty_cycle(PWM_OUTPUT_T2, THROTTLE_MIN);
                pwm.set_duty_cycle(PWM_OUTPUT_T3, THROTTLE_MIN);
                pwm.set_duty_cycle(PWM_OUTPUT_T4, THROTTLE_MIN);

                pwm.set_duty_cycle(PWM_OUTPUT_A1, 1.800); //TODO in define x51
                pwm.set_duty_cycle(PWM_OUTPUT_A2, 1.800);
                pwm.set_duty_cycle(PWM_OUTPUT_A3, 1.110);
                pwm.set_duty_cycle(PWM_OUTPUT_A4, 1.880);
                break;

            case 4:
               // T1 = T2 = T3 = T4 = THROTTLE_MIN;

                T1 = T1 > THROTTLE_MAX ? THROTTLE_MAX : T1;
                T1 = T1 < THROTTLE_MIN ? THROTTLE_MIN : T1;

                T2 = T2 > THROTTLE_MAX ? THROTTLE_MAX : T2;
                T2 = T2 < THROTTLE_MIN ? THROTTLE_MIN : T2;

                T3 = T3 > THROTTLE_MAX ? THROTTLE_MAX : T3;
                T3 = T3 < THROTTLE_MIN ? THROTTLE_MIN : T3;

                T4 = T4 > THROTTLE_MAX ? THROTTLE_MAX : T4;
                T4 = T4 < THROTTLE_MIN ? THROTTLE_MIN : T4;

                A1 = A1 > SERVO_MAX ? SERVO_MAX : A1;
                A1 = A1 < SERVO_MIN ? SERVO_MIN : A1;

                A2 = A2 > SERVO_MAX ? SERVO_MAX : A2;
                A2 = A2 < SERVO_MIN ? SERVO_MIN : A2;

                A3 = A3 > SERVO_MAX ? SERVO_MAX : A3;
                A3 = A3 < SERVO_MIN ? SERVO_MIN : A3;

                A4 = A4 > SERVO_MAX ? SERVO_MAX : A4;
                A4 = A4 < SERVO_MIN ? SERVO_MIN : A4;

                pwm.set_duty_cycle(PWM_OUTPUT_T1, T1);
                pwm.set_duty_cycle(PWM_OUTPUT_T2, T2);
                pwm.set_duty_cycle(PWM_OUTPUT_T3, T3);
                pwm.set_duty_cycle(PWM_OUTPUT_T4, T4);
                pwm.set_duty_cycle(PWM_OUTPUT_A1, A1);
                pwm.set_duty_cycle(PWM_OUTPUT_A2, A2);
                pwm.set_duty_cycle(PWM_OUTPUT_A3, A3);
                pwm.set_duty_cycle(PWM_OUTPUT_A4, A4);
                break;

            default:
                my_data->thread_status = Status::Stopped;
                printf("Stopped Thread ID :  %d\n", my_data->thread_id);
                pthread_exit(NULL);
                break;
        }

 
        pthread_mutex_lock( &mutexServo );

        my_data->ap->pullThrottleAndConversionAngle(T1, T2, T3, T4, A1, A2, A3, A4); 

        pthread_mutex_unlock( &mutexServo );

        if(timer_tick_servo++ > 100)
        {
            timer_tick_servo = 0;

            if(timer_tick_run != my_data->ap->timer_tick)
                my_data->thread_task = 4;
            else
                my_data->thread_task = 0;

            timer_tick_run = my_data->ap->timer_tick;  

            if(DEBUG_THROTTLE)
            {
                printf(" task %d \n", my_data->thread_task);
                printf(" T1 %f, T2 %f, T3 %f, T4 %f\n", T1, T2, T3, T4);
                printf(" A1 %f, A2 %f, A3 %f, A4 %f\n", A1, A2, A3, A4);
                printf("Rate PWM %f Hz\n", (1.0/dt));
            }
        }

       

        usleep(40);
    }
    my_data->thread_status = Status::Stopped;
    printf("Stopped Thread ID :  %d\n", my_data->thread_id);
    pthread_exit(NULL);
}

