/*
 * x51.h
 *
 *  Created on: 01.04.2016
 *      Author: Ivan
 */

#ifndef X51_H_
#define X51_H_

#define ERA51
//#define ERA101
//#define ERA52

#ifdef ERA51

#define AUTOPILOT5

//эра-51-3
#define XCT0 0.09
#define YCT0 0.06
#define ZCT0 -0.005

//#define XCT0 0.102
//#define YCT0 0.030
//#define ZCT0 0


#define 	GLA 5.3
#define 	SLA 0.051
#define 	CY_MAX 3.117
#define		ALFA_MAX 	(-6.738+6.67*CY_MAX)/ToGrad		//	14,05239/ToGrad[рад]
#define 	CY_ALFA		(ToGrad/6.67)	//[1/рад]
#define		CY0			(6.738/6.67)
#define 	ALFA_MIN 	(-5.0/ToGrad)//[рад]

#define KREN_MAX (30.0/ToGrad)
#define TANG_MAX (22.0/ToGrad)
#define H_ZAD_MAX 	1500
#define H_ZAD_MIN 	-100
#define H_AVAR   	250

#define DELTA_U_MAX (107.0/ToGrad)	//надо бы синхронизировать с DELTA_AX_MIN
#define DELTA_U_MIN 0.0
#define DELTA_U_UPOR_MAX (70.0/ToGrad)

#define DELTA_G_MAX 100.0
#define DELTA_G_MIN 0.0
#define D_DELTA_G_MAX (0.05*DELTA_G_MAX*DT_GIR)
#define NS_MAX (600/60*3.7*6)//максимальная скорость вращения винта [об/с]

#define DELTA_AY_MAX 100
#define DELTA_AY_MIN 0

#define DELTA_AX_MAX 100.0
#define DELTA_AX_MIN (-30)		//DELTA_AX_MIN = -DELTA_G_MAX*tan((DELTA_U_MAX-90)/ToGrad);

#define V_MAX  25
#define V_MIN  -5
#endif

#ifdef ERA101

#define AUTOPILOT3

//эра-101-1
#define XCT0 0.160
#define YCT0 0.030
#define ZCT0 0.003

#define 	GLA 18.5
#define 	SLA 0.051
#define 	CY_MAX 3.117
#define		ALFA_MAX 	(-6.738+6.67*CY_MAX)/ToGrad		//	14,05239/ToGrad[рад]
#define 	CY_ALFA		(ToGrad/6.67)	//[1/рад]
#define		CY0			(6.738/6.67)
#define 	ALFA_MIN 	(-5.0/ToGrad)//[рад]

#define KREN_MAX (30.0/ToGrad)
#define TANG_MAX (22.0/ToGrad)
#define H_ZAD_MAX 	1500
#define H_ZAD_MIN 	-100
#define H_AVAR   	250

#define DELTA_U_MAX (110.0/ToGrad)	//надо бы синхронизировать с DELTA_AX_MIN
#define DELTA_U_MIN 0.0
#define DELTA_U_UPOR_MAX (80.0/ToGrad)

#define DELTA_G_MAX 100.0
#define DELTA_G_MIN 0.0
#define D_DELTA_G_MAX (0.4*DELTA_G_MAX*dt_gir)
#define NS_MAX (600/60*3.7*6)//максимальная скорость вращения винта [об/с]

#define DELTA_AY_MAX 100
#define DELTA_AY_MIN 0

#define DELTA_AX_MAX 100.0
#define DELTA_AX_MIN (-30)		//DELTA_AX_MIN = -DELTA_G_MAX*tan((DELTA_U_MAX-90)/ToGrad);

#define V_MAX  25
#define V_MIN  -5
#endif

#ifdef ERA52

#define AUTOPILOT3

//эра-52-1
#define XCT0 0.297
#define YCT0 0.053
#define ZCT0 0.004

#define 	GLA 18.5
#define 	SLA 0.051
#define 	CY_MAX 3.117
#define		ALFA_MAX 	(-6.738+6.67*CY_MAX)/ToGrad		//	14,05239/ToGrad[рад]
#define 	CY_ALFA		(ToGrad/6.67)	//[1/рад]
#define		CY0			(6.738/6.67)
#define 	ALFA_MIN 	(-5.0/ToGrad)//[рад]

#define KREN_MAX (30.0/ToGrad)
#define TANG_MAX (22.0/ToGrad)
#define H_ZAD_MAX 	1500
#define H_ZAD_MIN 	-100
#define H_AVAR   	250

#define DELTA_U_MAX (110.0/ToGrad)	//надо бы синхронизировать с DELTA_AX_MIN
#define DELTA_U_MIN 0.0
#define DELTA_U_UPOR_MAX (80.0/ToGrad)

#define DELTA_G_MAX 100.0
#define DELTA_G_MIN 0.0
#define D_DELTA_G_MAX (0.4*DELTA_G_MAX*dt_gir)
#define NS_MAX (600/60*3.7*6)	//максимальная скорость вращения винта [об/с]

#define DELTA_AY_MAX 100
#define DELTA_AY_MIN 0

#define DELTA_AX_MAX 100.0
#define DELTA_AX_MIN (-30)		//DELTA_AX_MIN = -DELTA_G_MAX*tan((DELTA_U_MAX-90)/ToGrad);

#define V_MAX  25
#define V_MIN  -5


#endif

#endif /* X51_H_ */
