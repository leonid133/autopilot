#ifndef PID_HPP
#define PID_HPP

#include <sys/time.h>
#include <unistd.h>

class PID {
    private:
    double P = 0.0;
    double I = 0.0;
    double D = 0.0;

    double prevErr = 0.0;
    double sumErr = 0.0;

    struct timeval tv;
    unsigned long previoustime, currenttime;
    double dt = 0.0;

    double sumErrMin = -6.0;
    double sumErrMax = 6.0;

    double lastDerivative = 0.0;
    double fCut = 0.5;

    void updateDt(double dt_)
    {
        gettimeofday(&tv,NULL);
        previoustime = currenttime;
        currenttime = 1000000 * tv.tv_sec + tv.tv_usec;
        dt = (currenttime - previoustime) / 1000000.0;
        if(dt < dt_) usleep((dt_-dt)*1000000);
        gettimeofday(&tv,NULL);
        currenttime = 1000000 * tv.tv_sec + tv.tv_usec;
        dt = (currenttime - previoustime) / 1000000.0;        
        dt = dt > 1.0 ? 0.0 : dt;
    }

    public: 
    PID (double P_ = 0.0, double I_ = 0.0, double D_ = 0.0) {
        P = P_;
        I = I_;
        D = D_;
        sumErr = 0.0;
    }

    void setPID(double P_, double I_, double D_) {
        P = P_;
        I = I_;
        D = D_;
    }

    void setP(double P_) {
        P = P_;
    }

    void setI(double I_) {
        I = I_;
    }

    void setD(double D_) {
        D = D_;
    }

    void setMaxMin(double _sumErr){
    	sumErrMax = _sumErr;
    	sumErrMin = -_sumErr;
    }

    void setFCut(double _fCut){
    	if(_fCut > 0.0)
    		fCut = _fCut;
    }

    double calc (double current, double target, double dt_) 
    {
        dt_ = (dt_<0.0) ? 0.0 : dt_;
        updateDt(dt_);

        double force = 0.0;
        double err = target - current;

        if(fabs(P) > 0.0)
            force += P * err;

        if(dt > 0.0) {
            sumErr += err;

            if(sumErr < sumErrMin) {sumErr = sumErrMin;}
            if(sumErr > sumErrMax) {sumErr = sumErrMax;}
            
            if(fabs(I) > 0.0)
                force += I * sumErr * dt;
            
            if(fabs(D) > 0.0) {
            	
            	double derivative = (err - prevErr) / dt;

            	double rc = 1.0 / (2* M_PI * fCut);
            	derivative = lastDerivative + ((dt /(rc + dt)) * (derivative - lastDerivative));
                lastDerivative = derivative;
            	
                force += D * derivative;
            	prevErr = err;
                
    		}
        }
        return force;
    }

    double calc (double current, double target) 
    {
        calc(current, target, 0.0001);
    }

    void resetIntError ( void )
    {
        sumErr = 0.0;
    }

};

#endif // PID_hpp