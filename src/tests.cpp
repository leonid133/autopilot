/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_MyTest_init = false;
#include "/home/pi/autopilot/src/tests.h"

static MyTest suite_MyTest;

static CxxTest::List Tests_MyTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_MyTest( "tests.h", 4, "MyTest", suite_MyTest, Tests_MyTest );

static class TestDescription_suite_MyTest_testMethod : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MyTest_testMethod() : CxxTest::RealTestDescription( Tests_MyTest, suiteDescription_MyTest, 7, "testMethod" ) {}
 void runTest() { suite_MyTest.testMethod(); }
} testDescription_suite_MyTest_testMethod;

static UdpTest suite_UdpTest;

static CxxTest::List Tests_UdpTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_UdpTest( "tests.h", 14, "UdpTest", suite_UdpTest, Tests_UdpTest );

static class TestDescription_suite_UdpTest_testUdpSockets : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UdpTest_testUdpSockets() : CxxTest::RealTestDescription( Tests_UdpTest, suiteDescription_UdpTest, 18, "testUdpSockets" ) {}
 void runTest() { suite_UdpTest.testUdpSockets(); }
} testDescription_suite_UdpTest_testUdpSockets;

static PidTest suite_PidTest;

static CxxTest::List Tests_PidTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_PidTest( "tests.h", 99, "PidTest", suite_PidTest, Tests_PidTest );

static class TestDescription_suite_PidTest_testP : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PidTest_testP() : CxxTest::RealTestDescription( Tests_PidTest, suiteDescription_PidTest, 102, "testP" ) {}
 void runTest() { suite_PidTest.testP(); }
} testDescription_suite_PidTest_testP;

static class TestDescription_suite_PidTest_testI : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PidTest_testI() : CxxTest::RealTestDescription( Tests_PidTest, suiteDescription_PidTest, 131, "testI" ) {}
 void runTest() { suite_PidTest.testI(); }
} testDescription_suite_PidTest_testI;

static class TestDescription_suite_PidTest_testD : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PidTest_testD() : CxxTest::RealTestDescription( Tests_PidTest, suiteDescription_PidTest, 160, "testD" ) {}
 void runTest() { suite_PidTest.testD(); }
} testDescription_suite_PidTest_testD;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
