
#ifndef UDP_HPP
#define UDP_HPP

#define NPU_PORT 7001
#define BPU_PORT 7654
#define BCASTIP "192.168.1.255"
//#define BCASTIP "192.168.0.255"
//#define BCASTIP "192.168.1.51"

#define MAXBUF 80
#define TIMEOUT_MS 100     /* Seconds between retransmits */
#define DEBUG_UDP false

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <unistd.h> /* для вызова close() для сокета */

class UdpServer
{

private:

    int sock;

    struct sockaddr_in sa;
    int status;
    socklen_t *address_len=NULL;

    int recsize;
    char rec_bufer[MAXBUF];

    void cleanRecBufer( void )
    {
    	for(int i=0; i < MAXBUF; i++)
    	{
    		rec_bufer[i] = 0x00;
    	}
    }
	
	char sendline[MAXBUF];
	
	void cleanSendline()
    {
    	for(int i=0; i < MAXBUF; i++)
        {
          sendline[i] = 0x00;
        }
    }

public:

	UdpServer( int port = 0 )
	{
		port = (port < 0) ? 0 : port;

		sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
	    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    	sa.sin_port = htons( 0 == port ? BPU_PORT : port );

    	status = bind( sock, ( struct sockaddr* )&sa, sizeof( struct sockaddr ) );

    	struct timeval tv;
		tv.tv_sec = 0;
		tv.tv_usec = TIMEOUT_MS;
		status = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv));

    	cleanRecBufer();
	};

	~UdpServer()
	{
		shutdown(sock, 2);
		close( sock );
	};

	bool checkStatus()
	{
		return  ( status < 0 ) ? false : true;
	}

	int recSize()
	{
		return recsize;
	}

	int recieve(unsigned char rx_buf[])
	{
		if ( !checkStatus() )
			return 0;
		cleanRecBufer();
		recsize = recvfrom( sock, ( void* )rec_bufer,  MAXBUF, 0, ( struct sockaddr* )&sa, address_len );
		for(int i=0; i< MAXBUF; i++)
		{
		  rx_buf[i] = (char)rec_bufer[i];
		  if(DEBUG_UDP)
		  	printf("%i bytes read : %X\n", i, rx_buf[i] );
		}
		
		return strlen(rec_bufer);
	}
	
	int send(unsigned char sendBuf[])
	{
		if ( !checkStatus() )
			return -1;

		cleanSendline();
		for(int i=0; i< MAXBUF; i++)
		{
		  sendline[i] = sendBuf[i];
		  if(DEBUG_UDP)
		  	printf("%i bytes send : %X\n", i, sendBuf[i] );
		}

		int send_status = sendto(sock, sendline, strlen(sendline), 0, (struct sockaddr *)&sa, sizeof(sa));
		return send_status;
	}

};

class UdpSocket
{

private:

    int sockfd;
    int status;
    struct sockaddr_in servaddr = {0};
    char sendline[MAXBUF];

    int yes = 1;

    void cleanSendline()
    {
    	for(int i=0; i<MAXBUF; i++)
        {
          sendline[i] = 0x00;
        }
    }

public:

	UdpSocket( int port = 0 )
	{
		port = (port < 0) ? 0 : port;
		sockfd = socket(AF_INET,SOCK_DGRAM,0);

	    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  		servaddr.sin_port = htons(0);
  		servaddr.sin_family = AF_INET;

  		status = bind(sockfd, (struct sockaddr *)&servaddr, sizeof( struct sockaddr ));

  		status = setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(int) );

	    //servaddr.sin_addr.s_addr = htonl(-1);
	    servaddr.sin_addr.s_addr = inet_addr(BCASTIP);
	    servaddr.sin_port = htons( 0 == port ? NPU_PORT : port );
	    servaddr.sin_family = AF_INET;
      
	    /*
		servaddr.sin_addr.s_addr = inet_addr("192.168.0.42");
		servaddr.sin_port = htons( 0 == port ? NPU_PORT : port );
		*/
	    cleanSendline();
	};

	~UdpSocket()
	{
		shutdown(sockfd, 2);
		close( sockfd );
	};

	bool checkStatus()
	{
		return ( status < 0 ) ? false : true;
    	//return true;
	}

	int send(unsigned char sendBuf[])
	{
		if ( !checkStatus() )
			return -1;

		cleanSendline();
		for(int i=0; i< MAXBUF; i++)
		{
		  sendline[i] = (char)sendBuf[i];
		  if(DEBUG_UDP)
		  	printf("%i bytes send : %X\n", i, sendline[i] );
		}

		int send_status = send_status = send_status = sendto(sockfd, sendline, strlen(sendline), 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
		
		return send_status;
	}
};

#endif // UDP_hpp
