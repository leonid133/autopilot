//------------------------------------------------------------------------------
// Copyright (c) 2012 by Silicon Laboratories.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Silicon Laboratories End User
// License Agreement which accompanies this distribution, and is available at
// http://developer.silabs.com/legal/version/v10/License_Agreement_v10.htm
// Original content and implementation provided by Silicon Laboratories.
//------------------------------------------------------------------------------

#ifndef __MYI2C0_H__
#define __MYI2C0_H__

// library
#include <stdbool.h>
// hal
// application
#include "gI2C0.h"

void myI2C_run(void);
void myI2C_run2(void);
void myI2C_run3(void);

extern char  I2C_RUN, I2C_READY;
extern long in[6];

void I2C0_tx_complete_handler(void);
void I2C0_rx_complete_handler(void);
void I2C0_ack_intr_handler(void);
void I2C0_stop_handler(void);
void I2C0_start_handler(void);
void I2C0_arb_lost_handler(void);
void I2C0_timer3_handler(void);

#endif // __MYI2C0_H__

//-eof--------------------------------------------------------------------------
