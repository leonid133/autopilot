// Copyright (c) 2015

#include "myUSART0.h"
#include "SI32_USART_A_Type.h"
#include <si32_device.h>

unsigned char BufferToUSART_0[NToUSART_0], r0USART_0, rkUSART_0, int_ByteUSART_0;			// Для передачи в USART0
unsigned char BufferFromUSART_0[NFromUSART_0], rFromUSART_0, wFromUSART_0, marFromUSART_0;	// Для примема с USART0
bool flTransmiterUSART_0;

//------------------------------------------------------------------------------
void OutUSART_02(unsigned int Data, unsigned char i)
{
	BufferToUSART_0[i] = (Data & 0x007f)| 0x80;
	BufferToUSART_0[i+1] = ((Data & 0x3f80) >> 7)| 0x80;
}

//------------------------------------------------------------------------------
void USART0_tx_data_req_handler(void)
{
	SI32_USART_A_clear_tx_complete_interrupt(SI32_USART_0);
	if(r0USART_0 < rkUSART_0)
		SI32_USART_A_write_data_u8(SI32_USART_0, BufferToUSART_0[r0USART_0++]);
	else
	{
		SI32_USART_A_disable_tx_data_request_interrupt(SI32_USART_0);
		flTransmiterUSART_0 = 0;			//Окончание передачи
	}
}
//------------------------------------------------------------------------------
void USART0_rx_data_req_handler(void)
{
	for(int i = 0; i < 4; i++)
	{
		if(SI32_USART_A_read_rx_fifo_count(SI32_USART_0) == 0)
				break;
		BufferFromUSART_0 [wFromUSART_0++] = SI32_USART_A_read_data_u8(SI32_USART_0);  // read character
		if(wFromUSART_0 >= NFromUSART_0)
		{
	 		wFromUSART_0 = 0;
			marFromUSART_0 = 1;
		}
	}
	SI32_USART_A_clear_rx_data_request_interrupt(SI32_USART_0);
}
