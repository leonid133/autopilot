#ifndef __MYUART0_H__
#define __MYUART0_H__

#include <stdbool.h>

#include "gUART0.h"

void UART0_tx_data_req_handler(void);
void UART0_rx_data_req_handler(void);

#define NBFM 		80
extern unsigned char BuferFromModem[]; // Для анализа с последовательного порта
extern unsigned char wBFM, rBFM, marBFM;

#define SIZE_BUFFER0	80
extern unsigned char BufferInModem[]; // Для отправки в последовательный порт
extern unsigned char r0, rk;
extern bool flTransmiter;

#endif //__MYUART0_H__
