// Copyright (c) 2015

#ifndef __MYUSART1_H__
#define __MYUSART1_H__

#include <stdbool.h>

// INCLUDE GENERATED CONTENT
#include "gUSART1.h"

void USART1_tx_data_req_handler(void);
void USART1_rx_data_req_handler(void);

#endif //__MYUSART1_H__
