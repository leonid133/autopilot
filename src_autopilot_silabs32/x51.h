/*
 * x51.h
 *
 *  Created on: 01.04.2016
 *      Author: Ivan
 */

#ifndef X51_H_
#define X51_H_

#define ERA51
//#define ERA101
//#define ERA52

#ifdef ERA51

//#define autopilot6	//эра-51 борт 3
#define autopilot3	//эра-51 борт 4
#define TYPE1


#define xct0 0.102
#define yct0 0.030
#define zct0 0

#define 	Gla 5.3
#define 	Sla 0.051
#define 	Cy_max 3.117
#define		alfa_max 	(-6.738+6.67*Cy_max)/ToGrad		//	14,05239/ToGrad[рад]
#define 	Cy_alfa		(ToGrad/6.67)	//[1/рад]
#define		Cy0			(6.738/6.67)
#define 	alfa_min 	(-5.0/ToGrad)//[рад]

#define kren_max (30.0/ToGrad)
#define tang_max (22.0/ToGrad)
#define H_zad_max 	1500
#define H_zad_min 	-100
#define H_avar   	250

#define delta_u_max (110.0/ToGrad)	//надо бы синхронизировать с delta_ax_min
#define delta_u_min 0.0
#define delta_u_upor_max (70.0/ToGrad)

#define delta_g_max 100.0
#define delta_g_min 0.0
#define d_delta_g_max (0.05*delta_g_max*dt_gir)
#define ns_max (600/60*3.7*6)//максимальная скорость вращения винта [об/с]

#define delta_ay_max 100
#define delta_ay_min 0

#define delta_ax_max 100.0
#define delta_ax_min (-30)		//delta_ax_min = -delta_g_max*tan((delta_u_max-90)/ToGrad);

#define V_max  25
#define V_min  -5
#endif

#ifdef ERA101

#define autopilot3

//эра-101-1
#define xct0 0.160
#define yct0 0.030
#define zct0 0.003

#define 	Gla 18.5
#define 	Sla 0.051
#define 	Cy_max 3.117
#define		alfa_max 	(-6.738+6.67*Cy_max)/ToGrad		//	14,05239/ToGrad[рад]
#define 	Cy_alfa		(ToGrad/6.67)	//[1/рад]
#define		Cy0			(6.738/6.67)
#define 	alfa_min 	(-5.0/ToGrad)//[рад]

#define kren_max (30.0/ToGrad)
#define tang_max (22.0/ToGrad)
#define H_zad_max 	1500
#define H_zad_min 	-100
#define H_avar   	250

#define delta_u_max (110.0/ToGrad)	//надо бы синхронизировать с delta_ax_min
#define delta_u_min 0.0
#define delta_u_upor_max (80.0/ToGrad)

#define delta_g_max 100.0
#define delta_g_min 0.0
#define d_delta_g_max (0.4*delta_g_max*dt_gir)
#define ns_max (600/60*3.7*6)//максимальная скорость вращения винта [об/с]

#define delta_ay_max 100
#define delta_ay_min 0

#define delta_ax_max 100.0
#define delta_ax_min (-30)		//delta_ax_min = -delta_g_max*tan((delta_u_max-90)/ToGrad);

#define V_max  25
#define V_min  -5
#endif

#ifdef ERA52

#define TYPE1
#define autopilot4

//эра-52-1
#define xct0 0.297
#define yct0 0.053
#define zct0 0.004

#define 	Gla 18.5
#define 	Sla 0.051
#define 	Cy_max 3.117
#define		alfa_max 	(-6.738+6.67*Cy_max)/ToGrad		//	14,05239/ToGrad[рад]
#define 	Cy_alfa		(ToGrad/6.67)	//[1/рад]
#define		Cy0			(6.738/6.67)
#define 	alfa_min 	(-5.0/ToGrad)//[рад]

#define kren_max (30.0/ToGrad)
#define tang_max (22.0/ToGrad)
#define H_zad_max 	1500
#define H_zad_min 	-100
#define H_avar   	250

#define delta_u_max (110.0/ToGrad)	//надо бы синхронизировать с delta_ax_min
#define delta_u_min 0.0
#define delta_u_upor_max (80.0/ToGrad)

#define delta_g_max 100.0
#define delta_g_min 0.0
#define d_delta_g_max (0.4*delta_g_max*dt_gir)
#define ns_max (600/60*3.7*6)	//максимальная скорость вращения винта [об/с]

#define delta_ay_max 100
#define delta_ay_min 0

#define delta_ax_max 100.0
#define delta_ax_min (-30)		//delta_ax_min = -delta_g_max*tan((delta_u_max-90)/ToGrad);

#define V_max  25
#define V_min  -5


#endif

#endif /* X51_H_ */
