// Copyright (c) 2015

#ifndef __MYSPI0_H__
#define __MYSPI0_H__

#include <stdio.h>
#include "gSPI0.h"

short ReadFromadis16265ViaSpi(unsigned int RegisterAddress, unsigned int axis);

short readSPI0(unsigned int RegisterAddress);


#endif //__MYSPI0_H__


