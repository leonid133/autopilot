#include "myEPCA0.h"
#include <stdio.h>
#include <stdint.h>
#include <SI32_EPCA_A_Type.h>
#include <si32_device.h>
#include <SI32_EPCACH_A_Type.h>
#include <SI32_PBSTD_A_Type.h>
#include <SI32_WDTIMER_A_Type.h>

#include "adis16265.h"
#include "main.h"
#include "mySPI0.h"
#include "mySPI1.h"
#include "myUSART0.h"

#define sb_u_p1 8
#define sb_g_p1 6
#define sb_u_l1 7
#define sb_g_l1 5

#define sb_u_p2 13
#define sb_g_p2 11
#define sb_u_l2 14
#define sb_g_l2 12

unsigned char nRun = 0, ph, cnt;
unsigned int tick = 0;

//==============================================================================
// 2nd Level Interrupt Handlers (Called from generated code)
//==============================================================================
void myEPCA0_enter_timer_capture_config(void)
{
   SI32_EPCA_A_select_input_clock_apb(SI32_EPCA_0);
   SI32_EPCA_A_select_input_clock_divisor(SI32_EPCA_0, 1);
   SI32_EPCA_A_write_limit(SI32_EPCA_0, LIM );

   SI32_EPCACH_A_select_output_mode_toggle(SI32_EPCA_0_CH0);
   SI32_EPCACH_A_clear_output_state(SI32_EPCA_0_CH0);
   SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH0, (0) );	//???

   // ENABLE INTERRUPTS
   NVIC_ClearPendingIRQ(EPCA0_IRQn);
   NVIC_EnableIRQ(EPCA0_IRQn);
   SI32_EPCA_A_enable_counter_overflow_interrupt(SI32_EPCA_0);
   //nRun = 0;
}

//-------------------------------------------------------------------------------------------------
void EPCA0_counter_overflow_handler(void)
{
	float tmp; 	unsigned char buf0; short stmp;
	SI32_EPCA_A_clear_counter_timer_overflow_interrupt(SI32_EPCA_0);


	if (CountRun++ < 50)
		SI32_WDTIMER_A_reset_counter(SI32_WDTIMER_0);	//Перезапустить охранный таймер

	SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH4, ((int)(LIM/2)<<1) );	//для сонара

	//------------------------------------------------------------------
	nRun = !nRun;
	if(nRun)
	{
//-----------------------------
		//SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, (1<<1) );
//-----------------------------

		if(flStop == 0)
		{
			SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_u_p1) );
			SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_u_l1) );
			SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_u_p2) );
			SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_u_l2) );
		}
		if(delta_u_p1 > delta_u_max) delta_u_p1 = delta_u_max;
		if(delta_u_p1 < delta_u_min) delta_u_p1 = delta_u_min;
		tmp = dt0_p1+(dt90_p1-dt0_p1)/(0.5*M_PI)*delta_u_p1;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH0, ((int)(MKS*tmp)<<1) );

		if(delta_u_l1 > delta_u_max) delta_u_l1 = delta_u_max;
		if(delta_u_l1 < delta_u_min) delta_u_l1 = delta_u_min;
		tmp = dt0_l1+(dt90_l1-dt0_l1)/(0.5*M_PI)*delta_u_l1;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH1, ((int)(MKS*tmp)<<1) );

		if(delta_u_p2 > delta_u_max) delta_u_p2 = delta_u_max;
		if(delta_u_p2 < delta_u_min) delta_u_p2 = delta_u_min;
		tmp = dt0_p2+(dt90_p2-dt0_p2)/(0.5*M_PI)*delta_u_p2;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH2, ((int)(MKS*tmp)<<1) );

		if(delta_u_l2 > delta_u_max) delta_u_l2 = delta_u_max;
		if(delta_u_l2 < delta_u_min) delta_u_l2 = delta_u_min;
		tmp = dt0_l2+(dt90_l2-dt0_l2)/(0.5*M_PI)*delta_u_l2;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH3, ((int)(MKS*tmp)<<1) );

		timer_tick++;	//один раз в 5 мсек, т.е 200 Гц
		liTimer_tick++;
		if (NoCommand++ > 21*FREQ_GIR)	NoCommand = 21*FREQ_GIR;
		int_Byte = int_Byte-7;	//1400 Byte/200 раз
		int_ByteUSART_0 = int_ByteUSART_0-7;	//1400 Byte/200 раз

		buf0 = ReadFromADXL345ViaSpi(0xb600, 1);
		stmp = (0x000000ff & ReadFromADXL345ViaSpi(0xb700, 1));
		stmp = (stmp << 8) | buf0;
#ifdef autopilot1
		a_dat.y = -(9.767189e-01+1.523743e-01*stmp);
#endif
#ifdef autopilot2
		a_dat.y = -6.112004e-01-1.535036e-01*stmp;
#endif
#ifdef autopilot3
		a_dat.y = -7.819684e-01-1.540824e-01*stmp;
#endif
#ifdef autopilot4	//общий
		a_dat.y = -0.8-1.535e-01*stmp;
#endif
#ifdef autopilot5
		a_dat.y = -8.886969e-01-1.532236e-01*stmp;
#endif
#ifdef autopilot6
		a_dat.y = -2.954318-1.552181e-01*stmp;
#endif
#ifdef autopilot_tar
		a_dat.y = a_dat.y+((float)stmp-a_dat.y)/FREQ_GIR*0.8;
#endif

		buf0 = ReadFromADXL345ViaSpi(0xb400, 1);
		stmp = (0x000000ff & ReadFromADXL345ViaSpi(0xb500, 1));
		stmp = (stmp << 8) | buf0;
#ifdef autopilot1
		a_dat.z = -(-9.616445e-02-1.431022e-01*stmp);	//№1
#endif
#ifdef autopilot2
		a_dat.z = -9.001900e-02+1.451920e-01*stmp;
#endif
#ifdef autopilot3
		a_dat.z = -1.101687e-01+1.462412e-01*stmp;
#endif
#ifdef autopilot4
		a_dat.z = 1.45e-01*stmp;
#endif
#ifdef autopilot5
		a_dat.z = 3.431352e-01+1.441744e-01*stmp;
#endif
#ifdef autopilot6
		a_dat.z = 7.754638e-01+1.134825e-01*stmp;
#endif

#ifdef autopilot_tar
		a_dat.z = a_dat.z+((float)stmp-a_dat.z)/FREQ_GIR*0.8;
#endif

		buf0 = ReadFromADXL345ViaSpi(0xb200, 1);
		stmp = (0x000000ff & ReadFromADXL345ViaSpi(0xb300, 1));
		stmp = (stmp << 8) | buf0;
#ifdef autopilot1
		a_dat.x = 7.618479e-02-1.451135e-01*stmp;
#endif
#ifdef autopilot2
		a_dat.x = -2.647617e-01-1.453404e-01*stmp;
#endif
#ifdef autopilot3
		a_dat.x = 9.147881e-02-1.452044e-01*stmp;
#endif
#ifdef autopilot4//общий
		a_dat.x = -1.45e-01*stmp;
#endif
#ifdef autopilot5
		a_dat.x = -8.884901e-02-1.480817e-01*stmp;
#endif
#ifdef autopilot6
		a_dat.x = 1.338283e-01-1.159690e-01*stmp;
#endif
#ifdef autopilot_tar
		a_dat.x = a_dat.x+((float)stmp-a_dat.x)/FREQ_GIR*0.8;
#endif

		  //0x2ef0 => w = -320(min)
		  //0x1110 => w = 320(max)

#ifdef TYPE1

	    SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 0x0008);
	    for(unsigned int k = 0; k < 30*k_nop; k++)	 __asm__("nop");
		short who_am_i = readSPI0( 0x8000|0x0f00 )&0x00FF;
		SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0008);
		for(unsigned int k = 0; k < 30*k_nop; k++)	 __asm__("nop");

		SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 0x0008);
		for(unsigned int k = 0; k < 30*k_nop; k++)	 __asm__("nop");
		short status =  readSPI0(0x8000|0x1E00) & 0x00FF;
		if((status & 0x02) && (who_am_i == 0x69))
		{
			 long T1 =  readSPI0(0);
			 long R1 =  readSPI0(0);
			 long R2 =  readSPI0(0);
			 long R3 =  readSPI0(0);
			 long R4 =  readSPI0(0);

			 tmp =  17.5/ToGrad*0.001*(short)((R1&0x00FF) | (R2&0xFF00));
			 if(rgStart)					w_dat.x = tmp-wx0;
			 else if(timer_tick == 1)	wx0 = tmp;
			 else						wx0 = wx0+(tmp-wx0)/FREQ_GIR*0.5;

			 tmp =  17.5/ToGrad*0.001*(short)((R2&0x00FF) | (R3&0xFF00));
			 if(rgStart)				w_dat.y = tmp-wy0;
			 else if(timer_tick == 1)	wy0 = tmp;
			 else						wy0 = wy0+(tmp-wy0)/FREQ_GIR*0.5;

			 tmp =  17.5/ToGrad*0.001*(short)((R3&0x00FF) | (R4&0xFF00));
			 if(rgStart)				w_dat.z = tmp-wz0;
			 else if(timer_tick == 1)	wz0 = tmp;
			 else						wz0 = wz0+(tmp-wz0)/FREQ_GIR*0.5;
		}
		else
		{
			if((status & 0x02) == 0) flFaultWy = 1;
			if (who_am_i != 0x69) flFaultWz = 1;
		}
		SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0008);

#else
		ReadFromadis16265ViaSpi(0x0400, Wx);
		for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
		stmp = ReadFromadis16265ViaSpi(0x0400, Wx);
		if ((stmp & 0x4000) == 0)
		{
			stmp = (stmp & 0x3fff) << 2;
			tmp = stmp*G_SCALE/4.0/ToGrad;
			if(rgStart)					w_dat.x = tmp-wx0;
			else if(timer_tick == 1)	wx0 = tmp;
			else						wx0 = wx0+(tmp-wx0)/FREQ_GIR*0.5;
		}
		else
		{
			for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
			erWx = ReadFromadis16265ViaSpi(DIAG_STAT, Wx);
			flFaultWx = 1;
		}

		ReadFromadis16265ViaSpi(0x0400, Wy);
		for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
		stmp = ReadFromadis16265ViaSpi(0x0400, Wy);
		if ((stmp & 0x4000) == 0)
		{
			stmp = (stmp & 0x3fff) << 2;
			tmp = stmp*G_SCALE/4.0/ToGrad;
			if(rgStart)					w_dat.y = tmp-wy0;
			else if(timer_tick == 1)	wy0 = tmp;
			else						wy0 = wy0+(tmp-wy0)/FREQ_GIR*0.5;
		}
		else
		{
			for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
			erWy = ReadFromadis16265ViaSpi(DIAG_STAT, Wy);
			flFaultWy = 1;
		}

		ReadFromadis16265ViaSpi(0x0400, Wz);
		for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
		stmp = ReadFromadis16265ViaSpi(0x0400, Wz);
		if ((stmp & 0x4000) == 0)
		{
			stmp = (stmp & 0x3fff) << 2;
			tmp = -stmp*G_SCALE/4.0/ToGrad;
			if(rgStart)					w_dat.z = tmp-wz0;
			else if(timer_tick == 1)	wz0 = tmp;
			else						wz0 = wz0+(tmp-wz0)/FREQ_GIR*0.5;
		}
		else
		{
			for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
			erWz = ReadFromadis16265ViaSpi(DIAG_STAT, Wz);
			flFaultWz = 1;
		}

#endif

		SI32_PBSTD_A_write_pins_low(SI32_PBSTD_2, 16);
		for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
		stmp = ReadFromSpi2();
		if( (stmp >> 14) == 0 )	iH = stmp & 0x3fff;
		else flFaultH = 1;
		SI32_PBSTD_A_write_pins_high(SI32_PBSTD_2, 16);

		SI32_PBSTD_A_write_pins_low(SI32_PBSTD_2, 32);
		for(int i = 0; i < k_nop*75; i++)	__asm__("nop");
		stmp = ReadFromSpi2();
		if( (stmp >> 14) == 0)	iq = stmp & 0x3fff;
		else flFaultV = 1;
		SI32_PBSTD_A_write_pins_high(SI32_PBSTD_2, 32);


		flRun = 1;
//-----------------------------
//		SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, (1<<1) );
//-----------------------------
	}
	else
	{
		SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_g_p1) );
		SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_g_l1) );
		SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_g_p2) );
		SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, PIN(sb_g_l2) );

		if(delta_g_p1 > delta_g_max) delta_g_p1 = delta_g_max;
		if(delta_g_p1 < delta_g_min) delta_g_p1 = delta_g_min;
		tmp = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(delta_g_max-delta_g_min)*delta_g_p1;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH0, ((int)(MKS*tmp)<<1) );

		if(delta_g_l1 > delta_g_max) delta_g_l1 = delta_g_max;
		if(delta_g_l1 < delta_g_min) delta_g_l1 = delta_g_min;
		tmp = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(delta_g_max-delta_g_min)*delta_g_l1;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH1, ((int)(MKS*tmp)<<1) );

		if(delta_g_p2 > delta_g_max) delta_g_p2 = delta_g_max;
		if(delta_g_p2 < delta_g_min) delta_g_p2 = delta_g_min;
		tmp = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(delta_g_max-delta_g_min)*delta_g_p2;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH2, ((int)(MKS*tmp)<<1) );

		if(delta_g_l2 > delta_g_max) delta_g_l2 = delta_g_max;
		if(delta_g_l2 < delta_g_min) delta_g_l2 = delta_g_min;
		tmp = tdelta_g_min+(tdelta_g_max-tdelta_g_min)/(delta_g_max-delta_g_min)*delta_g_l2;
		SI32_EPCACH_A_write_ccapv(SI32_EPCA_0_CH3, ((int)(MKS*tmp)<<1) );
	}
	//сонар
	if((ph == 1) && (SI32_PBSTD_A_read_pin(SI32_PBSTD_2,12)) )
	{
		cnt++;
	}

	if(tick > 19)
	{
		if(ph == 1)
		{
			tmp = 8.416666-2.083333e-01*cnt;//-0.2*cnt + 6.4;
			if (tmp < 0) tmp = 0;
			H_son = H_son+(tmp-H_son)*0.9/FREQ_GIR*20*4;
			SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 1);//disabled
		}
		else if(ph == 2)
			SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 1);//enabled
		else if(ph == 3)	//этот импульс успешно пропадает, потому что после ресета сонар не может ответить
		{
			SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, (1<<2) );
			for(unsigned int k = 0; k < 30*k_nop; k++)
				__asm__("nop");
			SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, (1<<2) );
		}
		else//здесь запрос на нормальный ответ
		{
			SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, (1<<2) );
			for(unsigned int k = 0; k < 30*k_nop; k++)
				__asm__("nop");
			SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, (1<<2) );
			ph = cnt = 0;
		}
		ph++;
		tick = 0;
	}
	tick++;
}

//-----------------------------------------------------------------------------
void EPCA0_EPCACH0_capture_compare_handler(void)
{
	SI32_EPCA_A_clear_channel_n_capture_compare_interrupt(SI32_EPCA_0, 0);
	if(nRun)	SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_u_p1) );
	else		SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_g_p1) );
}

//-----------------------------------------------------------------------------
void EPCA0_EPCACH1_capture_compare_handler(void)
{
	SI32_EPCA_A_clear_channel_n_capture_compare_interrupt(SI32_EPCA_0, 1);
	if(nRun)SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_u_l1) );
	else	SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_g_l1) );
}

//-----------------------------------------------------------------------------
void EPCA0_EPCACH2_capture_compare_handler(void)
{
	SI32_EPCA_A_clear_channel_n_capture_compare_interrupt(SI32_EPCA_0, 2);
	if(nRun)	SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_u_p2) );
	else		SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_g_p2) );
}

//-----------------------------------------------------------------------------
void EPCA0_EPCACH3_capture_compare_handler(void)
{
	SI32_EPCA_A_clear_channel_n_capture_compare_interrupt(SI32_EPCA_0, 3);
	if(nRun)	SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_u_l2) );
	else		SI32_PBSTD_A_write_pins_high(SI32_PBSTD_1, PIN(sb_g_l2) );
}

//-----------------------------------------------------------------------------
void EPCA0_EPCACH4_capture_compare_handler(void)
{
	SI32_EPCA_A_clear_channel_n_capture_compare_interrupt(SI32_EPCA_0, 4);
	if((ph == 1) && (SI32_PBSTD_A_read_pin(SI32_PBSTD_2,12)))
	{
		cnt++;
	}
}
/*
//-----------------------------------------------------------------------------
void EPCA0_EPCACH5_capture_compare_handler(void)
{
	SI32_EPCA_A_clear_channel_n_capture_compare_interrupt(SI32_EPCA_0, 5);
}*/
