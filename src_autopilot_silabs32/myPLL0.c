// Copyright (c) 2015

#include "myPLL0.h"
#include <SI32_PLL_A_Type.h>
#include <si32_device.h>
#include <stdint.h>
#include "gCPU.h"
//==============================================================================
// User Functions
//==============================================================================
void PLLInit(void)
{
	NVIC_ClearPendingIRQ(PLL0_IRQn);
	  NVIC_DisableIRQ(PLL0_IRQn);

	  SI32_PLL_A_select_reference_clock_source_ext0osc(SI32_PLL_0);
	  SI32_PLL_A_set_numerator(SI32_PLL_0,33);
	  SI32_PLL_A_set_denominator(SI32_PLL_0, 8);
	  pll_lock_freq_to_hz_(45619200, false);
}
void pll_lock_freq_to_hz_(uint32_t freq, bool phase_lock)
{
  // Calculate initial mode guess
  uint8_t mode = 4;
  if (freq < 35000000)
  {
    mode = 0;
  }
  else if (freq < 45000000)
  {
    mode = 1;
  }
  else if (freq < 55000000)
  {
    mode = 2;
  }
  else if (freq < 70000000)
  {
    mode = 3;
  }
  else
  {
    mode = 4;
  }

  // This loop will poll forever if the PLL is unable to lock based on
  // the current configuration.
  while(SI32_PLL_A_is_locked(SI32_PLL_0) == false)
  {
    // Set range
    SI32_PLL_A_select_disable_dco_output(SI32_PLL_0);
    SI32_PLL_A_set_frequency_adjuster_value(SI32_PLL_0, 0xFFF);
    SI32_PLL_A_set_output_frequency_range(SI32_PLL_0, mode);

    // Place PLL in either frequency lock or phase lock mode
    if (phase_lock)
    {
      SI32_PLL_A_select_dco_phase_lock_mode(SI32_PLL_0);
    }
    else
    {
      SI32_PLL_A_select_dco_frequency_lock_mode(SI32_PLL_0);
    }

    // Poll until the range saturates or the PLL locks
    while (!((SI32_PLL_A_is_locked(SI32_PLL_0) == true)
             || (SI32_PLL_A_is_saturation_low_interrupt_pending(SI32_PLL_0) == true)
             || (SI32_PLL_A_is_saturation_high_interrupt_pending(SI32_PLL_0) == true)));

    // If the PLL saturated in the current range, switch to the next range
    if (SI32_PLL_A_is_locked(SI32_PLL_0) == false)
    {
      if (SI32_PLL_A_is_saturation_low_interrupt_pending(SI32_PLL_0))
      {
        // Check the range boundaries
        if (mode < 4)
        {
           ++mode;
        }
      }
      else
      {
        // Check the range boundaries
        if (mode > 0)
        {
          --mode;
        }
      }
    }
  }
}
