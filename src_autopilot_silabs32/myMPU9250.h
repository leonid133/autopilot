/*
 * myMPU9250.h
 *
 *  Created on: 11.07.2016
 *      Author: SainQuake
 */
#ifndef MYMPU9250_H_
#define MYMPU9250_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include "gSPI0.h"
#include "main.h"

#define ERROR_MPU9250_FAIL 		1
#define ERROR_MAGNETOMETER_FAIL 2
#define ERROR_DATA_0			3
#define ERROR_DEV_BY_ZERO		4
#define OK 						0

//extern float mag[3];
//extern float yaw;
extern struct vektor rsk_dat, mag, mag_max, mag_min;
void request_mag();
char read_mag();
unsigned int WriteReg( uint8_t WriteAddr, uint8_t WriteData);
void init_mag();

#endif /* MYMPU9250_H_ */
