// Copyright (c) 2015

#include <stdio.h>
#include "myUART0.h"
#include "SI32_UART_A_Type.h"
#include <si32_device.h>
#include "main.h"

#include <SI32_WDTIMER_A_Type.h>

//-------------------------------------------------------------------------------------
unsigned char BuferFromModem [NBFM]; // Для анализа с последовательного порта
unsigned char wBFM, rBFM, marBFM;

unsigned char BufferInModem[SIZE_BUFFER0]; // Для отправки в последовательный порт
unsigned char r0, rk;
bool flTransmiter;

//-------------------------------------------------------------------------------------------------
void UART0_tx_data_req_handler(void)
{
	if(r0 < rk)
		SI32_UART_A_write_data_u8(SI32_UART_0, BufferInModem[r0++]);
	else
	{
		SI32_UART_A_disable_tx_data_request_interrupt(SI32_UART_0);
		flTransmiter = 0;			//Окончание передачи
	}
	SI32_UART_A_clear_tx_complete_interrupt(SI32_UART_0);
}

//---------------------------------------------------------------------------------------
void UART0_rx_data_req_handler(void)
{
	int size = SI32_UART_A_read_rx_fifo_count(SI32_UART_0);
	for(int i = 0; i < size; i++)
	{
		BuferFromModem [wBFM++] = SI32_UART_A_read_data_u8(SI32_UART_0);  // read character
		if(wBFM >= NBFM)
		{
			wBFM = 0;
			marBFM = 1;
		}
	}
	SI32_UART_A_clear_rx_data_request_interrupt(SI32_UART_0);
}

//---------------------------------------------------------------------------------------
/*void UART0_rx_data_req_handler(void)
{
	for(int i = 0; i < 4; i++)
	{
		if(SI32_UART_A_read_rx_fifo_count(SI32_UART_0) == 0)
				break;
		BuferFromModem [wBFM++] = SI32_UART_A_read_data_u8(SI32_UART_0);  // read character
		if(wBFM >= NBFM)
		{
			wBFM = 0;
			marBFM = 1;
		}
	}
	SI32_UART_A_clear_rx_data_request_interrupt(SI32_UART_0);
}*/

