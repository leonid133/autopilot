#include "mySPI2.h"

#include <stdio.h>
#include <stdint.h>
#include <si32_device.h>
#include <si32_SPI_A_Type.h>
#include <SI32_PBHD_A_Type.h>
#include <SI32_PBSTD_A_Type.h>

short ReadFromSpi2(void)
{
	SI32_SPI_A_flush_rx_fifo(SI32_SPI_2);
	SI32_SPI_A_flush_tx_fifo(SI32_SPI_2);
	SI32_SPI_A_clear_all_interrupts(SI32_SPI_2);
	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_2, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_2) < 1)
		;
	return SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_2);
}
