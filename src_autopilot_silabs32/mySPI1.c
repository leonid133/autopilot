#include "mySPI1.h"

#include <stdio.h>
#include <stdint.h>
#include <si32_device.h>
#include <si32_SPI_A_Type.h>

#define READ 0x8000

//-------------------------------------------------------------------------------------------------
int ReadFromADXL345ViaSpi(unsigned int RegisterAddress, unsigned char NumberofRegisters)
{
	SI32_SPI_A_flush_rx_fifo(SI32_SPI_1);
	SI32_SPI_A_flush_tx_fifo(SI32_SPI_1);

	SI32_SPI_A_clear_all_interrupts(SI32_SPI_1);

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_1, RegisterAddress);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_1) < 1)
		;
	return SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_1);
}

//-------------------------------------------------------------------------------------------------
void SPI1Init()
{
	unsigned char buf;
	//buf = ReadFromADXL345ViaSpi( (READ|0x0000) , 2);
	//printf("XL345_DEVID =  %X", buf);
	//buf = ReadFromADXL345ViaSpi( (READ|0x3000) , 1);
	//printf(" INT_SOURCE =  %d", buf);
//	buf = ReadFromADXL345ViaSpi(0x2c0c, 1);		//частота опроса 400 Гц
	//printf(" BW_RATE =  %d\n", buf);
	buf = ReadFromADXL345ViaSpi(0x2D0A, 1);		//не спать
	//printf(" PWR_CTL =  %d\n", buf);
	buf = ReadFromADXL345ViaSpi(0x3103, 1);		//выравнивание данных по младшему биту
	//buf = ReadFromADXL345ViaSpi( (READ|0x3100) , 1);
	//printf(" DATA_FORMAT =  %d\n", buf);
}
