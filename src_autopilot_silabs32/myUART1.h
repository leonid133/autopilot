#ifndef __MYUART1_H__
#define __MYUART1_H__

#include <stdbool.h>

// INCLUDE GENERATED CONTENT
#include "gUART1.h"

#define NS 	75
extern unsigned char mess_buf[], r_buf, w_buf, mar_buf;		// Для анализа посылки GPS

void UART1_rx_data_req_handler(void);

#endif //__MYUART1_H__
