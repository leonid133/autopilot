#include "mySPI0.h"

#include <stdio.h>
#include <stdint.h>
#include <si32_device.h>
#include <si32_SPI_A_Type.h>
#include <SI32_PBHD_A_Type.h>
#include <SI32_PBSTD_A_Type.h>

#include "adis16265.h"
#include "main.h"
#include <sim3c1xx.h>

//-------------------------------------------------------------------------------------------------
short ReadFromadis16265ViaSpi(unsigned int RegisterAddress, unsigned int axis)
{
	uint16_t	ReceiveData;

	SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, axis);
	for(unsigned int k = 0; k < 100*k_nop; k++)	__asm__("nop");

	SI32_SPI_A_flush_rx_fifo(SI32_SPI_0);
	SI32_SPI_A_flush_tx_fifo(SI32_SPI_0);

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, RegisterAddress);

	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1)
				;
	ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);

	SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, axis);
	for(unsigned int k = 0; k < 100*k_nop; k++)	__asm__("nop");
	return ReceiveData;
}

//-------------------------------------------------------------------------------------------------
short readSPI0(unsigned int RegisterAddress)
{
	SI32_SPI_A_flush_rx_fifo(SI32_SPI_0);
	SI32_SPI_A_flush_tx_fifo(SI32_SPI_0);

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, RegisterAddress);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1)
		;
	return SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
}


