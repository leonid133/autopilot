// Copyright (c) 2015

#ifndef __MYSPI1_H__
#define __MYSPI1_H__

#include <stdbool.h>
#include <stdio.h>
// INCLUDE GENERATED CONTENT
#include "gSPI1.h"

int ReadFromADXL345ViaSpi(unsigned int RegisterAddress, unsigned char NumberofRegisters);
void SPI1Init(void);
void GetAccel(void);

#endif //__MYSPI1_H__

