#ifndef MAIN_H_
#define MAIN_H_
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "x51.h"

#ifdef DEBUG
#define k_nop 1
#else
#define k_nop 3
#endif

#define HIGH 1
#define LOW 0
#define PIN(num) (1<<num)
#define READ 0x8000
#define MULTI 0x4000

#define SYS_CLK 		49766400 					//	тактовая частота
#define FREQ_OSN 		400                  	// основная частота

#define FREQ_GIR  	(FREQ_OSN/2)      		// Частота опроса датчиков - 200Гц
#define dt_gir 		(1.0/FREQ_GIR)

#define Rz  6380.//???
#define G 9.8065
#define ToGrad 57.2957795130823
#define M_PI   3.14159265358979323846
#define 	ro 0.125

extern float delta_g_p1, delta_g_l1, delta_g_p2, delta_g_l2, delta_u_p1, delta_u_l1, delta_u_p2, delta_u_l2;
extern float dt0_p1, dt90_p1, dt0_l1, dt90_l1, dt0_p2, dt90_p2, dt0_l2, dt90_l2;
extern float tdelta_g_max, tdelta_g_min_infly, tdelta_g_min;
extern float wx0, wy0, wz0;
extern unsigned int timer_tick, NoCommand, iH, iq;
extern long int liTimer_tick;     //абсолютное
extern int int_Byte;
extern bool flRun;
extern unsigned char CountRun;
extern signed char rgStart;
extern bool flStop, flFaultH, flFaultV, flFaultWx, flFaultWy, flFaultWz;
extern signed char erWx, erWy, erWz;
extern float H_son;

//----------------------------------------------------------------------------------------
struct vektor
{
   float x, y, z;
};
extern struct vektor w_dat, a_dat, rsk_dat;//, mag, mag_max, mag_min;
float calc_tang_zad(void);
void Landing(void);

#endif /* MAIN_H_ */
