// Copyright (c) 2015

#include "myUSART1.h"
#include "SI32_USART_A_Type.h"
#include <si32_device.h>

void USART1_tx_data_req_handler(void)
{
	SI32_USART_A_clear_tx_complete_interrupt(SI32_USART_1);
}
void USART1_rx_data_req_handler(void)
{
	SI32_USART_A_clear_rx_data_request_interrupt(SI32_USART_0);
}
