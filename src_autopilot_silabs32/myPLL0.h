// Copyright (c) 2015

#ifndef __MYPLL0_H__
#define __MYPLL0_H__

#include <stdint.h>
#include <stdbool.h>

void PLLInit(void);
void pll_lock_freq_to_hz_(uint32_t freq, bool phase_lock);

#endif //__MYPLL0_H__
