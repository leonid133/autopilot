//-------------------------------------------------------------------------------
// Copyright (c) 2012 by Silicon Laboratories
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Silicon Laboratories End User
// License Agreement which accompanies this distribution, and is available at
// http://developer.silabs.com/legal/version/v10/License_Agreement_v10.htm
//
//
// Original content and implementation provided by Silicon Laboratories
//-------------------------------------------------------------------------------

//==============================================================================
// WARNING:
//
// This file is auto-generated by AppBuilder and should not be modified.
// Any hand modifications will be lost if the project is regenerated.
//==============================================================================

#include "gSPI0.h"
#include "gCPU.h"

// Include peripheral access modules used in this file
#include <SI32_SPI_A_Type.h>
#include <si32_device.h>

//==============================================================================
// Configuration Functions
//==============================================================================
void SPI0_enter_default_mode_from_reset(void)
{
  SI32_SPI_A_select_master_mode(SI32_SPI_0);
  SI32_SPI_A_select_clock_idle_high(SI32_SPI_0);
  SI32_SPI_A_select_data_change_first_edge(SI32_SPI_0);
  SI32_SPI_A_set_data_length(SI32_SPI_0, 16);
  SI32_SPI_A_set_clock_divisor(SI32_SPI_0, 26);
  SI32_SPI_A_enable_module(SI32_SPI_0);
}

