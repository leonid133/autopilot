//-------------------------------------------------------------------------------
// Copyright (c) 2012 by Silicon Laboratories
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Silicon Laboratories End User
// License Agreement which accompanies this distribution, and is available at
// http://developer.silabs.com/legal/version/v10/License_Agreement_v10.htm
//
//
// Original content and implementation provided by Silicon Laboratories
//-------------------------------------------------------------------------------

//==============================================================================
// WARNING:
//
// This file is auto-generated by AppBuilder and should not be modified.
// Any hand modifications will be lost if the project is regenerated.
//==============================================================================

#include "gUART1.h"
#include "gCPU.h"

// Include peripheral access modules used in this file
#include <SI32_UART_A_Type.h>
#include <si32_device.h>

//==============================================================================
// 2nd Level Interrupt Handlers
//==============================================================================
extern void UART1_rx_data_req_handler(void);

//==============================================================================
// 1st Level Interrupt Handlers
//==============================================================================
void UART1_IRQHandler()
{
  if (SI32_UART_A_is_rx_data_request_interrupt_pending(SI32_UART_1)
    && SI32_UART_A_is_rx_data_request_interrupt_enabled(SI32_UART_1))
  {
    UART1_rx_data_req_handler();
  }
}

//==============================================================================
// Configuration Functions
//==============================================================================
void UART1_enter_default_mode_from_reset(void)
{
  SI32_UART_A_set_rx_baudrate(SI32_UART_1, SystemPeripheralClock/2/9600 - 1);
  SI32_UART_A_set_tx_baudrate(SI32_UART_1, SystemPeripheralClock/2/9600 - 1);
  SI32_UART_A_select_rx_parity(SI32_UART_1, 2);
  SI32_UART_A_select_tx_parity(SI32_UART_1, 2);
  SI32_UART_A_select_rx_fifo_threshold_4(SI32_UART_1);
  SI32_UART_A_select_tx_fifo_threshold_for_request_to_4(SI32_UART_1);
  SI32_UART_A_enable_rx(SI32_UART_1);
  SI32_UART_A_enable_rx_data_request_interrupt(SI32_UART_1);
  NVIC_ClearPendingIRQ(UART1_IRQn);
  NVIC_EnableIRQ(UART1_IRQn);
}

