//-------------------------------------------------------------------------------
// Copyright (c) 2012 by Silicon Laboratories
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Silicon Laboratories End User
// License Agreement which accompanies this distribution, and is available at
// http://developer.silabs.com/legal/version/v10/License_Agreement_v10.htm
//
//
// Original content and implementation provided by Silicon Laboratories
//-------------------------------------------------------------------------------

//==============================================================================
// WARNING:
//
// This file is auto-generated by AppBuilder and should not be modified.
// Any hand modifications will be lost if the project is regenerated.
//==============================================================================

#include "gEXTOSC0.h"
#include "gCPU.h"
#include <stdint.h>
#include <SI32_EXTOSC_A_Type.h>
#include <si32_device.h>

//==============================================================================
// Configuration Functions
//==============================================================================

void EXTOSC0_enter_default_mode()
{
  // Configure FREQCN field
  SI32_EXTOSC_A_set_frequency_control_range(SI32_EXTOSC_0, 7);

  // Set OSCMD field
  SI32_EXTOSC_A_select_oscillator_mode_crystal(SI32_EXTOSC_0);

  // Wait at least 1 ms
  uint32_t now = get_msTicks();
  while (get_msTicks() <= now + 1);

  // Poll on OSCVLDF flag to determine if oscillator is running and stable
  while (!((SI32_EXTOSC_A_is_oscillator_valid(SI32_EXTOSC_0) == true)));
}


//==============================================================================
// Support Functions
//==============================================================================

