

#ifndef __MYSPI2_H__
#define __MYSPI2_H__

#include <stdbool.h>

// INCLUDE GENERATED CONTENT
#include "gSPI2.h"

short ReadFromSpi2(void);

#endif //__MYSPI2_H__
