// Copyright (c) 2015

#ifndef __MYUSART0_H__
#define __MYUSART0_H__

#include <stdbool.h>

// INCLUDE GENERATED CONTENT
#include "gUSART0.h"

void USART0_tx_data_req_handler(void);
void USART0_rx_data_req_handler(void);

#define NToUSART_0 	20
extern unsigned char BufferToUSART_0[], r0USART_0, rkUSART_0, int_ByteUSART_0;	// Для передачи в USART0

#define NFromUSART_0 	20
extern unsigned char BufferFromUSART_0[], rFromUSART_0, wFromUSART_0, marFromUSART_0;	// Для примема с USART0
extern bool flTransmiterUSART_0;

void OutUSART_02(unsigned int Data, unsigned char i);

#endif //__MYUSART0_H__
