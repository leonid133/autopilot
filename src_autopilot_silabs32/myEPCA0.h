#ifndef __MYEPCA0_H__
#define __MYEPCA0_H__

// INCLUDE GENERATED CONTENT
#include "gEPCA0.h"
void myEPCA0_enter_timer_capture_config(void);
void EPCA0_EPCACH0_capture_compare_handler(void);
void EPCA0_EPCACH1_capture_compare_handler(void);
void EPCA0_EPCACH2_capture_compare_handler(void);
void EPCA0_EPCACH3_capture_compare_handler(void);
void EPCA0_EPCACH4_capture_compare_handler(void);
/*void EPCA0_EPCACH5_capture_compare_handler(void);*/
void EPCA0_counter_overflow_handler(void);

#define MKS 25000000	//сколько в одном секунде тиков EPCA
#define LIM 62166//55467

#endif //__MYEPCA0_H__
