/*
 * adis16265.h
 *
 *  Created on: 13.08.2015
 *      Author: binny
 */

#ifndef ADIS16265_H_
#define ADIS16265_H_

//unsigned int Wx = 0x0020, Wy = 0x0010, Wz = 0x0008;

#define Wx 0x0020
#define Wy 0x0010
#define Wz 0x0008

#define ANG_SCALE 0.03663
#define G_SCALE 0.07326

#define FLASH_CNT =		0x0000
#define SUPPLY_OUT = 	0x0200
#define GYRO_OUT  		0x0400
#define AUX_ADC = 		0x0A00
#define TEMP_OUT = 		0x0C00
#define ANGL_OUT  		0x0E00
#define GYRO_OFF  		0x1400
#define GYRO_SCALE = 	0x1600
#define ALM_MAG1 = 		0x2000
#define ALM_MAG2 = 		0x2200
#define ALM_SMPL1 = 	0x2400
#define ALM_SMPL2 = 	0x2600
#define ALM_CTRL = 		0x2800
#define AUX_DAC = 		0x3000
#define GPIO_CTRL = 	0x3200
#define MSC_CTRL = 		0x3400
#define SMPL_PRD = 		0x3600
#define SENS_AVG  		0x3800
#define SLP_CNT = 		0x3A00
#define DIAG_STAT	 	0x3C00
#define GLOB_CMD  		0x3E00
#define LOT_ID1 = 		0x5200
#define LOT_ID2 = 		0x5400
#define PROD_ID = 		0x5600
#define SERIAL_NUM = 	0x5800


#endif /* ADIS16265_H_ */
