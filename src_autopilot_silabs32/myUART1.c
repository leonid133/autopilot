// Copyright (c) 2015

#include "myUART1.h"
#include "SI32_UART_A_Type.h"
#include <si32_device.h>

unsigned char mess_buf[NS], r_buf, w_buf, mar_buf;		// Для анализа посылки GPS
//unsigned char mess[NS]="$GPRMC,064600,A,5551.8573,N,04906.4012,E,251.315,312.7,200500,11.5,E*40";
//unsigned char mess[]="$GPRMC,100508,A,5550.9399,N,04906.4464,E,1.640,343.1,170703,11.6,E*4E";

//-------------------------------------------------------------------------------------------------
void UART1_rx_data_req_handler(void)
{
	for(int i = 0; i < 4; i++)
	{
		if(SI32_UART_A_read_rx_fifo_count(SI32_UART_1) == 0)
				break;
		mess_buf [w_buf++] = SI32_UART_A_read_data_u8(SI32_UART_1);  // read character
		if(w_buf >= NS)
		{
			w_buf = 0;
			mar_buf = 1;
		}
	}
	SI32_UART_A_clear_rx_data_request_interrupt(SI32_UART_1);
}
