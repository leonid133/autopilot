#include <math.h>
#include <stdlib.h>
#include <float.h>
#include "main.h"
#include "myUSART0.h"
#include "myMPU9250.h"

#include <SI32_PBCFG_A_Type.h>
#include <SI32_PBSTD_A_Type.h>
#include "SI32_UART_A_Type.h"
#include "SI32_USART_A_Type.h"
#include <si32_device.h>

#include <SI32_SARADC_A_Type.h>
#include <SI32_WDTIMER_A_Type.h>
#include <SI32_RSTSRC_A_Type.h>
#include <SI32_EPCA_A_Type.h>
#include <SI32_EPCACH_A_Type.h>
#include <SI32_I2C_A_Type.h>

#include "myEPCA0.h"
#include "myUART0.h"
#include "myUART1.h"
#include "myUSART0.h"
#include "mySPI0.h"
#include "mySPI1.h"
#include "mySPI2.h"
#include "myPLL0.h"
#include "myI2C0.h"

#include "adxl350.h"
#include "adis16265.h"


float V_bum = 25;

unsigned long DecodeLatOrLon(unsigned char *Array, unsigned char nomer);
unsigned long int LatFly, LonFly, LatFly_tmp, LonFly_tmp, LatMar[128], LonMar[128];
int H_Mar[128];
unsigned char Vz_Mar[128], n_, i_mar;
float cos_Lat0;

//время(сек) = timer_tick*FREQ
unsigned int timer_tick;   //относительное (счётчик)
long int liTimer_tick, liTimer_tick_start, liTimer_tick_rsk_m, liTimer_tick_rsk_m1;     //абсолютное
long int liTimer_tick_GPS; //прихода последней GPS посылки
unsigned long time_t;		//POSIX - время

unsigned int iH, iq;
float H0, H_dat, H_son, H_dat_pr, Vy_dat, H_filtr, H_filtr_pr, Vy_filtr, H_start, delta_ro = 1, Vz, Vz_tmp, V_dat, kV, q_dat;
float koors_zad, koors, koors_tmp, koors_zad1;
float  H_zad, H_max;
float V_zad = 15, ax_zad, ax_filtr, Vv_dat, Vxv_dat, Vzv_dat;
signed char Vz_zad = 1;
float kren_zad, kren_zad_filtr;
unsigned int NoCommand;
char KrenKam, UgolKam, KrenKam_zad, UgolKam_zad;
float wx0, wy0, wz0;

float delta_g_p1, delta_g_l1, delta_g_p2, delta_g_l2, delta_u_p1, delta_u_l1, delta_u_p2, delta_u_l2, delta_u;
float g_p1, g_l1, g_p2, g_l2, u_p1, u_l1, u_p2, u_l2;
float delta_g, delta_u, delta_u_upor;
float rsk_gir, tang_gir, kren_gir, teta_and_alfa, rsk_m, rsk_gps, alfa, kren_dat;
float kren_din, tang_din;
float cos_kren_gir, sin_kren_gir, sin_tang_gir, cos_tang_gir;
float drsk_gir, dtang_gir, dkren_gir, int_dtang, int_dtang0, int_dkoors;
float int_dV, int_dkren, int_dVy;
float kren_zad_effect, slip, wx_zad, wz_zad, Vy_zad;

unsigned char mess [NS], r; // Для анализа посылки GPS
//unsigned char mess[NS]="$GPRMC,064600,A,5551.8573,N,04906.4012,E,251.315,312.7,200500,11.5,E*40";
//unsigned char mess[]="$GPRMC,100508,A,5550.9399,N,04906.4464,E,1.640,343.1,170703,11.6,E*4E";

unsigned char CountRun, flAnswer;
bool flRun   //сработал таймер основного цикла
, flOtkazRK
, flKoord
, flTele
, flNewGPS
, flStop
, flCommandStop
, flAutoStop
, flStoika
, flOutTang
, flOutKren
, flOutH
, flOutU
, flFaultH
, flFaultV
, flFaultWx
, flFaultWy
, flFaultWz
, flOutLand
, flOtkazRK05;

signed char erWx, erWy, erWz;


signed char rgfly;  /*  0 - полуавтомат(крен, угол рыска, высота, скорость)
                  		1 - управление джойстиком (крен, угол рыска, газ, угол наклона мотогондол)
                  		2 - автономный полет (с автоматическим взлет-посадкой)
                  		3 - прошивка маршрута
                  		4 - возврат
						5 - спасение */

signed char rgStart;         	/* -1 - тест силовых установок
									0 - стоим
									1 - тест
									2 - замедленный тест
									3 - ждем координаты
									4 - системная готовность
                                    5 - старт!
                                 	6 - летим       */

signed char rgTest;					/*	0 - нет теста
										1 - 255 этапы теста			*/

float rasst_toch_mar;

void OutModem1(unsigned char Data, unsigned char i);
void OutModem2(unsigned int Data, unsigned char i);
void OutModem4(unsigned long int Data, unsigned char i);

float dt0_p1 = 1.75e-3, dt90_p1 = 1.15e-3, dt0_l1 = 1.15e-3, dt90_l1 = 1.75e-3
			 , dt0_p2 = 1.75e-3, dt90_p2 = 1.15e-3, dt0_l2 = 1.15e-3, dt90_l2 = 1.75e-3;//пока
float alfa0, i_tang, p_tang, d_tang, d2_tang, k1_tang, k2_tang, g_tang, v_tang, tang_zad, tang_zad_filtr;
float kren0, i_kren, p_kren, d_kren, d2_kren, k1_kren, k2_kren, kren_zad_wy;
float i_rsk, p_rsk, d_rsk, d2_rsk, k1_rsk, k2_rsk;
float i_ay, p_ay, d_ay, k1_ay, dH_max;
float i_ax, p_ax, d_ax, k1_ax, U_end;
float delta_ax, delta_ay, delta_u_zad, delta_g_zad, delta_tang, delta_rsk, delta_kren;
float v_rsk, g_kren, g_rsk, v_kren, kl, gaz0, tdelta_g_max = 2.0e-3, tdelta_g_min_infly = 1.0e-3, tdelta_g_min = 1.0e-3, w_max, delta_max;
char rgAnswer;

int int_Byte;

//---------------------------------------------------------------------------
unsigned int FromEarth2(unsigned char *mess, unsigned char i);
unsigned int FromEarth2(unsigned char mess[], unsigned char i)
{
   return((unsigned int)mess[i]& 0x7f)+((mess[i+1]& 0x7f)<< 7);
}

struct vektor w_dat, w_dat_pr, e_dat, e_zad, a_dat, a_gir, V_gir, S_gir, l, v1, v2, v3, a_dat_ct, a_dat1, vVz;

//Векторное прои-ние----------------------------------------------------------
struct vektor vp (struct vektor u, struct vektor v);
struct vektor vp (struct vektor u, struct vektor v)
{
	struct vektor a;
	a.x = u.y*v.z-u.z*v.y;
	a.y = u.z*v.x-u.x*v.z;
	a.z = u.x*v.y-u.y*v.x;
	return a;
}
//------------------------------------------------------------------------------
void OutModem1(unsigned char Data, unsigned char i)
{
	BufferInModem[i] = Data | 0x80;
}

//------------------------------------------------------------------------------
void OutModem2(unsigned int Data, unsigned char i)
{
	BufferInModem[i] = (Data & 0x007f)| 0x80;
	BufferInModem[i+1] = ((Data & 0x3f80) >> 7)| 0x80;
}

//------------------------------------------------------------------------------
void OutModem4(unsigned long int Data, unsigned char i)
{
	BufferInModem[i] = (Data & 0x0000007f)| 0x80;
	BufferInModem[i+1] = ((Data & 0x3f80) >> 7) | 0x80;
	BufferInModem[i+2] = ((Data & 0x1fc000) >> 14) | 0x80;
	BufferInModem[i+3] = ((Data & 0xfe00000)>> 21) | 0x80;
}

unsigned char last_reset_source, count_reset_source = 0;

//------------------------------------------------------------------------------
float U_AKB, I_AKB, C_AKB;
unsigned char fl_AKB, last_reset_source_AKB, count_reset_source_AKB = 0, command_AKB;

unsigned char nByteFromUSART_0, KontrSummaFromUSART_0, NPackageFromUSART_0, codeFromUSART_0[NFromUSART_0];
unsigned char ggg, ggg_save;
char error_mag;
//------------------------------------------------------------------------------
int main(void)
{
	//Для работы с последовательным портом "Модем"
	unsigned char RK_code[66], nByte = 0, KontrSumma = 0, NPackage;
	float angle, l_km, tmp, tmp1, /*rsk_gps, */napr_vetv_mar, napr_toch_mar, dz, dx, dz_pr, dx_pr; //Для автоуправления
	int i;
	float rsk_zad_, wy_zad;	//наверное можно заменить на tmp, tmp1

	bool flPoint;
	unsigned char ValidGPS, i_gps, i_comma;
	char tmpGPS[10], tmpX[3], KontrSummaFromUART_1;
	unsigned long temp_koord;
	last_reset_source = SI32_RSTSRC_A_get_last_reset_source(SI32_RSTSRC_0);
	count_reset_source++;
//----------
	ggg_save = ggg;
//--------

	enter_default_mode_from_reset();// Enter the default operating mode for this application
    myEPCA0_enter_timer_capture_config();//timer

/*    SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x010);
    SI32_PBSTD_A_write_pins_low(SI32_PBSTD_3, 0x000);*/
    SI32_PBSTD_A_write_pins_low(SI32_PBSTD_1, (1<<1) );//выбор акселерометра

//    SI32_PBSTD_A_write_pins_high(SI32_PBSTD_2, 32);//выбирается датчик скорости

    SPI1Init();
    NVIC_SetPriority(I2C0_IRQn, 12);
    NVIC_SetPriority(UART1_IRQn, 11);
    NVIC_SetPriority(UART0_IRQn, 10);
    NVIC_SetPriority(USART0_IRQn, 2);
    NVIC_SetPriority(EPCA0_IRQn, 1);
    //init_mag();

#ifdef TYPE1
	SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 0x0008);
	for(unsigned int k = 0; k < k_nop*200; k++) __asm__("nop");
	short gyro_set = readSPI0( 0x1144 );
	SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0008);
	for(unsigned int k = 0; k < k_nop*200; k++) __asm__("nop");

    SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 0x0008);
    for(unsigned int k = 0; k < k_nop*200; k++) __asm__("nop");
    short acc_set = readSPI0( 0x1031 );
    SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0008);
#endif

    SI32_WDTIMER_A_reset_counter(SI32_WDTIMER_0);
    SI32_WDTIMER_A_start_counter(SI32_WDTIMER_0);

    if ((last_reset_source == SI32_WDT_RESET) && (rgStart > 4))
    {
		rgAnswer = 5;
    }
    else
	{
		delta_u_zad = u_p1 = u_l1 = u_p2 = u_l2 = delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = delta_u = 0.5*M_PI;
		delta_u_upor = delta_u_upor_max;

		a_dat.y = -G;

  		l.x = xct0;
        l.y = yct0;
        l.z = zct0;
	}

    while(1)	//Управление движением -----------------------------------------------------------
	{
    	if(I2C_READY == 1)
    	{
//------------
ggg = 1;
//------------
//			long ttt = get_msTicks();
    		I2C_READY = 0;

    		tmp = 0.1/ToGrad*((in[5] << 8) | in[4])+M_PI/2;
    		rsk_m = 2.0*M_PI-tmp;
			while (rsk_m > 2.0*M_PI)	rsk_m -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
			while (rsk_m < 0)	rsk_m += 2.0*M_PI;
			//if((rgfly != 5) /*&& (flStoika == 0)*/)
			/*{
				tmp = rsk_gir;
				while (tmp > M_PI)	tmp -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
				while (tmp < -M_PI)	tmp += 2.0*M_PI;

				tmp1 = rsk_m;
				while (tmp1 > M_PI)	tmp1 -= 2.0*M_PI; //загоняем в диапазон от -M_PI до M_PI
				while (tmp1 < -M_PI)tmp1 += 2.0*M_PI;

				if(fabs(rsk_m-rsk_gir) < fabs(tmp1-tmp))	rsk_gir = 0.95*rsk_gir+0.05*rsk_m;
				else						            	rsk_gir = 0.95*tmp+0.05*tmp1;
				while (rsk_gir > 2.0*M_PI)  rsk_gir -= 2.0*M_PI;//загоняем в диапазон от 0 до 2*M_PI
				while (rsk_gir < 0)			rsk_gir += 2.0*M_PI;
			}*/
    	}
		if (liTimer_tick-liTimer_tick_rsk_m > 0.2*FREQ_GIR) //опрос датчика магнитного рыска
		{
			//------------
			ggg = 2;
			//------------
			liTimer_tick_rsk_m = liTimer_tick;

			myI2C_run3();
		}

/*		if (liTimer_tick-liTimer_tick_rsk_m1 > 0.1*FREQ_GIR) 			//выносной магнитометр
		{
			//------------
			ggg = 2;
			//------------
			liTimer_tick_rsk_m1 = liTimer_tick;
			error_mag = read_mag();
			if(error_mag == 0)
			{
				rsk_dat.x;
/*				case ERROR_MPU9250_FAIL: 		printf("mpu dead\n"); break;
				case ERROR_MAGNETOMETER_FAIL: 	printf("mag dead\n"); break;
				case ERROR_DATA_0: 				printf("data no valid\n"); break;
				case OK:						printf("	yaw=%f\n" , yaw );*/
			/*}
			request_mag();
		}*/

		CountRun = 0;
		if(rBFM < (wBFM+marBFM*NBFM))//связь с землей
		{
			if ((BuferFromModem[rBFM] & 0xC0) == 0x40)
			{
				nByte = 0;
				KontrSumma = 0;
				NPackage = BuferFromModem[rBFM] & 0x3f;
				NoCommand = 0;
				flOtkazRK = 0;
				flOtkazRK05 = 0;
			}

			if (nByte > 65)
				nByte = 65;
			RK_code[nByte] = BuferFromModem[rBFM] & 0x7f;
			KontrSumma = KontrSumma^RK_code[nByte++];

			if ( (nByte == 3) && (KontrSumma == 0) )
			{
				//------------
				ggg = 3;
				//------------
				if (NPackage == 2)
				{
					if(RK_code[1] == 1)  //Телеметрия
					{
//						flCommand = 1;
					}
					else if((RK_code[1] == 2) && (rgfly != 5))  // ручное управление
					{
						rgAnswer = 5;
						rgfly = 1;
						if(rgStart > 5)
						{
							delta_g_zad = gaz0;           //переводим в спасение
							delta_u_zad = 0.5*M_PI;
//							kren_zad = 0;
						}
					}
					if(RK_code[1] == 3)  //stop
					{
						if(flStoika)
						{
							flCommandStop = 1;
							flStop = 1;
						}
						else
						{
							flCommandStop = 1;
							Landing();
						}
						rgAnswer = 5;
					}
//		      		else if ((RK_code[1] == 4) && (rgStart == 4))  //Старт
					else if ((RK_code[1] == 4) && (rgStart > 2) && (rgStart < 6))  //Старт (условие rgStart > 3) на время отладки
					{
						rgAnswer = 3;
						rgStart = 5;
						n_ = 1;
     				}
					else if((RK_code[1] == 5) && (flStoika == 0) &&  (rgfly != 5))  //автомат
					{
						if(rgfly == 1)
						{
							if (H_dat < 100) 	H_zad = 100;
							else			 	H_zad = H_dat+Vy_filtr;
							if(H_filtr < H_zad)	H_max = H_filtr;//летим снизу
							else				H_max = H_zad;

							//V_zad = 15;
							V_zad = V_dat;
							int_dV = delta_g*cos(delta_u);

							if(fabs(1-kV) > FLT_EPSILON)	int_dVy = delta_g*sin(delta_u)/(1-kV);
							else							int_dVy = delta_g*sin(delta_u);
							if (int_dVy < (gaz0-d_ay*dH_max))  	int_dVy = gaz0-d_ay*dH_max;
							if (int_dVy > delta_ay_max)  		int_dVy = delta_ay_max;
						}
						rgAnswer = 5;
						rgfly = 0;
					}
					else if ((RK_code[1] == 8) && (rgStart < 5))  //Замедленный тест рулей + Инициализация
					{
						rgStart = 2;
						rgTest++;
						rgAnswer = 5;
					}
					else if ((RK_code[1] == 10) && (flStoika == 0) &&  (rgfly != 5))  //Возврат
					{
						rgAnswer = 5;
						rgfly = 4;
						n_ = i_mar-1;

						if (H_dat < 100) 	H_zad = 100;
						else			 	H_zad = H_dat+Vy_filtr;
						if(H_filtr < H_zad)	H_max = H_filtr;//летим снизу
						else				H_max = H_zad;

						Vz_zad = 25;
						if(rgfly == 1)
						{
							int_dV = delta_g*cos(delta_u);
							if(fabs(1-kV) > FLT_EPSILON)		int_dVy = delta_g*sin(delta_u)/(1-kV);
							else								int_dVy = delta_g*sin(delta_u);
							if (int_dVy < (gaz0-d_ay*dH_max))  	int_dVy = gaz0-d_ay*dH_max;
							if (int_dVy > delta_ay_max)  		int_dVy = delta_ay_max;
						}
					}
					else if ((RK_code[1] == 11) && (rgStart < 5))  //Тест силовых установок
					{
						rgStart = -1;
						rgTest++;
						rgAnswer = 5;
					}
					else if ((RK_code[1] == 12) && (rgStart < 5))
					{
						flStoika = 1;
						rgAnswer = 5;
					}
					else if (RK_code[1] == 13)
					{
						flStoika = 0;
						rgAnswer = 5;
					}
					else if (RK_code[1] == 14)
					{
						command_AKB = 2;
					}
					else if (RK_code[1] == 15)
					{
						command_AKB = 3;
					}
				}
				else if((NPackage == 4) && (flStoika == 0) &&  (rgfly != 5))	         //автономный полет
				{
					//------------
					ggg = 5;
					//------------

						rgfly = 2;
						rgAnswer = 5;
						n_ = RK_code[1];
						H_zad = H_Mar[n_];
						V_zad = Vz_Mar[n_];
						if(H_filtr < H_zad) H_max = H_filtr;	//летим снизу
						else				H_max = H_zad;

						if(rgfly == 1)
						{
							int_dV = delta_g*cos(delta_u);
							if(fabs(1-kV) > FLT_EPSILON)	int_dVy = delta_g*sin(delta_u)/(1-kV);
							else					int_dVy = delta_g*sin(delta_u);
							if (int_dVy < (gaz0-d_ay*dH_max))  	int_dVy = gaz0-d_ay*dH_max;
							if (int_dVy > delta_ay_max)  		int_dVy = delta_ay_max;
						}
				}
				else if(NPackage == 8)	//Крен камеры зад.
				{
					rgAnswer = 5;
					KrenKam_zad = RK_code[1];
					KrenKam_zad = KrenKam_zad-60;
				}
				else if(NPackage == 9)	//Угол камеры к горизонту зад.UgolKam_zad
				{
					rgAnswer = 5;
					UgolKam_zad = RK_code[1];
				}
			}	//if ( nByte == 3 )
			else if(NPackage == 1 && (nByte == 4) && (KontrSumma == 0))
			{
				//------------
				ggg = 6;
				//------------

				if (rgfly == 0)
				{
					i = RK_code[2];
					H_zad = -100+0.1*(RK_code[1]+(i << 7));
					if(H_zad < H_zad_min)  		H_zad = H_zad_min;
					else if(H_zad > H_zad_max)  H_zad = H_zad_max;
					if(H_filtr < H_zad)			H_max = H_filtr;//летим снизу
					else						H_max = H_zad;
				}
				rgAnswer = 5;
			}
			else if((NPackage == 3) && (nByte == 4) && (KontrSumma == 0))
			{
				//------------
				ggg = 7;
				//------------

				rgAnswer = 5;
				if  ((rgfly == 0) || (rgfly == 1) || (rgfly == 5))
				{
					i = RK_code[2];
					koors_zad = RK_code[1]+(i << 7);
				}
			}
			else if((NPackage == 5) && (nByte == 4) && (KontrSumma == 0))
			{
				//------------
				ggg = 8;
				//------------

				if ((rgfly == 0) || (rgfly == 1) || (rgfly == 5))
				{
					i = RK_code[2];
					i = RK_code[1]+(i << 7);
					kren_zad = ((float)i-8000)/2500;
					if(kren_zad > kren_max) kren_zad = kren_max;
					if(kren_zad < -kren_max)kren_zad = -kren_max;
				}
			}
        	else if((NPackage == 6) && (nByte == 4) && (KontrSumma == 0))	//V_zad
        	{
    			//------------
    			ggg = 9;
    			//------------

        		if (rgfly == 0)
        		{
        			i = RK_code[2];
        			i = RK_code[1]+(i << 7);
        			V_zad = 0.1*i-10;
					if (V_zad > V_max) V_zad = V_max;
					if (V_zad < V_min) V_zad = V_min;
        		}
				else if(flStoika)
				{
					i = RK_code[2];
					i = RK_code[1]+(i << 7);
					V_dat = 0.1*i-10;
					if (V_dat > V_max) V_dat = V_max;
					if (V_dat < V_min) V_dat = V_min;
				}
        	}
			else if((NPackage == 7) && (nByte == 4) && (KontrSumma == 0))
			{
				//------------
				ggg = 10;
				//------------

				if ((rgfly == 1) || (rgfly == 5))
				{
					i = RK_code[2];
					i = RK_code[1]+(i << 7);
					delta_g_zad = 0.1*i;
					if (delta_g_zad < delta_g_min) delta_g_zad = delta_g_min;
					if (delta_g_zad > delta_g_max) delta_g_zad = delta_g_max;
				}
			}
			else if((NPackage == 10) && (nByte == 7) && (time_t == 0) && (KontrSumma == 0))	//POSIX время
			{
				//------------
				ggg = 11;
				//------------

				time_t = RK_code[5];
				time_t = (time_t << 7)+RK_code[4];
				time_t = (time_t << 7)+RK_code[3];
				time_t = (time_t << 7)+RK_code[2];
				time_t = (time_t << 7)+RK_code[1];
			}
			else if((NPackage == 12) && (nByte == 4) && (KontrSumma == 0))
			{
				//------------
				ggg = 12;
				//------------

				if(flStoika)
				{
					i = RK_code[2];
					i = RK_code[1]+(i << 7);
					tang_zad = ((float)i-8168)/1300;
					if (tang_zad < -tang_max) tang_zad = -tang_max;
					if (tang_zad > tang_max) tang_zad = tang_max;
				}
			}
			else if((NPackage == 13) && (nByte == 4) && (KontrSumma == 0))
			{
				//------------
				ggg = 13;
				//------------

				if ((rgfly == 1) || (rgfly == 5))
				{
					i = RK_code[2];
					i = (i << 7)+RK_code[1];
					delta_u_zad = 0.1*i/ToGrad;
					if (delta_u_zad < delta_u_min) delta_u_zad = delta_u_min;
					if (delta_u_zad > delta_u_max) delta_u_zad = delta_u_max;
				}
			}
			//Координаты ППМ------------------------------------------------------
			else if ((NPackage == 11) && (nByte == 13) && (rgStart < 5) && (KontrSumma == 0))
			{
				//------------
				ggg = 14;
				//------------

				n_ = RK_code[11];
				if(n_ > i_mar+1) i_mar = n_+1;
				LatMar[n_] = DecodeLatOrLon(RK_code, 1);
				LonMar[n_] = DecodeLatOrLon(RK_code, 5);
				if (n_ == 0)
				{
				  	cos_Lat0 = LatMar[0];
				  	cos_Lat0 = cos((cos_Lat0/60/10000-90)/ToGrad);
				}

        		H_Mar[n_] = (int)RK_code[9]*50-1000;
        		if(H_Mar[n_] < H_zad_min)		H_Mar[n_]= H_zad_min;
        		else if(H_Mar[n_] > H_zad_max)	H_Mar[n_] = H_zad_max;
        		Vz_Mar[n_] = (int)RK_code[10];

				rgAnswer = 4;
			}
			else if((NPackage == 14) && (nByte == 50) && (KontrSumma == 0))
			{
				//------------
				ggg = 15;
				//------------

				dt0_p1 = 1e-5*FromEarth2(RK_code, 1);
				dt90_p1 = 1e-5*FromEarth2(RK_code, 3);
				dt0_l1 = 1e-5*FromEarth2(RK_code, 5);
				dt90_l1 = 1e-5*FromEarth2(RK_code, 7);
				dt0_p2 = 1e-5*FromEarth2(RK_code, 9);
				dt90_p2 = 1e-5*FromEarth2(RK_code, 11);
				dt0_l2 = 1e-5*FromEarth2(RK_code, 13);
				dt90_l2 = 1e-5*FromEarth2(RK_code, 15);

				alfa0 = ((float)FromEarth2(RK_code, 17)-8168)/1300;
				i_tang = 1e-2*FromEarth2(RK_code, 19);
				p_tang = 1e-2*FromEarth2(RK_code, 23);
				d_tang = 1e-2*FromEarth2(RK_code, 25);
				kl = 1e-2*FromEarth2(RK_code, 27);

				kren0 = ((float)FromEarth2(RK_code, 29)-8168)/1300;
				i_kren = 1e-2*FromEarth2(RK_code, 31);
				p_kren = 1e-2*FromEarth2(RK_code, 33);
				d_kren = 1e-2*FromEarth2(RK_code, 35);
				gaz0 = 1e-2*FromEarth2(RK_code, 37);

				i_rsk = 1e-2*FromEarth2(RK_code, 39);
				p_rsk = 1e-2*FromEarth2(RK_code, 41);
				d_rsk = 1e-2*FromEarth2(RK_code, 43);
				w_max = 1e-2*FromEarth2(RK_code, 45);
				delta_max = 1e-2*FromEarth2(RK_code, 47);
				rgAnswer = 1;
			}
			else if((NPackage == 15) && (nByte == 50) && (KontrSumma == 0))
			{
				//------------
				ggg = 16;
				//------------

				i_ay = 1e-2*FromEarth2(RK_code, 1);
				p_ay = 1e-2*FromEarth2(RK_code, 3);

				tdelta_g_min_infly = 1e-5*FromEarth2(RK_code, 7);
				i_ax = 1e-2*FromEarth2(RK_code, 9);
				p_ax = 1e-2*FromEarth2(RK_code, 11);
				tdelta_g_max = 1e-5*FromEarth2(RK_code, 13);

				v_rsk = 1e-2*FromEarth2(RK_code, 17);
				g_kren = 1e-2*FromEarth2(RK_code, 19);
				g_rsk = 1e-2*FromEarth2(RK_code, 21);
				v_kren = 1e-2*FromEarth2(RK_code, 23);
				g_tang = 1e-2*FromEarth2(RK_code, 25);
				v_tang = 1e-2*FromEarth2(RK_code, 27);
				int_dtang0 = -80.0+1e-2*FromEarth2(RK_code, 29);
				dH_max = 1e-2*FromEarth2(RK_code, 31);
				d2_tang = 1e-2*FromEarth2(RK_code, 33)-80;
				d2_kren = 1e-2*FromEarth2(RK_code, 35)-80;
				d2_rsk = 1e-2*FromEarth2(RK_code, 37)-80;

				if (d_tang > 0.01)	k1_tang = p_tang/d_tang;
				else   				k1_tang = p_tang/0.01;

				if (d_kren > 0.01)	k1_kren = p_kren/d_kren;
				else   				k1_kren = p_kren/0.01;

				if (d_rsk > 0.01)	k1_rsk = p_rsk/d_rsk;
				else   				k1_rsk = p_rsk/0.01;

				if (d2_tang > 0.01)	k2_tang = d_tang/d2_tang;
				else   				k2_tang = d_tang/0.01;

				if (d2_kren > 0.01)	k2_kren = d_kren/d2_kren;
				else   				k2_kren = d_kren/0.01;

				if (d2_rsk > 0.01)	k2_rsk = d_rsk/d2_rsk;
				else   				k2_rsk = d_rsk/0.01;

				kren_zad_wy = 1e-2*FromEarth2(RK_code, 39);
				d_ay = 1e-2*FromEarth2(RK_code, 41);
				d_ax = 1e-2*FromEarth2(RK_code, 43);
				U_end = 1e-2*FromEarth2(RK_code, 45);

				if (d_ay > 0.01)	k1_ay = p_ay/d_ay;
				else   				k1_ay = p_ay/0.01;

				if (d_ax > 0.01)	k1_ax = p_ax/d_ax;
				else   				k1_ax = p_ax/0.01;
				rgAnswer = 2;
			}
			rBFM++;
			if(rBFM >= NBFM)
			{
				rBFM = 0;
				marBFM = 0;
			}
		}

		//-----------------------------------------------------------------------------------
		if(flNewGPS)	//автономное управление
		{
			//------------
			ggg = 17;
			//------------

			flNewGPS = 0;
			if((rgStart > 4)/* && ((rgfly == 2) || (rgfly == 4))*/)
			{
					dz = LonMar[n_];
					dz = 0.1856*(dz - LonFly)*cos_Lat0;
					dx = LatMar[n_];
					dx = 0.1856*(dx-LatFly);

					if ( (fabs(dx) > FLT_EPSILON) || (fabs(dz) > FLT_EPSILON) )
						napr_toch_mar = atan2(dz, dx);

					rasst_toch_mar = sqrt(dz*dz+dx*dx);
					if ((rgfly == 2) || (rgfly == 4)) //пока входе не будет стоять условие if(rgfly == 2) || (rgfly == 4)
					{
						if (rasst_toch_mar < 100)
						{
							if (n_ < (i_mar-1))
							{
								n_++;
								rgAnswer = 5;
								V_zad = Vz_Mar[n_];
								H_zad = H_Mar[n_];

								if(H_filtr < H_zad)	H_max = H_filtr; //летим снизу
								else				H_max = H_zad;
							}
							else
							{
								flAutoStop = 1;
								Landing();
							}
						}
					}//пока
					dz_pr = 0.1856*cos_Lat0*LonMar[n_]-0.1856*cos_Lat0*LonMar[n_-1];
					dx_pr = 0.1856*LatMar[n_]-0.1856*LatMar[n_-1];
					if ( (fabs(dx_pr) > FLT_EPSILON) || (fabs(dz_pr) > FLT_EPSILON) )
						napr_vetv_mar = atan2(dz_pr, dx_pr);

					angle = napr_toch_mar-napr_vetv_mar;
					if (angle > M_PI) angle = angle-2.0*M_PI;
					if(angle < -M_PI) angle = 2.0*M_PI+angle;

					l_km = 10*V_dat;
					if (l_km < 80) l_km = 80;
					if(fabs(angle) > M_PI/2) l_km = -l_km;
					tmp = rasst_toch_mar*sin(angle);   //tmp = otkl_ot_mar
					dz = tmp*cos(napr_vetv_mar)+l_km*sin(napr_vetv_mar);	//dz = z_toch_pricel
					dx = -tmp*sin(napr_vetv_mar)+l_km*cos(napr_vetv_mar);	//dx = x_toch_pricel
					if ((rgfly == 2) || (rgfly == 4))
					{
						if ((fabs(dx) > FLT_EPSILON) || (fabs(dz) > FLT_EPSILON))
							koors_zad = ToGrad*atan2(dz, dx);
					}
					else
					{
						if ((fabs(dx) > FLT_EPSILON) || (fabs(dz) > FLT_EPSILON))
							koors_zad1 = ToGrad*atan2(dz, dx);
					}
			}
		}
		else if (liTimer_tick-liTimer_tick_GPS > 2*FREQ_GIR) //Отсутствие координат
		{
			flKoord = 0;
			liTimer_tick_GPS = liTimer_tick;
			flTele = 1;
		}

		//Расшифровка посылки GPS
		if (r_buf < w_buf+mar_buf*NS)
		{
			//------------
			ggg = 18;
			//------------

			if(mess_buf[r_buf] == '$')
			{
				r = 0;

				ValidGPS = 1;
				KontrSummaFromUART_1 = 0;
				i_comma = 0;
			}

			if(ValidGPS)
			{
				mess[r] = mess_buf[r_buf];
				if (r == 1)
				{
					if(mess[r] == 'G')	ValidGPS = 1;
					else				ValidGPS = 0;
				}
				else if (r == 2)
				{
					if(mess[r] == 'P')	ValidGPS = 1;
					else				ValidGPS = 0;
				}
				else if (r == 3)
				{
					if(mess[r] == 'R')	ValidGPS = 1;
					else				ValidGPS = 0;
				}
				else if (r == 4)
				{
					if(mess[r] == 'M')	ValidGPS = 1;
					else				ValidGPS = 0;
				}
				else if (r == 5)
				{
					if(mess[r] == 'C')
					{
						ValidGPS = 1;
						i_comma = 0;
					}
					else	ValidGPS = 0;
				}

				if(mess[r] == '*')
				{
					ValidGPS = 2;
					i_gps = 0;
					sprintf(tmpX, "%X", KontrSummaFromUART_1);
				}
				else if((mess[r] != '$') && (ValidGPS != 2))
					KontrSummaFromUART_1 = KontrSummaFromUART_1^mess[r];

				if(mess[r] == ',')
				{
					i_comma++;
					i_gps = 0;
					flPoint = 0;
				}
				else if(i_comma == 2)
				{
					if(mess[r] == 'A')	ValidGPS = 1;
					else				ValidGPS = 0;
				}
				else if (i_comma == 3)                //Latitude
				{
					if(mess[r] == '.')
					{
							flPoint = 1;
							i_gps = 0;
					}
					else if (flPoint == 0)			//Целая часть
					{
						tmpGPS[i_gps++] = mess[r];
						if(i_gps == 2)
						{
							tmpGPS[i_gps] = 0;
							temp_koord = atoi(tmpGPS);
							temp_koord = 60UL*10000*temp_koord;
						}
						else if(i_gps == 4)
						{
							tmpGPS[0] = tmpGPS[1] = '0';
							tmpGPS[i_gps] = 0;
							temp_koord = temp_koord+10000UL*atoi(tmpGPS);
						}
					}
					else					//Дробная часть
					{
						tmpGPS[i_gps++] = mess[r];
						tmpGPS[i_gps] = 0;
					}
				}
				else if (i_comma == 4)
				{
					temp_koord = temp_koord+atoi(tmpGPS);//lLatFly = 55UL*60*10000+50UL*10000+8680;
					if (mess[r] == 'S')   				//знак Latitude
						LatFly_tmp = 54000000UL-temp_koord;		//90UL*60*10000-koord;
					else if(mess[r] == 'N')
						LatFly_tmp = 54000000UL+temp_koord;	//90UL*60*10000+koord;
					else
						ValidGPS = 0;
				}
				else if (i_comma == 5)                //Longitude
				{
					if(mess[r] == '.')
					{
						flPoint = 1;
						i_gps = 0;
					}
					else if (flPoint == 0)			//Целая часть
					{
						tmpGPS[i_gps++] = mess[r];
						if(i_gps == 3)
						{
							tmpGPS[i_gps] = 0;
							temp_koord = atoi(tmpGPS);
							temp_koord = 60UL*10000*temp_koord;
						}
						else if(i_gps == 5)
						{
							tmpGPS[0] = tmpGPS[1] = tmpGPS[2] = '0';
							tmpGPS[i_gps] = 0;
							temp_koord = temp_koord+10000UL*atoi(tmpGPS);
						}
					}
					else					//Дробная часть
					{
						tmpGPS[i_gps++] = mess[r];
						tmpGPS[i_gps] = 0;
					}
				}
				else if (i_comma == 6)
				{
					temp_koord = temp_koord+atoi(tmpGPS);//lLonFly = 49UL*60*10000+6UL*10000+3760;

					if (mess[r] == 'W')   //знак Longitude
						LonFly_tmp = 108000000UL-temp_koord;		//180UL*60*10000-koord;
					else if(mess[r] == 'E')
						LonFly_tmp = temp_koord+108000000UL;	//180UL*60*10000;
					else
						ValidGPS = 0;
				}
				else if (i_comma == 7)	//скорость в узлах
				{
					if(mess[r] == '.')//после запятой не считываю. Почему?
					{
						flPoint = 1;
						Vz_tmp = 1.852*atoi(tmpGPS)/3.6;   //??? Преобразовать из узлов в м/с
					}
					else if(flPoint == 0)
					{
						tmpGPS[i_gps++] = mess[r];
						tmpGPS[i_gps] = 0;
					}
				}
				else if (i_comma == 8)	//курс в градусах
				{
					if(mess[r] == '.')//после запятой не считываю. Почему?
					{
						flPoint = 1;
						koors_tmp = 1.0/ToGrad*atoi(tmpGPS);
					}
					else if(flPoint == 0)
					{
						tmpGPS[i_gps++] = mess[r];
						tmpGPS[i_gps] = 0;
					}
				}
				else if (ValidGPS == 2)	//контрольная сумма
				{
					tmpGPS[i_gps++] = mess[r];
					if (i_gps == 3)
					{
						if((tmpX[0] == tmpGPS[1]) && (tmpX[1] == tmpGPS[2]))
						{
							LatFly = LatFly_tmp;
							LonFly = LonFly_tmp;
							Vz = Vz_tmp;
							koors = koors_tmp;
/*
							if((Vz > 5) && (V_dat > 5) /*&& ((rgfly == 2) || (rgfly == 4))*//*)
							{
								tmp = rsk_gir;
								while (tmp > M_PI)	tmp -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
								while (tmp < -M_PI)	tmp += 2.0*M_PI;

								tmp1 = rsk_gps = 2.0*M_PI-koors;
				            	while (tmp1 > M_PI)	tmp1 -= 2.0*M_PI; //загоняем в диапазон от -M_PI до M_PI
				            	while (tmp1 < -M_PI)tmp1 += 2.0*M_PI;

				            	if(fabs(rsk_gps-rsk_gir) < fabs(tmp1-tmp))	rsk_gir = 0.9*rsk_gir+0.1*rsk_gps;
				            	else						            	rsk_gir = 0.9*tmp+0.1*tmp1;
								while (rsk_gir > 2.0*M_PI)  rsk_gir -= 2.0*M_PI;//загоняем в диапазон от 0 до 2*M_PI
								while (rsk_gir < 0)			rsk_gir += 2.0*M_PI;
							}*/
							flTele = flNewGPS = flKoord = 1;
							liTimer_tick_GPS = liTimer_tick;

							if (rgStart == 3)
							{
								rgStart = 4;
								rgAnswer = 5;
							}
						}
						else
						{
							flKoord = 0;
							liTimer_tick_GPS = liTimer_tick;
							flTele = 1;
						}
					}
				}
				if (i_gps > 9)
					i_gps = 9;
			}
			r++;
			if(r >= NS)	r = NS-1;
			r_buf++;
			if(r_buf >= NS)
			{
				r_buf = 0;
				mar_buf = 0;
			}
		}

		//Работа---------------------------------------------------------------
		if(flRun)
		{
			ggg = 18;

			flRun = 0;
//----------------------------------------------------------------
//			SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, (1<<1) );
//----------------------------------------------------------------

	      //Находим высоту------------------------------------------------
			tmp = 9.950297e+03-6.774441e-01*iH-H0;  //tmp = H_tmp
			H_filtr = H_filtr+(tmp-H_filtr)/FREQ_GIR*0.8;
 			Vy_filtr = Vy_filtr+((H_filtr-H_filtr_pr)*FREQ_GIR-Vy_filtr)/FREQ_GIR*0.8;
 			H_filtr_pr = H_filtr;

			H_dat = H_dat+(tmp-H_dat)/FREQ_GIR/0.3;
 			Vy_dat = Vy_dat+((H_dat-H_dat_pr)*FREQ_GIR-Vy_dat)/FREQ_GIR/0.3;
 			H_dat_pr = H_dat;

 			delta_ro = 1-0.000095*H_filtr+3.05e-09*H_filtr*H_filtr;
			if (delta_ro < 0.5)
				delta_ro = 0.5;

			//скорость
			V_bum = sqrt(2.0*Gla/(Cy_max*ro*delta_ro*Sla));

//			tmp = -7.680266e+02+9.376469e-02*iq;  //tmp = q_tmp
			tmp = -7.742599e+02+9.411205e-02*iq;  //tmp = q_tmp
			q_dat = q_dat+(tmp-q_dat)/FREQ_GIR*0.8;
			tmp = q_dat*2./0.125/delta_ro;
			if(flStoika == 0)
			{
	     		if (tmp > 0) 	V_dat = sqrt(tmp);
	     		else			V_dat = -sqrt(fabs(tmp));
			}

			if((rgStart == 0) && (timer_tick > 10.0*FREQ_GIR))//первоначальная инициализация
			{
				timer_tick = 0;
				rgStart = 1;
				rgAnswer = 5;
				H0 = H_filtr;
  				S_gir.y = V_gir.y = H_filtr = H_filtr_pr = Vy_filtr = H_dat = H_dat_pr = Vy_dat = ax_filtr = 0;

  				rsk_gir = rsk_m;
				if(fabs(a_dat.y) > FLT_EPSILON)
					kren_dat = kren_din = kren_gir = -atan(a_dat.z/a_dat.y);

				tmp = a_dat.y*cos(kren_gir)-a_dat.z*sin(kren_gir);
				if(fabs(tmp) > FLT_EPSILON)
					teta_and_alfa = tang_din = tang_gir = atan(a_dat.x/tmp);
			}
			//тест рулей----------------------------------------------------
			if (rgStart == 1)
			{
				if (timer_tick < FREQ_GIR)
	  				delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = 0.5*M_PI;
				else if (timer_tick < 2.0*FREQ_GIR)
	  				delta_u_p1 = delta_u_max;
				else if (timer_tick < 3.0*FREQ_GIR)
	  				delta_u_p1 = delta_u_min;
				else if (timer_tick < 4.0*FREQ_GIR)
				{
					delta_u_p1 = 0.5*M_PI;
					delta_u_l1 = delta_u_max;
				}
				else if (timer_tick < 5.0*FREQ_GIR)
	  				delta_u_l1 = delta_u_min;
				else if (timer_tick < 6.0*FREQ_GIR)
				{
					delta_u_l1 = 0.5*M_PI;
					delta_u_p2 = delta_u_max;
				}
				else if (timer_tick < 7.0*FREQ_GIR)
	  				delta_u_p2 = delta_u_min;
				else if (timer_tick < 8.0*FREQ_GIR)
				{
					delta_u_p2 = 0.5*M_PI;
					delta_u_l2 = delta_u_max;
				}
				else if (timer_tick < 9.0*FREQ_GIR)
	  				delta_u_l2 = delta_u_min;
				else
				{
					delta_u_l2 = 0.5*M_PI;
					timer_tick = 0;

					if (flKoord)
						rgStart = 4;		//системная готовность
					else
						rgStart = 3;		//ждем кординаты
					rgAnswer = 5;
				}
			}
			//тест силовых установок --------------------------------------------
			else if (rgStart == -1)
			{
#ifdef ERA51
tmp = 80;
#else
tmp = 100;
#endif
				if (rgTest == 1)
				{
	  				delta_g_p1 = delta_g_l1 = delta_g_p2 = delta_g_l2 = delta_g_min;
	  				delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = 0.5*M_PI;
				}
				else if (rgTest == 2)
				{
					delta_g_p1 += d_delta_g_max;
					if(delta_g_p1 > tmp)	delta_g_p1 = tmp;
				}
				else if (rgTest == 3)
	  				delta_g_p1 = delta_g_min;
				else if (rgTest == 4)
				{
/*					if(delta_g_l1 < 0.6*delta_g_max)	delta_g_l1 += d_delta_g_max;
					else 							delta_g_l1 = delta_g_max;
					delta_g_l1 += d_delta_g_max;
					if(delta_g_l1 > delta_g_max)	delta_g_l1 = delta_g_max;*/
					delta_g_l1 += d_delta_g_max;
					if(delta_g_l1 > tmp)	delta_g_l1 = tmp;
				}
				else if (rgTest == 5)
	  				delta_g_l1 = delta_g_min;
				else if (rgTest == 6)
				{
					delta_g_p2 += d_delta_g_max;
					if(delta_g_p2 > tmp)	delta_g_p2 = tmp;
				}
				else if (rgTest == 7)
					delta_g_p2 = delta_g_min;
				else if (rgTest == 8)
				{
					delta_g_l2 += d_delta_g_max;
					if(delta_g_l2 > tmp)	delta_g_l2 = tmp;
				}
				else
				{
					delta_g_l2 = delta_g_min;
					rgTest = 0;

					if (flKoord)
						rgStart = 4;		//системная готовность
					else
						rgStart = 3;		//ждем кординаты
					rgAnswer = 5;
				}
			}
			//Замедленный тест рулей --------------------------------------------
			else if (rgStart == 2)
			{
				if (rgTest == 1)
	  				delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = 0.5*M_PI;
				else if (rgTest == 2)
	  				delta_u_p1 = 0;
				else if (rgTest == 3)
	  				delta_u_l1 = 0;
				else if (rgTest == 4)
	  				delta_u_p2 = 0;
				else if (rgTest == 5)
	  				delta_u_l2 = 0;
				else if (rgTest == 6)
					delta_u_p1 = 0.5*M_PI;
				else if (rgTest == 7)
					delta_u_l1 = 0.5*M_PI;
				else if (rgTest == 8)
					delta_u_p2 = 0.5*M_PI;
				else
				{
					delta_u_l2 = 0.5*M_PI;
					rgTest = 0;

					if (flKoord)
						rgStart = 4;		//системная готовность
					else
						rgStart = 3;		//ждем кординаты
					rgAnswer = 5;
				}
			}
			else if (rgStart > 2)
			{
				//------------
				ggg = 20;
				//------------

	  			//гироскоп----------------------------------------------------------
				cos_kren_gir = cos(kren_gir);
				sin_kren_gir = sin(kren_gir);
				sin_tang_gir = sin(tang_gir);
				cos_tang_gir = cos(tang_gir);

				S_gir.y = 0.999*(S_gir.y+(V_gir.x*sin_tang_gir+V_gir.y*cos_tang_gir*cos_kren_gir-V_gir.z*cos_tang_gir*sin_kren_gir)*dt_gir)+0.001*H_filtr;

	 			e_dat.x = (w_dat.x-w_dat_pr.x)/dt_gir;
 				e_dat.y = (w_dat.y-w_dat_pr.y)/dt_gir;
 				e_dat.z = (w_dat.z-w_dat_pr.z)/dt_gir;
 				w_dat_pr.x = w_dat.x;
 				w_dat_pr.y = w_dat.y;
 				w_dat_pr.z = w_dat.z;

				v1 = vp(e_dat, l);
				v2 = vp(w_dat, l);
				v2 = vp(w_dat, v2);

				//показания акселей в цт
  				a_dat1.x = a_dat.x-v1.x-v2.x;
				a_dat1.y = a_dat.y-v1.y-v2.y;
				a_dat1.z = a_dat.z-v1.z-v2.z;

				//указатель скольжения
				if ((fabs(a_dat1.y) > FLT_EPSILON) || (fabs(a_dat1.z) > FLT_EPSILON))
				{
					tmp = atan2(-a_dat1.z, -a_dat1.y);
					slip = slip+((kren_zad+kren0+tmp)-slip)*kren_zad_wy*dt_gir;
				}

				if(flStoika || (rgfly == 5))
				{
					v3.x = 0;
					v3.y = 0;
					v3.z = 0;
				}
				else if(flKoord)
				{
					vVz.x = Vz;
					vVz.y = 0;
					vVz.z = 0;

					v3 = vp(w_dat, vVz);
				}
				else
				{
					vVz.x = V_dat;
					vVz.y = 0;
					vVz.z = 0;

					v3 = vp(w_dat, vVz);
				}
					/*v3.x = 0;
					v3.y = 0;
					v3.z = 0;*/

				//показания акселей в цт очищенные от вращательных перегрузок
  				a_dat_ct.x = a_dat1.x+2.0*v3.x;
				a_dat_ct.y = a_dat1.y+2.0*v3.y;
				a_dat_ct.z = a_dat1.z+2.0*v3.z;

				//ускорение цт относительно инерциальной СК паралельной связанным
				a_gir.x = -a_dat_ct.x-G*sin_tang_gir;
				a_gir.y = -a_dat_ct.y-G*cos_tang_gir*cos_kren_gir;
				a_gir.z = -a_dat_ct.z+G*cos_tang_gir*sin_kren_gir;
				ax_filtr = ax_filtr+(a_gir.x-ax_filtr)/FREQ_GIR*0.8;

				V_gir.x = V_gir.x+a_gir.x*dt_gir;
				V_gir.y = V_gir.y+a_gir.y*dt_gir;
				V_gir.z = V_gir.z+a_gir.z*dt_gir;

				//компенсация  V_gir
				if(flKoord)	V_gir.x = 0.999*V_gir.x+0.001*Vz;
				else		V_gir.x = 0.999*V_gir.x+0.001*V_dat;

				tmp = cos_tang_gir*cos_kren_gir;
				if(tmp > 10.0*FLT_EPSILON)
				{
					V_gir.y = 0.999*V_gir.y+0.001*Vy_filtr/tmp;
				}
				V_gir.z = 0.999*V_gir.z;

				drsk_gir = (w_dat.y*cos_kren_gir-w_dat.z*sin_kren_gir)/cos_tang_gir;
				dtang_gir = w_dat.y*sin_kren_gir+w_dat.z*cos_kren_gir;
				dkren_gir = w_dat.x-tan(tang_gir)*(w_dat.y*cos_kren_gir-w_dat.z*sin_kren_gir);

				rsk_gir = rsk_gir+drsk_gir*dt_gir;
				while (rsk_gir > 2.0*M_PI)  rsk_gir -= 2.0*M_PI;//загоняем в диапазон от 0 до 2*M_PI
				while (rsk_gir < 0)			rsk_gir += 2.0*M_PI;

/*				if((Vz > 5) && (V_dat > 5) && flKoord /*&& ((rgfly == 2) || (rgfly == 4))*//*)
				{
					tmp = rsk_gir;
					while (tmp > M_PI)	tmp -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
					while (tmp < -M_PI)	tmp += 2.0*M_PI;

					tmp1 = rsk_gps = 2.0*M_PI-koors;
	            	while (tmp1 > M_PI)	tmp1 -= 2.0*M_PI; //загоняем в диапазон от -M_PI до M_PI
	            	while (tmp1 < -M_PI)tmp1 += 2.0*M_PI;

	            	if(fabs(rsk_gps-rsk_gir) < fabs(tmp1-tmp))	rsk_gir = 0.999*rsk_gir+0.001*rsk_gps;
	            	else						            	rsk_gir = 0.999*tmp+0.001*tmp1;
					while (rsk_gir > 2.0*M_PI)  rsk_gir -= 2.0*M_PI;//загоняем в диапазон от 0 до 2*M_PI
					while (rsk_gir < 0)			rsk_gir += 2.0*M_PI;
				}
*/
				tang_gir = tang_gir+dtang_gir*dt_gir;
				while (tang_gir > M_PI)	tang_gir -= 2.0*M_PI;  //загоняем в диапазон от -M_PI до M_PI
				while (tang_gir < -M_PI)tang_gir += 2.0*M_PI;

				kren_gir = kren_gir+dkren_gir*dt_gir;
				while (kren_gir > M_PI)  kren_gir -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
				while (kren_gir < -M_PI) kren_gir += 2.0*M_PI;

				if (V_dat < 0)
				{
					teta_and_alfa = alfa = alfa0;
					kV = 0;
				}
				else if(V_dat < V_bum)
				{
					tmp = cos(kren_gir);
					if(tmp > FLT_EPSILON)
					{
						alfa = alfa0+(alfa_max-alfa0)/V_bum*V_dat/tmp;
						if(alfa > alfa_max) alfa = alfa_max;
						if(alfa < alfa_min) alfa = alfa_min;

						tmp = Vy_dat/V_dat;
						if(tmp > 1) tmp = 1;
						if(tmp < -1) tmp = -1;
						teta_and_alfa = alfa+V_dat/V_bum*asin(tmp);

						kV = ((Cy_alfa*alfa+Cy0)*ro*delta_ro*V_dat*V_dat*0.5*Sla)/Gla;
						if(kV > 1) kV = 1;
						if(kV < 0) kV = 0;
					}
				}
				else
				{
					tmp = cos(kren_gir);
					if(tmp > FLT_EPSILON)
					{
						tmp = 2.0*Gla/(ro*delta_ro*Sla*V_dat*V_dat)/tmp;	//tmp = Cy_potr
						alfa = (-6.738+6.67*tmp)/ToGrad;
						if(alfa > alfa_max) alfa = alfa_max;
						if(alfa < alfa_min) alfa = alfa_min;

						tmp = Vy_dat/V_dat;
						if(tmp > 1) tmp = 1;
						if(tmp < -1) tmp = -1;
						teta_and_alfa = alfa+asin(tmp);

						kV = 1;
					}
				}

				if(flKoord)	tmp = Vz;
				else	tmp = V_dat;
				if(tmp > 2)
					kren_dat = kren_dat+(dkren_gir-drsk_gir-1.0*G/tmp*tan(kren_dat))*dt_gir;
//					kren_dat = kren_dat+(1.0*w_dat.x-1.0*w_dat.y-G/tmp*tan(kren_dat))*dt_gir;
				else if ((fabs(a_dat_ct.y) > FLT_EPSILON) || (fabs(a_dat_ct.z) > FLT_EPSILON))
					kren_dat = kren_dat+(atan2(a_dat_ct.z, -a_dat_ct.y)-kren_dat)*dt_gir*0.8;
				while (kren_dat > M_PI)  kren_dat -= 2.0*M_PI;   //загоняем в диапазон от -M_PI до M_PI
				while (kren_dat < -M_PI) kren_dat += 2.0*M_PI;

				if ((fabs(kren_gir) < 70.0/ToGrad) && (fabs(tang_gir) < 70.0/ToGrad))
				{
					//поперечная компенсация
					if ((fabs(a_dat_ct.y) > FLT_EPSILON) || (fabs(a_dat_ct.z) > FLT_EPSILON))
					{
						kren_din = atan2(a_dat_ct.z, -a_dat_ct.y);
 					}
					if(flStoika || (rgfly == 5))
						kren_gir = 0.998*kren_gir+0.002*kren_din;
					else if (fabs(tmp) < 2)//пока
						kren_gir = 0.998*kren_gir+0.002*((1-tmp/2)*kren_din+tmp/2*kren_dat);
					else
						kren_gir = 0.998*kren_gir+0.002*kren_dat;

					//продольная компенсация
					angle = a_dat_ct.y*cos(kren_gir)-a_dat_ct.z*sin(kren_gir);
					tmp = (-a_dat_ct.x*sin(delta_u)+angle*cos(delta_u))/-G;	//tmp = ay
					if(tmp > 1)	tmp = 1;
					if(tmp < -1)	tmp = -1;
					tang_din = acos(tmp)-delta_u;	//tmp = tang

					if(flStoika)			tang_gir = 0.998*tang_gir+0.002*tang_din;
					else if(V_dat < V_bum)	tang_gir = 0.998*tang_gir+0.002*((1-kV)*tang_din+kV*teta_and_alfa);
					else					tang_gir = 0.998*tang_gir+0.002*teta_and_alfa;
				}
			}
       /*  	Vxv_dat = Vxv_dat+((Vz-V_dat)*cos(koors/ToGrad)-Vxv_dat)/FREQ_GIR*0.8;
         	Vzv_dat = Vzv_dat+((Vz-V_dat)*sin(koors/ToGrad)-Vzv_dat)/FREQ_GIR*0.8;
         	//проекция вектора скорости ветра на траекторию
         	Vv_dat = Vxv_dat*cos(koors/ToGrad)+Vzv_dat*sin(koors/ToGrad);*/

			if(rgStart == 5)	//инициализация до старта
			{
//				rsk_gir = (360-koors_zad)/ToGrad;
				H_max = int_dV = int_dkoors = int_dkren = 0;
				int_dtang = int_dtang0;
				delta_u_upor = asin(0.01*(gaz0+5));
			}

			//возвращение в режим старта
			if((rgStart > 5) && (H_filtr < 10) && (delta_g_zad < 5.0))	//на период отладки
			{
				rgStart = 5;
				rgAnswer = 5;
				flOutLand = 0;
			}
			if ((H_filtr > 20) && (rgStart > 5))
			{
				flOutLand = 1;
			}

			//отсечение нижнего порога у шима
			if(flStop)				tdelta_g_min = 1.0e-3;
			else if(rgStart > 5)	tdelta_g_min = tdelta_g_min_infly;
			else					tdelta_g_min = 1.0e-3;

			//автоматический перевод в режим спасения
			if((flStoika == 0) && (rgStart == 6) && (rgfly != 5))
			{
				tang_zad_filtr = tang_zad_filtr+(tang_zad-tang_zad_filtr)/FREQ_GIR*0.8;
				if((fabs(tang_gir) > 30.0/ToGrad) || (fabs(tang_gir-tang_zad_filtr) > 12.0/ToGrad))
				{
					Landing();
					flOutTang = 1;
				}
				kren_zad_filtr = kren_zad_filtr+(kren_zad_effect-kren_zad_filtr)/FREQ_GIR*0.8;
				if(fabs(kren_gir-kren_zad_filtr) > 25.0/ToGrad)
				{
					Landing();
					flOutKren = 1;
				}

				if(rgfly != 1)//срабатывает при просадке по высоте
				{
					if (H_filtr >= H_avar)	tmp = 100;	//tmp = deltaH
					else					tmp = 50;

					if ((H_filtr > H_max) && (H_filtr < H_zad))  H_max = H_filtr; //Если летим снизу
					if(H_filtr < (H_max-tmp )) //просадка
					{
						Landing();
						flOutH = 1;
					}
				}
				if(NoCommand > 15*FREQ_GIR)	//по отказу радиоканала
				{
					Landing();
					flOtkazRK = 1;
				}
				if(U_AKB < U_end)
				{
					Landing();
					flOutU = 1;
				}
				//добавить защиту при полете самолетом вблизи земли
			}

			//выключение аппарата при ударе
			if((rgStart > 5) && ((a_dat.x*a_dat.x+a_dat.y*a_dat.y+a_dat.z*a_dat.z) > 2500) && (H_dat < 10))
			{
				flStop = 1;
				rgAnswer = 5;
			}
			//выключение по истечении времени аварийной посадки
			if((flStop == 0) && (rgfly == 5) && ((liTimer_tick-liTimer_tick_start)/FREQ_GIR > 1.3*H_start))
			{
				if (/*(H_son < 1) || */(Vy_filtr > -1))
				{
					flStop = 1;
					rgAnswer = 5;
				}
			}

			if(flStop)
			{
				g_p1 = g_l1 = g_p2 = g_l2 = delta_g_p1 = delta_g_l1 = delta_g_p2 = delta_g_l2 = 0;
				u_p1 = u_l1 = u_p2 = u_l2 = delta_u_p1 = delta_u_l1 = delta_u_p2 = delta_u_l2 = delta_u_max;
			}
			else if(rgStart > 4)
			{
				//------------
				ggg = 21;
				//------------

				if(V_dat < V_bum)
				{
					if ((flStoika == 0) && (H_filtr < 10))	tmp = delta_u_upor_max; //запрет вблизи земли летать самолетом
					else	tmp = asin((1-kV)*0.01*(gaz0+5));//запрет опускать мотогондолы ниже несущих возможностей планера
															 //5 - это запас на случай крена и т.п.
				}
         		else
         			tmp = delta_u_min;

				//ограничение по критической скорости винта
				if((flStoika == 0) && (V_dat > 10))
				{
					tmp1 = 0.14*0.01*delta_g*ns_max*0.8/V_dat; //tmp1 = cos(alfa); 0.8 - коэф-т скольжения
					if (tmp1 < 0)	tmp1 = 0;
					else if(tmp1 > 1) tmp1 = 1;
					tmp1 = acos(tmp1);	//tmp1 = alfa;

					if(tmp < tmp1) tmp = tmp1;
				}
				if (tmp > delta_u_upor_max) tmp = delta_u_upor_max;
				if (tmp < delta_u_min) tmp = delta_u_min;

				delta_u_upor = delta_u_upor+(tmp-delta_u_upor)*dt_gir*0.8;//0.4;
				if (delta_u_upor > delta_u_upor_max) delta_u_upor = delta_u_upor_max;
				else if (delta_u_upor < delta_u_min) delta_u_upor = delta_u_min;

				if((rgfly == 0) || (rgfly == 2) || (rgfly == 4)) //неручное управление
				{
					//стабилизация скорости
					if (rgStart > 5)
					{
						ax_zad = V_zad-V_dat;	//ax_zad - сектор
						if (ax_zad > 10)  ax_zad = 10;
						if (ax_zad < -10) ax_zad = -10;

						int_dV = int_dV+i_ax*ax_zad*dt_gir;
						if (int_dV > delta_ax_max) int_dV = delta_ax_max;
						if (int_dV < delta_ax_min) int_dV = delta_ax_min;

						delta_ax = p_ax*ax_zad-d_ax*ax_filtr+int_dV;
						if (delta_ax > delta_ax_max) delta_ax = delta_ax_max;
						if (delta_ax < delta_ax_min) delta_ax = delta_ax_min;
						ax_zad = ax_zad*k1_ax;
					}
					else
					{
						delta_ax = 0;
					}

					Vy_zad = H_zad-H_dat;
					if (Vy_zad > dH_max) Vy_zad = dH_max;
					else if (Vy_zad < -dH_max) Vy_zad = -dH_max;

					if (rgStart == 5)
					{
						int_dVy = int_dVy+d_delta_g_max;
						if (int_dVy > gaz0)
						{
							rgStart = 6;
							rgAnswer = 5;
						}
					}
					else if (H_filtr < 10)
					{
						tmp = -0.25-H_filtr*0.1;
						if (tmp < -0.25)	tmp = -0.25;
						if (Vy_zad < tmp)	Vy_zad = tmp;

						int_dVy = int_dVy+i_ay*Vy_zad*dt_gir;
						if (int_dVy < (gaz0-d_ay*dH_max))  int_dVy = gaz0-d_ay*dH_max;
					}
					else
					{
						int_dVy = int_dVy+i_ay*Vy_zad*dt_gir;
						if (int_dVy < (gaz0-d_ay*dH_max))  int_dVy = gaz0-d_ay*dH_max;//не p_ay, a d_ay
					}
					if (int_dVy > delta_ay_max)  int_dVy = delta_ay_max;

					delta_ay = p_ay*Vy_zad-d_ay*Vy_dat+int_dVy;//проверить d_ay было d_ay = p_ay сейчас стало 0,02...
					if (delta_ay > delta_ay_max)  delta_ay = delta_ay_max;
					if (delta_ay < delta_ay_min) delta_ay = delta_ay_min;
					Vy_zad = k1_ay*Vy_zad;

					//tang_zad----------------------------------------------------
					tang_zad = calc_tang_zad();

					//смеситель----------------------------------------------------
					if(V_dat < V_bum)
					{
						tmp = (1-kV)*delta_ay/cos(kren_zad_effect);
						if(fabs(delta_ax) > FLT_EPSILON || fabs(tmp) > FLT_EPSILON)
							delta_u = atan2(tmp, delta_ax)+delta_u_min-tang_zad;//+delta_u0 = 0, т.к. при повороте вперед еффективность задних двигателей отстает

						delta_g = sqrt(delta_ax*delta_ax+tmp*tmp);
					}
					else
					{
						delta_u = delta_u_min;//-tang_zad;	//???
						delta_g = delta_ax;
					}

					if (delta_g > delta_g_max) delta_g = delta_g_max;
					if (delta_g < delta_g_min) delta_g = delta_g_min;
					g_p1 = g_l1 = g_p2 = g_l2 = delta_g;

					if (delta_u > delta_u_max)  delta_u = delta_u_max;
					if (delta_u < delta_u_upor) delta_u = delta_u_upor;
					if (delta_u < delta_u_min)  delta_u = delta_u_min;
					u_p1 = u_l1 = u_p2 = u_l2 = delta_u;
				}
            	else if(rgfly == 1)     //ручное управление
            	{
            		if(rgStart == 5)
            		{
            			if(flStoika && (delta_g_zad > 5))
            			{
            				rgStart = 6;
            				rgAnswer = 5;
            			}
            			else if ((flStoika == 0) && (delta_g_zad > gaz0))
            			{
            				rgStart = 6;
            				rgAnswer = 5;
            			}
            		}
           			delta_g = delta_g_zad/cos(kren_zad_effect);
       				if((flStoika == 0)&&(rgStart > 5) && (H_filtr < 10) && (delta_g < (gaz0-3)))
           					delta_g = (gaz0-3);	//защита от дурака
    				if (delta_g_zad < 1)	delta_g = delta_g_min;

            		if(delta_g > delta_g_max) delta_g = delta_g_max;
            		if(delta_g < delta_g_min) delta_g = delta_g_min;
            		g_p1 = g_l1 = g_p2 = g_l2 = delta_g;

            		Vy_zad = (delta_g-gaz0)/(delta_g_max-gaz0)*dH_max*k1_ay;	//Vy_max = dH_max*k1_ay
            		if(Vy_zad > k1_ay*dH_max) Vy_zad = k1_ay*dH_max;
            		if(Vy_zad < -k1_ay*dH_max) Vy_zad = -k1_ay*dH_max;

    				tang_zad = calc_tang_zad();
            		delta_u = delta_u_zad-tang_zad;
            		if((H_dat < 10) && flOutLand)
            		{
           				if (delta_u > (0.5*M_PI+5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI+5.0/ToGrad-tang_zad;
           				if (delta_u < (0.5*M_PI-5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI-5.0/ToGrad-tang_zad;
            		}
            		else
            		{
            			if (delta_u > delta_u_max)  delta_u = delta_u_max;
            			if (delta_u < delta_u_upor) delta_u = delta_u_upor;
            			if (delta_u < delta_u_min)  delta_u = delta_u_min;
            		}
            		u_p1 = u_l1 = u_p2 = u_l2 = delta_u;
            	}
            	else if(rgfly == 5)     //спасение
            	{
               		if(delta_g_zad > 1)	delta_g = gaz0;
    				if (delta_g_zad < 1)delta_g = delta_g_min;
            		g_p1 = g_l1 = g_p2 = g_l2 = delta_g;

    				tang_zad = alfa0;//calc_tang_zad();
           			if (flOtkazRK)
           				delta_u = 0.5*M_PI-tang_zad;
           			else
           			{
           				delta_u = delta_u_zad-tang_zad;
           				if (delta_u > (0.5*M_PI+5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI+5.0/ToGrad-tang_zad;
           				if (delta_u < (0.5*M_PI-5.0/ToGrad-tang_zad))  delta_u = 0.5*M_PI-5.0/ToGrad-tang_zad;
           			}
            		u_p1 = u_l1 = u_p2 = u_l2 = delta_u;
            	}

				//продольный канал-----------------------------------------------------------------
				wz_zad = tang_zad-tang_gir;		//сектор
				if (wz_zad > w_max)	         	wz_zad = w_max;
        		else if (wz_zad < -w_max)	   	wz_zad = -w_max;

				int_dtang = int_dtang+i_tang*wz_zad*dt_gir;
				if (int_dtang > delta_max)  int_dtang = delta_max;
				if (int_dtang < -delta_max) int_dtang = -delta_max;

				delta_tang = -(p_tang*wz_zad-d_tang*w_dat.z+d2_tang*e_dat.z+int_dtang);
				if (delta_tang > delta_max) delta_tang = delta_max;
				if (delta_tang < -delta_max) delta_tang = -delta_max;

				tmp = delta_tang*v_tang*sin(delta_u);	//tmp = dg
				g_p1 -= tmp;
				g_l1 -= tmp;
				g_p2 += tmp;
				g_l2 += tmp;

				tmp = delta_tang*g_tang*cos(delta_u)/ToGrad;	//tmp = du
				u_p1 -= tmp;
				u_l1 -= tmp;
				u_p2 += tmp;
				u_l2 += tmp;

				wz_zad = k1_tang*wz_zad;
				e_zad.z = -k2_tang*(wz_zad-w_dat.z);

				//Управляем влево-вправо--------------------------------------
				rsk_zad_ = (360-koors_zad)/ToGrad;
				while (rsk_zad_ >= 2.0*M_PI)    //загоняем в диапазон от 0 до 2*M_PI
					rsk_zad_ = rsk_zad_-2.0*M_PI;
				while (rsk_zad_ <= 0)
         	      rsk_zad_ = rsk_zad_+2.0*M_PI;
				wy_zad = rsk_zad_-rsk_gir;

				while (rsk_zad_ > M_PI)    //загоняем в диапазон от -M_PI до M_PI
					rsk_zad_ = rsk_zad_-2.0*M_PI;
				while (rsk_zad_ < -M_PI)
					rsk_zad_ = rsk_zad_+2.0*M_PI;

				tmp = rsk_gir;	//tmp = rsk_gir_
				while (tmp > M_PI)    //загоняем в диапазон от -M_PI до M_PI
					tmp = tmp-2.0*M_PI;
				while (tmp < -M_PI)
					tmp = tmp+2.0*M_PI;

				tmp = rsk_zad_-tmp;	//tmp = wy_zad_
				if(fabs(wy_zad) > fabs(tmp))  	wy_zad = tmp;
				if (wy_zad > w_max)	         	wy_zad = w_max;	//сектор
				else if (wy_zad < -w_max)	   	wy_zad = -w_max;

				if(flKoord)		tmp = Vz;	//ограничение по скорости
				else			tmp = V_dat;
				if (tmp < 1) tmp = 1;
				tmp = G*tan(kren_max)/k1_rsk/cos(tang_zad)/cos(kren_max)/tmp;//tmp = drsk_max
				if (wy_zad > tmp)	         	wy_zad = tmp;	//сектор
				else if (wy_zad < -tmp)	   		wy_zad = -tmp;

				int_dkoors = int_dkoors+i_rsk*wy_zad*dt_gir;
				if(int_dkoors > 0.2*delta_max)       int_dkoors = 0.2*delta_max;
				else if(int_dkoors < -0.2*delta_max) int_dkoors = -0.2*delta_max;

				delta_rsk = -(p_rsk*wy_zad-d_rsk*w_dat.y+d2_rsk*e_dat.y+int_dkoors);
				if (delta_rsk > delta_max) delta_rsk = delta_max;
				if (delta_rsk < -delta_max) delta_rsk = -delta_max;
				wy_zad = k1_rsk*wy_zad;								//это скорость

				//демпфируем боковой канал-------------------------------------
				if(NoCommand > 2.0*FREQ_GIR)	//по отказу радиоканала
				{
					kren_zad = 0;
					flOtkazRK05 = 1;
				}

				if (rgfly == 5)
				{
					kren_zad_effect = kren_zad+kren0;
				}
				else
				{
					kren_zad_effect = /*kren_zad+kren0+*/slip;
					/*					if(flKoord)	tmp = Vz;
										else		tmp = V_dat;
										if(tmp < 0.1) tmp = 0.1;
										tmp = -wy_zad*cos(tang_zad)*tmp/G;	//tmp = a

										kren_zad_effect = kren_zad+kren0+kren_zad_wy*asin((-1.0+sqrt(1.0+4.0*tmp*tmp))/(2.0*tmp));*/
				}
				e_zad.y = -k2_rsk*(wy_zad-w_dat.y);

				if (kren_zad_effect > kren_max)  kren_zad_effect = kren_max;
				if (kren_zad_effect < -kren_max) kren_zad_effect = -kren_max;

				wx_zad = kren_zad_effect-kren_gir;		 //сектор
				if (wx_zad > w_max)	         wx_zad = w_max;
				else if (wx_zad < -w_max)	   wx_zad = -w_max;

				int_dkren = int_dkren+i_kren*wx_zad*dt_gir;
				if (int_dkren > delta_max)  int_dkren = delta_max;
				if (int_dkren < -delta_max) int_dkren = -delta_max;

				delta_kren = -(p_kren*wx_zad-d_kren*w_dat.x+d2_kren*e_dat.x+int_dkren);
				if (delta_kren > delta_max) delta_kren = delta_max;
				if (delta_kren < -delta_max) delta_kren = -delta_max;

				wx_zad = k1_kren*wx_zad;
				e_zad.x = -k2_kren*(wx_zad-w_dat.x);

				//смеситель-----------------------------------------------------
				tmp = (v_rsk*delta_rsk*sin(delta_u)+g_kren*delta_kren*cos(delta_u))/ToGrad;	//tmp = du
				u_p1 += kl*tmp;
				u_l1 -= kl*tmp;
				u_p2 += tmp;
				u_l2 -= tmp;

				tmp = u_p1;
				if(tmp < u_l1) tmp = u_l1;
				if(tmp < u_p2) tmp = u_p2;
				if(tmp < u_l2) tmp = u_l2;
				if (tmp > delta_u_max)
				{
					tmp = tmp-delta_u_max;
					u_p1 = u_p1-tmp;
					u_l1 = u_l1-tmp;
					u_p2 = u_p2-tmp;
					u_l2 = u_l2-tmp;
				}

				tmp = u_p1;
				if(tmp > u_l1) tmp = u_l1;
				if(tmp > u_p2) tmp = u_p2;
				if(tmp > u_l2) tmp = u_l2;

				if (tmp < delta_u_upor)
				{
					tmp = delta_u_upor-tmp;
					u_p1 = u_p1+tmp;
					u_l1 = u_l1+tmp;
					u_p2 = u_p2+tmp;
					u_l2 = u_l2+tmp;
				}

				delta_u_p1 = u_p1;
				delta_u_l1 = u_l1;
				delta_u_p2 = u_p2;
				delta_u_l2 = u_l2;

				g_p1 += (-g_rsk*delta_rsk*cos(delta_u_p1)+v_kren*delta_kren*sin(delta_u_p1))*kl;
				g_l1 -= (-g_rsk*delta_rsk*cos(delta_u_l1)+v_kren*delta_kren*sin(delta_u_l1))*kl;
				g_p2 +=  -g_rsk*delta_rsk*cos(delta_u_p2)+v_kren*delta_kren*sin(delta_u_p2);
				g_l2 -=  -g_rsk*delta_rsk*cos(delta_u_l2)+v_kren*delta_kren*sin(delta_u_l2);

				//-------------------------------------------------------------
			/*	delta_g_p1 = delta_g_zad;
            	delta_g_l1 = delta_g_zad;
            	delta_g_p2 = delta_g_zad;
            	delta_g_l2 = delta_g_zad;
*/
				tmp = g_p1;
				if(tmp < g_l1) tmp = g_l1;
				if(tmp < g_p2) tmp = g_p2;
				if(tmp < g_l2) tmp = g_l2;

#ifdef ERA51
				if (V_dat > 0)	tmp1 = 80+V_dat;
				else tmp1 = 80;
				if(tmp1 > delta_g_max) tmp1 = delta_g_max;
				if (tmp > tmp1)
				{
					tmp = tmp-tmp1;
					g_p1 = g_p1-tmp;
					g_l1 = g_l1-tmp;
					g_p2 = g_p2-tmp;
					g_l2 = g_l2-tmp;
				}
#else
				if (tmp > delta_g_max)
				{
					tmp = tmp-delta_g_max;
					g_p1 = g_p1-tmp;
					g_l1 = g_l1-tmp;
					g_p2 = g_p2-tmp;
					g_l2 = g_l2-tmp;
				}
#endif
				tmp = g_p1;
				if(tmp > g_l1) tmp = g_l1;
				if(tmp > g_p2) tmp = g_p2;
				if(tmp > g_l2) tmp = g_l2;
				if (tmp < delta_g_min)
				{
					tmp = delta_g_min-tmp;
					g_p1 = g_p1+tmp;
					g_l1 = g_l1+tmp;
					g_p2 = g_p2+tmp;
					g_l2 = g_l2+tmp;
				}
				delta_g_p1 = g_p1;
				delta_g_l1 = g_l1;
				delta_g_p2 = g_p2;
				delta_g_l2 = g_l2;

/*				if ((delta_g_p1 < 50) && ((g_p1-delta_g_p1) > d_delta_g_max)) delta_g_p1 = delta_g_p1+d_delta_g_max;
				else	delta_g_p1 = g_p1;
				if ((delta_g_l1 < 50) && ((g_l1-delta_g_l1) > d_delta_g_max)) delta_g_l1 = delta_g_l1+d_delta_g_max;
				else	delta_g_l1 = g_l1;
				if ((delta_g_p2 < 50) && ((g_p2-delta_g_p2) > d_delta_g_max)) delta_g_p2 = delta_g_p2+d_delta_g_max;
				else	delta_g_p2 = g_p2;
				if ((delta_g_l2 < 50) && ((g_l2-delta_g_l2) > d_delta_g_max)) delta_g_l2 = delta_g_l2+d_delta_g_max;
				else	delta_g_l2 = g_l2;*/
			}
//----------------------------------------------------------------
			//SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, (1<<1) );
//----------------------------------------------------------------
		}	//if (flRun)
		//формирование ответа на землю
		if(int_Byte > 100) int_Byte = 100;
		if(flTransmiter && (int_Byte < -100))
			flTransmiter = 0;

		if(flTransmiter || (int_Byte > 0))
			;
		else if(rgAnswer == 1)
   		{
			rgAnswer = 0;

	    	BufferInModem[0] = 26 | 0x40;

	    	OutModem2(1e+5*dt0_p1+0.5, 1);
      		OutModem2(1e+5*dt90_p1+0.5, 3);
	    	OutModem2(1e+5*dt0_l1+0.5, 5);
	    	OutModem2(1e+5*dt90_l1+0.5, 7);
      		OutModem2(1e+5*dt0_p2+0.5, 9);
	    	OutModem2(1e+5*dt90_p2+0.5, 11);
	    	OutModem2(1e+5*dt0_l2+0.5, 13);
      		OutModem2(1e+5*dt90_l2+0.5, 15);

      		OutModem2(alfa0*1300+8168.5, 17);
      		OutModem2(1e+2*i_tang+0.5, 19);
      		OutModem2(0, 21);
	    	OutModem2(1e+2*p_tang+0.5, 23);
	    	OutModem2(1e+2*d_tang+0.5, 25);
      		OutModem2(1e+2*kl+0.5, 27);

	    	OutModem2(kren0*1300+8168.5, 29);
	    	OutModem2(1e+2*i_kren+0.5, 31);
      		OutModem2(1e+2*p_kren+0.5, 33);
	    	OutModem2(1e+2*d_kren+0.5, 35);
	    	OutModem2(1e+2*gaz0+0.5, 37);

      		OutModem2(1e+2*i_rsk+0.5, 39);
	    	OutModem2(1e+2*p_rsk+0.5, 41);
	    	OutModem2(1e+2*d_rsk+0.5, 43);
      		OutModem2(1e+2*w_max+0.5, 45);

	    	OutModem2(1e+2*delta_max+0.5, 47);

	    	BufferInModem[49] = 0;
      		for (i = 0; i < 49; i++ )
         		BufferInModem[49] = BufferInModem[49] ^ BufferInModem[i];
	    	OutModem1(BufferInModem[49], 49);
	    	BufferInModem[50] = 0;

			r0 = 0;
			rk = 50;
			int_Byte = rk;

			flTransmiter = 1;
			SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);
		}
		else if(rgAnswer == 2)
		{
			rgAnswer = 0;

	    	BufferInModem[0] = 27 | 0x40;

	    	OutModem2(1e+2*i_ay+0.5, 1);
      		OutModem2(1e+2*p_ay+0.5, 3);
	    	OutModem2(0, 5);
      		OutModem2(1e+5*tdelta_g_min_infly+0.5, 7);
      		OutModem2(1e+2*i_ax+0.5, 9);
      		OutModem2(1e+2*p_ax+0.5, 11);
      		OutModem2(1e+5*tdelta_g_max+0.5, 13);
	    	OutModem2(0, 15);
      		OutModem2(1e+2*v_rsk+0.5, 17);
      		OutModem2(1e+2*g_kren+0.5, 19);
      		OutModem2(1e+2*g_rsk+0.5, 21);
      		OutModem2(1e+2*v_kren+0.5, 23);
      		OutModem2(1e+2*g_tang+0.5, 25);
      		OutModem2(1e+2*v_tang+0.5, 27);
      		OutModem2(1e+2*int_dtang0+8000.5, 29);
      		OutModem2(1e+2*dH_max+0.5, 31);

      		OutModem2(1e+2*d2_tang+8000.5, 33);
      		OutModem2(1e+2*d2_kren+8000.5, 35);
      		OutModem2(1e+2*d2_rsk+8000.5, 37);
      		OutModem2(1e+2*kren_zad_wy+0.5, 39);
      		OutModem2(1e+2*d_ay+0.5, 41);
	    	OutModem2(1e+2*d_ax+0.5, 43);
	    	OutModem2(1e+2*U_end+0.5, 45);
      		OutModem2(0, 47);

      		BufferInModem[49] = 0;
      		for (i = 0; i < 49; i++ )
         		BufferInModem[49] = BufferInModem[49] ^ BufferInModem[i];
      		OutModem1(BufferInModem[49], 49);
      		BufferInModem[50] = 0;

			r0 = 0;
			rk = 50;
			int_Byte = rk;
			flTransmiter = 1;
			SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);
   		}
		else if(rgAnswer == 3)
   		{
      		rgAnswer = 5;

	   		BufferInModem[0] = 0x40 | 23;
			BufferInModem[1] = 0x80;
			BufferInModem[2] = (BufferInModem[0]^BufferInModem[1]) | 0x80;
      		BufferInModem[3] = 0;

        	r0 = 0;
        	rk = 3;
			int_Byte = rk;
        	flTransmiter = 1;
        	SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);//SI32_UART_A_set_tx(SI32_UART_0);
		}
		else if(rgAnswer == 4)
   		{
			rgAnswer = 0;
			BufferInModem[0] = 22 | 0x40;
			OutModem4(LatMar[n_], 1);
			OutModem4(LonMar[n_], 5);
        	BufferInModem[9] = (H_Mar[n_]+1000)/50 | 0x80;
        	BufferInModem[10] = Vz_Mar[n_] | 0x80;
			BufferInModem[11] = n_ | 0x80;

        	BufferInModem[12] = 0;
        	for (i = 0; i < 12; i++ )
	         	BufferInModem[12] = BufferInModem[12] ^ BufferInModem[i];
     		BufferInModem[12] = 0x80|BufferInModem[12];

        	r0 = 0;
        	rk = 13;
			int_Byte = rk;
        	flTransmiter = 1;
        	SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);
		}
		else if (rgAnswer == 5)
		{
			rgAnswer = 0;
			BufferInModem[0] = 21 | 0x40;
			OutModem2(10*H_zad+1000.5, 1);
			OutModem2(koors_zad, 3);
			OutModem1(n_, 5);
			OutModem1(KrenKam_zad+60, 6);
			OutModem1(UgolKam_zad, 7);
			BufferInModem[8] = 0x80;
			if(rgStart < 0)  	BufferInModem[9] = 0x80 | (0x07 << 2);
			else		      	BufferInModem[9] = 0x80 | ((rgStart & 0x07) << 2);

			if(flStoika)  		BufferInModem[10] = 0x88 | (rgfly & 0x07);
			else	      		BufferInModem[10] = 0x80 | (rgfly & 0x07);

			BufferInModem[11] = 0;
			for (i = 0; i < 11; i++ )
				BufferInModem[11] = BufferInModem[11] ^ BufferInModem[i];
			BufferInModem[11] =  0x80 | BufferInModem[11];

			r0 = 0;
	  		rk = 12;
			int_Byte = rk;

			flTransmiter = 1;
			SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);
		}
		else if (flTele )
		{
			flTele = 0;

	        BufferInModem[0] = 20 | 0x40;
			if(flKoord)
		   	{
     			OutModem4(LatFly, 1);
		      	OutModem4(LonFly, 5);
      			OutModem2(koors*ToGrad, 9);
      			OutModem1(Vz, 11);	   //Vзем
			}
		   	else
		   	{
		   		BufferInModem[1] = 0x80;
			    BufferInModem[2] = 0x80;
			    BufferInModem[3] = 0x80;
			    BufferInModem[4] = 0xff;
      			BufferInModem[5] = 0x80;
		      	BufferInModem[6] = 0x80;
	      		BufferInModem[7] = 0x80;
			    BufferInModem[8] = 0x80;
      			BufferInModem[9] = 0x80;
		   	    BufferInModem[10] = 0x80;
      			BufferInModem[11] = 0x80;
			}
			BufferInModem[12] = 0;
			for (i = 0; i < 12; i++ )
				BufferInModem[12] = BufferInModem[12] ^ BufferInModem[i];
			BufferInModem[12] =  0x80 | BufferInModem[12];

			r0 = 0;
			rk = 13;
			int_Byte = rk;

			flTransmiter = 1;
			SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);
		}
   		else if (flAnswer == 0)
   		{
      		flAnswer = 1;
      		BufferInModem[0] = 24 | 0x40;
      		OutModem4(liTimer_tick, 1);	//отправить в MK "Modem"
      		OutModem2(kren_din*1300+8168.5, 5);
      		OutModem2(tang_din*1300+8168.5, 7);
     		OutModem2(10*delta_g_zad+0.5, 9);

      		OutModem2(teta_and_alfa*1300+8168.5, 11);
      		OutModem1(last_reset_source, 13);
     		OutModem2((V_gir.x+10)*10+0.5, 14);
     		OutModem2((V_gir.y+10)*10, 16);
     		OutModem2((V_gir.z+10)*10, 18);

      		OutModem2((a_gir.x+800)*10, 20);
      		OutModem2((a_gir.y+800)*10, 22);
      		OutModem2((a_gir.z+800)*10, 24);
      		OutModem2(kren_gir*1300+8168.5, 26);
      		OutModem2(tang_gir*1300+8168.5, 28);
      		OutModem2(rsk_gir*1300+8168.5, 30);
      		OutModem2(w_dat.x*2500+8000.5, 32);
      		OutModem2(w_dat.y*2500+8000.5, 34);
      		OutModem2(w_dat.z*2500+8000.5, 36);

      		OutModem2(kren_zad*2500+8000.5, 38);
      		OutModem2(10*(V_zad+10)+0.5, 40);
      		OutModem1(count_reset_source, 42);
     		OutModem2(delta_g*10+0.5, 43);
     		OutModem2((H_filtr+100)*10, 45);
     		OutModem2((Vy_filtr+800)*10, 47);
     		OutModem2((V_dat+10)*10, 49);
     		OutModem2((Vy_zad+800)*10, 51);
      		OutModem2(tang_zad*1300+8168.5, 53);
      		OutModem2(kren_zad_effect*1300+8168.5, 55);
     		OutModem2(10.0*delta_u*ToGrad+0.5, 57);

     		BufferInModem[59] =  0x80 | fl_AKB;
		   	BufferInModem[60] =  0x80 | last_reset_source_AKB;
		   	BufferInModem[61] =  0x80 | count_reset_source_AKB;
      		OutModem2((ax_zad+800)*10, 62);
      		OutModem2((ax_filtr+800)*10, 64);

		   	BufferInModem[66] =  0x80|erWx;
		   	BufferInModem[67] =  0x80|erWy;
		   	BufferInModem[68] =  0x80|erWz;
		   	erWx = erWy = erWz = 0;
		    BufferInModem[69] = 0;
		   	for (i = 0; i < 69; i++ )
		   		BufferInModem[69] = BufferInModem[69] ^ BufferInModem[i];
		   	BufferInModem[69] =  0x80 | BufferInModem[69];

			r0 = 0;
	  		rk = 70;
			int_Byte = rk;

			flTransmiter = 1;
			SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);//SI32_UART_A_set_tx(SI32_UART_0);
   		}
   		else
   		{
      		flAnswer = 0;
      		BufferInModem[0] = 25 | 0x40;
     		OutModem2(10.0*delta_u_upor*ToGrad+0.5, 1);
     		OutModem2(10*delta_u_zad*ToGrad+0.5, 3);

     		OutModem2((int_dV+30)*10, 5);

			i = 10*(delta_ax+30);
			BufferInModem[7] = (i & 0x007f)| 0x80;
			BufferInModem[14] = ((i & 0x3f80) >> 7)| 0x80;

     		OutModem2((H_dat+100)*10, 8);
     		OutModem2((Vy_dat+800)*10, 10);
     		OutModem2((int_dVy+30)*10, 12);
//			BufferInModem[14] = delta_ax;

     		OutModem2(100*U_AKB, 15);
     		OutModem2(wy_zad*2500+8000, 17);
     		OutModem2((int_dkoors+20)*10, 19);
     		OutModem1(delta_rsk+60, 21);

     		OutModem2(10*delta_ay+0.5, 22);
     		OutModem2((int_dkren+20)*10, 24);
     		OutModem1(delta_kren+60, 26);

     		OutModem2(10*I_AKB, 27);
     		OutModem2((int_dtang+20)*10, 29);
     		OutModem1(delta_tang+60, 31);

     		OutModem1(delta_g_p1, 32);
     		OutModem1(delta_g_l1, 33);
     		OutModem1(delta_g_p2, 34);
     		OutModem1(delta_g_l2, 35);
//     		OutModem1(I2C_RUN, 35);

     		OutModem1(delta_u_p1*ToGrad+10, 36);
     		OutModem1(delta_u_l1*ToGrad+10, 37);
     		OutModem1(delta_u_p2*ToGrad+10, 38);
     		OutModem1(delta_u_l2*ToGrad+10, 39);
      		OutModem2(rsk_m*1300+8168.5, 40);
     		OutModem2(10*C_AKB, 42);

     		OutModem2((S_gir.y+100)*10, 44);
			OutModem1(0.5*koors_zad1, 46);
      		//BufferInModem[46] = 0x80;

   			BufferInModem[47] = 0x80;
	   		if(flStop)
		   		BufferInModem[47] = BufferInModem[47] | 0x01;
	   		if (flFaultH)
	   		{
		   		BufferInModem[47] = BufferInModem[47] | 0x02;
		   		flFaultH = 0;
	   		}
	   		if (flFaultV)
	   		{
		   		BufferInModem[47] = BufferInModem[47] | 0x04;
		   		flFaultV = 0;
	   		}
	   		if(flCommandStop)
	   			BufferInModem[47] = BufferInModem[47] | 0x08;
   			if(flOutTang)
	   			BufferInModem[47] = BufferInModem[47] | 0x10;
   			if(flOutKren)
	   			BufferInModem[47] = BufferInModem[47] | 0x20;
   			if(flOutH)
	   			BufferInModem[47] = BufferInModem[47] | 0x40;

   			BufferInModem[48] = 0x80;
	   		if(flOutU)
		   		BufferInModem[48] = BufferInModem[48] | 0x01;
	   		if(flAutoStop)
	   			BufferInModem[48] = BufferInModem[48] | 0x02;
	   		if(flOutLand)
	   			BufferInModem[48] = BufferInModem[48] | 0x04;
	   		if(flOtkazRK)
		   		BufferInModem[48] = BufferInModem[48] | 0x08;
	   		if (flFaultWx)
	   		{
		   		BufferInModem[48] = BufferInModem[48] | 0x10;
		   		flFaultWx = 0;
	   		}
	   		if (flFaultWy)
	   		{
		   		BufferInModem[48] = BufferInModem[48] | 0x20;
		   		flFaultWy = 0;
	   		}
	   		if (flFaultWz)
	   		{
		   		BufferInModem[48] = BufferInModem[48] | 0x40;
		   		flFaultWz = 0;
	   		}
     		OutModem2(wx_zad*2500+8000.5, 49);
     		OutModem2(wz_zad*2500+8000.5, 51);
        	OutModem2((a_dat.x+800)*10, 53);
      		OutModem2((a_dat.y+800)*10, 55);
      		OutModem2((a_dat.z+800)*10, 57);
      		OutModem2(kren_dat*1300+8168.5, 59);
     		OutModem2((H_son+100)*10, 61);

			BufferInModem[63] = 0x80+ggg_save;
      		OutModem2(e_dat.x*2500+8000.5, 64);
      		OutModem2(e_zad.x*2500+8000.5, 66);

      		BufferInModem[68] = 0x80;
	   		if (flOtkazRK05)
		   		BufferInModem[68] = BufferInModem[68] | 0x01;

			BufferInModem[69] = 0;
		   	for (i = 0; i < 69; i++ )
		   		BufferInModem[69] = BufferInModem[69] ^ BufferInModem[i];
		   	BufferInModem[69] =  0x80 | BufferInModem[69];

			r0 = 0;
	  		rk = 70;
			int_Byte = rk;

			flTransmiter = 1;
			SI32_UART_A_enable_tx_data_request_interrupt(SI32_UART_0);//SI32_UART_A_set_tx(SI32_UART_0);
   		}

		//работа с USART_0-------------------------------------------------------------------------
		if(rFromUSART_0 < (wFromUSART_0+marFromUSART_0*NFromUSART_0))
		{
			if ((BufferFromUSART_0[rFromUSART_0] & 0xC0) == 0x40)
			{
				nByteFromUSART_0 = 0;
				KontrSummaFromUSART_0 = 0;
				NPackageFromUSART_0 = BufferFromUSART_0[rFromUSART_0] & 0x3f;
			}

			if (nByteFromUSART_0 > 65)
				nByteFromUSART_0 = 65;
			codeFromUSART_0[nByteFromUSART_0] = BufferFromUSART_0[rFromUSART_0] & 0x7f;
			KontrSummaFromUSART_0 = KontrSummaFromUSART_0^codeFromUSART_0[nByteFromUSART_0++];

			if ((NPackageFromUSART_0 == 1) && (nByteFromUSART_0 == 11) && (KontrSummaFromUSART_0 == 0) )
			{
						U_AKB = 1e-2*FromEarth2(codeFromUSART_0, 1);
						I_AKB = 1e-1*FromEarth2(codeFromUSART_0, 3);
						C_AKB = 1e-1*FromEarth2(codeFromUSART_0, 5);
						fl_AKB = codeFromUSART_0[7];
						last_reset_source_AKB = codeFromUSART_0[8];
						count_reset_source_AKB = codeFromUSART_0[9];
			}
			rFromUSART_0++;
			if(rFromUSART_0 >= NFromUSART_0)
			{
				rFromUSART_0 = 0;
				marFromUSART_0 = 0;
			}
		}

		if(flTransmiterUSART_0)
			;
		else if(int_ByteUSART_0 > 0)
			;
		else if (command_AKB == 0)
		{
      		BufferToUSART_0[0] = 0x40 | 1;
      		BufferToUSART_0[1] = 0x80;
      		BufferToUSART_0[2] = (BufferToUSART_0[0]^BufferToUSART_0[1]) | 0x80;
      		BufferToUSART_0[3] = 0;

        	r0USART_0 = 0;
        	rkUSART_0 = 3;
        	int_ByteUSART_0 += 100;
        	flTransmiterUSART_0 = 1;
        	SI32_USART_A_enable_tx_data_request_interrupt(SI32_USART_0);
		}
		else if (command_AKB == 2)
		{
			command_AKB = 0;
      		BufferToUSART_0[0] = 0x40 | 2;	//DVS Start
      		BufferToUSART_0[1] = 0x80;
      		BufferToUSART_0[2] = (BufferToUSART_0[0]^BufferToUSART_0[1]) | 0x80;
      		BufferToUSART_0[3] = 0;

        	r0USART_0 = 0;
        	rkUSART_0 = 3;
        	int_ByteUSART_0 += 100;
        	flTransmiterUSART_0 = 1;
        	SI32_USART_A_enable_tx_data_request_interrupt(SI32_USART_0);
		}
		else if (command_AKB == 3)
		{
			command_AKB = 0;
      		BufferToUSART_0[0] = 0x40 | 3;	//DVS Stop
      		BufferToUSART_0[1] = 0x80;
      		BufferToUSART_0[2] = (BufferToUSART_0[0]^BufferToUSART_0[1]) | 0x80;
      		BufferToUSART_0[3] = 0;

        	r0USART_0 = 0;
        	rkUSART_0 = 3;
        	int_ByteUSART_0 += 100;
        	flTransmiterUSART_0 = 1;
        	SI32_USART_A_enable_tx_data_request_interrupt(SI32_USART_0);
		}

	}	//while (1)
}

//----------------------------------------------------------------------------
unsigned long DecodeLatOrLon(unsigned char Array[], unsigned char n)
{
	unsigned long koord, tmp;
	koord = Array[n] & 0x7f;
	tmp = Array[n+1] & 0x7f;
	koord = koord+(tmp << 7);
	tmp = Array[n+2] & 0x7f;
	koord = koord+(tmp << 14);
	tmp = Array[n+3] & 0x7f;
	koord = koord+(tmp << 21);
	return koord;
}

//-------------------------------------------------------------------------------------------------
float calc_tang_zad(void)
{
	float tang_zad /*= alfa0*/, tmp;
	/*if(V_dat < 0)
	{
		tang_zad = alfa0;
	}
	else */if(fabs(V_dat) < V_bum)
	{
		tang_zad = alfa0+(alfa_max-alfa0)/V_bum*V_dat;
		if(tang_zad > alfa_max) tang_zad = alfa_max;
		if(tang_zad < alfa_min) tang_zad = alfa_min;
		tang_zad += V_dat/V_bum*Vy_zad/V_bum;
	}
	else
	{
		tmp = 2.0*Gla/(ro*delta_ro*Sla*V_dat*V_dat);	//tmp = Cy_potr
		tang_zad = (-6.738+6.67*tmp)/ToGrad;
		if(tang_zad > alfa_max) tang_zad = alfa_max;
		if(tang_zad < alfa_min) tang_zad = alfa_min;
		tang_zad += Vy_zad/V_dat;
	}
	if (tang_zad > tang_max)	 	tang_zad = tang_max;
	else if (tang_zad < -tang_max)	tang_zad = -tang_max;
	return tang_zad;
}

//-------------------------------------------------------------------------------------------------
void Landing(void)
{
	rgfly = 5;
	rgAnswer = 5;
	liTimer_tick_start = liTimer_tick;
	H_start = H_filtr;
	if (H_start < 10)	H_start = 10;
}
