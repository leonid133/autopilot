/* myMPU9250.c
 *
 *  Created on: 11.07.2016
 *      Author: SainQuake
 */
#include <math.h>
#include <float.h>
#include "myMPU9250.h"

#include <stdio.h>
#include <stdint.h>
#include <si32_device.h>
#include <si32_SPI_A_Type.h>
#include <SI32_PBHD_A_Type.h>
#include <SI32_PBSTD_A_Type.h>
#include "MPU9250RM.h"
#include <sim3c1xx.h>

int16_t mag_data_raw[3];
struct vektor rsk_dat, mag, mag_max, mag_min, Magnetometer_ASA;

//-------------------------------------------------------------------------------------------------
unsigned int WriteReg( uint8_t WriteAddr, uint8_t WriteData)
{
	SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 0x0020);//0x0008

	SI32_SPI_A_flush_rx_fifo(SI32_SPI_0);
	SI32_SPI_A_flush_tx_fifo(SI32_SPI_0);

//	__asm__("nop");//asm=20ns//min delay 8 ns
	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, (WriteAddr<<8)|WriteData );
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1)
		;
	uint16_t ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);

	SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0020);
	return ReceiveData;
}

//-------------------------------------------------------------------------------------------------
void request_mag()//после вызова считвание возможно через 100 мс
{
	WriteReg(MPUREG_I2C_SLV0_ADDR,AK8963_I2C_ADDR|READ_FLAG); //Set the I2C slave addres of AK8963 and set for read.
	WriteReg(MPUREG_I2C_SLV0_REG,AK8963_WIA); 	//I2C slave 0 register address from where to begin data transfer
	WriteReg(MPUREG_I2C_SLV0_CTRL,0x8A); 		//Read 6 bytes from the magnetometer
}

//-------------------------------------------------------------------------------------------------
char read_mag()
{
	uint8_t response[12];
	short temp = WriteReg(READ_FLAG|MPUREG_WHOAMI, 0);
 	//printf("GYRO:	x=%x,%d	", temp, temp==0x71 );
 	if( temp != 0x71 ) return 1;//MPU9250_FAIL

	SI32_PBSTD_A_write_pins_low(SI32_PBHD_4, 0x0020);//0x0008
	SI32_SPI_A_flush_rx_fifo(SI32_SPI_0);
	SI32_SPI_A_flush_tx_fifo(SI32_SPI_0);
	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, ((MPUREG_EXT_SENS_DATA_00) | READ_FLAG) << 8);

	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	response[0] = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0) & 0xff;

	if(response[0] != 0x48)
	{
		SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0020);
		return 2;//MAGNETOMETER_FAIL
	}

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	uint16_t ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
	response[1] = (ReceiveData >> 8) & 0xff;
	response[2] = (ReceiveData) & 0xff;

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
//	response[3] = (ReceiveData >> 8) & 0xff;
//	response[4] = (ReceiveData) & 0xff;
	mag_data_raw[0] = ((ReceiveData & 0xff) << 8) | ((ReceiveData >> 8) & 0xff);
	mag.x = Magnetometer_ASA.x*(float)mag_data_raw[0];
	if(mag_max.x < mag.x)	mag_max.x = mag.x;
	if(mag_min.x > mag.x)	mag_min.x = mag.x;

/*	mag_data_raw[i] = ((int16_t)response[i*2+1+AK8963_HXL]<<8)|response[i*2+AK8963_HXL];
	mag[i] = Magnetometer_ASA[i]*(float)mag_data_raw[i];
*/
	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
/*	response[5] = (ReceiveData >> 8) & 0xff;
	response[6] = (ReceiveData) & 0xff;*/
	mag_data_raw[1] = ((ReceiveData & 0xff) << 8) | ((ReceiveData >> 8) & 0xff);
	mag.y = Magnetometer_ASA.y*(float)mag_data_raw[1];
	if(mag_max.y < mag.y)	mag_max.y = mag.y;
	if(mag_min.y > mag.y)	mag_min.y = mag.y;

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
	//response[7] = (ReceiveData >> 8) & 0xff;
	//response[8] = (ReceiveData) & 0xff;
	mag_data_raw[2] = ((ReceiveData & 0xff) << 8) | ((ReceiveData >> 8) & 0xff);
	mag.z = Magnetometer_ASA.z*(float)mag_data_raw[2];
	if(mag_max.z < mag.z)	mag_max.z = mag.z;
	if(mag_min.z > mag.z)	mag_min.z = mag.z;

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
	response[9] = (ReceiveData >> 8) & 0xff;
	response[10] = (ReceiveData) & 0xff;

	SI32_SPI_A_write_tx_fifo_u16(SI32_SPI_0, 0);
	while (SI32_SPI_A_get_rx_fifo_count(SI32_SPI_0) < 1);
	ReceiveData = SI32_SPI_A_read_rx_fifo_u16(SI32_SPI_0);
	response[11] = (ReceiveData >> 8) & 0xff;
	response[12] = (ReceiveData) & 0xff;

	SI32_PBSTD_A_write_pins_high(SI32_PBHD_4, 0x0020);

	if( (mag_data_raw[0] == 0) && (mag_data_raw[1] == 0) && (mag_data_raw[2] == 0) ) return 3;//DATA_0

	rsk_dat.x = mag.x-(mag_max.x+mag_min.x)/2;
	rsk_dat.y = mag.y-(mag_max.y+mag_min.y)/2;
	rsk_dat.z = mag.z-(mag_max.z+mag_min.z)/2;
	return OK;
}

//-------------------------------------------------------------------------------------------------
void init_mag()
{
	mag_max.x = 0;// , mag_min;
	WriteReg(MPUREG_PWR_MGMT_1, 0x80);
	WriteReg(MPUREG_I2C_SLV0_CTRL, 0x00 );	//Enable I2C and set 1 byte
	for(unsigned int k = 0; k < 2500000; k++)  __asm__("nop");//100ms delay

	WriteReg(MPUREG_PWR_MGMT_1, 0x01);
	WriteReg(MPUREG_PWR_MGMT_2, 0x00);
	for(unsigned int k = 0; k < 5000000; k++)  __asm__("nop");//200ms delay

    WriteReg(MPUREG_I2C_MST_CTRL, 0x00); // Disable I2C master
    WriteReg(MPUREG_USER_CTRL, 0x12);    // Disable FIFO and I2C master modes

    // Configure device for bias calculation
    /*WriteReg(MPUREG_INT_ENABLE, 0x00);   // Disable all interrupts
    WriteReg(MPUREG_FIFO_EN, 0x00);      // Disable FIFO
    WriteReg(MPUREG_PWR_MGMT_1, 0x00);   // Turn on internal clock source
    WriteReg(MPUREG_I2C_MST_CTRL, 0x00); // Disable I2C master
    WriteReg(MPUREG_USER_CTRL, 0x00);    // Disable FIFO and I2C master modes
    WriteReg(MPUREG_USER_CTRL, 0x0C);    // Reset FIFO and DMP
    for(unsigned int k=0;k<375000;k++)  __asm__("nop");*/

    WriteReg(MPUREG_USER_CTRL, 0x20 );	// I2C Master mode
    WriteReg(MPUREG_I2C_MST_CTRL, 0x10+1 );	//  I2C configuration multi-master  IIC 400KHz

    WriteReg(MPUREG_I2C_SLV0_ADDR, AK8963_I2C_ADDR );	//Set the I2C slave addres of AK8963 and set for write.

    WriteReg(MPUREG_I2C_SLV0_REG, AK8963_CNTL2 );	//I2C slave 0 register address from where to begin data transfer
    WriteReg(MPUREG_I2C_SLV0_DO, 0x01 );	// Reset AK8963
    WriteReg(MPUREG_I2C_SLV0_CTRL, 0x81 );	//Enable I2C and set 1 byte
    for(unsigned int k = 0; k < 2500000; k++)  __asm__("nop");//100ms delay
	WriteReg(MPUREG_I2C_SLV0_REG, AK8963_CNTL1 );	//I2C slave 0 register address from where to begin data transfer
	WriteReg(MPUREG_I2C_SLV0_DO, 0x12 );	// Register value to 8Hz continuous measurement in 16bit
	WriteReg(MPUREG_I2C_SLV0_CTRL, 0x81 );	//Enable I2C and set 1 byte

	for(unsigned int k = 0; k < 2500000; k++)  __asm__("nop");//100ms delay

    WriteReg(MPUREG_I2C_SLV0_ADDR,AK8963_I2C_ADDR|READ_FLAG); //Set the I2C slave addres of AK8963 and set for read.
    WriteReg(MPUREG_I2C_SLV0_REG,AK8963_ASAX); //I2C slave 0 register address from where to begin data transfer
    WriteReg(MPUREG_I2C_SLV0_CTRL,0x83); //Read 6 bytes from the magnetometer
    for(unsigned int k = 0; k < 2500000; k++)  __asm__("nop");//100ms delay

    float data = WriteReg(MPUREG_EXT_SENS_DATA_00|READ_FLAG, 0);
	Magnetometer_ASA.x = ((data-128)/256+1);

	data = WriteReg(MPUREG_EXT_SENS_DATA_01|READ_FLAG, 0);
	Magnetometer_ASA.y = ((data-128)/256+1);

	data = WriteReg(MPUREG_EXT_SENS_DATA_02|READ_FLAG, 0);
	Magnetometer_ASA.z = ((data-128)/256+1);

	for(unsigned int k = 0; k < 200000; k++)  __asm__("nop");//7.3ms delay
}
