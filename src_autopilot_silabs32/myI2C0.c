#include <stdio.h>
#include <si32_device.h>
#include <SI32_I2C_A_Type.h>
#include <SI32_PBSTD_A_Type.h>
#include <SI32_CLKCTRL_A_Type.h>
#include "myI2C0.h"
#define WAIT_TIME_I2C 3000

uint8_t rst = 0;
uint32_t count = 6;
long in[6];

char I2C_RUN = 0, I2C_READY = 0;

//-------------------------------------------------------------------------------
void I2C0_tx_complete_handler(void)
{
	if (SI32_I2C_A_is_ack_received(SI32_I2C_0))
	{
		if(rst==0)
		{
			rst=1;
			SI32_I2C_A_set_start(SI32_I2C_0);
		}
		else //if (SI32_I2C_A_is_ack_received(SI32_I2C_0))
		{
			SI32_I2C_A_arm_rx(SI32_I2C_0); // Arm reception(RXARM=1)
		}
	}
	else
	{ // NACK was received
        SI32_I2C_A_set_stop (SI32_I2C_0); // Set STO to terminte transfer
    }
	SI32_I2C_A_clear_tx_interrupt(SI32_I2C_0); // clear TXI
	//SI32_I2C_A_clear_ack_interrupt(SI32_I2C_0); // clear ACKI
}

//-------------------------------------------------------------------------------
void I2C0_rx_complete_handler(void)
{
	char tmp = SI32_I2C_A_read_data(SI32_I2C_0);
    SI32_I2C_A_clear_rx_interrupt(SI32_I2C_0);
    SI32_I2C_A_clear_ack_interrupt(SI32_I2C_0);

	if (count)
    {
        in[--count] =  tmp;
    }
    if (count)
    {
        SI32_I2C_A_send_ack(SI32_I2C_0); // send an ACK
        SI32_I2C_A_arm_rx(SI32_I2C_0); // Arm reception(RXARM=1)
    }
    else
    {
        SI32_I2C_A_send_nack(SI32_I2C_0); // send an NACK
        SI32_I2C_A_arm_rx(SI32_I2C_0); // Arm reception(RXARM=1)
        SI32_I2C_A_set_stop (SI32_I2C_0); // Set STO to terminte transfer

    	I2C_RUN |= (1<<6);
    	I2C_READY = 1;
    	SI32_I2C_0->CONTROL_CLR =  SI32_I2C_A_CONTROL_STA_MASK | SI32_I2C_A_CONTROL_STAI_MASK;//
    }
    SI32_I2C_0->CONTROL_CLR =  SI32_I2C_A_CONTROL_STA_MASK | SI32_I2C_A_CONTROL_STAI_MASK;//
}

void I2C0_ack_intr_handler(void)
{
	if (count)
	    {
	        SI32_I2C_A_send_ack(SI32_I2C_0); // send an ACK
	    }
	    else
	    {
	        SI32_I2C_A_send_nack(SI32_I2C_0); // send an NACK
	    }
	//int a;
	SI32_I2C_A_clear_ack_interrupt(SI32_I2C_0);
	//if (count == 0)
	//	a = 1;
}

void I2C0_stop_handler(void)
{
	I2C_RUN |= (1<<7);
	SI32_I2C_A_send_nack(SI32_I2C_0);
    SI32_I2C_0->CONTROL_CLR = SI32_I2C_A_CONTROL_STO_MASK | SI32_I2C_A_CONTROL_STOI_MASK;
}


///////////////////////////////
void I2C0_start_handler(void)
{
    uint32_t tmp;//ADDRESS

    if(rst)
    {
    	tmp = 0x33;
    	SI32_I2C_A_set_byte_count(SI32_I2C_0, 1); // set bytes count(BC)
    }
    else
    {
    	tmp = 0x00005032;
    	SI32_I2C_A_set_byte_count(SI32_I2C_0, 0); // set bytes count(BC)
    }
    SI32_I2C_A_write_data(SI32_I2C_0,tmp);
    SI32_I2C_A_arm_tx(SI32_I2C_0); // Arm transmission(TXARM=1)
    // Start bit comes with ACKI
    SI32_I2C_0->CONTROL_CLR =  SI32_I2C_A_CONTROL_STA_MASK | SI32_I2C_A_CONTROL_STAI_MASK;//
    //SI32_I2C_0->CONTROL_CLR =  SI32_I2C_A_CONTROL_STA_MASK | SI32_I2C_A_CONTROL_STAI_MASK;//
}

//---------------------------------------------------------------------------------------------
void I2C0_arb_lost_handler(void )
{
    SI32_I2C_A_clear_arblost_interrupt(SI32_I2C_0);
    SI32_I2C_A_reset_module(SI32_I2C_0); // Reset I2C module for abnormal case

    /*SI32_I2C_A_clear_rx_interrupt(SI32_I2C_0);
    SI32_I2C_A_clear_tx_interrupt(SI32_I2C_0);
    SI32_I2C_A_clear_ack_interrupt(SI32_I2C_0);
    SI32_I2C_A_clear_start_interrupt(SI32_I2C_0);
    SI32_I2C_A_clear_stop_interrupt(SI32_I2C_0);
	SI32_I2C_0->CONTROL_CLR = SI32_I2C_A_CONTROL_STA_MASK | SI32_I2C_A_CONTROL_STO_MASK | SI32_I2C_A_CONTROL_STOI_MASK;
    SI32_I2C_A_read_data(SI32_I2C_0);*/
 //   I2C_RUN=0;
    //printf("arb\n");
    I2C_RUN |= (1<<5);
}

//--------------------------------------------------------------------------------------
void I2C0_timer3_handler(void )
{
    SI32_I2C_A_clear_timer3_interrupt(SI32_I2C_0);
    //SI32_I2C_A_reset_module(SI32_I2C_0);
//    I2C_RUN=0;
    //printf("t3\n");
}

//--------------------------------------------------------------------------------------
void myI2C_run3(void)
{
	rst=0;
	count = 6;
	in[0] = in[1] = in[2] = in[3] = in[4] = in[5] = 0;

	SI32_I2C_A_clear_timer3_interrupt(SI32_I2C_0);

	//if( SI32_I2C_0->CONTROL.STA  /*|| SI32_I2C_0->CONTROL.STO*/)
	{
		//rst = 1;
		//-----------------
			if( SI32_I2C_0->CONTROL.STA )		I2C_RUN |= (1<<0);
			if( SI32_I2C_0->CONTROL.STO )		I2C_RUN |= (1<<1);
		//----------------
		SI32_I2C_A_reset_module(SI32_I2C_0); // Reset I2C module for abnormal case
			//SI32_I2C_0->CONTROL_CLR =  SI32_I2C_A_CONTROL_STA_MASK; //
	}
	I2C_READY = I2C_RUN = 0;
	//if(!SI32_I2C_0->CONTROL.BUSYF)
	SI32_I2C_A_set_start(SI32_I2C_0);


}
