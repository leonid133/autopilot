#!/bin/bash

PROGRAM="Pthread"

export AUTOPILOT_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

read -d '' help <<- EOF

Autopilot

this programm for Raspberry Pi3 and Navio2

no argument   start as is

-- start      programm start as daemon, 

-- stop       stop if programm working

EOF

if [ "$1" == "--help" ] || [ "$1" == "-h" ] || [ "$2" == "--help" ] || [ "$2" == "-h" ]
then
    echo "${help}"
    exit 0
fi

if [ "$1" == "--start" ] || [ "$1" == "-start" ] 
then
echo "Start Pi Navio Autopilot as daemon"
    PID_FILE="${AUTOPILOT_HOME}/pilot.pid"
    sudo ./${SAUTOPILOT_HOM}/src/Pthread &
    echo $! > ${PID_FILE}
    exit 0
fi

if [ "$1" == "--stop" ] || [ "$1" == "-stop" ] 
then
    PID=$(<${AUTOPILOT_HOME}/pilot.pid)
    echo Stopping $PID    
    sudo kill -9 $PID
    sudo rm  ${AUTOPILOT_HOME}/pilot.pid

    for PID in `ps -aef | grep $PROGRAM | awk '{print $2}'`
    do
        sudo kill $PID
    done
    
exit 0
fi
    echo "Start Pi Navio Autopilot"
    PID_FILE="${AUTOPILOT_HOME}/pilot.pid"
    sudo ./${SAUTOPILOT_HOM}/src/Pthread  
    echo $! > ${PID_FILE}

exit 0
