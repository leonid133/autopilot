#ifndef  flyingH
#define flyingH

//---------------------------------------------------------------------------
#include "common1.h"
#include "atm.h"
//---------------------------------------------------------------------------

#define SQ(x) ((x)*(x))
#define EPSILON 100

extern const double tang_max, kren_max, Vy_max, Vy_min;
extern const double Wak_max;
extern const float alfa_max, alfa_min;
#define H_zad_max 	1500
#define H_zad_min 	-100
extern const int V_max, V_min;
extern const float delta_u_max, delta_u_min;
extern const float delta_g_max, delta_g_min;
extern const float delta_ax_max, delta_ax_min;
extern const float delta_ay_max, delta_ay_min;
extern const float Wak0;

//---------------------------------------------------------------------------
int fsgn (double a);
int fsgn (double a)
{
	 if (a >= 0) return 1;
	 return -1;
};

/******************************************************************************/
class flying{
public:
   flying(void){};
	~flying(void) {};
   virtual int read(char *Buffer){return 0;};
	virtual void write(char *RK_code){};
};

//----------------------------------------------------------------------------
class bpla: public flying{
public:
   bpla(void);
	~bpla(void);
   int read(char *Buffer);
	void write(char *RK_code);
};

//----------------------------------------------------------------------------
class piter: public flying{
public:
   piter(void);
	~piter(void);
   int read(char *Buffer);
	void write(char *RK_code);
};
//----------------------------------------------------------------------------
class wifi: public flying{
public:
   wifi(void);
	~wifi(void);
   int read(char *Buffer);
  	void write(char *RK_code);
};
//----------------------------------------------------------------------------
class novosib: public flying{
public:
   novosib(void);
	~novosib(void);
   int read(char *Buffer);
	void write(char *RK_code);
};

#endif
