//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Main11.h"
#include "About.h"
#include <time.h>

//---------------------------------------------------------------------------
#pragma link "CPort"
#pragma resource "*.dfm"
TTerm *Term;

double  t, t_interval, dt, t_pr, t_GPS, dt_GPS = 1;
unsigned long t0;

//------------------------------------------------------------------------------
char flMatmodel;
String FileNameHistory;
fstream file_his;
const char cpPlane = 8; //1  2    3   4    5    6    7    8
int z_Plane[cpPlane] =   {0, 10, 70, 70, -70, -70, -10,   0};
int x_Plane[cpPlane] =   {70, 20, 5,-14, -14,   5,  20,  70};

const char cpStrelka = 5;  //1  2  3   4    5
int y_Strelka[cpStrelka] =   {0, 8, 0, -8, 0};
int x_Strelka[cpStrelka] =  {70, 0,-8, 0, 70};

const int sName = 0;
const int sFileName = 1;
const int sStepNet = 2;
const int sScaleMap = 3;
const int sEditMap = 4;
const int snapr_vet = 5;
const int sV_vet = 6;
const int snapr_vzlet = 7;
const int si_mar = 8;

const int salfa0 = 0;
const int si_tang = 1;
const int sp_tang = 2;
const int sd_tang = 3;
const int sd2_tang = 4;
const int sint_dtang0 = 5;
const int sv_tang = 6;
const int sg_tang = 7;

const int skren0 = 8;
const int si_kren = 9;
const int sp_kren = 10;
const int sd_kren = 11;
const int sd2_kren = 12;
const int sv_kren = 13;
const int sg_kren = 14;
const int skren_zad_wy = 15;

const int si_rsk = 16;
const int sp_rsk = 17;
const int sd_rsk = 18;
const int sd2_rsk = 19;
const int sv_rsk = 20;
const int sg_rsk = 21;

const int si_ay = 22;
const int sp_ay = 23;
const int sd_ay = 24;
const int sdH_max = 25;

const int si_ax = 26;
const int sp_ax = 27;
const int sd_ax = 28;

const int sdt0_p1 = 29;
const int sdt90_p1 = 30;
const int sdt0_l1 = 31;
const int sdt90_l1 = 32;
const int sdt0_p2 = 33;
const int sdt90_p2 = 34;
const int sdt0_l2 = 35;
const int sdt90_l2 = 36;

const int skl = 37;
const int snamemodem = 38;
const int sgaz0 = 39;
const int stdelta_g_max = 40;
const int stdelta_g_min = 41;
const int sw_max = 42;
const int sdelta_max = 43;
const int sU_end = 44;

//-------- ��������� �������������� ����� �� �������� ����� -----------------
KPoint TTerm::ToEarth  (POINT point)
{
   vektor temp;
   temp.z = (double)((point.x-RectMap.left-0.5*RectMap.Width())/Zoom+Center.x)/ScaleMap;
   temp.x = -(double)((point.y-RectMap.top-0.5*RectMap.Height())/Zoom+Center.y)/ScaleMap;
   temp.y = 0;
   return Point0Map + KPoint(temp, Lat0);
}

//��������� ����� �� Map �� �������������� ����� ----------------------------
POINT TTerm::ToMap(const KPoint &point)
{
   KPoint localPoint = point - Point0Map ;
   vektor  temp = localPoint.ToRect(Lat0);
   POINT p;
   p.x = (int)((temp.z*ScaleMap-Center.x)*Zoom)+0.5*RectMap.Width();
   p.y = (int)((-temp.x*ScaleMap-Center.y)*Zoom)+0.5*RectMap.Height();
   return p;
}

//��������� �������� ����� �� �������������� ����� --------------------------
POINT TTerm::ToScreen(const KPoint &point)
{
   POINT p = ToMap(point);
   p.x += RectMap.left;
   p.y += RectMap.top;
   return p;
}

//---------------------------------------------------------------------------
__fastcall TTerm::TTerm(TComponent *Owner)
	: TForm(Owner)
   , iter(0)
   , Regime(0)
   , Zoom(1)
   , ScaleMap (1)
   , INIFile ("npu.ini")
   , koors_zad(0)
   , otkl_ot_mar(0)
   , Vy (0)
   , Vy_zad (20)
   , Vy_zad_bort(0)
   , H (0, H_zad_min, H_zad_max, 400, "H, �")
   , V (WIDTH_MONITOR*1, -50, 200, 50, "V, ��/�", 3.6)
   , Vz (WIDTH_MONITOR*2, -50, 200, 50, "Vz, ��/�", 3.6)
   , delta_g (WIDTH_MONITOR*3, delta_g_min, delta_g_max, 20, "delta_g, %")
   , delta_u (WIDTH_MONITOR*4, delta_u_min*ToGrad, delta_u_max*ToGrad, 20, "delta_u, ��")
   , U (WIDTH_MONITOR*5, 0, 25, 5, "U, �")
   , I (WIDTH_MONITOR*6, 0, 200, 50, "I, �")
   , C (WIDTH_MONITOR*7, 0, 20, 5, "C, A�")
   , MyFly(NULL)
   , flModifiedFile(false)
   , flModifiedCoeff(false)
{
   Application->OnHint = &MyOnHint;
   Application->ShowHint = true;

   ImageTmp = new Graphics::TBitmap;
   ImageTmp->Width = 0;
   Map = new Graphics::TBitmap;

   String txt = ExtractFilePath(Application->ExeName)+"\Mar"+"\autosave.mai";
   FileNameMar = txt.c_str();
   txt = ExtractFilePath(Application->ExeName)+"\Mar"+"\autosave.cof";
   FileNameCoeff = txt.c_str();
   fstream file;
   file.open(INIFile.c_str(), ios:: in);
   if (file)
      file >> FileNameMar >> FileNameCoeff;
   file.close();

   file.open(FileNameMar.c_str(), ios:: in);
   if (file)
      LoadFileMarsh(file);
   file.close();

   CB_TypeWork->ItemIndex = TypeWork;

   SG->Cells[0][sName] = "��� ����� ��������";
   SG->Cells[0][sFileName] = "��� BMP-�����";
   SG->Cells[0][sStepNet] = "��� �����";
   SG->Cells[0][sScaleMap] = "�������";
   SG->Cells[0][sEditMap] = "����� ��������";
   SG->Cells[0][snapr_vet] = "����������� �����";
   SG->Cells[0][sV_vet] = "�������� �����";
   SG->Cells[0][snapr_vzlet] = "����������� �����";
   SG->Cells[0][si_mar] = "����� �����";

   SG_Mar->Cells[0][0] = "  N";
   SG_Mar->Cells[1][0] = "  ����������";
   SG_Mar->Cells[2][0] = " H, �";
   SG_Mar->Cells[3][0] = " Vz, �";
   SG_Mar->ColWidths[0] = 25;
   SG_Mar->ColWidths[1] = 130;
   SG_Mar->ColWidths[2] = 55;
   SG_Mar->ColWidths[3] = SG_Mar->Width-SG_Mar->ColWidths[0]-SG_Mar->ColWidths[1]- SG_Mar->ColWidths[2]-7;

   dt0_p1[0] = dt90_p1[0] = dt0_l1[0] = dt90_l1[0] = dt0_p2[0] = dt90_p2[0] = dt0_l2[0] = dt90_l2[0] = 1.5;
   alfa0[0] = kren0[0] = 0;
   i_tang[0] = p_tang[0] = d_tang[0] = d2_tang[0] = i_kren[0] = p_kren[0] = d_kren[0] = d2_kren[0] = 1.0;
   i_rsk [0] = p_rsk[0] = d_rsk[0] = d2_rsk[0] = 1.0;

   file.open(FileNameCoeff.c_str(), ios:: in);
   if (file)
   {
      LNameFile->Caption = FileNameCoeff.c_str();
      LoadFileCoeff(file);
   }
   file.close();
   ShowCoeff();
   SG_init->Cells[0][salfa0] = "alfa0";
   SG_init->Cells[0][si_tang] = "i_tang";
   SG_init->Cells[0][sp_tang] = "p_tang";
   SG_init->Cells[0][sd_tang] = "d_tang";
   SG_init->Cells[0][sd2_tang] = "d2_tang";
   SG_init->Cells[0][sint_dtang0] = "int_dtang0";

   SG_init->Cells[0][skren0] = "kren0";
   SG_init->Cells[0][si_kren] = "i_kren";
   SG_init->Cells[0][sp_kren] = "p_kren";
   SG_init->Cells[0][sd_kren] = "d_kren";
   SG_init->Cells[0][sd2_kren] = "d2_kren";

   SG_init->Cells[0][si_rsk] = "i_rsk";
   SG_init->Cells[0][sp_rsk] = "p_rsk";
   SG_init->Cells[0][sd_rsk] = "d_rsk";
   SG_init->Cells[0][sd2_rsk] = "d2_rsk";

   SG_init->Cells[0][si_ay] = "i_ay";
   SG_init->Cells[0][sp_ay] = "p_ay";
   SG_init->Cells[0][sd_ay] = "d_ay";
//   SG_init->Cells[0][sk_dH] = "k_dH";
   SG_init->Cells[0][sdH_max] = "dH_max";

   SG_init->Cells[0][si_ax] = "i_ax";
   SG_init->Cells[0][sp_ax] = "p_ax";
   SG_init->Cells[0][sd_ax] = "d_ax";

   SG_init->Cells[0][sdt0_p1] = "dt0_p1";
   SG_init->Cells[0][sdt90_p1] = "dt90_p1";
   SG_init->Cells[0][sdt0_l1] = "dt0_l1";
   SG_init->Cells[0][sdt90_l1] = "dt90_l1";
   SG_init->Cells[0][sdt0_p2] = "dt0_p2";
   SG_init->Cells[0][sdt90_p2] = "dt90_p2";
   SG_init->Cells[0][sdt0_l2] = "dt0_l2";
   SG_init->Cells[0][sdt90_l2] = "dt90_l2";

   SG_init->Cells[0][sv_rsk] = "v_rsk";
   SG_init->Cells[0][sv_kren] = "v_kren";
   SG_init->Cells[0][sg_kren] = "g_kren";
   SG_init->Cells[0][skren_zad_wy] = "kren_zad_wy";

   SG_init->Cells[0][sg_rsk] = "g_rsk";
   SG_init->Cells[0][sg_tang] = "g_tang";
   SG_init->Cells[0][sv_tang] = "v_tang";
   SG_init->Cells[0][skl] = "kl";
   SG_init->Cells[0][snamemodem] = "name modem";
   SG_init->Cells[0][sgaz0] = "gaz0";
   SG_init->Cells[0][stdelta_g_max] = "tdelta_g_max";
   SG_init->Cells[0][stdelta_g_min] = "tdelta_g_min";
   SG_init->Cells[0][sw_max] = "w_max";
   SG_init->Cells[0][sdelta_max] = "delta_max";
   SG_init->Cells[0][sU_end] = "U_end";

   //------------------------------------------------------------------------
   int joycount = joyGetNumDevs();
   connect_joy = false;
   if (joycount)
   {
      JOYINFOEX JoyInfo;
      JoyInfo.dwSize = sizeof(JOYINFOEX);
   	JoyInfo.dwFlags = JOY_RETURNALL;
      if (joyGetPosEx(JOYSTICKID1, &JoyInfo) == JOYERR_NOERROR)
      {
         connect_joy = true;
         jnum = JOYSTICKID1;
      }
      else if(joyGetPosEx(JOYSTICKID2,&JoyInfo) == JOYERR_NOERROR)
      {
         connect_joy = true;
         jnum = JOYSTICKID2;
      }
   }

/*   joyGetDevCaps(jnum,&JoyCaps, sizeof(JOYCAPS));
   if (connect) Memo1->Lines->Add("�������� ���������");
   else  Memo1->Lines->Add("�������� �� ���������");
   Memo1->Lines->Add("����� ���������� -  "+ IntToStr(jnum));
   Memo1->Lines->Add("������������ - " + AnsiString(JoyCaps.szPname));
   Memo1->Lines->Add("����� ������ - " +   IntToStr(JoyCaps.wNumButtons));
   Memo1->Lines->Add("������������ ���������� ���� - " + IntToStr (JoyCaps.wMaxAxes));
   if (JoyCaps.wCaps & JOYCAPS_HASPOV)
      Memo1->Lines->Add("POV - ����");
   else       Memo1->Lines->Add("POV - ���");
   if (connect)  joySetCapture(Handle,jnum,2*JoyCaps.wPeriodMin,FALSE);
   stepX = (JoyCaps.wXmax - JoyCaps.wXmin)/ PaintBox1->Width;
   stepY = (JoyCaps.wYmax - JoyCaps.wYmin)/ PaintBox1->Height;
   stepZ = (JoyCaps.wZmax - JoyCaps.wZmin)/ PaintBox2->Width;
   stepR = (JoyCaps.wRmax - JoyCaps.wRmin)/ PaintBox2->Height;
   stepU = (JoyCaps.wUmax - JoyCaps.wUmin)/ PaintBox3->Width;
   stepV = (JoyCaps.wVmax - JoyCaps.wVmin)/ PaintBox3->Height;*/
   //------------------------------------------------------------------------

   return;
}

//----------------------------------------------------------------------------
void __fastcall TTerm::FormMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
   if(Regime == 7)
   {
      Regime = 0;
      Cursor = crCross;
   }
}

//---------------------------------------------------------------------------
void __fastcall TTerm::FormMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
   if (X > RectMap.left && X < RectMap.right && Y > RectMap.top && Y < RectMap.bottom )
   {
      sMouse.x = X;
      sMouse.y = Y;
      eMouse = ToEarth(sMouse);
      if (i_mar > 0)
      {
         KPoint point = eMouse - PointMar[0];
         vektor vp = point.ToRect(Lat0);
         Hint2.sprintf("dz = %4.2f, dx = %4.2f, dr = %4.2f, al = %3.0f� ",vp.z, vp.x, !point, ToGrad*point.dir(Lat0));
      }
      Hint1 = eMouse.show();

      if (Regime == 1 && Button == mbLeft) // ������ ���������
      {
         Net0.x = (X-RectMap.left-0.5*RectMap.Width())/Zoom+Center.x;
         Net0.y = (Y-RectMap.top-0.5*RectMap.Height())/Zoom+Center.y;
         FormResize(Sender);
         Regime = 0;
      }
      else if (Regime == 2 && Button == mbLeft) // ����� �������� �����
      {
         KPoint pEditMap;
         if (FromString(SG->Cells[1][sEditMap], pEditMap))
         {
            Lat0 = pEditMap.Lat*ToGrad+0.5;
            SG->Cells[1][sEditMap] = pEditMap.show();
            Point0Map = KPoint();
            Point0Map = pEditMap - ToEarth (sMouse);
            FormResize(Sender);
         }
         Regime = 0;
      }
      else if(Regime == 6 && Button == mbLeft)  //��������� ��������
      {
         if(i_mar < 127)
         {
            PointMar[n_] = eMouse;
            SG_Mar->Cells[1][n_+1] = eMouse.show();
            if (n_ == 0)
            {
               Net0.x = (X-RectMap.left-0.5*RectMap.Width())/Zoom+Center.x;
               Net0.y = (Y-RectMap.top-0.5*RectMap.Height())/Zoom+Center.y;
               Lat0 = PointMar[0].Lat*ToGrad+0.5;
            }
            n_++;
            i_mar_pr = i_mar;
            if (n_ >= i_mar)
            {
               SG_Mar->RowCount = n_+2;
               i_mar = n_+1;
               SG->Cells[1][si_mar] = i_mar;

               char txt[20];
               sprintf(txt, "  %d", n_);
               SG_Mar->Cells[0][i_mar] = txt;
            }
            PointMar_pr = PointMar[n_];
            PointMar[n_] = eMouse;
            FormResize(Sender);
         }
         else
            MessageBox(NULL, "������� ��������� 127 �������" ,"������� ����������", MB_OK);
      }
      else if ((Button == mbRight) && Shift.Contains(ssShift))    //Zoom
      {
         flModifiedFile = true;
         Zoom0 = Zoom;
         sMouse0 = sMouse;
         Regime = 4;
         Cursor = crSizeAll;
      }
      else if (Button == mbRight)    //�����������
      {
         flModifiedFile = true;
         Center0 = Center;
         sMouse0 = sMouse;
         Regime = 7;
         Cursor = crHandPoint;
      }
   }
}

//---------------------------------------------------------------------------
void __fastcall TTerm::FormMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
   if (X > RectMap.left && X < RectMap.right && Y > RectMap.top && Y < RectMap.bottom )
   {
      sMouse.x = X;
      sMouse.y = Y;
      eMouse = ToEarth(sMouse);
      if (i_mar > 0)
      {
         KPoint point = eMouse - PointMar[0];
         vektor vp = point.ToRect(Lat0);
         Hint2.sprintf("dz = %4.2f, dx = %4.2f, dr = %4.2f, al = %3.0f� ",vp.z, vp.x, !point, ToGrad*point.dir(Lat0));
      }
      Hint1 = eMouse.show();

      if (Regime == 1)    //������ ���������
         Hint = "�������� ����� �� �����";
      else if(Regime == 2) //����������� �����
         Hint = "�������� ����� �� �����";
      else if (Regime == 4)  //zoom
      {
         if (Shift.Contains(ssRight) && Shift.Contains(ssShift))
         {
            Zoom = Zoom0*(1+3.*(double)(sMouse.y-sMouse0.y)/RectMap.Height());
            if ((double)Zoom < FLT_EPSILON)
               Zoom = FLT_EPSILON;
         }
         else
            Regime = 0;
         FormResize(Sender);
      }
      else if(Regime == 6) //����������� ����� ��������
      {
         PointMar[n_] = eMouse;
         SG_Mar->Cells[1][n_+1] = eMouse.show();
  //       Hint = "�������� ���";
         FormResize(Sender);
      }
      else if (Regime == 7)  //�������������
      {
         if (Shift.Contains(ssRight))
         {
            Center.x = Center0.x-(sMouse.x-sMouse0.x)/Zoom;
            Center.y = Center0.y-(sMouse.y-sMouse0.y)/Zoom;
         }
         else
            Regime = 0;
         FormResize(Sender);
      }
   }
   else if (Regime == 4)
         Cursor = crSizeAll;
   else if (Cursor != crHourGlass)
         Cursor = crCross;

   MyOnHint(Sender);
}

//---------------------------------------------------------------------------
void __fastcall TTerm::SGSelectCell(TObject *Sender, int ACol, int ARow,
      bool &CanSelect)
{
 	RowSG = ARow;
   ColSG = ACol;
   return;
}

//---------------------------------------------------------------------------
void __fastcall TTerm::SGKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if (RowSG == sScaleMap)
   {
      ScaleMap = atof(SG->Cells[1][sScaleMap].c_str());
      if (ScaleMap < 0.000001)
         ScaleMap = 0.000001;
   }
   else if (RowSG == sEditMap)
      Regime = 2;
   else if (RowSG == sStepNet)
   {
      Regime = 1;
      StepNet = atof(SG->Cells[1][sStepNet].c_str());
   }
   koors = napr_vzlet = atoi(SG->Cells[1][snapr_vzlet].c_str());
   i_mar = atoi(SG->Cells[1][si_mar].c_str());
   SG_Mar->RowCount = i_mar+1;
   SG_Mar->Height =  SG_Mar->RowCount*(SG_Mar->DefaultRowHeight+1)+4;
  
   flModifiedFile = true;
   FormResize(Sender);
   return;
}

//11---------------------������ ������ ���--------------------------------------
void __fastcall TTerm:: MyOnHint (TObject *Sender)
{
   StatusBar5->Panels->Items[0]->Text = Application->Hint;
   StatusBar5->Panels->Items[1]->Text = Hint1;
   StatusBar5->Panels->Items[2]->Text = Hint2;
}

//------------ ������� ����� -------------------------------------------------
void __fastcall TTerm::B4Click_Mar1(TObject *Sender)
{
  if (ColorDialog1->Execute())
  {
      flModifiedFile = true;
      ColorTraectoria = ColorDialog1->Color;
      Regime = 6;
      n_ = 0;
      i_mar = 1;
      SG_Mar->RowCount = 2;
      SG_Mar->Height =  SG_Mar->RowCount*(SG_Mar->DefaultRowHeight+1)+4;
      FormResize(Sender);
  }
}

//18----------------------������� �������---------------------------------------
void __fastcall TTerm::OpenFileMar(TObject *Sender)
{
  OpenDialog->FilterIndex = 2;                      // 2 ��������� ���� �����
  OpenDialog->FileName = "";                        // 3 ��������� ���� ��������
  OpenDialog->Title = "�������� ��������";
  OpenDialog->InitialDir = ExtractFilePath(Application->ExeName)+"\Mar";
  if (OpenDialog->Execute())
  {
      FileNameMar = OpenDialog->FileName.c_str();

      fstream file;
      file.open(FileNameMar.c_str());
      LoadFileMarsh(file);        // � �.�. ������ �������
      file.close();

      Regime = 0;
      FormResize(Sender);
   }
}

//������� ���������----------------------------------------------------------
void __fastcall TTerm::MarSave(TObject *Sender)
{
   SaveDialog->FilterIndex = 2;
   SaveDialog->DefaultExt = "mai";
   SaveDialog->Title = "���������� ��������";
   SaveDialog->InitialDir = ExtractFilePath(Application->ExeName)+"\Mar" ;

   if (SaveDialog->Execute())
   {
      FileNameMar = SaveDialog->FileName.c_str();
      SG->Cells[1][sName] = ExtractFileName(FileNameMar.c_str());

      fstream file;
      file.open(FileNameMar.c_str(), ios:: out);
      SaveFileMarsh(file);
      file.close();
      flModifiedFile = false;
      FormResize(Sender);
   }
}

//C��� -------------------------------------------------------------------------
void __fastcall TTerm::ChancelFly(TObject *Sender)
{
   OnClose = FormClose;

   Timer1->Enabled = false;
   Repaint();
   TB_StartStop->OnClick = TB_StartClick;
   TB_StartStop->ImageIndex = 5;
   TB_StartStop->Hint = "������ �������������";

   CB_TypeWork->Enabled = true;
//   ComPort->Close();

   if(file_his.is_open())
      file_his.close();

   if(MyFly)
   {
      delete MyFly;
      MyFly = NULL;
   }
   TB_Ok->Enabled = false;

   TabSheet1->Enabled = true;
   TabSheet1->Visible = true;
   TabSheet1->TabVisible = true;
}

//----------------------------------------------------------------------------
void __fastcall TTerm::FormResize(TObject *Sender)
{
   StatusBar5->Top = ClientHeight-StatusBar5->Height;
   StatusBar5->Left = 2;
   StatusBar5->Width = ClientWidth-4;

   PageControl->Left = 2;
   PageControl->Width = ClientWidth-PageControl->Left-2;
   PageControl->Top = StatusBar5->Top-PageControl->Height;

   pInit->Left = ClientWidth-300;
   pInit->Width = 300-2;
   pInit->Top = 2;
   pInit->Height = PageControl->BoundsRect.Bottom;
   
   PB->Top = 0;
   PB->Height = P3_1->Height;
   PB->Left = P3_1->Width+2;
   PB->Width = PageControl->Width-PB->Left;

   SG_Mar->Height = SG_Mar->RowCount*(SG_Mar->DefaultRowHeight+1)+4;
   SG_Mar->Height =  SG_Mar->RowCount*(SG_Mar->DefaultRowHeight+1)+4;
   if (SG_Mar->ClientHeight+SG_Mar->Top > TabSheet1->Height)
      SG_Mar->ClientHeight = TabSheet1->Height-SG_Mar->Top;

//   RectMap = TRect(StatusBar5->Left, 2, ClientWidth/2-2, PageControl->Top-2);
   RectMap = TRect(StatusBar5->Left, 2, ClientWidth-4, PageControl->Top-2);
   Map->Height = RectMap.Height();
   Map->Width = RectMap.Width();

   TRect RectMapInImage;
   RectMapInImage.Left = Center.x -0.5*RectMap.Width()/Zoom;
   RectMapInImage.Right = Center.x+0.5*RectMap.Width()/Zoom;
   RectMapInImage.Top = Center.y-0.5*RectMap.Height()/Zoom;
   RectMapInImage.Bottom = Center.y+0.5*RectMap.Height()/Zoom;

   //�������� �� ��������� ����� ��������� � ������ ����������
   Map->Canvas->Pen->Color = clBlack;
   Map->Canvas->Brush->Color = clBtnFace;
   Map->Canvas->Rectangle(Map->Canvas->ClipRect);
   Map->Canvas->CopyMode = cmSrcCopy;
   Map->Canvas->CopyRect(Map->Canvas->ClipRect, ImageTmp->Canvas, RectMapInImage);

   //�����
   if( StepNet != 0)  // �� ����� ��� �� ������, � ������ � ��������
   {
      Map->Canvas->Pen->Color = clBlack;
      Map->Canvas->Pen->Width = 1;

      int hag = StepNet*ScaleMap*Zoom;
      if (hag == 0)
         hag = 1;
      int x = (Net0.x-RectMapInImage.Left)*Zoom;
      while (x < Map->Width)          //������������ �����
      {
         Map->Canvas->MoveTo(x, 0);
         Map->Canvas->LineTo(x, Map->Height);
         x += hag;
      }
      x = (Net0.x-RectMapInImage.Left)*Zoom-hag;
      while (x > 0)                       //������������ �����
      {
         Map->Canvas->MoveTo(x, 0);
         Map->Canvas->LineTo(x, Map->Height);
         x -= hag;
      }

      int y = (Net0.y-RectMapInImage.Top)*Zoom;
      while (y < Map->Height)         //������������ �����
      {
         Map->Canvas->MoveTo(0, y);
         Map->Canvas->LineTo(Map->Width, y);
         y += hag;
      }
      y = (Net0.y-RectMapInImage.Top)*Zoom-hag;
      while (y > 0)                       //������������ �����
      {
         Map->Canvas->MoveTo(0, y);
         Map->Canvas->LineTo(Map->Width, y );
         y -= hag;
      }
   }

   if (i_mar != 0)     //���������� �������
   {
      Map->Canvas->Pen->Width = 3;
      Map->Canvas->Brush->Style = bsClear;
            Map->Canvas->Font->Color = ColorTraectoria;
            Map->Canvas->Pen->Color = ColorTraectoria;

      int radius = 0.001*40* ScaleMap*Zoom;
      POINT spos = ToMap(PointMar[0]);
      Map->Canvas->Ellipse ( spos.x - radius, spos.y - radius,
                            spos.x + radius, spos.y + radius  );

      int sizeFont = 0.001*EPSILON * ScaleMap*Zoom;
      Map->Canvas->Font->Height = sizeFont;
      Map->Canvas->Font->Style = TFontStyles()<< fsBold;
      sizeFont = Map->Canvas->Font->Height;

      String txt("0");
      int widthFont = Map->Canvas->TextWidth(txt);
      Map->Canvas->TextOutA(spos.x-widthFont/2, spos.y-sizeFont/2, txt);
/*int it = t;
TColor clBlinky = ColorTraectoria;
if (it%2 == 0)
      clBlinky = clRed;

      for(int i = 1; i < i_mar; i++)
      {
         if (n_ == i)
         {
            Map->Canvas->Font->Color = clBlinky;
            Map->Canvas->Pen->Color = clBlinky;
         }
         else
         {
            Map->Canvas->Font->Color = ColorTraectoria;
            Map->Canvas->Pen->Color = ColorTraectoria;
         }        */
      for(int i = 1; i < i_mar; i++)
      {
         Map->Canvas->Font->Color = ColorTraectoria;
         Map->Canvas->Pen->Color = ColorTraectoria;
         radius = 0.001*EPSILON * ScaleMap*Zoom;
         POINT spos = ToMap(PointMar[0]);
         Map->Canvas->Ellipse ( spos.x - radius, spos.y - radius,
                            spos.x + radius, spos.y + radius  );
         KPoint Point_i_1 = PointMar[i-1] - PointMar[0];
         vektor  v_i_1 = Point_i_1.ToRect(Lat0);
         KPoint Point_i = PointMar[i] - PointMar[0];
         vektor  v_i = Point_i.ToRect(Lat0);
         vektor dir = v_i-v_i_1;
         double fdir = !dir;
         if (fdir < FLT_EPSILON)
            continue;
         vektor vEp = dir/fdir*0.001*EPSILON;
         int sizeFont = !vEp * ScaleMap*Zoom;
         KPoint pMari_1 = PointMar[i-1]+KPoint(vEp, Lat0);
         KPoint pMari = PointMar[i]-KPoint(vEp, Lat0);

         spos = ToMap(pMari_1);
         Map->Canvas->MoveTo( spos.x, spos.y);
//         Map->Canvas->Pen->Color = ColorTraectoria;
         spos = ToMap(pMari);
         Map->Canvas->LineTo( spos.x, spos.y);

//         TColor color;
         spos = ToMap(PointMar[i]);
         Map->Canvas->Ellipse (spos.x - radius, spos.y - radius,
                            spos.x + radius, spos.y + radius);

         txt.sprintf("%d", i);
         widthFont = Map->Canvas->TextWidth(txt);
         Map->Canvas->TextOutA(spos.x-widthFont/2, spos.y-sizeFont/2, txt);
      }
   }
   DrawDevice();
   H.Draw();
   Vz.Draw();
   V.Draw();
   delta_g.Draw();
   delta_u.Draw();
   U.Draw();
   I.Draw();
   C.Draw();
//   ras.Draw();
   FormPaint(Sender);
}

//---------------------------------------------------------------------------
void __fastcall TTerm::FormClose(TObject *Sender, TCloseAction &Action)
{
   fstream file;
   if (flModifiedFile || flModifiedCoeff)
   {
      int b = Application->MessageBox("��������� ���������", "", MB_YESNOCANCEL);
      if (b == IDYES)
      {
         file.open(FileNameMar.c_str());
         SaveFileMarsh(file);
         file.close();

         file.open(FileNameCoeff.c_str());
         SaveFileCoeff(file);
         file.close();
      }
      else if (b == IDCANCEL)
      {
         FormNonClose(Sender, Action);
         return;
      }
   }

   String txt = ExtractFilePath(Application->ExeName)+INIFile;
   file.open(txt.c_str());
   file << FileNameMar <<"\n";
   file << FileNameCoeff <<"\n";
   file.close();

   if(file_his.is_open())
      file_his.close();
   if(MyFly)
   {
      delete MyFly;
      MyFly = NULL;
   }
//   ComPort->Close();
}

//---------------------------------------------------------------------------
void __fastcall TTerm::FormNonClose(TObject *Sender,
      TCloseAction &Action)
{
   Action = caNone;
}

//---------------------------------------------------------------------------
//hitec ecclips 7
void __fastcall TTerm::Timer1Timer(TObject *Sender)
{
   String txt;
   static int Upos_pr = 0;
   if (connect_joy)
   {
      JOYINFOEX JoyInfo;
      JoyInfo.dwSize = sizeof(JOYINFOEX);
   	JoyInfo.dwFlags = JOY_RETURNALL;
      if ((joyGetPosEx(jnum, &JoyInfo) == JOYERR_NOERROR) && ((rgfly == 0) || (rgfly == 1) || (rgfly == 5)))
      {
         if(rgStart == 3 || rgStart == 4)
         {
            if(JoyInfo.dwVpos > 12000)
            {
               Start();
               if (flMatmodel)
                  rgStart = 5;
            }
         }
         else if(rgStart == 6)
         {
            if(JoyInfo.dwVpos < 12000)
            {
               rgfly = 5;
            }
         }

         if ((JoyInfo.dwUpos != Upos_pr) && (rgfly != 5))
         {
            if (JoyInfo.dwUpos < 12000)   rgfly = 1;
            else                          rgfly = 0;
            Upos_pr = JoyInfo.dwUpos;
         }
         else
         {
            //����--------------------------------------------------------------
            float x0 = 33288, xleft = 9471, xright = 57473, maxx = 2;
            if(JoyInfo.dwXpos < xleft) JoyInfo.dwXpos = xleft;
            if(JoyInfo.dwXpos > xright) JoyInfo.dwXpos = xright;
            float k = kren_max/(exp(maxx)-1);
            float x = (float)(x0-JoyInfo.dwXpos)/(x0-xright);
            if (x > 0)  kren_zad = k*(exp(x*maxx)-1);
            else        kren_zad = k*(1-exp(-x*maxx));
            if(kren_zad > kren_max) kren_zad = kren_max;
            if(kren_zad < -kren_max)kren_zad = -kren_max;

            //koors-------------------------------------------------------------
            float maxr = 4, rleft = 9471, rright = 57473, dkoors_max = 1.0, dkoors, r0 = 33288;
            if (JoyInfo.dwRpos < rleft) JoyInfo.dwRpos = rleft;
            if (JoyInfo.dwRpos > rright) JoyInfo.dwRpos = rright;
            k = dkoors_max/exp(maxr);
            float r = (float)(r0-JoyInfo.dwRpos)/(r0-rright);
            if (r > 0)  dkoors = k*(exp(r*maxr)-1);
            else        dkoors = k*(1-exp(-r*maxr));
            if(fabs(dkoors) > 0.01)
            {
               if(dkoors > dkoors_max) dkoors = dkoors_max;
               if(dkoors < -dkoors_max) dkoors = -dkoors_max;
               koors_zad = koors_zad+dkoors;
               while (koors_zad >= 360)    //�������� � �������� �� 0 �� 2*M_PI
                     koors_zad = koors_zad-360;
               while (koors_zad < 0)
                     koors_zad = koors_zad+360;
            }

            //delta_g-----------------------------------------------------------
            float ztop = 57473, zbottom = 10751, z0 = 32768, maxz = 4, ddelta_g_max = 1.0, ddelta_g;
            if (JoyInfo.dwZpos > ztop) JoyInfo.dwZpos = ztop;
            if (JoyInfo.dwZpos < zbottom) JoyInfo.dwZpos = zbottom;

            k = ddelta_g_max/exp(maxz);
            float z = (float)(z0-JoyInfo.dwZpos)/(z0-zbottom);
            if (z > 0)  ddelta_g = k*(exp(z*maxz)-1);
            else        ddelta_g = k*(1-exp(-z*maxz));
            if(fabs(ddelta_g) > 0.01)
            {
               if(ddelta_g > ddelta_g_max)   ddelta_g = ddelta_g_max;
               if(ddelta_g < -ddelta_g_max)  ddelta_g = -ddelta_g_max;
               if ((rgfly == 1) || (rgfly == 5))
               {
                  float fdelta_g = delta_g.GetValue_zad()+ddelta_g;
                  if(fdelta_g > delta_g_max) fdelta_g = delta_g_max;
                  if(fdelta_g < delta_g_min) fdelta_g = delta_g_min;
                  delta_g.SetValue_zad(fdelta_g);

                  if (fdelta_g < 1)  delta_u.SetValue_zad(90);
               }
               else
               {
                  float f_H_zad = H.GetValue_zad()+ddelta_g;
                  if(f_H_zad > H_zad_max) f_H_zad = H_zad_max;
                  if(f_H_zad < H_zad_min) f_H_zad = H_zad_min;
                  H.SetValue_zad(f_H_zad);
               }
            }

            // delta_u-----------------------------------------------------------
            float y0 = 32767, ytop = 7935, ybottom = 55912, maxy = 4, ddelta_u_max = 1, ddelta_u;
            if (JoyInfo.dwYpos < ytop) JoyInfo.dwYpos = ytop;
            if (JoyInfo.dwYpos > ybottom) JoyInfo.dwYpos = ybottom;
            k = ddelta_u_max/exp(maxy);
            float y = (float)(y0-JoyInfo.dwYpos)/(y0-ybottom);
            if (y > 0)  ddelta_u = k*(exp(y*maxy)-1);
            else        ddelta_u = k*(1-exp(-y*maxy));
            if(fabs(ddelta_u) > 0.01)
            {
               if(ddelta_u > ddelta_u_max)  ddelta_u = ddelta_u_max;
               if(ddelta_u < -ddelta_u_max) ddelta_u = -ddelta_u_max;
               if((rgfly == 1) || (rgfly == 5))
               {
                     float fdelta_u = delta_u.GetValue_zad()+ddelta_u;
                     if(fdelta_u > delta_u_max*ToGrad) fdelta_u = delta_u_max*ToGrad;
                     if(fdelta_u < delta_u_min*ToGrad) fdelta_u = delta_u_min*ToGrad;

                     delta_u.SetValue_zad(fdelta_u);
               }
               else
               {
                     float f_v = V.GetValue_zad()-0.2*ddelta_u;
                     if(f_v > V_max) f_v = V_max;
                     if(f_v < V_min) f_v = V_min;

                     V.SetValue_zad(f_v);
               }
            }
         }
    /*   txt.sprintf("kren = %5.2f, delta_g = %5.2f, koors = %5.2f, delta_u = %5.2f"
         ,kren_zad*ToGrad, fdelta_g, koors_zad, fdelta_u);

         txt.sprintf("X=%d, Y=%d, Z=%d, R=%d, U=%d, V=%d, B=%d, BN=%d, POV=%d, R1=%d, R2=%d"
         ,JoyInfo.dwXpos, JoyInfo.dwYpos, JoyInfo.dwZpos, JoyInfo.dwRpos, JoyInfo.dwUpos, JoyInfo.dwVpos, JoyInfo.dwButtons,
         JoyInfo.dwButtonNumber, JoyInfo.dwPOV, JoyInfo.dwReserved1, JoyInfo.dwReserved2);

         StatusBar5->Panels->Items[3]->Text = txt;         */
      }
   }

   size_F_buffer = MyFly->read(F_buffer);
   if (flMatmodel == 0)
   {
      time_t secs_now;
      time(&secs_now);
      t = secs_now-t0;
   }
   if (AnalisBuffer())
   {
      if (rgStart > 4)   //����� �������������
      {
         String txt;
         int ch = t/3600;
         int min = t/60;
         int sek = t-min*60;
         if (ch)  txt.sprintf("%d � %d � %d �", ch, min-ch*60, sek);
         else if (min) txt.sprintf("%d � %d �", min-ch*60, sek);
         else               txt.sprintf("%d �", sek);
         SB_t->Caption = txt;
      }
   }
   if(flStoika != flStoika_bort)
   {
         if(flStoika) CoderPack3 (2, 12, RK_code);   //flStoika = true;
         else         CoderPack3 (2, 13, RK_code);   //flStoika = false;
   }
   else if (flCoeff < 3)
      flCoeff++;
   else if(flCoeff == 3)
   {
      flCoeff = 1;
	   RK_code[0] = 14 | 0x40;

      dt0_p1[iter] = atof(SG_init->Cells[1][sdt0_p1].c_str());
      OutModem2(1e+2*dt0_p1[iter]+0.5, RK_code, 1);
      dt90_p1[iter] = atof(SG_init->Cells[1][sdt90_p1].c_str());
      OutModem2(1e+2*dt90_p1[iter]+0.5, RK_code, 3);
      dt0_l1[iter] = atof(SG_init->Cells[1][sdt0_l1].c_str());
      OutModem2(1e+2*dt0_l1[iter]+0.5, RK_code, 5);
      dt90_l1[iter] = atof(SG_init->Cells[1][sdt90_l1].c_str());
      OutModem2(1e+2*dt90_l1[iter]+0.5, RK_code, 7);
      dt0_p2[iter] = atof(SG_init->Cells[1][sdt0_p2].c_str());
      OutModem2(1e+2*dt0_p2[iter]+0.5, RK_code, 9);
      dt90_p2[iter] = atof(SG_init->Cells[1][sdt90_p2].c_str());
      OutModem2(1e+2*dt90_p2[iter]+0.5, RK_code, 11);
      dt0_l2[iter] = atof(SG_init->Cells[1][sdt0_l2].c_str());
      OutModem2(1e+2*dt0_l2[iter]+0.5, RK_code, 13);
      dt90_l2[iter] = atof(SG_init->Cells[1][sdt90_l2].c_str());
      OutModem2(1e+2*dt90_l2[iter]+0.5, RK_code, 15);

      alfa0[iter] = atof(SG_init->Cells[1][salfa0].c_str())/ToGrad;
      OutModem2(alfa0[iter]*1300+8168.5, RK_code, 17);
      i_tang[iter] = atof(SG_init->Cells[1][si_tang].c_str());
      OutModem2(1e+2*i_tang[iter]+0.5, RK_code, 19);

      OutModem2(0, RK_code, 21);

      p_tang[iter] = atof(SG_init->Cells[1][sp_tang].c_str());
      OutModem2(1e+2*p_tang[iter]+0.5, RK_code, 23);
      d_tang[iter] = atof(SG_init->Cells[1][sd_tang].c_str());
      OutModem2(1e+2*d_tang[iter]+0.5, RK_code, 25);
      kl[iter] = atof(SG_init->Cells[1][skl].c_str());
      OutModem2(1e+2*kl[iter]+0.5, RK_code, 27);
      kren0[iter] = atof(SG_init->Cells[1][skren0].c_str())/ToGrad;
      OutModem2(kren0[iter]*1300+8168.5, RK_code, 29);
      i_kren[iter] = atof(SG_init->Cells[1][si_kren].c_str());
      OutModem2(1e+2*i_kren[iter]+0.5, RK_code, 31);

      p_kren[iter] = atof(SG_init->Cells[1][sp_kren].c_str());
      OutModem2(1e+2*p_kren[iter]+0.5, RK_code, 33);
      d_kren[iter] = atof(SG_init->Cells[1][sd_kren].c_str());
      OutModem2(1e+2*d_kren[iter]+0.5, RK_code, 35);
      gaz0[iter] = atof(SG_init->Cells[1][sgaz0].c_str());
      OutModem2(1e+2*gaz0[iter]+0.5, RK_code, 37);
      i_rsk[iter] = atof(SG_init->Cells[1][si_rsk].c_str());
      OutModem2(1e+2*i_rsk[iter]+0.5, RK_code, 39);
      p_rsk[iter] = atof(SG_init->Cells[1][sp_rsk].c_str());
      OutModem2(1e+2*p_rsk[iter]+0.5, RK_code, 41);
      d_rsk[iter] = atof(SG_init->Cells[1][sd_rsk].c_str());
      OutModem2(1e+2*d_rsk[iter]+0.5, RK_code, 43);

      w_max[iter] = atof(SG_init->Cells[1][sw_max].c_str());
      OutModem2(1e+2*w_max[iter]+0.5, RK_code, 45);
      delta_max[iter] = atof(SG_init->Cells[1][sdelta_max].c_str());
      OutModem2(1e+2*delta_max[iter]+0.5, RK_code, 47);

      RK_code[49] = 0;
      for (int i = 0; i < 49; i++ )
         RK_code[49] = RK_code[49] ^ RK_code[i];
      OutModem1(RK_code[49], RK_code, 49);
      RK_code[50] = 0;
   }
   else if (flCoeff < 6)
      flCoeff++;
   else if(flCoeff == 6)
   {
      flCoeff = 4;
	   RK_code[0] = 15 | 0x40;

      i_ay[iter] = atof(SG_init->Cells[1][si_ay].c_str());
      OutModem2(1e+2*i_ay[iter]+0.5, RK_code, 1);
      p_ay[iter] = atof(SG_init->Cells[1][sp_ay].c_str());
      OutModem2(1e+2*p_ay[iter]+0.5, RK_code, 3);

      OutModem2(0, RK_code, 5);

      tdelta_g_min[iter] = atof(SG_init->Cells[1][stdelta_g_min].c_str());
      OutModem2(1e+2*tdelta_g_min[iter]+0.5, RK_code, 7);

      i_ax[iter] = atof(SG_init->Cells[1][si_ax].c_str());
      OutModem2(1e+2*i_ax[iter]+0.5, RK_code, 9);
      p_ax[iter] = atof(SG_init->Cells[1][sp_ax].c_str());
      OutModem2(1e+2*p_ax[iter]+0.5, RK_code, 11);
      tdelta_g_max[iter] = atof(SG_init->Cells[1][stdelta_g_max].c_str());
      OutModem2(1e+2*tdelta_g_max[iter]+0.5, RK_code, 13);

      v_rsk[iter] = atof(SG_init->Cells[1][sv_rsk].c_str());
      OutModem2(1e+2*v_rsk[iter]+0.5, RK_code, 17);
      g_kren[iter] = atof(SG_init->Cells[1][sg_kren].c_str());
      OutModem2(1e+2*g_kren[iter]+0.5, RK_code, 19);
      g_rsk[iter] = atof(SG_init->Cells[1][sg_rsk].c_str());
      OutModem2(1e+2*g_rsk[iter]+0.5, RK_code, 21);
      v_kren[iter] = atof(SG_init->Cells[1][sv_kren].c_str());
      OutModem2(1e+2*v_kren[iter]+0.5, RK_code, 23);
      g_tang[iter] = atof(SG_init->Cells[1][sg_tang].c_str());
      OutModem2(1e+2*g_tang[iter]+0.5, RK_code, 25);
      v_tang[iter] = atof(SG_init->Cells[1][sv_tang].c_str());
      OutModem2(1e+2*v_tang[iter]+0.5, RK_code, 27);
      int_dtang0[iter] = atof(SG_init->Cells[1][sint_dtang0].c_str());
      OutModem2(1e+2*int_dtang0[iter]+8000.5, RK_code, 29);
      dH_max[iter] = atof(SG_init->Cells[1][sdH_max].c_str());
      OutModem2(1e+2*dH_max[iter]+0.5, RK_code, 31);
      d2_tang[iter] = atof(SG_init->Cells[1][sd2_tang].c_str());
      OutModem2(1e+2*d2_tang[iter]+8000.5, RK_code, 33);
      d2_kren[iter] = atof(SG_init->Cells[1][sd2_kren].c_str());
      OutModem2(1e+2*d2_kren[iter]+8000.5, RK_code, 35);
      d2_rsk[iter] = atof(SG_init->Cells[1][sd2_rsk].c_str());
      OutModem2(1e+2*d2_rsk[iter]+8000.5, RK_code, 37);

      kren_zad_wy[iter] = atof(SG_init->Cells[1][skren_zad_wy].c_str());
      OutModem2(1e+2*kren_zad_wy[iter]+0.5, RK_code, 39);

      d_ay[iter] = atof(SG_init->Cells[1][sd_ay].c_str());
      OutModem2(1e+2*d_ay[iter]+0.5, RK_code, 41);

      d_ax[iter] = atof(SG_init->Cells[1][sd_ax].c_str());
      OutModem2(1e+2*d_ax[iter]+0.5, RK_code, 43);

      U_end[iter] = atof(SG_init->Cells[1][sU_end].c_str());
      OutModem2(1e+2*U_end[iter]+0.5, RK_code, 45);
      OutModem2(0, RK_code, 47);

      RK_code[49] = 0;
      for (int i = 0; i < 49; i++ )
         RK_code[49] = RK_code[49] ^ RK_code[i];
      OutModem1(RK_code[49], RK_code, 49);
      RK_code[50] = 0;
   }
   else if(rgfly != rgfly_bort)
   {
      if (rgfly == 0)  //���������� ������ ���������� ������ � �������� ����(��������: �����-������, ������-�����), � ����� (�� �����: j - m)
      {
         if(rgfly_bort > 1)
         {
            if(flKoord)
               koors_zad = koors;
            else
               koors_zad = 360-ToGrad*rsk_gir;
            while (koors_zad >= 360)    //�������� � �������� �� 0 �� 2*M_PI
               koors_zad = koors_zad-360;
            while (koors_zad < 0)
               koors_zad = koors_zad+360;
         }
         if(rgfly_bort == 1)
         {
            float H_zad = H.GetValue()+Vy;
				if (H_zad < 100) 	H_zad = 100;
            if (H_zad > H_zad_max) H_zad = H_zad_max;
            H.SetValue_zad(H_zad);      //��������� �������� �� ����� �� ���������

            V.SetValue_zad(15);

/*            float f_v = V.GetValue();
            if (f_v > V_max) f_v = V_max;
            if (f_v < V_min) f_v = V_min;
            V.SetValue_zad(f_v);*/
         }
         CoderPack3  (2, 5, RK_code);
      }
      else if(rgfly == 1)
      {
         CoderPack3  (2, 2, RK_code);
         if((rgfly_bort == 2) || (rgfly_bort == 4))
         {
            koors_zad = koors_zad_bort;
            while (koors_zad >= 360)    //�������� � �������� �� 0 �� 2*M_PI
               koors_zad = koors_zad-360;
            while (koors_zad < 0)
               koors_zad = koors_zad+360;
         }
      }
      else if (rgfly == 2)   //���������� �����
      {
         CoderPack3  (4, n_, RK_code);
      }
      else if (rgfly == 4)    //����� ��������
      {
         CoderPack3  (2, 10, RK_code);
      }
      else if((rgfly_bort == 5) || (rgfly == 5))
      {
         rgfly = 5;
         ::MessageBeep(0x30);

         CoderPack3  (2, 3, RK_code);  //stop
         if((rgfly_bort == 2) || (rgfly_bort == 4))
         {
            koors_zad = koors_zad_bort;
            while (koors_zad >= 360)    //�������� � �������� �� 0 �� 2*M_PI
               koors_zad = koors_zad-360;
            while (koors_zad < 0)
               koors_zad = koors_zad+360;
         }
         if(rgStart > 5)   //��������� � ��������
         {
            delta_g.SetValue_zad(gaz0[iter]);
            delta_u.SetValue_zad(90);
         }
      }
   }
   if (rgfly == 0)   //���������
   {
      if ((int)koors_zad != koors_zad_bort)
         CoderPack4(3, koors_zad, RK_code);
      if (fabs(kren_zad-kren_zad_bort) > 10.0/2500.0)
         CoderPack4(5, kren_zad*2500+8000.5, RK_code);
      if (H.Command())
         CoderPack4(1, 10*H.GetValue_zad()+1000.5, RK_code);
      if(V.Command())
         CoderPack4(6, 10*(V.GetValue_zad()+10)+0.5, RK_code);
   }
   if ((rgfly == 1) || (rgfly == 5))
   {
      if ((int)koors_zad != koors_zad_bort)
         CoderPack4(3, koors_zad, RK_code);
      if (fabs(kren_zad-kren_zad_bort) > 10.0/2500.0)
         CoderPack4(5, kren_zad*2500+8000.5, RK_code);
      if(delta_g.Command())
         CoderPack4(7, delta_g.GetValue_zad()*10.0+0.5, RK_code);
      if(delta_u.Command())
         CoderPack4(13, delta_u.GetValue_zad()*10.0+0.5, RK_code);
      if(flStoika)
      {
         if(fabs(V.GetValue()-V.GetValue_zad()) > 0.1)
            CoderPack4(6, 10*(V.GetValue_zad()+10)+0.5, RK_code);
      }
   }
   else if (rgfly == 2)
   {
      if(n_ != n_bort)
         CoderPack3  (4, n_, RK_code);
   }
   else if (rgfly == 3)
         CoderPack13 (PointMar[n_3], H_Mar[n_3], Vz_Mar[n_3], RK_code);

   if( kren_kam_zad != kren_kam_zad_bort )
         CoderPack3(8, kren_kam_zad+60, RK_code);
   if( ugol_kam_zad != ugol_kam_zad_bort )
         CoderPack3(9, ugol_kam_zad, RK_code);

   if (fabs(t-t_pr) > 0.3)
         CoderPack3  ( 2, 1, RK_code);
   if  (strlen(RK_code))
         t_pr = t;

   if(MyFly)
   {
      MyFly->write(RK_code);

      if (int d = strlen(RK_code))
      {
        for (int i = 0; i < d; i++)
           file_his.put(RK_code[i]);
      }
      AnalisRK_code();
   }
   return;
}

//---------------------------------------------------------------------------
void __fastcall TTerm::FormPaint(TObject *Sender)
{
   static bool schet;
   if(ImageTmp->Width == 0)
   {
      Canvas->Pen->Color = clBlack;
      Canvas->Brush->Color = clBtnFace;
      Canvas->Rectangle(RectMap);
      return;
   }
   Canvas->CopyMode = cmSrcCopy;
   Canvas->CopyRect(RectMap, Map->Canvas, Map->Canvas->ClipRect);

   ////������� �������� ����� ------------------------------------------------
   if (i_mar)
   {
      TColor color;
      if(schet)   color = ColorTraectoria;
      else        color = !ColorTraectoria;
      schet = !schet;
      Canvas->Pen->Width = 3;
      Canvas->Brush->Style = bsClear;
      Canvas->Pen->Color = color;
      Canvas->Font->Color = color;
      double fepsilon = EPSILON;

      int radius = 0.001*fepsilon * ScaleMap*Zoom;
      POINT spos = ToScreen(PointMar[n_]);
      Canvas->Ellipse (spos.x-radius, spos.y-radius, spos.x+radius, spos.y+radius);

      int sizeFont = 0.001*EPSILON * ScaleMap*Zoom;
      Canvas->Font->Height = sizeFont;
      Canvas->Font->Style = TFontStyles()<< fsBold;
      sizeFont = Canvas->Font->Height;

      String txt; txt.sprintf("%d", n_);
      int widthFont = Canvas->TextWidth(txt);
      Canvas->TextOutA(spos.x-widthFont/2, spos.y-sizeFont/2, txt);
   }
      //���������� ������----------------------------------------------------
//      if(PointFly.Lat != 100)
      {
         POINT sp = ToScreen(PointFly);
         if (flKoord)
         {
            if (Canvas->Pen->Color == clYellow)
            {
               Canvas->Pen->Color = clBlue;
               Canvas->Brush->Color = clBlue;
            }
            else
            {
               Canvas->Pen->Color = clYellow;
               Canvas->Brush->Color = clYellow;
            }
         }
         else
         {
            Canvas->Pen->Color = clBlack;
            Canvas->Brush->Color = clBlack;
         }
         Canvas->Brush->Style = bsClear;
         Canvas->Pen->Width  = 3;
         TPoint points[16];

         double kk = 1.5;
         double Value_r;
/*         if(flKoord)
            Value_r = koors/ToGrad;
         else                       */
            Value_r = 2*M_PI-rsk_gir;

         for (int i = 0; i < cpPlane; i++)
         {
            points[i].x = sp.x+(sin(Value_r)*x_Plane[i]+cos(Value_r)*z_Plane[i])/kk;
            points[i].y = sp.y-(cos(Value_r)*x_Plane[i]-sin(Value_r)*z_Plane[i])/kk;
         }
         Canvas->Polygon(points, cpPlane-1);

         if (rgfly < 2)
         {
            Value_r = (2*M_PI-koors_zad)/ToGrad+M_PI_2;
            Canvas->Pen->Color = clRed;
            for (int i = 0; i < cpStrelka; i++)
            {
               points[i].x = sp.x+(cos(Value_r)*x_Strelka[i]-sin(Value_r)*y_Strelka[i])/kk;
               points[i].y = sp.y-(sin(Value_r)*x_Strelka[i]+cos(Value_r)*y_Strelka[i])/kk;
            }
            Canvas->Polygon(points, cpStrelka-1);
         }
//            Value_r = (2*M_PI-koors_zad1)/ToGrad+M_PI_2;
/*         Value_r = rsk_gir+M_PI_2;
         Canvas->Pen->Color = clFuchsia;
         for (int i = 0; i < cpStrelka; i++)
         {
               points[i].x = sp.x+(cos(Value_r)*x_Strelka[i]-sin(Value_r)*y_Strelka[i])/kk;
               points[i].y = sp.y-(sin(Value_r)*x_Strelka[i]+cos(Value_r)*y_Strelka[i])/kk;
         }
         Canvas->Polygon(points, cpStrelka-1);*/
      }
   return;
}

//---------------------------------------------------------------------------
unsigned int TTerm::FromFly2(char mess[], char i)
{
   return((unsigned int)mess[i]& 0x7f)+((mess[i+1]& 0x7f)<< 7) ;
}
//---------------------------------------------------------------------------
unsigned long int TTerm::FromFly4(char mess[], char i)
{
   unsigned long ret = ((unsigned long int)mess[i]& 0x7f)+((mess[i+1]& 0x7f)<< 7)
         +((mess[i+2]& 0x7f)<< 14)+((mess[i+3]& 0x7f)<< 21);
   return ret;
}

//---------------------------------------------------------------------------
void TTerm:: CoderPack3(char NPackage, char Data, char mess[])
{
   char i = strlen(mess);
	mess[i++] = NPackage | 0x40;
	mess[i++] = (Data & 0x7f) | 0x80;
	mess[i++] = (mess[i-1]^mess[i-2]) | 0x80;
   mess[i] = 0;
   return;
}
//---------------------------------------------------------------------------
void TTerm:: CoderPack4(char NPackage, int Data, char mess[])
{
   char i = strlen(mess);
/*   if(i >= 46)
      return;*/
	mess[i++] = NPackage | 0x40;
	mess[i++] = (Data & 0x7f) | 0x80;
   mess[i++] = ((Data & 0x3f80   ) >> 7)  | 0x80;
   mess[i++] = (mess[i-3]^mess[i-2]^mess[i-1]) | 0x80;
   mess[i] = 0;
   return;
}
//---------------------------------------------------------------------------
void TTerm:: CoderPack13(const KPoint & pTag, int H_zad, int Vz_zad, char mess[])
{
   char i = strlen(mess);
/*   if(i >= 37)
      return;
  */
   double fLat = (pTag.Lat*ToGrad+90)*60*10000;
   unsigned long int Data1 = fLat;
   double fLon = (pTag.Lon*ToGrad+180)*60*10000;
   unsigned long int Data2 = fLon;

   char k = i;
	mess[i++] = 11 |  0x40;

	mess[i++] = ( Data1 & 0x007f   ) |  0x80;
	mess[i++] = ((Data1 & 0x3f80   ) >> 7)  | 0x80;
	mess[i++] = ((Data1 & 0x1fc000 ) >> 14) | 0x80;
	mess[i++] = ((Data1 & 0xfe00000) >> 21) | 0x80;

	mess[i++] = ( Data2 & 0x007f   ) |  0x80;
	mess[i++] = ((Data2 & 0x3f80   ) >> 7 ) | 0x80;
	mess[i++] = ((Data2 & 0x1fc000 ) >> 14) | 0x80;
	mess[i++] = ((Data2 & 0xfe00000) >> 21) | 0x80;

   mess[i++] = ((H_zad+1000)/50) |  0x80;
   mess[i++] = Vz_zad | 0x80;
   mess[i++] = n_3 | 0x80;

	char KontrSumma = 0;
	for (int j = 0 ; j < 12; j++)
		KontrSumma = KontrSumma^mess[k+j];
	mess[i++] = KontrSumma | 0x80;
   mess[i] = 0;
   return;
}

//---------- ������� ���� �������� -------------------------------------------
void TTerm:: LoadFileMarsh(fstream &file)
{
   string tmp;
   file.seekg(0);
   file >> FileNameImage;

start_load_from_file:
   try{
      Screen->Cursor = crHourGlass;
      ImageTmp->LoadFromFile(FileNameImage.c_str());
      Screen->Cursor = crDefault;
   }
   catch(...){
      OpenDialog->FilterIndex = 3;
      OpenDialog->FileName = ExtractFileName(FileNameImage.c_str());
      OpenDialog->Title = "�������� �����������";
      if (OpenDialog->Execute())
      {
         flModifiedFile = true;
         FileNameImage = OpenDialog->FileName.c_str();
         goto start_load_from_file;
      }
      else
         return;
   }

   if(GetPos (file, "ScaleMap"))
      file >> ScaleMap;
   if(GetPos (file, "Net0"))
      file >> Net0.x >> tmp >> Net0.y;
   if(GetPos (file, "StepNet"))
      file >> StepNet;
   if(GetPos (file, "Point0Map"))
      file >> Point0Map;
   if(GetPos (file, "napr_vet"))
      file >> napr_vet;
   if(GetPos (file, "V_vet"))
      file >> V_vet;
   if(GetPos (file, "Zoom"))
      file >> Zoom;
   if(GetPos (file, "Center"))
      file >> Center.x >> tmp >> Center.y;

   if(GetPos (file, "napr_vzlet"))
      file >> napr_vzlet;
   if(GetPos (file, "ColorTraectoria"))
      file >> (long)ColorTraectoria;
   if(GetPos (file, "TypeWork"))
      file >> TypeWork;
   if(GetPos (file, "i_mar"))
      file  >> i_mar;
   String depo; char ch;
   for(int i = 0; i < i_mar; i++)
   {
      depo.sprintf("PointMar%d", i);
      if(GetPos (file, depo.c_str()))
         file >> PointMar[i] >> H_Mar[i] >> ch >> Vz_Mar[i];
   }
   Lat0 = PointMar[0].Lat*ToGrad+0.5;


   String txt;
   SG->Cells[1][sName] = ExtractFileName(FileNameMar.c_str());
   SG->Cells[1][sFileName] = ExtractFileName(FileNameImage.c_str());
   SG->Cells[1][sStepNet] = txt.sprintf("%5.3f", StepNet);
   SG->Cells[1][sScaleMap] = txt.sprintf("%f", (double)ScaleMap);
   SG->Cells[1][snapr_vet] = txt.sprintf("%d", (int)napr_vet);
   SG->Cells[1][sV_vet] = txt.sprintf("%d", (int)V_vet);
   SG->Cells[1][snapr_vzlet] =  txt.sprintf("%d", (int)napr_vzlet);
   SG->Cells[1][si_mar] = i_mar;

  PointFly = PointMar[0];
  koors = napr_vzlet;
//   PointFly.Lat = 100;
   SG_Mar->RowCount = i_mar+1;

   for (int i = 0; i < i_mar; i++)
   {
      char txt[20];
      sprintf(txt, "  %d", i);
      SG_Mar->Cells[0][i+1] = txt;
      SG_Mar->Cells[1][i+1] = PointMar[i].show();
      SG_Mar->Cells[2][i+1] = H_Mar[i];
      SG_Mar->Cells[3][i+1] = Vz_Mar[i];
   }
   return;
}

//16---------------------��������� ���� ��������--------------------------------
void TTerm:: SaveFileMarsh(fstream &file)
{
   file << FileNameImage << "\n";
   file << "ScaleMap = " << ScaleMap << "\n";
   file << "Point0Map = "; file << Point0Map <<  "\n";
   file << "StepNet = " << StepNet <<  "\n";;
   file << "Net0 = " << Net0.x << ", " << Net0.y <<  "\n";
   file << "Zoom = " << Zoom <<  "\n";
   file << "Center = " << Center.x << ", " << Center.y <<  "\n";
   file << "napr_vet = " << napr_vet <<  "\n";
   file << "V_vet = " << V_vet <<  "\n";
   file << "TypeWork = " << TypeWork  <<  "\n";
   file << "napr_vzlet = " << napr_vzlet <<  "\n";
   file << "ColorTraectoria = " << (long)ColorTraectoria <<  "\n";
   file << "i_mar = " << i_mar <<  "\n";
   String depo;
   for (int i = 0; i < i_mar; i++)
   {
      depo.sprintf("PointMar%d = ",i);
      file << depo.c_str();
      file << PointMar[i] << ", " << H_Mar[i] << ", " << Vz_Mar[i] << "\n";
   }
}

//------------------------------------------------------------------------------
char TTerm:: AnalisBuffer(void)
{
   String NameOfPackage;
   String txt;
   static char NPackage = -1, KontrSumma = 0, BufShort[100], nByte = 0;
   static float t_pr_error = -1;
   char flRun = 0;

  	for (int i = 0; i < size_F_buffer; i++)
   {
      if( (F_buffer[i] & 0xc0) == 0x40)    // ������ ��������� ��������
      {
         if ((t-t_pr_error) > FLT_EPSILON)
            n_error = n_error+(float)nByte/(t-t_pr_error)*0.8e-3*Timer1->Interval;
         txt.sprintf("error = %4.1f b", n_error);
         Panel2->Caption = txt;
         KontrSumma = nByte = 0;
         NPackage = F_buffer[i] & 0x3f;
		}
		if (nByte > 99) nByte = 99;
      file_his.put(F_buffer[i]);
		BufShort[nByte] = F_buffer[i] & 0x7f;
		KontrSumma = KontrSumma^BufShort[nByte++];

      if ((NPackage == 20) && (nByte == 13) && (KontrSumma == 0))	//����������
      {
         Memo->Lines->Add("<< ����������");
         nByte = 0;
         flRun++;

         if (BufShort[4] == 0x7f)
         {
            flKoord = false;

            P_ZX->Font->Color = clRed;
            P_ZX->Caption = "����-�� �����������";
            P_PointFly->Font->Color = clRed;
            P_PointFly->Caption = "����-�� �����������";
         }
         else
         {
            flKoord = true;

            P_ZX->Font->Color = clBlack;
            P_PointFly->Font->Color = clBlack;

            unsigned long fLat = FromFly4(BufShort, 1); //������
            PointFly.Lat =  (0.0001*fLat/60.-90)/ToGrad;
            unsigned long fLon = FromFly4(BufShort, 5);//�������
            PointFly.Lon =  (0.0001*fLon/60.-180)/ToGrad;

            Map->Canvas->Pen->Color = ColorTraectoria;
            Map->Canvas->Brush->Color = ColorTraectoria;
            POINT spos = ToMap(PointFly);
            Map->Canvas->Ellipse (spos.x-2, spos.y-2, spos.x+2, spos.y+2);

            KPoint p = PointFly - PointMar[0];
            vektor vdr = p.ToRect(Lat0);
            P_ZX->Caption = NameOfPackage.sprintf("dz=%4.1f, dx=%4.1f, dr=%4.1f, al=%3.0f� ",vdr.z, vdr.x, !vdr, ToGrad*p.dir(Lat0));      //???
            P_PointFly->Caption = PointFly.show();

            koors = FromFly2(BufShort, 9);     //�� 0 �� 360
            Vz.SetValue (BufShort[11]);
         }
     }
      //�������� ��������� --------------------------------------------------
      else if ((NPackage == 21) && (nByte == 12) && (KontrSumma == 0))
      {
         nByte = 0;
         flRun++;
         Memo->Lines->Add("<< ��������");

         unsigned char Dat9 = BufShort[9]; //��������� ������
         rgStart = (Dat9 & 0x1c) >> 2;
         if(rgStart > 1 && rgStart < 5)   TB_Test->Enabled = true;
         else                             TB_Test->Enabled = false;
         String txt; txt.sprintf("rgStart = %d", rgStart);
         Panel->Caption = txt;
/*         if (rgStart > 3)
            TB_Ok->Enabled = true;
         else
            TB_Ok->Enabled = false; */

         unsigned char Dat10 = BufShort[10]; //��������� ������
         flStoika_bort = Dat10 & 0x08;

         //-------------------------------------------------------------------
         rgfly_bort = Dat10 & 0x07;
         txt.sprintf("rgfly = %d", rgfly_bort);
         SB_RegimeFly->Caption = txt;

         H.SetValue_B_zad (0.1*FromFly2(BufShort, 1)-100);
         koors_zad_bort = BufShort[4];
        	koors_zad_bort = BufShort[3]+(koors_zad_bort << 7);
         n_bort = BufShort[5];

         kren_kam_zad_bort = -60+BufShort[6];
         ugol_kam_zad_bort = BufShort[7];
         DrawDevice();
      }
      else if((NPackage == 22) && (rgStart < 5) && (nByte == 13) && (KontrSumma == 0)) //�������� � xdata ����� ��������
      {
         nByte = 0;
         flRun++;

         KPoint pCheckMar;
         unsigned long int uliLat = FromFly4(BufShort, 1); //������
         pCheckMar.Lat =  ((double)uliLat/10000./60.-90)/ToGrad;
         unsigned long int uliLon = FromFly4(BufShort, 5);//�������
         pCheckMar.Lon =  ((double)uliLon/10000./60.-180)/ToGrad;
         int HH = ((int)BufShort[9]& 0x7f)*50-1000;
         int VV = (int)BufShort[10];

         char nn_ = BufShort[11];
         if(nn_ == n_3)
         {
            NameOfPackage.sprintf("<< %d ����a", n_3);
            Memo->Lines->Add(NameOfPackage);
            if ((pCheckMar == PointMar[n_3]) && (HH == H_Mar[n_3]) && (VV == Vz_Mar[n_3]))
            {
               n_3++;          // ������� ��������� ����� � �������� �
               if (n_3 == i_mar)
               {
                  TB_Ok->Enabled = true;
                  n_ = 1;
                  rgfly = 1;
                  rgfly_bort = 0;
                  F_buffer[0] = 0;
                  flCoeff = 1;
                  return 0;
               }
            }
        }
      }
  		else if ((NPackage  == 23) && (nByte == 3) && (KontrSumma == 0))
		{
         nByte = 0;
         flRun++;

         TB_Test->Enabled = false;
         TB_Ok->Enabled = false;

         Memo->Lines->Add("<< ������������� ��");
      }
  		else if ((NPackage  == 24) && (nByte == 70) && (KontrSumma == 0))
		{
         nByte = 0;
         Memo->Lines->Add("<< ���������� 4");

         delta_g.SetValue_B_zad(0.1*FromFly2(BufShort, 9));

         last_reset_source = BufShort[13] & 0x7f;
         count_last_reset_source = BufShort[42] & 0x7f;

//         Vy = 0.1*FromFly2(BufShort, 16)-10;          //V_gir.y
         kren_gir = ((float)FromFly2(BufShort, 26)-8168)/1300;
         tang_gir = ((float)FromFly2(BufShort, 28)-8168)/1300;
         rsk_gir = ((float)FromFly2(BufShort, 30)-8168)/1300;

         float wx_gir1 = ((float)FromFly2(BufShort, 32)-8000)/2500;
         float wy_gir1 = ((float)FromFly2(BufShort, 34)-8000)/2500;
         float wz_gir1 = ((float)FromFly2(BufShort, 36)-8000)/2500;
         txt.sprintf(" wx_gir=%5.2f, wy_gir=%5.2f, wz_gir=%5.2f, ax_dat=%5.2f, ay_dat=%5.2f, az_dat=%5.2f", ToGrad*wx_gir1, ToGrad*wy_gir1, ToGrad*wz_gir1, ax_dat, ay_dat, az_dat);
         StatusBar5->Panels->Items[3]->Text = txt;     
  
         kren_zad_bort = FromFly2(BufShort, 38);
         kren_zad_bort = (kren_zad_bort-8000)/2500;
         V.SetValue_B_zad(0.1*FromFly2(BufShort, 40)-10);
//         float fdelta_g_bort = 0.1*FromFly2(BufShort, 43);
         delta_g.SetValue(0.1*FromFly2(BufShort, 43));

//         H.SetValue (0.1*FromFly2(BufShort, 45)-100);    //H_filtr
         Vy = 0.1*FromFly2(BufShort, 47)-800;   //Vy_filtr
         V.SetValue (0.1*FromFly2(BufShort, 49)-10); //V_dat
         tang_zad_bort = ((float)FromFly2(BufShort, 53)-8168)/1300;

         delta_u.SetValue(0.1*FromFly2(BufShort, 57));
/*         unsigned int fl_AKB = BufShort[59]& 0x7f;
         unsigned int RSTSRC = BufShort[60]& 0x7f;
         unsigned int countRST = BufShort[61]& 0x7f;*/
      }
  		else if ((NPackage  == 25) && (nByte == 70) && (KontrSumma == 0))
		{
         nByte = 0;
         Memo->Lines->Add("<< ���������� 5");
         delta_u.SetValue_B_zad(0.1*FromFly2(BufShort, 3));

         H.SetValue (0.1*FromFly2(BufShort, 8)-100);      //H_dat
//         Vy = 0.1*FromFly2(BufShort, 10)-800;   //Vy_dat

         U.SetValue (0.01*FromFly2(BufShort, 15));
         I.SetValue (0.1*FromFly2(BufShort, 27));
         float rsk_m = ((float)FromFly2(BufShort, 40)-8168)/1300*ToGrad;
/*         txt.sprintf(" rsk_m = %5.2f", rsk_m);
         StatusBar5->Panels->Items[3]->Text = txt;
  */
         C.SetValue (0.1*FromFly2(BufShort, 42));

//         H.SetValue (0.1*FromFly2(BufShort, 44)-100);      //H_gir
         koors_zad1 = 2.0*BufShort[46];
         unsigned char Dat47 = BufShort[47];
         if(Dat47 & 1)
             Panel1->Caption = "Stop";
         else
             Panel1->Caption = "";
         unsigned char Dat48 = BufShort[48]; //��������� ������
         bool flOtkazRK = (Dat48 & 0x08) >> 3;
         if (flOtkazRK)
         {
               Panel4->Font->Color = clRed;
               Panel4->Color = clWhite;
               Panel4->Caption = "����� ��";
         }
         else
         {
               Panel4->Caption = "";
         }

         ax_dat = 0.1*FromFly2(BufShort, 53)-800;
         ay_dat = 0.1*FromFly2(BufShort, 55)-800;
         az_dat = 0.1*FromFly2(BufShort, 57)-800;

         float H_son = 0.1*FromFly2(BufShort, 61)-100;
         txt.sprintf(" H_son=%5.2f", H_son);
         Panel5->Caption = txt;

         char ggg = BufShort[63];
         txt.sprintf("rst_src = %d, count = %d ggg = %d", last_reset_source, count_last_reset_source, ggg);
         Panel6->Caption = txt;

/*         txt.sprintf(" ax_dat=%5.2f, ay_dat=%5.2f, az_dat=%5.2f", ax_dat, ay_dat, az_dat);
         StatusBar5->Panels->Items[3]->Text = txt;     */

         DrawDevice();
      }
      else if((NPackage == 26) && (nByte == 50) && (KontrSumma == 0))
      {
         Memo->Lines->Add("<< ������������� 1");

         flCoeff = 4;
         float tmp = 1e-2*FromFly2(BufShort, 1);
         if(fabs(dt0_p1[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 3);
         if(fabs(dt90_p1[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 5);
         if(fabs(dt0_l1[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 7);
         if(fabs(dt90_l1[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 9);
         if(fabs(dt0_p2[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 11);
         if(fabs(dt90_p2[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 13);
         if(fabs(dt0_l2[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 15);
         if(fabs(dt90_l2[iter]-tmp) >= 0.01)
            flCoeff = 1;

         tmp = ((float)FromFly2(BufShort, 17)-8168)/1300;
         if(fabs(alfa0[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 19);
         if(fabs(i_tang[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 23);
         if(fabs(p_tang[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 25);
         if(fabs(d_tang[iter]-tmp) >= 0.01)
            flCoeff = 1;

         tmp = 1e-2*FromFly2(BufShort, 27);
         if(fabs(kl[iter]-tmp) >= 0.01)
            flCoeff = 1;

         tmp = ((float)FromFly2(BufShort, 29)-8168)/1300;
         if(fabs(kren0[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 31);
         if(fabs(i_kren[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 33);
         if(fabs(p_kren[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 35);
         if(fabs(d_kren[iter]-tmp) >= 0.01)
            flCoeff = 1;

         tmp = 1e-2*FromFly2(BufShort, 37);
         if(fabs(gaz0[iter]-tmp) >= 0.01)
            flCoeff = 1;

         tmp = 1e-2*FromFly2(BufShort, 39);
         if(fabs(i_rsk[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 41);
         if(fabs(p_rsk[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 43);
         if(fabs(d_rsk[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 45);
         if(fabs(w_max[iter]-tmp) >= 0.01)
            flCoeff = 1;
         tmp = 1e-2*FromFly2(BufShort, 47);
         if(fabs(delta_max[iter]-tmp) >= 0.01)
            flCoeff = 1;

      }
      else if((NPackage == 27) && (nByte == 50) && (KontrSumma == 0))
      {
         Memo->Lines->Add("<< ������������� 2");

         flCoeff = 7;
         float tmp = 1e-2*FromFly2(BufShort, 1);
         if(fabs(i_ay[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 3);
         if(fabs(p_ay[iter]-tmp) >= 0.01)
            flCoeff = 4;

         tmp = 1e-2*FromFly2(BufShort, 7);
         if(fabs(tdelta_g_min[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 9);
         if(fabs(i_ax[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 11);
         if(fabs(p_ax[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 13);
         if(fabs(tdelta_g_max[iter]-tmp) >= 0.01)
            flCoeff = 4;

         tmp = 1e-2*FromFly2(BufShort, 17);
         if(fabs(v_rsk[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 19);
         if(fabs(g_kren[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 21);
         if(fabs(g_rsk[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 23);
         if(fabs(v_kren[iter]-tmp) >= 0.01)
            flCoeff = 4;

         tmp = 1e-2*FromFly2(BufShort, 25);
         if(fabs(g_tang[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 27);
         if(fabs(v_tang[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 29)-80;
         if(fabs(int_dtang0[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 31);
         if(fabs(dH_max[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 33)-80;
         if(fabs(d2_tang[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 35)-80;
         if(fabs(d2_kren[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 37)-80;
         if(fabs(d2_rsk[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 39);
         if(fabs(kren_zad_wy[iter]-tmp) >= 0.01)
            flCoeff = 4;

         tmp = 1e-2*FromFly2(BufShort, 41);
         if(fabs(d_ay[iter]-tmp) >= 0.01)
            flCoeff = 4;
         tmp = 1e-2*FromFly2(BufShort, 43);
         if(fabs(d_ax[iter]-tmp) >= 0.01)
            flCoeff = 4;

         tmp = 1e-2*FromFly2(BufShort, 45);
         if(fabs(U_end[iter]-tmp) >= 0.1)
            flCoeff = 4;

/*         if (flCoeff > 6)
         {
            Timer1->Enabled = false;
            Timer1->Interval = 10;
            Timer1->Enabled = true;
         }  */
      }
   } //while
   if (flRun)
      FormPaint(NULL);

   t_pr_error = t;
   F_buffer[0] = 0;
   return flRun;
}

//------------------------------------------------------------------------------
void TTerm:: AnalisRK_code(void)
{
   String NameOfPackage;
   char NPackage = -1, KontrSumma = 0, Code[60], nByte;

  	for (int r = 0; r < strlen(RK_code); r++)
   {
      if( (RK_code[r] & 0xc0) == 0x40)    // ������ ��������� ��������
      {
			nByte = 0;
			KontrSumma = 0;
			NPackage = RK_code[r] & 0x3f;
		}
		if (nByte > 55)
			nByte = 55;
		Code[nByte] = RK_code[r] & 0x7f;
		KontrSumma = KontrSumma^Code[nByte++];

		if ( (nByte == 3) && (KontrSumma == 0) )
		{
         if(NPackage == 2)
         {
            char tmp = Code[1]& 0x7f;	//tmp = �����
            if(tmp == 1)
               Memo->Lines->Add(">> �������?");
            else if (tmp == 2)
               Memo->Lines->Add(">> ���������� ����������");
     		   else if (tmp == 3)
               Memo->Lines->Add(">> Stop ��");
            else if (tmp == 4)
               Memo->Lines->Add(">> ������������� ��");
            else if (tmp == 5)
               Memo->Lines->Add(">> �������");
/*            else if (tmp == 7)
               Memo->Lines->Add(">> ���� �����");*/
            else if(NPackage == 10)
               Memo->Lines->Add(">> �������");
         }
         else if(NPackage == 4)
         {
            char n_ = Code[1];
            NameOfPackage.sprintf(">> ������ � %d �����, ", n_);
            Memo->Lines->Add(NameOfPackage);
         }

         else if(NPackage == 8)
            Memo->Lines->Add(">> ���� ������");
         else if(NPackage == 9)
            Memo->Lines->Add(">> ������ ������");
      }
      if ( (nByte == 4) && (KontrSumma == 0) )
		{
         if ( NPackage == 1 )	//H_zad
            Memo->Lines->Add(">> H_zad");
 	      else if ( NPackage == 3 )
            Memo->Lines->Add(">> koors_zad");
         else if(NPackage == 5)
            Memo->Lines->Add(">> kren_zad");
         else if(NPackage == 6) //V_zad
            Memo->Lines->Add(">> V_zad");
         else if(NPackage == 7)
            Memo->Lines->Add(">> delta_g");
         else if(NPackage == 13)
            Memo->Lines->Add(">> delta_u");
      }
      if((NPackage == 10) && (nByte == 7) && (KontrSumma == 0))
            Memo->Lines->Add(">> �����");
      if ( (nByte == 50) && (KontrSumma == 0) )
		{
         if(NPackage == 14)
            Memo->Lines->Add(">> ���������1");
         else if(NPackage == 15)
            Memo->Lines->Add(">> ���������2");
      }
      else if ((NPackage == 11) && (nByte == 13) && (KontrSumma == 0))	//����� ��������
      {
         char n_point = Code[11];
         NameOfPackage.sprintf(">> %d ����a ��������", n_point);
         Memo->Lines->Add(NameOfPackage);
      }
   } //for
   RK_code[0] = 0;
}

//----------------------------------------------------------------------------
POINT TTerm:: ToDevice(int x, int y, double kren_r)
{
   POINT p;
   p.x = xCenter+x*cos(kren_r)-y*sin(kren_r);
   p.y = yCenter-x*sin(kren_r)-y*cos(kren_r);
   return p;
}

//----------------------------------------------------------------------------
void TTerm::  DrawDevice(void)
{
   int Sidev = PB->Height;
   int HeigthD = Sidev;
   int WidthD = Sidev;

   TRect RectDevice = TRect(0, 0, WidthD, HeigthD);
   TRect RectARM(1, TOP_DEVICE, 1+WidthD, TOP_DEVICE+HeigthD);
   Graphics:: TBitmap *BitmapDevice;
   BitmapDevice = new Graphics::TBitmap;
   BitmapDevice->Height = RectDevice.Height();
   BitmapDevice->Width = RectDevice.Width();

   BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Brush->Color = clBtnFace;
   BitmapDevice->Canvas->Rectangle(RectDevice);

   //---------------------------------------------------------------------------
   BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Pen->Width  = 1;
   BitmapDevice->Canvas->MoveTo(WidthD/2, HeigthD);
   BitmapDevice->Canvas->LineTo(WidthD/2, 0);

   int Diap = 10;
   for(int metkaVy = -Diap/2; metkaVy <= Diap/2; metkaVy += 1)
   {
      int y = 0.5*HeigthD - 0.8*HeigthD/Diap*metkaVy;
      BitmapDevice->Canvas->MoveTo(WidthD/2, y);
      BitmapDevice->Canvas->LineTo(WidthD/2+5, y);
   }
   BitmapDevice->Canvas->MoveTo(5, 0.5*HeigthD);
   BitmapDevice->Canvas->LineTo(WidthD-5, 0.5*HeigthD);
   double Vy_tmp = Vy;
   if (Vy_tmp > 5) Vy_tmp = 5;
   else if (Vy_tmp < -5) Vy_tmp = -5;
   int y_Vy = 0.5*HeigthD - 0.8*Vy_tmp*HeigthD/Diap;
//   int y_Vy_zad = 0.5*HeigthD - 0.8*0.1*Vy_zad*HeigthD/Diap;
   BitmapDevice->Canvas->MoveTo(0, y_Vy);
   BitmapDevice->Canvas->LineTo(WidthD, y_Vy);

   BitmapDevice->Canvas->Brush->Color = clSkyBlue;
   BitmapDevice->Canvas->FloodFill(1,y_Vy-1,clBlack, fsBorder);
   BitmapDevice->Canvas->FloodFill(WidthD-2, y_Vy-1,clBlack, fsBorder);
   BitmapDevice->Canvas->Brush->Color = clLime;
   BitmapDevice->Canvas->FloodFill(1,y_Vy+1,clBlack, fsBorder);
   BitmapDevice->Canvas->FloodFill(WidthD-2, y_Vy+1,clBlack, fsBorder);

   for(int metkaVy = -Diap/2; metkaVy <= Diap/2; metkaVy += 2)
   {
      int y = 0.5*HeigthD - 0.8*HeigthD/Diap*metkaVy;
      if (y < y_Vy)
         BitmapDevice->Canvas->Brush->Color = clSkyBlue;
      else
         BitmapDevice->Canvas->Brush->Color = clLime;

      String str;
      str.sprintf("%2d", metkaVy);
      int txtHe = BitmapDevice->Canvas->TextHeight(str);
      BitmapDevice->Canvas->TextOutA(WidthD/2+7, y-txtHe/2, str);
   }

   //------------------------------------------------------------------------
   xCenter = Sidev/2;
   yCenter = y_Vy;
   int Size = Sidev/2;
   int nSize = 0.9*Size;
   int oSize = 0.3*Size;

   BitmapDevice->Canvas->Pen->Width  = 3;

      double val = -kren_gir;    //���� �������
      BitmapDevice->Canvas->Pen->Color = clYellow;


      POINT p = ToDevice(-nSize, 0, val);
      BitmapDevice->Canvas->MoveTo(p.x, p.y);
      POINT p1 = ToDevice(-oSize, 0, val);
      BitmapDevice->Canvas->LineTo(p1.x, p1.y);

      p = ToDevice(nSize, 0, val);
      BitmapDevice->Canvas->MoveTo(p.x, p.y);
      POINT p2 = ToDevice(oSize, 0, val);
      BitmapDevice->Canvas->LineTo(p2.x, p2.y);
      BitmapDevice->Canvas->Arc( xCenter+oSize, yCenter+oSize, xCenter-oSize, yCenter-oSize, p1.x, p1.y, p2.x, p2.y);

      val = -kren_zad;           //���� ��������
      BitmapDevice->Canvas->Pen->Color = clRed;

      p = ToDevice(-nSize, 0, val);
      BitmapDevice->Canvas->MoveTo(p.x, p.y);
      p1 = ToDevice(-oSize, 0, val);
      BitmapDevice->Canvas->LineTo(p1.x, p1.y);

      p = ToDevice(nSize, 0, val);
      BitmapDevice->Canvas->MoveTo(p.x, p.y);
      p2 = ToDevice(oSize, 0, val);
      BitmapDevice->Canvas->LineTo(p2.x, p2.y);
      BitmapDevice->Canvas->Arc( xCenter+oSize, yCenter+oSize, xCenter-oSize, yCenter-oSize, p1.x, p1.y, p2.x, p2.y);

   //������------------------------------------------------------------------
   BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Pen->Width  = 1;
   int x0_kam = 0.75*WidthD;
   int y0_kam = 2;//0.01*HeigthD;
   int rad = 30;
   int dlina = 5;
   for (int i = 0; i < 128; i = i+15)
   {
      double gr = (double)i/ToGrad;
      int x = x0_kam-rad*cos(gr);
      int y = y0_kam+rad*sin(gr);
      BitmapDevice->Canvas->MoveTo(x, y);
      x = x - dlina*cos(gr);
      y = y +dlina*sin(gr);
      BitmapDevice->Canvas->LineTo(x, y);
   }
   if (ugol_kam_zad == ugol_kam_zad_bort)
      BitmapDevice->Canvas->Pen->Color = clRed;
   else
      BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Pen->Width  = 3;
   BitmapDevice->Canvas->MoveTo(x0_kam, y0_kam);
   double gr = (double)ugol_kam_zad/ToGrad;
   int x = x0_kam-(rad-1)*cos(gr);
   int y = y0_kam+(rad-1)*sin(gr);
   BitmapDevice->Canvas->LineTo(x, y);

   //���� ������
   BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Pen->Width  = 1;
   x0_kam = 0.25*WidthD;
   for (int i = -70; i < 71; i = i+20)
   {
      double gr = (i+90)/ToGrad;
      int x = x0_kam-rad*cos(gr);
      int y = y0_kam+rad*sin(gr);
      BitmapDevice->Canvas->MoveTo(x, y);
      x = x - dlina*cos(gr);
      y = y +dlina*sin(gr);
      BitmapDevice->Canvas->LineTo(x, y);
   }
   if (kren_kam_zad == kren_kam_zad_bort)
      BitmapDevice->Canvas->Pen->Color = clRed;
   else
      BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Pen->Width  = 3;
   BitmapDevice->Canvas->MoveTo(x0_kam, y0_kam);
   gr = (double)(kren_kam_zad+90)/ToGrad;
   x = x0_kam-(rad-1)*cos(gr);
   y = y0_kam+(rad-1)*sin(gr);
   BitmapDevice->Canvas->LineTo(x, y);

   //---------------------------------------------------------------------------
   BitmapDevice->Canvas->Pen->Color = clBlack;
   BitmapDevice->Canvas->Brush->Color = clLime;
   String msg;
//   msg.sprintf("����_zad = %3d�",kren_zad);
//   int h_msg = BitmapDevice->Canvas->TextHeight(msg);
//   BitmapDevice->Canvas->TextOutA(10, 0.85*HeigthD-2*h_msg, msg);
   msg.sprintf("Vy_zad = %4.1f �/�", 0.1*Vy_zad);
   int h_msg = BitmapDevice->Canvas->TextHeight(msg);
   BitmapDevice->Canvas->TextOutA(10, 0.85*HeigthD-h_msg, msg);
   msg.sprintf("���� = %5.0f �", otkl_ot_mar);
   BitmapDevice->Canvas->TextOutA(10, 0.85*HeigthD, msg);
//   if (MyFly)
   {
      msg.sprintf("kren_gir = %4.2f", kren_gir*ToGrad);
      BitmapDevice->Canvas->TextOutA(WidthD/2+30, 0.85*HeigthD-h_msg, msg);
      msg.sprintf("tang_gir = %4.2f", tang_gir*ToGrad);
      BitmapDevice->Canvas->TextOutA(WidthD/2+30, 0.85*HeigthD, msg);

      msg.sprintf("rsk_gir = %4.2f", rsk_gir*ToGrad);
      BitmapDevice->Canvas->TextOutA(WidthD/2+30, 0.85*HeigthD+h_msg, msg);
   }
   //---------------------------------------------------------------------------
   PB->Canvas->CopyMode = cmSrcCopy;
   PB->Canvas->CopyRect(RectARM, BitmapDevice->Canvas, RectDevice);

   delete BitmapDevice;
}

//---------------------------------------------------------------------------
TTerm:: KMonitor:: KMonitor(int Left_,
                    int MinValue_, int MaxValue_, int Step_, String NameMonitor_, double k_)
   : Left (Left_)
   , Heigth(Term->PB->Height), Width(WIDTH_MONITOR)
   , k (k_), MinValue(MinValue_), MaxValue(MaxValue_), Step(Step_)
   , NameMonitor("")
   , x0 (5)
   , x1 (6)
   , x2 (19)
   , x3 (22)
   , h  (8)
{
   Bitmap = new Graphics::TBitmap;
   Bitmap->Height = Heigth;
   Bitmap->Width = Width;

   char NameTmp[100];
   int n = NameMonitor_.AnsiPos(",");
   NameMonitor = NameMonitor_.SubString(1, n);
   SizeValue = NameMonitor_.SubString(n+2, NameMonitor_.Length());
}

//---------------------------------------------------------------------------
void TTerm:: KMonitor:: SetValue(float Value_)
{
   Value = Value_;
   Draw();
}
//---------------------------------------------------------------------------
void TTerm:: KMonitor:: DrawBitMap(void)
{
      Bitmap->Canvas->Brush->Color = clBtnFace;
      Bitmap->Canvas->Pen->Color = clBtnFace;

   Bitmap->Canvas->Rectangle(0, 0, Width, Heigth);
   Bitmap->Canvas->Pen->Color = clBlack;
   Bitmap->Canvas->Pen->Width = 1;
   Bitmap->Canvas->Rectangle(1, 1, Width-2, Heigth-2);

//   Bitmap->Canvas->Pen->Color = clBlack;
   Bitmap->Canvas->MoveTo(x0, GetTop(MinValue));
   Bitmap->Canvas->LineTo(x0, GetTop(MaxValue));

   for(int metka = MinValue+Step/2; metka <= MaxValue; metka += Step)
   {
      int top_metka = GetTop(metka);
      Bitmap->Canvas->MoveTo(x0, top_metka);
      Bitmap->Canvas->LineTo(x0+3, top_metka);
   }
   for(int metka = MinValue; metka <= MaxValue; metka += Step)
   {
      int top_metka = GetTop(metka);
      Bitmap->Canvas->MoveTo(x0, top_metka);
      Bitmap->Canvas->LineTo(x0+5, top_metka);
      String name_metka;
      name_metka.sprintf("%4d", metka);
      Bitmap->Canvas->Font->Height = 13;
      Bitmap->Canvas->TextOut(x0+8, top_metka-6, name_metka);
   }
   Bitmap->Canvas->Font->Height = 10;
   int heigth_txt = Bitmap->Canvas->TextHeight(NameMonitor);
   int width_txt = Bitmap->Canvas->TextWidth(NameMonitor);
   Bitmap->Canvas->TextOut(Width/2-width_txt/2, heigth_txt/2, NameMonitor);

   width_txt = Bitmap->Canvas->TextWidth(SizeValue);
   Bitmap->Canvas->TextOut(Width/2-width_txt/2, 3*heigth_txt/2, SizeValue);

   //------------------------------------------------------------------------
   String ImageValue;
   ImageValue.sprintf("%5.1f", Value*k);
   Bitmap->Canvas->Font->Height = 13;
   width_txt = Bitmap->Canvas->TextWidth(ImageValue);
   int top_metka = GetTop(Value*k);
   Bitmap->Canvas->Pen->Color = clYellow;
   Bitmap->Canvas->TextOut(17, top_metka-6, ImageValue);

   for(int i = 0; i < 2; i++)
   {
      Bitmap->Canvas->MoveTo(x1, top_metka);
      Bitmap->Canvas->LineTo(x2, top_metka-h);
      Bitmap->Canvas->LineTo(x3+width_txt, top_metka-h);
      Bitmap->Canvas->LineTo(x3+width_txt, top_metka+h);
      Bitmap->Canvas->LineTo(x2, top_metka+h);
      Bitmap->Canvas->LineTo(x1, top_metka);
      if(i != 0)
         break;
      Bitmap->Canvas->Brush->Color = clYellow;
      Bitmap->Canvas->FloodFill(10, top_metka, clYellow, fsBorder);
      Bitmap->Canvas->Pen->Color = clBlack;
   }
   Bitmap->Canvas->TextOut(17, top_metka-6, ImageValue);
}
//---------------------------------------------------------------------------
void TTerm:: KMonitor:: Draw(void)
{
   DrawBitMap();

   TRect RectBitMap = Rect(0,0,Width,Heigth);
   TRect RectARM = Rect(Term->PB->Height+Left+2, 0, Term->PB->Height+Left+Width+2, Heigth);
  
   Term->PB->Canvas->CopyMode = cmSrcCopy;
   Term->PB->Canvas->CopyRect(RectARM, Bitmap->Canvas, RectBitMap);
}
//---------------------------------------------------------------------------
void TTerm:: Monitor:: SetValue(float Value_)
{
   Value = Value_;
   Draw();
}
//---------------------------------------------------------------------------
TTerm:: Monitor:: Monitor(int Left_,
                    int MinValue_, int MaxValue_, int Step_, String NameMonitor_, double k_)
   : KMonitor(Left_, MinValue_, MaxValue_, Step_, NameMonitor_, k_)
{
}

//---------------------------------------------------------------------------
void TTerm:: Monitor:: SetValue_zad(float Value_)
{
   Value_zad = Value_;
   Draw(); //???
}

//---------------------------------------------------------------------------
void TTerm:: Monitor:: SetValue_B_zad(float Value_)
{
   Value_B_zad = Value_;
   Draw();
}

//---------------------------------------------------------------------------
void TTerm:: Monitor:: Draw(void)
{
   DrawBitMap();

      TColor color_Value_zad;
      if(Command())
         color_Value_zad = clWhite;
      else
         color_Value_zad = clRed;
      Bitmap->Canvas->Pen->Color = color_Value_zad;
      int metka_zad = GetTop(Value_zad*k);
      int radius = 4;
      Bitmap->Canvas->Ellipse(x0-radius, metka_zad-radius, x0+radius, metka_zad+radius);
      Bitmap->Canvas->Brush->Color = color_Value_zad;
      Bitmap->Canvas->FloodFill(x0, metka_zad, color_Value_zad, fsBorder);

      String ImageValue_zad;
      ImageValue_zad.sprintf("%5.1f", Value_zad*k);
      Bitmap->Canvas->Pen->Color = color_Value_zad;
      Bitmap->Canvas->Brush->Color = clBtnFace;
      Bitmap->Canvas->Rectangle(x0, Heigth-22, Width-x0, Heigth-x0);

      Bitmap->Canvas->Pen->Color = clBlack;
      Bitmap->Canvas->TextOut(0.3*Width, Heigth-20, ImageValue_zad);


   TRect RectBitMap = Rect(0,0,Width,Heigth);
   TRect RectARM = Rect(Term->PB->Height+Left+2, 0, Term->PB->Height+Left+Width+2, Heigth);

   Term->PB->Canvas->CopyMode = cmSrcCopy;
   Term->PB->Canvas->CopyRect(RectARM, Bitmap->Canvas, RectBitMap);
}

//---------------------------------------------------------------------------
bool TTerm:: Monitor:: Command(void)
{
   if(fabs(Value_zad-Value_B_zad) < 0.1)
      return false;
   return true;
}
//----------------------------------------------------------------------------

void __fastcall TTerm::TB_OkClick(TObject *Sender)
{
   if(connect_joy)
      return;
   JOYINFOEX JoyInfo;
   JoyInfo.dwSize = sizeof(JOYINFOEX);
   JoyInfo.dwFlags = JOY_RETURNALL;
   if (joyGetPosEx(jnum, &JoyInfo) != JOYERR_NOERROR)
         Start();
   return;
}
//----------------------------------------------------------------------------

void TTerm::Start(void)
{
/*   if (flKoord == false)           //�� ����� ������� ��������������
   {
      CoderPack3 (2, 1, RK_code);
      return;
   }        */

   delta_g.SetValue_zad(0);
   koors_zad = 360-rsk_gir*ToGrad;
   Timer1->Enabled = false;
   Timer1->Interval = 10;
   Timer1->Enabled = true;
   CoderPack3 (2, 4, RK_code);   //�����!
//   t_pr_delta_g = t;
   return;
}
//----------------------------------------------------------------------------
void __fastcall TTerm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
   keyShift = Shift.Contains(ssShift);
   keyAlt = Shift.Contains(ssAlt);
   keyCtrl = Shift.Contains(ssCtrl);

   int delta_g_tmp;
      switch(Key)
      {
      case 116:   //shift +f5 - stop
         if (keyShift)
            CoderPack3  (2, 3, RK_code);
         break;      
      case 74:
         if(rgfly == 0)    //"j" - ������ + X �
         {
            float f_H_zad;
            if (keyShift)
               f_H_zad = (int)(0.02*H.GetValue_zad()+0.5)*50+50;
            else
               f_H_zad = H.GetValue_zad() + 0.1;
            if (f_H_zad > H_zad_max) f_H_zad = H_zad_max;
            if (f_H_zad < H_zad_min) f_H_zad = H_zad_min;
            H.SetValue_zad(f_H_zad);
         }
         else if(rgfly == 1)   //��������� ������� �������� + 1%
         {
            delta_g_tmp = delta_g.GetValue_zad() + 1;
            if (delta_g_tmp > delta_g_max) delta_g_tmp = delta_g_max;
            if (delta_g_tmp < delta_g_min) delta_g_tmp = delta_g_min;
            delta_g.SetValue_zad(delta_g_tmp);
         }
         break;
      case 77:          //"m"
         if (rgfly == 0)      // ������ - X �
         {
            float f_H_zad;
            if (keyShift)
               f_H_zad = (int)(0.02*H.GetValue_zad()+0.5)*50-50;
            else
               f_H_zad = H.GetValue_zad() - 0.1;
            if (f_H_zad > H_zad_max) f_H_zad = H_zad_max;
            if (f_H_zad < H_zad_min) f_H_zad = H_zad_min;
            H.SetValue_zad(f_H_zad);
         }
         else if (rgfly == 1)   //  ��������� ������� �������� - 1%
         {
            delta_g_tmp = delta_g.GetValue_zad()-1;
            if (delta_g_tmp > delta_g_max) delta_g_tmp = delta_g_max;
            if (delta_g_tmp < delta_g_min) delta_g_tmp = delta_g_min;
            delta_g.SetValue_zad(delta_g_tmp);
         }
         break;
      case 38:      //"������� �����"  - ������ (�������)
         if(flStoika)
         {
            float f_v = V.GetValue_zad()+0.1;
            if (f_v > V_max) f_v = V_max;
            if (f_v < V_min) f_v = V_min;
            V.SetValue_zad(f_v);
         }
         else if (rgfly == 1)
         {
            float fdelta_u = delta_u.GetValue_zad()+0.1;
            if(fdelta_u > delta_u_max*ToGrad) fdelta_u = delta_u_max*ToGrad;
            if(fdelta_u < delta_u_min*ToGrad) fdelta_u = delta_u_min*ToGrad;
            delta_u.SetValue_zad(fdelta_u);
         }
         else if(rgfly == 0)
         {
            float f_v = V.GetValue_zad()+0.1;
            if (f_v > V_max) f_v = V_max;
            if (f_v < V_min) f_v = V_min;
            V.SetValue_zad(f_v);
         }
         break;
      case 40:      //"������� ����" - ����� (���������)
         if(flStoika)
         {
            float f_v = V.GetValue_zad()-0.1;
            if (f_v > V_max) f_v = V_max;
            if (f_v < V_min) f_v = V_min;
            V.SetValue_zad(f_v);
         }
         else if (rgfly == 1)
         {
            float fdelta_u = delta_u.GetValue_zad()-0.1;
            if(fdelta_u > delta_u_max*ToGrad) fdelta_u = delta_u_max*ToGrad;
            if(fdelta_u < delta_u_min*ToGrad) fdelta_u = delta_u_min*ToGrad;
            delta_u.SetValue_zad(fdelta_u);
         }
         else if (rgfly == 0)
         {
            float f_v = V.GetValue_zad()-0.1;
            if (f_v > V_max) f_v = V_max;
            if (f_v < V_min) f_v = V_min;
            V.SetValue_zad(f_v);
         }
         break;
      case 37:       //"������� �����" - koors_zad - 1 ��
          if (keyShift)
          {
               kren_zad = kren_zad-0.5/ToGrad;
               if(kren_zad > kren_max)
                  kren_zad = kren_max;
               if(kren_zad < -kren_max)
                  kren_zad = -kren_max;
         }
         else if (rgfly < 2)
         {
               koors_zad = koors_zad - 0.5;
               while (koors_zad >= 360)    //�������� � �������� �� 0 �� 360
                  koors_zad = koors_zad-360;
               while (koors_zad < 0)
                  koors_zad = koors_zad+360;
         }
         break;
      case 39:       //"������� ������" - koors_zad + 1 ��
          if (keyShift)
          {
               kren_zad = kren_zad+0.5/ToGrad;
               if(kren_zad > kren_max)
                  kren_zad = kren_max;
               if(kren_zad < -kren_max)
                  kren_zad = -kren_max;
         }
         if (rgfly < 2)
         {
               koors_zad = koors_zad + 0.5;
               while (koors_zad >= 360)    //�������� � �������� �� 0 �� 360
                  koors_zad = koors_zad-360;
               while (koors_zad < 0)
                  koors_zad = koors_zad+360;
         }
         break;
      case 69: //������ ������ - 2 ��
         if (keyShift) //������ ������ ������
         {
            if (ugol_kam_zad != 0 || kren_kam_zad != 0)
            {
               ugol_kam_zad = 0;
               kren_kam_zad = 0;
            }
         }
         else
         {
            ugol_kam_zad = ugol_kam_zad - 2;
            if (ugol_kam_zad > 126)
               ugol_kam_zad = 126;
            if (ugol_kam_zad < 0)
               ugol_kam_zad = 0;
         }
         break;
      case 88: //������ ����� + 2 ��
         if (keyShift) //������ ������ ����
         {
            if (ugol_kam_zad != 90 || kren_kam_zad != 0)
            {
               ugol_kam_zad = 90;
               kren_kam_zad = 0;
            }
         }
         else
         {
            ugol_kam_zad = ugol_kam_zad + 2;
            if (ugol_kam_zad > 126)
               ugol_kam_zad = 126;
            if (ugol_kam_zad < 0)
               ugol_kam_zad = 0;
         }
         break;
      case 68: //������ ����� + 2 ��
         kren_kam_zad = kren_kam_zad + 2;
         if (kren_kam_zad > 60)
            kren_kam_zad = 60;
         if (kren_kam_zad < -60)
            kren_kam_zad = -60;
         break;
      case 83: //������ ������ - 2 ��
         kren_kam_zad = kren_kam_zad - 2;
         if (kren_kam_zad > 60)
            kren_kam_zad = 60;
         if (kren_kam_zad < -60)
            kren_kam_zad = -60;
         break;
      default:
         ;
      }
}

//---------------------------------------------------------------------------
void __fastcall TTerm::FormKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   fstream file;
   JOYINFOEX JoyInfo;
   JoyInfo.dwSize = sizeof(JOYINFOEX);
	JoyInfo.dwFlags = JOY_RETURNALL;

      switch(Key)
      {
      case 81:  //q - ������ ���������� ������, ����� ������� ���������� � ����� (��������: �����-������, ������-�����) � ��������� (�� �����: ������� �����-����)
         if((rgfly == 2) || (rgfly == 4))
            rgfly = 1;
/*         else if (connect_joy && (joyGetPosEx(jnum, &JoyInfo) != JOYERR_NOERROR))
            rgfly = 1;*/
         break;
      case 116:   //shift +f5 - stop
         if (keyShift)
            rgfly = 5;
         break;
      case 65:  //a - �����������
         if((rgfly == 2) || (rgfly == 4))
            rgfly = 0;
/*         else  if (connect_joy && (joyGetPosEx(jnum, &JoyInfo) != JOYERR_NOERROR))
            rgfly = 0;*/
         break;
      case 112:
         AboutBox->Show();
         break;
      case 19: //�����
         {
            static int iClick;
            if (iClick)
            {
               iClick = 0;
               Timer1->Enabled=true;
            }
            else
            {
               iClick = 1;
               Timer1->Enabled=false;
            }
         }
         break;
      case 32:    //" " - ������� ������
         FormResize(Sender);
         break;
      case 70:      //"f" - ���������� �����
         rgfly = 2;
         break;
      case 72:    //"h" - ����� ��������
         rgfly = 4;
         break;
      case 187:   //"shift +" - ������������ �� ��������� ����� ��������
         if (keyShift)
         {
            n_ = n_ + 1;
            if (n_ > i_mar-1) n_ = i_mar-1;
            if (rgfly == 2) CoderPack3  (4, n_, RK_code);
         }
         break;
      case 189:   //"shift -" - ������������ �� ���������� ����� ��������
         if (keyShift)
         {
            n_ = n_ - 1;
            if (n_ < 1) n_ = 1;
            if (rgfly == 2) CoderPack3  (4, n_, RK_code);
         }
         break;
      case 27:
         if (Regime == 6 && Key == 27)
         {
            i_mar = i_mar_pr;
            if ((n_ >= 0) && (n_ < i_mar))
            {
               PointMar[n_] = PointMar_pr;
               SG_Mar->Cells[1][n_+1] = PointMar[n_].show();
            }
            n_ = 1;
            Regime = 0;
            FormResize(Sender);
         }
         else if (keyShift)
            ChancelFly (Sender);    //"Shift  + Esq" - ����� �� ���������
         break;
      default:
         ;
      }
   return;
}

//�����----------------------------------------------------------------------
void __fastcall TTerm::TB_StartClick(TObject *Sender)
{
   if(connect_joy)
   {
      JOYINFOEX JoyInfo;
      JoyInfo.dwSize = sizeof(JOYINFOEX);
	   JoyInfo.dwFlags = JOY_RETURNALL;
      if (joyGetPosEx(jnum, &JoyInfo) == JOYERR_NOERROR)
      {
         if ((JoyInfo.dwVpos > 12000) || (JoyInfo.dwUpos > 12000))
            return;
         if(rgfly != 5)
         {
            if (JoyInfo.dwUpos < 12000)   rgfly = 1;
            else  rgfly = 0;
         }
      }
   }
   TabSheet1->Enabled = false;
   TabSheet1->Visible = false;
   TabSheet1->TabVisible = false;
   F_buffer[0] = 0;
   otkl_ot_mar = 0;
   Lat0 = PointMar[0].Lat*ToGrad+0.5;
   PointFly = PointMar[0];

   kren_zad = kren_kam = kren_kam_zad = 0;
   kren_kam_zad_bort = 1;
   ugol_kam = ugol_kam_zad = 0;
   ugol_kam_zad_bort = 1;

   delta_g.SetValue_zad(0);
   delta_u.SetValue_zad(90);
   Vy = 0;

   Timer1->OnTimer = Timer1Timer;
   Timer1->Enabled = false;
   Timer1->Interval = 100;
   Timer1->Enabled = true;

   flStoika = false;
   flMatmodel = false;
   if (TypeWork == 0)  //�����  ����������� �����
   {
      MyFly = new bpla();
   }
   else if (TypeWork == 1)  //����� ��������� �����
   {
      MyFly = new piter();
   }
   else if (TypeWork == 2)  //����� wifi
   {
      flStoika = true;
      MyFly = new wifi();
   }
   else if (TypeWork == 3)  //����� novosib
   {
      MyFly = new novosib();
   }
   else   //��������� �� ������ ��������� �����
   {
      flStoika = true;
      MyFly = new piter();
   }
   n_error = 0;
   rgStart = 0;
   SB_t->Caption = "";

   TB_StartStop->OnClick = ChancelFly;
   TB_StartStop->ImageIndex = 6;
   TB_StartStop->Hint = "����|���������� �����";

   OnClose = FormNonClose;

   time_t secs_now;
   time(&secs_now);
   t0 = secs_now;
   t = t_GPS = 0;
   t_pr = t-5;
//   t_pr_delta_g = t;
//   Wak.SetValue(100);

   struct tm *time_now;
   time_now = localtime(&secs_now);
   char str[120];
   sprintf(str, "%d_%d_%d_%d_%d_%d.his", time_now->tm_year+1900, time_now->tm_mon+1, time_now->tm_mday
                                   , time_now->tm_hour, time_now->tm_min, time_now->tm_sec);
   FileNameHistory = ExtractFilePath(Application->ExeName)+"His\\"+str;
   file_his.open(FileNameHistory.c_str(), ios:: out);
   if(!file_his)
   {
      Application->MessageBox("���� ������ �� ������", "", MB_OK);
      ChancelFly(Sender);
   }
   else
   {
      SaveFileMarsh(file_his);
      file_his << FileNameCoeff << "\n";
      SaveFileCoeff(file_his);
      file_his << "EndMar" << "\n";
   }
   CB_TypeWork->Enabled = false;

   Repaint();
}

//---------------------------------------------------------------------------
void __fastcall TTerm::CB_TypeWorkChange(TObject *Sender)
{
   TypeWork = CB_TypeWork->ItemIndex;
   flModifiedFile = true;
}

//---------------------------------------------------------------------------
void __fastcall TTerm::SGDblClick(TObject *Sender)
{
   if (RowSG == sFileName)
   {
      OpenDialog->FilterIndex = 3;
      OpenDialog->FileName = "";
      OpenDialog->Title = "�������� �����������";
      OpenDialog->InitialDir = ExtractFilePath(Application->ExeName)+"\Map" ;
      if (OpenDialog->Execute())
      {
         FileNameImage = OpenDialog->FileName.c_str();
         ImageTmp->LoadFromFile(FileNameImage.c_str());
         Regime = 0;
         SG->Cells[1][sFileName] = ExtractFileName(FileNameImage.c_str());
         FormResize(Sender);
         flModifiedFile = true;
      }
   }   
}
//---------------------------------------------------------------------------

void __fastcall TTerm::TB_TestClick(TObject *Sender)
{
/*   Timer1->Enabled = false;
   Timer1->Interval = 40;
   Timer1->Enabled = true;  */

   unsigned char ksumma = 0, i = strlen(RK_code);
   RK_code[i] = 0x40 | 10;
	ksumma = ksumma^RK_code[i++];

   RK_code[i] = (t0 & 0x007f) | 0x80;
	ksumma = ksumma^RK_code[i++];

	RK_code[i] = ((t0 & 0x3f80) >> 7) | 0x80;
	ksumma = ksumma^RK_code[i++];

   RK_code[i] = ((t0 & 0x1fc000) >> 14) | 0x80;
	ksumma = ksumma^RK_code[i++];

   RK_code[i] = ((t0 & 0xfe00000) >> 21) | 0x80;
	ksumma = ksumma^RK_code[i++];

   RK_code[i] = ((t0 & 0xf0000000) >> 28) | 0x80;
	ksumma = ksumma^RK_code[i++];

   RK_code[i++] = ksumma |0x80;
   RK_code[i] = 0;

   n_3 = 0;
   rgfly = 3;
   CoderPack13 (PointMar[n_3], H_Mar[n_3], Vz_Mar[n_3], RK_code);
}
//---------------------------------------------------------------------------

void __fastcall TTerm::SGKeyPress(TObject *Sender, char &Key)
{
	Set <char, '0', '9'> Dig;
	Dig << '0' << '1' << '2' << '3' << '4' << '5' << '6' <<'7'<<'8'<< '9';
   if ((Key == 'g' || Key == '�') &&  ColSG == 1)
      Key = '�';
   else if ((Key == '\'' || Key == '�') && ColSG == 1)
      Key = '\'';
	else if(Key == 8 || Key == 0x2c || Key == 0x2e || Key == 27 || Key == '-' || Key == 0x20) //����� ��� ����� ��� ������� ��� Esq
		;
/*	else if(Key == 0x2c) //�������
		Key = 0x2e; */
	else if ( !Dig.Contains(Key) )
	{
		Key = 0;
		Beep();
	}
}

//---------------------------------------------------------------------------

void __fastcall TTerm::SG_MarSelectCell(TObject *Sender, int ACol,
      int ARow, bool &CanSelect)
{
   n_ = ARow-1;
   if (ACol == 1)
   {
      Regime = 6;
      PointMar_pr = PointMar[n_];
   }
   else
      Regime = 0;
   return;
}
//---------------------------------------------------------------------------

void __fastcall TTerm::SG_MarKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   flModifiedFile = true;
   if (Regime == 6)
   {
      FormResize(Sender);
   }
   H_Mar[n_] =  atoi(SG_Mar->Cells[2][n_+1].c_str());
   Vz_Mar[n_] = atoi(SG_Mar->Cells[3][n_+1].c_str());
   return;
}

//���������� ���� �����------------------------------------------------------
void __fastcall TTerm::ToolButton2Click(TObject *Sender)
{
   CoderPack3 (2, 8, RK_code);
}
//---------------------------------------------------------------------------

void __fastcall TTerm::ToolButton3Click(TObject *Sender)
{
   pInit->Visible = !pInit->Visible;
}
//---------------------------------------------------------------------------

void __fastcall TTerm::Button2Click(TObject *Sender)
{
   iter++;
   if(iter >= 1023) iter = 1023;
   flModifiedCoeff = true;
   flCoeff = 1;
}
//---------------------------------------------------------------------------

void __fastcall TTerm::Button1Click(TObject *Sender)
{
   if (flModifiedCoeff)
   {
      if (Application->MessageBox("��������� ���������", "", MB_YESNOCANCEL) == IDYES)
      {
         fstream file;
         file.open(FileNameCoeff.c_str());
         SaveFileCoeff(file);
         file.close();
      }
   }

   OpenDialog->FilterIndex = 4;
   OpenDialog->FileName = "";
   OpenDialog->Title = "������������";
   OpenDialog->InitialDir = ExtractFilePath(Application->ExeName)+"\Mar";
   if (OpenDialog->Execute())
   {
      FileNameCoeff = OpenDialog->FileName.c_str();
      LNameFile->Caption = FileNameCoeff.c_str();

      fstream file;
      file.open(FileNameCoeff.c_str());
      LoadFileCoeff(file);
      file.close();
   }
}

//---------------------------------------------------------------------------
void TTerm:: LoadFileCoeff(fstream &file)
{
   String txt;

   if(GetPos (file, "alfa0"))
   {
      file >> alfa0[0];
      alfa0[0] = alfa0[0]/ToGrad;
      SG_init->Cells[1][salfa0] = txt.sprintf("%5.2f", alfa0[0]*ToGrad);
   }
   if(GetPos (file, "i_tang"))
   {
      file >> i_tang[0];
      SG_init->Cells[1][si_tang] = txt.sprintf("%5.2f", i_tang[0]);
   }
   if(GetPos (file, "p_tang"))
   {
      file >> p_tang[0];
      SG_init->Cells[1][sp_tang] = txt.sprintf("%5.2f", p_tang[0]);
   }
   if(GetPos (file, "d_tang"))
   {
      file >> d_tang[0];
      SG_init->Cells[1][sd_tang] = txt.sprintf("%5.2f", d_tang[0]);
   }
   if(GetPos (file, "d2_tang"))
   {
      file >> d2_tang[0];
      SG_init->Cells[1][sd2_tang] = txt.sprintf("%5.2f", d2_tang[0]);
   }
   if(GetPos (file, "int_dtang0"))
   {
      file >> int_dtang0[0];
      SG_init->Cells[1][sint_dtang0] = txt.sprintf("%5.2f", int_dtang0[0]);
   }

   if(GetPos (file, "kl"))
   {
      file >> kl[0];
      SG_init->Cells[1][skl] = txt.sprintf("%5.2f", kl[0]);
   }
   if(GetPos (file, "namemodem"))
   {
      string namemodem;
      file >> namemodem;
      SG_init->Cells[1][snamemodem] = namemodem.c_str();
   }
   if(GetPos (file, "gaz0"))
   {
      file >> gaz0[0];
      SG_init->Cells[1][sgaz0] = txt.sprintf("%5.2f", gaz0[0]);
   }
   if(GetPos (file, "kren0"))
   {
      file >> kren0[0];
      kren0[0] = kren0[0]/ToGrad;
      SG_init->Cells[1][skren0] = txt.sprintf("%5.2f", kren0[0]*ToGrad);
   }
   if(GetPos (file, "i_kren"))
   {
      file >> i_kren[0];
      SG_init->Cells[1][si_kren] = txt.sprintf("%5.2f", i_kren[0]);
   }
   if(GetPos (file, "p_kren"))
   {
      file >> p_kren[0];
      SG_init->Cells[1][sp_kren] = txt.sprintf("%5.2f", p_kren[0]);
   }
   if(GetPos (file, "d_kren"))
   {
      file >> d_kren[0];
      SG_init->Cells[1][sd_kren] = txt.sprintf("%5.2f", d_kren[0]);
   }
   if(GetPos (file, "d2_kren"))
   {
      file >> d2_kren[0];
      SG_init->Cells[1][sd2_kren] = txt.sprintf("%5.2f", d2_kren[0]);
   }

   if(GetPos (file, "i_rsk"))
   {
      file >> i_rsk[0];
      SG_init->Cells[1][si_rsk] = txt.sprintf("%5.2f", i_rsk[0]);
   }
   if(GetPos (file, "p_rsk"))
   {
      file >> p_rsk[0];
      SG_init->Cells[1][sp_rsk] = txt.sprintf("%5.2f", p_rsk[0]);
   }
   if(GetPos (file, "d_rsk"))
   {
      file >> d_rsk[0];
      SG_init->Cells[1][sd_rsk] = txt.sprintf("%5.2f", d_rsk[0]);
   }
   if(GetPos (file, "d2_rsk"))
   {
      file >> d2_rsk[0];
      SG_init->Cells[1][sd2_rsk] = txt.sprintf("%5.2f", d2_rsk[0]);
   }

   if(GetPos (file, "w_max"))
   {
      file >> w_max[0];
      SG_init->Cells[1][sw_max] = txt.sprintf("%5.2f", w_max[0]);
   }
   if(GetPos (file, "delta_max"))
   {
      file >> delta_max[0];
      SG_init->Cells[1][sdelta_max] = txt.sprintf("%5.2f", delta_max[0]);
   }

   if(GetPos (file, "i_ay"))
   {
      file >> i_ay[0];
      SG_init->Cells[1][si_ay] = txt.sprintf("%5.2f", i_ay[0]);
   }
   if(GetPos (file, "p_ay"))
   {
      file >> p_ay[0];
      SG_init->Cells[1][sp_ay] = txt.sprintf("%5.2f", p_ay[0]);
   }
   if(GetPos (file, "d_ay"))
   {
      file >> d_ay[0];
      SG_init->Cells[1][sd_ay] = txt.sprintf("%5.2f", d_ay[0]);
   }
   if(GetPos (file, "d_ax"))
   {
      file >> d_ax[0];
      SG_init->Cells[1][sd_ax] = txt.sprintf("%5.2f", d_ax[0]);
   }
   if(GetPos (file, "dH_max"))
   {
      file >> dH_max[0];
      SG_init->Cells[1][sdH_max] = txt.sprintf("%5.2f", dH_max[0]);
   }

   if(GetPos (file, "i_ax"))
   {
      file >> i_ax[0];
      SG_init->Cells[1][si_ax] = txt.sprintf("%5.2f", i_ax[0]);
   }
   if(GetPos (file, "p_ax"))
   {
      file >> p_ax[0];
      SG_init->Cells[1][sp_ax] = txt.sprintf("%5.2f", p_ax[0]);
   }
   if(GetPos (file, "tdelta_g_max"))
   {
      file >> tdelta_g_max[0];
      SG_init->Cells[1][stdelta_g_max] = txt.sprintf("%5.2f", tdelta_g_max[0]);
   }
   if(GetPos (file, "tdelta_g_min"))
   {
      file >> tdelta_g_min[0];
      SG_init->Cells[1][stdelta_g_min] = txt.sprintf("%5.2f", tdelta_g_min[0]);
   }
   if(GetPos (file, "v_rsk"))
   {
      file >> v_rsk[0];
      SG_init->Cells[1][sv_rsk] = txt.sprintf("%5.2f", v_rsk[0]);
   }
   if(GetPos (file, "g_kren"))
   {
      file >> g_kren[0];
      SG_init->Cells[1][sg_kren] = txt.sprintf("%5.2f", g_kren[0]);
   }
   if(GetPos (file, "kren_zad_wy"))
   {
      file >> kren_zad_wy[0];
      SG_init->Cells[1][skren_zad_wy] = txt.sprintf("%5.2f", kren_zad_wy[0]);
   }
   if(GetPos (file, "g_rsk"))
   {
      file >> g_rsk[0];
      SG_init->Cells[1][sg_rsk] = txt.sprintf("%5.2f", g_rsk[0]);
   }
   if(GetPos (file, "v_kren"))
   {
      file >> v_kren[0];
      SG_init->Cells[1][sv_kren] = txt.sprintf("%5.2f", v_kren[0]);
   }
   if(GetPos (file, "g_tang"))
   {
      file >> g_tang[0];
      SG_init->Cells[1][sg_tang] = txt.sprintf("%5.2f", g_tang[0]);
   }
   if(GetPos (file, "v_tang"))
   {
      file >> v_tang[0];
      SG_init->Cells[1][sv_tang] = txt.sprintf("%5.2f", v_tang[0]);
   }
   if(GetPos (file, "dt0_p1"))
   {
      file >> dt0_p1[0];
      SG_init->Cells[1][sdt0_p1] = txt.sprintf("%6.5f", dt0_p1[0]);
   }
   if(GetPos (file, "dt90_p1"))
   {
      file >> dt90_p1[0];
      SG_init->Cells[1][sdt90_p1] = txt.sprintf("%6.5f", dt90_p1[0]);
   }
   if(GetPos (file, "dt0_l1"))
   {
      file >> dt0_l1[0];
      SG_init->Cells[1][sdt0_l1] = txt.sprintf("%6.5f", dt0_l1[0]);
   }
   if(GetPos (file, "dt90_l1"))
   {
      file >> dt90_l1[0];
      SG_init->Cells[1][sdt90_l1] = txt.sprintf("%6.5f", dt90_l1[0]);
   }
   if(GetPos (file, "dt0_p2"))
   {
      file >> dt0_p2[0];
      SG_init->Cells[1][sdt0_p2] = txt.sprintf("%6.5f", dt0_p2[0]);
   }
   if(GetPos (file, "dt90_p2"))
   {
      file >> dt90_p2[0];
      SG_init->Cells[1][sdt90_p2] = txt.sprintf("%6.5f", dt90_p2[0]);
   }
   if(GetPos (file, "dt0_l2"))
   {
      file >> dt0_l2[0];
      SG_init->Cells[1][sdt0_l2] = txt.sprintf("%6.5f", dt0_l2[0]);
   }
   if(GetPos (file, "dt90_l2"))
   {
      file >> dt90_l2[0];
      SG_init->Cells[1][sdt90_l2] = txt.sprintf("%6.5f", dt90_l2[0]);
   }
   if(GetPos (file, "U_end"))
   {
      file >> U_end[0];
      SG_init->Cells[1][sU_end] = txt.sprintf("%5.2f", U_end[0]);
   }
   return;
}

//---------------------��������� ���� ��������--------------------------------
void TTerm:: SaveFileCoeff(fstream &file)
{
   file << "alfa0 = " << atof(SG_init->Cells[1][salfa0].c_str()) << "\n";
   file << "i_tang = " << atof(SG_init->Cells[1][si_tang].c_str()) << "\n";
   file << "p_tang = " << atof(SG_init->Cells[1][sp_tang].c_str()) << "\n";
   file << "d_tang = " << atof(SG_init->Cells[1][sd_tang].c_str()) << "\n";
   file << "d2_tang = " << atof(SG_init->Cells[1][sd2_tang].c_str()) << "\n";
   file << "int_dtang0 = " << atof(SG_init->Cells[1][sint_dtang0].c_str()) << "\n";
   file << "kl = " << atof(SG_init->Cells[1][skl].c_str()) << "\n";
   file << "namemodem = " << SG_init->Cells[1][snamemodem].c_str() << "\n";
   file << "gaz0 = " << atof(SG_init->Cells[1][sgaz0].c_str()) << "\n";
   file << "tdelta_g_max = " << atof(SG_init->Cells[1][stdelta_g_max].c_str()) << "\n";
   file << "tdelta_g_min = " << atof(SG_init->Cells[1][stdelta_g_min].c_str()) << "\n";
   file << "kren0 = " << atof(SG_init->Cells[1][skren0].c_str()) << "\n";
   file << "i_kren = " << atof(SG_init->Cells[1][si_kren].c_str()) << "\n";
   file << "p_kren = " << atof(SG_init->Cells[1][sp_kren].c_str()) << "\n";
   file << "d_kren = " << atof(SG_init->Cells[1][sd_kren].c_str()) << "\n";
   file << "d2_kren = " << atof(SG_init->Cells[1][sd2_kren].c_str()) << "\n";
   file << "i_rsk = " << atof(SG_init->Cells[1][si_rsk].c_str()) << "\n";
   file << "p_rsk = " << atof(SG_init->Cells[1][sp_rsk].c_str()) << "\n";
   file << "d_rsk = " << atof(SG_init->Cells[1][sd_rsk].c_str()) << "\n";
   file << "d2_rsk = " << atof(SG_init->Cells[1][sd2_rsk].c_str()) << "\n";
   file << "w_max = " << atof(SG_init->Cells[1][sw_max].c_str()) << "\n";
   file << "delta_max = " << atof(SG_init->Cells[1][sdelta_max].c_str()) << "\n";
   file << "i_ay = " << atof(SG_init->Cells[1][si_ay].c_str()) << "\n";
   file << "p_ay = " << atof(SG_init->Cells[1][sp_ay].c_str()) << "\n";
   file << "d_ay = " << atof(SG_init->Cells[1][sd_ay].c_str()) << "\n";
   file << "dH_max = " << atof(SG_init->Cells[1][sdH_max].c_str()) << "\n";
   file << "i_ax = " << atof(SG_init->Cells[1][si_ax].c_str()) << "\n";
   file << "p_ax = " << atof(SG_init->Cells[1][sp_ax].c_str()) << "\n";
   file << "d_ax = " << atof(SG_init->Cells[1][sd_ax].c_str()) << "\n";
   file << "dt0_p1 = " << atof(SG_init->Cells[1][sdt0_p1].c_str()) << "\n";
   file << "dt90_p1 = " << atof(SG_init->Cells[1][sdt90_p1].c_str()) << "\n";
   file << "dt0_l1 = " << atof(SG_init->Cells[1][sdt0_l1].c_str()) << "\n";
   file << "dt90_l1 = " << atof(SG_init->Cells[1][sdt90_l1].c_str()) << "\n";
   file << "dt0_p2 = " << atof(SG_init->Cells[1][sdt0_p2].c_str()) << "\n";
   file << "dt90_p2 = " << atof(SG_init->Cells[1][sdt90_p2].c_str()) << "\n";
   file << "dt0_l2 = " << atof(SG_init->Cells[1][sdt0_l2].c_str()) << "\n";
   file << "dt90_l2 = " << atof(SG_init->Cells[1][sdt90_l2].c_str()) << "\n";
   file << "v_rsk = " << atof(SG_init->Cells[1][sv_rsk].c_str()) << "\n";
   file << "g_kren = " << atof(SG_init->Cells[1][sg_kren].c_str()) << "\n";
   file << "kren_zad_wy = " << atof(SG_init->Cells[1][skren_zad_wy].c_str()) << "\n";
   file << "g_rsk = " << atof(SG_init->Cells[1][sg_rsk].c_str()) << "\n";
   file << "v_kren = " << atof(SG_init->Cells[1][sv_kren].c_str()) << "\n";
   file << "g_tang = " << atof(SG_init->Cells[1][sg_tang].c_str()) << "\n";
   file << "v_tang = " << atof(SG_init->Cells[1][sv_tang].c_str()) << "\n";
   file << "U_end = " << atof(SG_init->Cells[1][sU_end].c_str()) << "\n";
   return;
}

//----------------------------------------------------------------------------
void __fastcall TTerm::Button4Click(TObject *Sender)
{
   SaveDialog->FilterIndex = 4;
   SaveDialog->DefaultExt = "cof";
   SaveDialog->Title = "���������� ��������";
   SaveDialog->InitialDir = ExtractFilePath(Application->ExeName)+"\Mar";

   if (SaveDialog->Execute())
   {
      fstream file;
      FileNameCoeff = SaveDialog->FileName.c_str();
      LNameFile->Caption = FileNameCoeff.c_str();

      file.open(FileNameCoeff.c_str(), ios:: out);
      SaveFileCoeff(file);
      file.close();
      flModifiedCoeff = false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TTerm::Button5Click(TObject *Sender)
{
   pInit->Visible = false;
}

//----------------------------------------------------------------------------
void TTerm::ShowCoeff(void)
{
   String txt;
   SG_init->Cells[1][salfa0] = txt.sprintf("%5.2f", ToGrad*alfa0[iter]);
   SG_init->Cells[1][si_tang] = txt.sprintf("%5.2f", i_tang[iter]);
   SG_init->Cells[1][sp_tang] = txt.sprintf("%5.2f", p_tang[iter]);
   SG_init->Cells[1][sd_tang] = txt.sprintf("%5.2f", d_tang[iter]);
   SG_init->Cells[1][sd2_tang] = txt.sprintf("%5.2f", d2_tang[iter]);
   SG_init->Cells[1][sint_dtang0] = txt.sprintf("%5.2f", int_dtang0[iter]);

   SG_init->Cells[1][skl] = txt.sprintf("%5.2f", kl[iter]);
   SG_init->Cells[1][sgaz0] = txt.sprintf("%5.2f", gaz0[iter]);
   SG_init->Cells[1][sw_max] = txt.sprintf("%5.2f", w_max[iter]);
   SG_init->Cells[1][sdelta_max] = txt.sprintf("%5.2f", delta_max[iter]);

   SG_init->Cells[1][skren0] = txt.sprintf("%5.2f", ToGrad*kren0[iter]);
   SG_init->Cells[1][si_kren] = txt.sprintf("%5.2f", i_kren[iter]);
   SG_init->Cells[1][sp_kren] = txt.sprintf("%5.2f", p_kren[iter]);
   SG_init->Cells[1][sd_kren] = txt.sprintf("%5.2f", d_kren[iter]);
   SG_init->Cells[1][sd2_kren] = txt.sprintf("%5.2f", d2_kren[iter]);

   SG_init->Cells[1][si_rsk] = txt.sprintf("%5.2f", i_rsk[iter]);
   SG_init->Cells[1][sp_rsk] = txt.sprintf("%5.2f", p_rsk[iter]);
   SG_init->Cells[1][sd_rsk] = txt.sprintf("%5.2f", d_rsk[iter]);
   SG_init->Cells[1][sd2_rsk] = txt.sprintf("%5.2f", d2_rsk[iter]);

   SG_init->Cells[1][si_ay] = txt.sprintf("%5.2f", i_ay[iter]);
   SG_init->Cells[1][sp_ay] = txt.sprintf("%5.2f", p_ay[iter]);
   SG_init->Cells[1][sd_ay] = txt.sprintf("%5.2f", d_ay[iter]);
   SG_init->Cells[1][sdH_max] = txt.sprintf("%5.2f", dH_max[iter]);

   SG_init->Cells[1][si_ax] = txt.sprintf("%5.2f", i_ax[iter]);
   SG_init->Cells[1][sp_ax] = txt.sprintf("%5.2f", p_ax[iter]);
   SG_init->Cells[1][sd_ax] = txt.sprintf("%5.2f", d_ax[iter]);
   SG_init->Cells[1][stdelta_g_max] = txt.sprintf("%5.2f", tdelta_g_max[iter]);
   SG_init->Cells[1][stdelta_g_min] = txt.sprintf("%5.2f", tdelta_g_min[iter]);

   SG_init->Cells[1][sdt0_p1] = txt.sprintf("%5.2f", dt0_p1[iter]);
   SG_init->Cells[1][sdt90_p1] = txt.sprintf("%5.2f", dt90_p1[iter]);
   SG_init->Cells[1][sdt0_l1] = txt.sprintf("%5.2f", dt0_l1[iter]);
   SG_init->Cells[1][sdt90_l1] = txt.sprintf("%5.2f", dt90_l1[iter]);
   SG_init->Cells[1][sdt0_p2] = txt.sprintf("%5.2f", dt0_p2[iter]);
   SG_init->Cells[1][sdt90_p2] = txt.sprintf("%5.2f", dt90_p2[iter]);
   SG_init->Cells[1][sdt0_l2] = txt.sprintf("%5.2f", dt0_l2[iter]);
   SG_init->Cells[1][sdt90_l2] = txt.sprintf("%5.2f", dt90_l2[iter]);

   SG_init->Cells[1][sv_rsk] = txt.sprintf("%5.2f", v_rsk[iter]);
   SG_init->Cells[1][sg_kren] = txt.sprintf("%5.2f", g_kren[iter]);
   SG_init->Cells[1][skren_zad_wy] = txt.sprintf("%5.2f", kren_zad_wy[iter]);
   SG_init->Cells[1][sg_rsk] = txt.sprintf("%5.2f", g_rsk[iter]);
   SG_init->Cells[1][sv_kren] = txt.sprintf("%5.2f", v_kren[iter]);
   SG_init->Cells[1][sg_tang] = txt.sprintf("%5.2f", g_tang[iter]);
   SG_init->Cells[1][sv_tang] = txt.sprintf("%5.2f", v_tang[iter]);
   SG_init->Cells[1][sU_end] = txt.sprintf("%5.2f", U_end[iter]);

   return;
}

//---------------------------------------------------------------------------
void __fastcall TTerm::SG_initKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   flModifiedCoeff = true;
}
//---------------------------------------------------------------------------

void __fastcall TTerm::Button3Click(TObject *Sender)
{
   iter--;
   if(iter < 0) iter = 0;
   ShowCoeff();
   flCoeff = true;
   flModifiedCoeff = true;
}

//---------------------------------------------------------------------------

void __fastcall TTerm::Button7Click(TObject *Sender)
{
   CoderPack3 (2, 8, RK_code);   
}
//---------------------------------------------------------------------------
void __fastcall TTerm::ClientSocket1Connect(TObject *Sender,
      TCustomWinSocket *Socket)
{
     ClientSocket1->Socket->SendText(String("user link command ")+SG_init->Cells[1][snamemodem]);
}
//---------------------------------------------------------------------------


void __fastcall TTerm::SG_MarKeyPress(TObject *Sender, char &Key)
{
	Set <char, '0', '9'> Dig;
	Dig << '0' << '1' << '2' << '3' << '4' << '5' << '6' <<'7'<<'8'<< '9';
   if ((Regime == 6) && ((Key == 'g') || (Key == '�')))
      Key = '�';
   else if ((Regime == 6) && ((Key == '\'') || (Key == '�')))
      Key = '\'';
	else if(Key == 8 || Key == 0x2e || Key == 27 || Key == '-' || Key == 0x20) //����� ��� ����� ��� Esq
		;
	else if((Regime == 6) && (Key == 0x2c)) //�������
		;
	else if(Key == 0x2c) //������ ������� �����
		Key = 0x2e;
	else if ( !Dig.Contains(Key) )
	{
		Key = 0;
		Beep();
	}
}
//---------------------------------------------------------------------------

void __fastcall TTerm::SG_initKeyPress(TObject *Sender, char &Key)
{
	Set <char, '0', '9'> Dig;
	Dig << '0' << '1' << '2' << '3' << '4' << '5' << '6' <<'7'<<'8'<< '9';
   if(Key == 8 || Key == 0x2e || Key == 27 || Key == '-' || Key == 'b' || Key == 'd') //����� ��� ����� ��� Esq
		;
	else if(Key == 0x2c) //�������
		Key = 0x2e;
	else if ( !Dig.Contains(Key) )
	{
		Key = 0;
		Beep();
	}
}

//---------------------------------------------------------------------------

void __fastcall TTerm::Button10Click(TObject *Sender)
{
   CoderPack3 (2, 11, RK_code);     
}
//---------------------------------------------------------------------------

void __fastcall TTerm::Button6Click(TObject *Sender)
{
   if(flStoika)
   {
      tang_zad = tang_zad_bort+tang_max;
      if (tang_zad > tang_max) tang_zad = tang_max;
      if (tang_zad < -tang_max) tang_zad = -tang_max;
      CoderPack4(12, tang_zad*1300+8168.5, RK_code);
   }
}
//---------------------------------------------------------------------------

void __fastcall TTerm::Button8Click(TObject *Sender)
{
   if(flStoika)
   {
      tang_zad = tang_zad_bort-tang_max;
      if (tang_zad > tang_max) tang_zad = tang_max;
      if (tang_zad < -tang_max) tang_zad = -tang_max;
      CoderPack4(12, tang_zad*1300+8168.5, RK_code);
   }
}
//---------------------------------------------------------------------------


void __fastcall TTerm::ToolButton4Click(TObject *Sender)
{
   CoderPack3 (2, 14, RK_code);   //������ ���������
}
//---------------------------------------------------------------------------

void __fastcall TTerm::ToolButton9Click(TObject *Sender)
{
   CoderPack3 (2, 15, RK_code);   //���� ���������!
}
//---------------------------------------------------------------------------


