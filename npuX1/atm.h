//---------------------------------------------------------------------------

#ifndef atmH
#define atmH

#include <math.h>
#include "common1.h"
const double temperature0 = 15;

//---------------------------------------------------------------------------
extern const double g, temperature0;

//---------------------------------------------------------------------------
inline double fn_a(double h);
inline double fn_t(double h);
inline double Delta_ro(double h);

double fn_mju(double h);
double fn_nju(double h);
double fn_p(double h);
double fn_ro(double h);


inline double fn_a(double h)
{
   return 20.046796*sqrt(273.15+fn_t(h));
}

inline double fn_t(double h)
{
   return temperature0-0.0065*h;
}

inline double Delta_ro(double h)
{
   return fn_ro(h)/fn_ro(0);
}

//---------------------------------------------------------------------------
#endif
