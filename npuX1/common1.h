#ifndef common1H
   #define common1H
#include <cstring.h>
#include <sysdefs.h>
#include <fstream.h>
#include <math.h>
#include <vcl.h>

#define Rz  6380.
extern const double ToGrad, D_PI; 
const double g = 9.80665;
#define G 9.8065

void OutModem1(unsigned char ByteData, char *mess, char i);
void OutModem2(unsigned int ByteData, char *mess, char i);
void OutModem4(unsigned long int ByteData, char *mess, char i);

bool GetPos (fstream &file, string Caption);
/*  ������ ������� ���������
   z - ��� ������, x - ��� �������   */

//--------------------------------------------------------------------------
class vektor{
public:
	double x,y,z;

	vektor (double vx = 0, double vy = 0, double vz = 0): x(vx), y(vy), z(vz) { };
	vektor (const vektor& v): x(v.x), y(v.y), z(v.z) { };
	vektor& operator = (const vektor& v)
		{ x = v.x; y = v.y; z = v.z; return *this;};
	vektor& operator = (double f)	{ x = y = z = f; return *this;};
	vektor  operator - () const	{ return vektor(-x,-y,-z);};
	vektor& operator += (const vektor& v)
		{x += v.x; y += v.y; z += v.z; return *this;};
	vektor& operator -= (const vektor& v)
		{x -= v.x; y -= v.y; z -= v.z; return *this;};
	vektor& operator *= (const vektor& v);
	vektor& operator *= ( double v)
		{	x *= v;	y *= v; z *= v; return *this;};
	vektor& operator /= ( double v)
		{ x /= v; y /= v; z /= v; return *this;};
	friend vektor operator + (const vektor &u, const vektor &v);//�������� ��������
	friend vektor operator - (const vektor &u, const vektor &v);//��������� ��������
	friend vektor operator * (const vektor &u, const vektor&v)//��������� ����-���.
   {  return vektor(u.y*v.z-u.z*v.y, u.z*v.x-u.x*v.z, u.x*v.y-u.y*v.x);  };
	friend double operator  ^ (const vektor& u, const vektor& v)
			  { return u.x*v.x+u.y*v.y+u.z*v.z; };//��������� ������������.
	friend vektor operator * (const vektor &v, double f)//��������� ������� ��
   {     return vektor (f*v.x, f*v.y, f*v.z);   };    //    ������.
	friend vektor operator * (double f, const vektor &v)
   {     return vektor (f*v.x, f*v.y, f*v.z);   };
	friend vektor operator / (const vektor &v, double f)//������� ������� �� ������.
   {  return vektor (v.x/f, v.y/f, v.z/f);   };
	double operator ! ()const { return (double)sqrt(x*x+y*y+z*z); }//������
	friend double mod(const vektor & v) { return v.x*v.x+v.y*v.y+v.z*v.z; }//������� ������
	friend double cos(const vektor &u, const vektor &v)
   {
		double z = (!u)*(!v);
		if (fabs(z) < DBL_EPSILON) return 0;
		double r = (u^v)/z;
		if (r > 1) r = 1;
		if (r < -1)r = -1;
		return r;
   };
	friend double acos(const vektor &u, const vektor &v)
   {
		double r = acos( cos(u, v) );
		if (r > M_PI) r = M_PI;
		if (r < -M_PI)r = -M_PI;
		return  r;
   };
	void show(void);
};

//�������� ��������----------------------------------------------------------
inline vektor operator + (const vektor &u,const vektor &v)
{
	return vektor (u.x+v.x, u.y+v.y, u.z+v.z);
}

//��������� ��������---------------------------------------------------------
inline vektor operator - (const vektor &u,const vektor &v)
{
	return vektor (u.x-v.x, u.y-v.y, u.z-v.z);
}

//����� �� �����, [���]********************************************************
class KPoint
{
public:
   double Lat, Lon; //������ (Latitude), ������� (Longitude) � ��������

   KPoint(double Lt = 0, double Ln = 0):Lat(Lt), Lon(Ln) {};
      //�������������� �� ����-��� ����-� � �������. ����-�� � ��������
   KPoint(double dz, double dx, int Lat0)
   {
      Lat = atan(dx/Rz);
      Lon = atan(dz/Rz/cos(Lat0/ToGrad));
   };
   KPoint(const vektor &v, int Lat0)
   {
      Lat = atan(v.x/Rz);
      Lon = atan(v.z/Rz/cos(Lat0/ToGrad));
   };
   vektor ToRect(int Lat0) //�������������� �������� �������������� ���������
   {                            // � �� �������������
      return vektor(Rz*tan(Lat), 0, Rz*tan(Lon)*cos(Lat0/ToGrad));
   }

	KPoint (const KPoint& v): Lat(v.Lat), Lon(v.Lon) {};
	KPoint& operator = (const KPoint& v)
      { Lat = v.Lat; Lon = v.Lon; return *this;};

	KPoint  operator - () const	{ return KPoint(-Lat, -Lon);};
	friend KPoint operator + (const KPoint& u, const KPoint& v);
	friend KPoint operator - (const KPoint&, const KPoint&);

	double operator ! ()const     //������  (�����)
   {
      return Rz*sqrt(Lat*Lat+Lon*Lon);
   }
   double dir(int Lat0)
   {
      double x = tan(Lat);
      double z = tan(Lon)*cos(Lat0/ToGrad);

      if ((fabs(x) > FLT_EPSILON) || (fabs(z) > FLT_EPSILON) )
         return atan2(z, x);
      return 0;
   };
   //���������� ��������
	friend bool operator == (const KPoint& u, const KPoint& v)
   {
      std::numeric_limits <double> ep;
      if(fabs(u.Lat-v.Lat) > 5.*ep.epsilon()) return false;
      if(fabs(u.Lon-v.Lon) > 5.*ep.epsilon()) return false;
      return true;
   };

   //����� � ����  - ������������� �� �����
   friend fstream &operator<<(fstream &file, KPoint &obj);
   friend fstream &operator >>(fstream &file,KPoint &obj);
   friend bool FromString(String src, KPoint &a);

   String show(void);
   String showLat(void);
   String showLon(void);
};


#endif

