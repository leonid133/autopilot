#include "common1.h"

const double ToGrad = 57.2957795130823;
const double D_PI = 6.28318530717958647692;

//------------------------------------------------------------------------------
void OutModem1(unsigned char Data, char mess[], char i)
{
	mess[i] = (Data | 0x80);
//   mess[i+1] = 0;
}

//------------------------------------------------------------------------------
void OutModem2(unsigned int Data, char mess[], char i)
{
	mess[i] = (Data & 0x007f)| 0x80;
	mess[i+1] = ((Data & 0x3f80) >> 7)| 0x80;
//   mess[i+2] = 0;
}

//------------------------------------------------------------------------------
void OutModem4(unsigned long int Data, char mess[], char i)
{
	mess[i] = (Data & 0x007f)| 0x80;
	mess[i+1] = ((Data & 0x3f80) >> 7) | 0x80;
	mess[i+2] = ((Data & 0x1fc000) >> 14) | 0x80;
	mess[i+3] = ((Data & 0xfe00000) >> 21) | 0x80;
//   mess[i+4] = 0;
}

//----------------------------------------------------------------------------
bool GetPos (fstream &file, string Caption)
{
   string temp;
   file.seekg(0);
   while (!file.eof())
   {
      file >> temp;
      if(temp == Caption)
      {
         file >> temp;
         return true;
      }
   }
   MessageBox(NULL,"������ �� ������", Caption.c_str(), MB_OK);
   return false;
};

//���������� �������� +  ---------------------------------------------------

KPoint operator + (const KPoint& u,const KPoint& v)
{
	return KPoint (u.Lat+v.Lat, u.Lon+v.Lon);
}

//���������� �������� -  ---------------------------------------------------

KPoint operator - (const KPoint& u,const KPoint& v)
{
	return KPoint (u.Lat-v.Lat, u.Lon-v.Lon);
}

//����� ������ �����: <������> ��.� ' , <�������> ��.� '-------------------
String KPoint:: show(void)
{
   return showLat() + ", " + showLon();
}

//����� ������ �����: <������> ��.� '---------------------------------------
String KPoint:: showLat(void)
{
   double Minutes;
   String returnString, bufferMinutes;

   long Gradus = Lat*ToGrad;        //����� ����� �������� � �������
   if (Lat >= 0)                          // ���� ���� � ��� ������������
   {
      Minutes = (Lat*ToGrad-Gradus)*60;   //���� ������� ����� * �� 60 = ������
      returnString = IntToStr(Gradus)+"� ";   //���������� �������
   }
   else if (Gradus > -1)           // ���� ����� ���� � �������� ����� � ����� � 0
   {                                    // �� ����� �������� ��� � ��- ������
      Minutes = -Lat*ToGrad*60;
      returnString = "-0� ";
   }
   else
   {
      Minutes = (Gradus-Lat*ToGrad)*60;
      returnString = IntToStr(Gradus)+"� ";
   }
   bufferMinutes.sprintf("%6.4f'", Minutes);
   return returnString + bufferMinutes;
}

//����� ������ �����: <�������> ��.� '--------------------------------------
String KPoint:: showLon(void)
{
   double Minutes;
   String returnString, bufferMinutes;

   if (Lon < 0) Lon = Lon +2*M_PI;
   if (Lon > 2*M_PI) Lon = Lon - 2*M_PI;

   double copyLon = Lon;
   if(copyLon > M_PI )  copyLon = copyLon - 2* M_PI;
   long Gradus = copyLon*ToGrad;
   if (copyLon >= 0)
   {
      Minutes = (copyLon*ToGrad-Gradus)*60;
      returnString = IntToStr(Gradus)+"� ";
   }
   else if (Gradus > -1)
   {
      Minutes = -copyLon*ToGrad*60;
      returnString = "-0� ";
   }
   else
   {
      Minutes = (Gradus-copyLon*ToGrad)*60;
      returnString = IntToStr(Gradus)+"� ";
   }
   bufferMinutes.sprintf("%6.4f'", Minutes);
   return returnString + bufferMinutes;
}

//-- ����� � ���� --------------------------------------------------------------
fstream &operator<<(fstream &file, KPoint &obj)
{
   String temp;
   temp = obj.show();
   file << temp.c_str();
   return file;
}

//--- ������������� �� �����-------------------------------------------------------
fstream &operator >>(fstream &file, KPoint &obj)
{
   bool minus = false;
   double Lat,Lon;
   string tmp;

   file >> tmp;
   tmp.erase(tmp.length()-1,1);  //�������� �����������
   Lat = atoi(tmp.c_str())/ToGrad;
   if(tmp.c_str()[0] == '-')             //����� �.�. -0 == 0
   {
      minus = true;
      Lat = -Lat;
   }

   file >> tmp;
   tmp.erase(tmp.length()-2,2);
   obj.Lat = Lat + atof(tmp.c_str())/ToGrad/60;
   if(minus) obj.Lat = -(obj.Lat);
   minus = false;

   file >> tmp;
   tmp.erase(tmp.length()-1,1);
   Lon = atoi(tmp.c_str())/ToGrad;
   if(tmp.c_str()[0]== '-')             //����� �.�. -0 == 0
   {
      minus = true;
      Lon = -Lon;
   }

   file>>tmp;
   tmp.erase(tmp.length()-1,1);
   obj.Lon = Lon + atof(tmp.c_str())/ToGrad/60;
   if(minus ) obj.Lon = 2*M_PI - obj.Lon;
   minus = false;

   return file;
}

//----------------------------------------------------------------------------
bool FromString(String src, KPoint &a)
{
   bool flminus = false;
   char *s = src.c_str(), tmp[10];
   for(int i = 0; i < 10; i++)
   {
      if (*s == '�')
      {
         tmp[i] = 0;
         s++;
         int lat = atoi(tmp);
         if (lat > 90)
         {
            Application->MessageBox("������ > 90�", "������");
            return false;
         }
         if (lat < -90)
         {
            Application->MessageBox("������ < -90�", "������");
            return false;
         }
         a.Lat = (double)lat/ToGrad;
         break;
      }
      tmp[i] = *s;
      if(tmp[i] == '-')
         flminus = true;
      s++;
   }
   for(int i = 0; i < 10; i++)
   {
      if (*s == '\'')
      {
         tmp[i] = 0;
         s++;
         double lat = atof(tmp);
         if (lat > 60)
         {
            Application->MessageBox("������ ������ > 60�", "������");
            return false;
         }
         if (lat < 0)
         {
            Application->MessageBox("������ ������ < 0�", "������");
            return false;
         }
         if (flminus && a.Lat == 0)
            a.Lat = -lat/60./ToGrad;
         else
            a.Lat = a.Lat+lat/60./ToGrad;
         break;
      }
      tmp[i] = *s;
      s++;
   }
   s++;
   flminus = false;
   for(int i = 0; i < 10; i++)
   {
      if (*s == '�')
      {
         tmp[i] = 0;
         s++;
         int lon = atoi(tmp);
         if (lon > 180)
         {
            Application->MessageBox("������� > 180�", "������");
            return false;
         }
         if (lon < -180)
         {
            Application->MessageBox("������� < -180�", "������");
            return false;
         }

         a.Lon = (double)lon/ToGrad;
         break;
      }
      tmp[i] = *s;
      if(tmp[i] == '-')
         flminus = true;
      s++;
   }
   for(int i = 0; i < 10; i++)
   {
      if (*s == '\'')
      {
         tmp[i] = 0;
         double lon = atof(tmp);
         if (lon > 60)
         {
            Application->MessageBox("������ ������� > 60�", "������");
            return false;
         }
         if (lon < 0)
         {
            Application->MessageBox("������ ������� < 0�", "������");
            return false;
         }
         if (flminus && a.Lon == 0)
            a.Lon = -lon/60./ToGrad;
         else
            a.Lon = a.Lon+lon/60./ToGrad;
         break;
      }
      tmp[i] = *s;
      s++;
   }
   return true;
}


