//----------------------------------------------------------------------------
#ifndef Main11H
#define Main11H
//----------------------------------------------------------------------------
#include "common1.h"
#include "flying.h"

//----------------------------------------------------------------------------
#include <vcl\ComCtrls.hpp>
#include <vcl\ExtCtrls.hpp>
#include <vcl\Messages.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\Dialogs.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\Classes.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Windows.hpp>
#include <vcl\System.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <StdActns.hpp>
#include <ToolWin.hpp>
#include <Grids.hpp>
#include <CPort.hpp>
#include <mmsystem.hpp>
#include <jpeg.hpp>
#include <ScktComp.hpp>
#include <MPlayer.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdUDPBase.hpp>
#include <IdUDPServer.hpp>
#include <Sockets.hpp>
#include <IdAntiFreeze.hpp>
#include <IdAntiFreezeBase.hpp>

//------------------------------------------------------------------------------
#define TOP_DEVICE 0
#define WIDTH_MONITOR 49

extern double t, t_interval, t_pr, t_GPS, dt_GPS;
extern int x_Plane[], y_Plane[];

//----------------------------------------------------------------------------
class TTerm : public TForm
{
__published:
   TSaveDialog *SaveDialog;
   TOpenDialog *OpenDialog;
   TTimer *Timer1;
   TImageList *ImageList;
   TImageList *ImageList_Disabled;
   TColorDialog *ColorDialog1;
   TPageControl *PageControl;
   TTabSheet *TabSheet1;
   TTabSheet *TabSheet2;
   TPanel *P3_1;
   TPanel *P_RegimeV;
   TPanel *Panel;
   TPanel *SB_t;
   TPanel *SB_RegimeFly;
   TPanel *P_PointFly;
   TPanel *P_ZX;
   TStringGrid *SG;
   TToolBar *ToolBar2;
   TToolButton *ToolButton5;
   TToolButton *ToolButton6;
   TToolButton *ToolButton7;
   TImageList *ImageList2;
   TMemo *Memo;
   TStringGrid *SG_Mar;
   TStatusBar *StatusBar5;
   TToolBar *ToolBar1;
   TComboBox *CB_TypeWork;
   TToolButton *TB_StartStop;
   TToolButton *TB_Test;
   TToolButton *TB_Ok;
   TPanel *Panel1;
   TPanel *Panel2;
   TPanel *Panel3;
   TPanel *Panel4;
   TPanel *Panel5;
   TToolButton *ToolButton1;
   TToolButton *ToolButton2;
   TToolButton *ToolButton3;
   TImage *PB;
   TPanel *pInit;
   TButton *Button1;
   TButton *Button2;
   TButton *Button3;
   TButton *Button4;
   TButton *Button5;
   TStringGrid *SG_init;
   TButton *Button7;
   TClientSocket *ClientSocket1;
   TLabel *LNameFile;
   TButton *Button10;
   TButton *Button6;
   TButton *Button8;
   TPanel *Panel6;
   TToolButton *ToolButton4;
   TToolButton *ToolButton8;
   TToolButton *ToolButton9;
        TIdUDPServer *IdUDPServer1;
        TUdpSocket *UdpSocket1;

   void __fastcall B4Click_Mar1  (TObject *Sender) ;
   void __fastcall OpenFileMar  (TObject *Sender) ;
   void __fastcall MarSave  (TObject *Sender) ;
   void __fastcall ChancelFly  (TObject *Sender) ;

   void __fastcall FormResize (TObject *Sender);
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall FormNonClose(TObject *Sender, TCloseAction &Action);
   void __fastcall FormPaint(TObject *Sender);

   void __fastcall FormKeyUp (TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);

   void __fastcall Timer1Timer     (TObject *Sender);

   void __fastcall FormMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
   void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall FormMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);

   void __fastcall TB_OkClick(TObject *Sender);
   void __fastcall TB_StartClick  (TObject *Sender) ;

   void __fastcall CB_TypeWorkChange(TObject *Sender);

   void __fastcall SGDblClick(TObject *Sender);
   void __fastcall SGKeyPress(TObject *Sender, char &Key);
   void __fastcall SGKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
   void __fastcall SGSelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect);

   void __fastcall SG_MarKeyPress(TObject *Sender, char &Key);
   void __fastcall SG_MarKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
   void __fastcall SG_MarSelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect);

   void __fastcall SG_initKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
   void __fastcall SG_initKeyPress(TObject *Sender, char &Key);

   void __fastcall TB_TestClick(TObject *Sender);
   void __fastcall ToolButton2Click(TObject *Sender);
   void __fastcall ToolButton3Click(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall Button4Click(TObject *Sender);
   void __fastcall Button5Click(TObject *Sender);
   void __fastcall Button3Click(TObject *Sender);
   void __fastcall Button7Click(TObject *Sender);
   void __fastcall ClientSocket1Connect(TObject *Sender,
          TCustomWinSocket *Socket);
   void __fastcall Button10Click(TObject *Sender);
   void __fastcall Button6Click(TObject *Sender);
   void __fastcall Button8Click(TObject *Sender);
   void __fastcall ToolButton4Click(TObject *Sender);
   void __fastcall ToolButton9Click(TObject *Sender);

public:
   void Start(void);
   POINT Net0;
   POINT Center, Center0;  //���������� ������ Map �� ImageTmp
   double Zoom, Zoom0;     //RectMap = Zoom*RectMapInImageTmp
   TRect RectMap;    //������� ������� �� ����� ��� ��������� �����

   int Lat0;                  // ����� ������ ����� ������� ��������
   KPoint Point0Map;                         // ��� ������� ����
   float StepNet;
   double ScaleMap;
//---------------
   float rsk_gir, kren_gir, tang_gir;
//---------------
   unsigned char last_reset_source, count_last_reset_source;
private:
   int iter, iter_pr;
   float dt0_p1[1024], dt90_p1[1024], dt0_l1[1024], dt90_l1[1024], dt0_p2[1024], dt90_p2[1024], dt0_l2[1024], dt90_l2[1024],
      alfa0[1024], i_tang[1024], p_tang[1024], d_tang[1024], d2_tang[1024], int_dtang0[1024],
      kren0[1024], i_kren[1024], p_kren[1024], d_kren[1024], d2_kren[1024], kren_zad_wy[1024],
      i_rsk [1024], p_rsk[1024], d_rsk[1024], d2_rsk[1024],
      i_ay[1024], p_ay[1024], d_ay[1024], dH_max[1024],
      i_ax[1024], p_ax[1024], d_ax[1024],
      v_rsk[1024], g_kren[1024], g_rsk[1024], v_kren[1024],
      v_tang[1024], g_tang[1024], kl[1024], gaz0[1024], tdelta_g_max[1024], tdelta_g_min[1024], w_max[1024], delta_max[1024], U_end[1024];
      float ax_dat, ay_dat, az_dat;
   char flCoeff;
   void LoadFileCoeff(fstream &stream);
   void SaveFileCoeff(fstream &stream);
   void ShowCoeff(void);

   string FileNameImage, FileNameMar, FileNameCoeff;
   String INIFile;
   bool flModifiedFile, flModifiedCoeff;
   POINT sMouse, sMouse0;
   String Hint1, Hint2;
public:
   Graphics::TBitmap *ImageTmp;
   Graphics:: TBitmap *Map;   //��������� ����� ��������� ���������� � ����������

   POINT ToScreen (const KPoint &EarthPoint);
   POINT ToMap (const KPoint &EarthPoint);
   KPoint ToEarth(POINT PointScreen);

private:
   KPoint eMouse;          // �������������� ���������� "����"  (������� )

//   __fastcall TForm1(TComponent* Owner);
   void __fastcall MyOnHint (TObject *Sender);
public:
   int Regime;             /* 0 - ������
                              1 - ������ ���������
                              2 - ����������� �����
                              3 -
                              4 - zoom
                              5 -
                              6 - ��������� ��������
                              7 - �����������                        */
bool flStoika, flStoika_bort;
public:
   bool keyAlt, keyShift, keyCtrl;  //co������� ����������� ������ ����������

   char F_buffer[65536], RK_code[256];
   int size_F_buffer;
   void CoderPack13 ( const KPoint & pTag, int H_zad, int Vz_zad,char *mess);
   void CoderPack4  ( char NPackage, int ByteData, char *mess);
   void CoderPack3  ( char NPackage, char ByteData, char *mess);

   char AnalisBuffer(void);
   void AnalisRK_code(void);

   KPoint PointMar[128], PointMar_pr;
   int H_Mar[128], Vz_Mar[128];
   int ColSG, RowSG;
   int  i_mar, i_mar_pr, n_, n_3, napr_vet, napr_vzlet, TypeWork;
   double   V_vet;
   TColor ColorTraectoria;
   KPoint PointFly;           // ���������� ��

   int rgfly, rgfly_bort;  /*������������� ��� ������ ����������
                  0 - �����������(����, ���� �����, ������, ��������)
                  1- ���������� ���������� (����, ���� �����, ���, ���� ������� ����������)
                  2 - ���������� ����� (� �������������� �����-��������)
                  3- �������� ��������
                  4 - �������
                  */

   int rgStart;                     /* 0 - �����
												1 - ����
												2 - ����������� ����
												3 - ���� ����������
												4 - ��������� ����������
                                    5 - �����!
                                 	6 - �����                                */

   bool flKoord;            /* true - ��������� ����
                                       false - ���                           */
   bool flOtkazRK;//, lWDTRun;
   float n_error;
private:
   int jnum;
   bool connect_joy;

   //-------------------------------------------------------------------------
public:
   double napr_toch_mar, rasst_toch_mar, napr_vetv_mar, otkl_ot_mar;
   float kren_zad, kren_zad_bort, koors_zad, tang_zad, tang_zad_bort;
   int koors_zad_bort, koors_zad1, n_bort;
   float koors;
   int kren_kam, kren_kam_zad,  kren_kam_zad_bort;
   int ugol_kam, ugol_kam_zad, ugol_kam_zad_bort;
   double Vy;
   int Vy_zad, Vy_zad_bort;

   void DrawDevice(void);
   POINT ToDevice(int x, int y, double kren_r);
   int xCenter, yCenter;

public:
   //-------------------------------------------------------------------------
   class KMonitor
   {
   protected:
      int x0, x1, x2, x3, h;
      double k;
      float Value;
      int MinValue, MaxValue, Step;
      int Left, Top, Heigth, Width;
      bool flColorBg;

      String NameMonitor, SizeValue;
      Graphics:: TBitmap *Bitmap;
      virtual void DrawBitMap(void);

      int GetTop(int Value)
      { return 0.87*Heigth-(Value-MinValue)*Heigth*0.7/(MaxValue-MinValue);}

   public:
      KMonitor(int Left, int MinValue, int MaxValue, int Step
               , String NameMonitor, double k = 1);
      ~KMonitor(){if (Bitmap) delete Bitmap;};
      void SetValue(float Value);
      float GetValue (void) {return Value;};
      virtual void Draw(void);
   }U, I, C;

   //------------------------------------------------------------------------
   class Monitor: public KMonitor
   {
   protected:
      float Value_zad, Value_B_zad;

   public:
      Monitor(int Left, int MinValue, int MaxValue, int Step
               , String NameMonitor, double k = 1);
      ~Monitor(){};
      void SetValue(float Value);
      void SetValue_zad(float Value);
      float GetValue_zad(void) {return Value_zad;};
      void SetValue_B_zad(float Value);
      float GetValue_B_zad(void) {return Value;};
      void Draw(void);
      bool Command(void);
   }H, Vz, V, delta_g, delta_u;

   void LoadFileMarsh(fstream &stream);
   void SaveFileMarsh(fstream &stream);
   flying *MyFly;

private:
   unsigned int      FromFly2 ( char *mess, char i );
   unsigned long int FromFly4 ( char *mess, char i );

	virtual __fastcall TTerm(TComponent *Owner);
   __fastcall virtual ~TTerm(void)
   {
         delete ImageTmp;
         delete Map;
   };
};
//----------------------------------------------------------------------------
extern TTerm *Term;

//----------------------------------------------------------------------------
#endif

