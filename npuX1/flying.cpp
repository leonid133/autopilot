#include "flying.h"
#include "Main11.h"
#include <math.h>
#include <limits.h>
#include <vector>
                                                             
const double FREQ_GIR = 200;
const double dt_gir =  1.0/FREQ_GIR;

const double tang_max = 20.0/ToGrad, kren_max = 30.0/ToGrad, Vy_max = 5, Vy_min = -2;
const int V_max = 25, V_min = -5, V_bum = 80/3.6;
const double Wak_max = 3.3; //A/� ��� 11.1 �
const float delta_u_max = 110.0/ToGrad, delta_u_min = 0, delta_u_upor_max = (70.0/ToGrad);
const float delta_g_max = 100.0, delta_g_min = 0;
const float delta_ax_max = 100.0, delta_ax_min = (-30.0);
const float delta_ay_max = 100.0, delta_ay_min = 0;
const double a_max = 40;
const float alfa_max = 15.3, alfa_min = -11.1;
const float Wak0 = 6.0*3600;
#define d_delta_g_max (0.4*delta_g_max*dt_gir)

//���-51-2
#define lx 0.170
#define ly 0.02
#define lz (-0.005)

#define xct0 0.102
#define yct0 0.030
#define zct0 0

#define 	Gla 5.3
#define 	Sla 0.051          //???
#define  Ba 0.08       //???
#define  L 0.43   //???
#define 	Cy_max 3.117
#define		alfa_max 	(-6.738+6.67*Cy_max)/ToGrad		//	14,05239/ToGrad[���]
#define 	Cy_alfa		(ToGrad/6.67)	//[1/���]
#define		Cy0			(6.738/6.67)
#define 	alfa_min 	(-5.0/ToGrad)//[���]

#define H_avar   	250

#define delta_u_max (110.0/ToGrad)	//���� �� ���������������� � delta_ax_min
#define delta_u_min 0.0
#define delta_u_upor_max (70.0/ToGrad)

#define delta_g_max 100.0
#define delta_g_min 0.0
#define ns_max (600/60*3.7*6)//������������ �������� �������� ����� [��/�]

#define delta_ay_max 100
#define delta_ay_min 0

#define delta_ax_max 100.0
#define delta_ax_min (-30)		//delta_ax_min = -delta_g_max*tan((delta_u_max-90)/ToGrad);

#define V_max  25
#define V_min  -5
//----------------------------------------------------------------------------
bpla:: bpla(void)
{
 //  Term->ComPort->Open();
};

//----------------------------------------------------------------------------
bpla:: ~bpla(void)
{
  // Term->ComPort->Close();
};

//----------------------------------------------------------------------------
int bpla:: read(char Buffer[])
{
   int size_Buffer =0;// Term->ComPort->InputCount();
   if (size_Buffer)
   {
      if (size_Buffer > 65535)  size_Buffer = 65535;
     // Term->ComPort->Read(Buffer, size_Buffer);
   }
   Buffer[size_Buffer] = 0;
   return size_Buffer;
}

//----------------------------------------------------------------------------
void bpla:: write(char *RK_code)
{
 //  Term->ComPort->WriteStr(String(RK_code));
   return;
};

//----------------------------------------------------------------------------
piter:: piter(void)
{
//   Term->ClientSocket1->Host = String("127.0.0.1");
//   Term->ClientSocket1->Port = 64300;
   Term->ClientSocket1->Active = 1;
};

//----------------------------------------------------------------------------
piter:: ~piter(void)
{
   Term->ClientSocket1->Active = 0;
};

//----------------------------------------------------------------------------
int piter:: read(char Buffer[])
{
   int size_Buffer = Term->ClientSocket1->Socket->ReceiveLength();
   if (size_Buffer)
   {
      if (size_Buffer > 1024)  size_Buffer = 1024;

      Term->ClientSocket1->Socket->ReceiveBuf(Buffer, size_Buffer);
   }
   Buffer[size_Buffer] = 0;
   return size_Buffer;
}

//----------------------------------------------------------------------------
void piter:: write(char *RK_code)
{
   Term->ClientSocket1->Socket->SendText(String(RK_code));
   return;
};
//----------------------------------------------------------------------------
wifi:: wifi(void)
{
   Term->UdpSocket1->Active = true;
};
wifi:: ~wifi(void)
{
   Term->UdpSocket1->Active = false;
};
int wifi:: read(char Buffer[])
{
   try
   {
        int size_Buffer = Term->IdUDPServer1->ReceiveBuffer(Buffer, Term->IdUDPServer1->BufferSize, 100);
        if(size_Buffer<=0)
                return 0;
        Buffer[size_Buffer] = 0;
        return size_Buffer;
   }
   catch(...){
        return 0;
   }

};

void wifi:: write(char *RK_code)
{
   try
   {
      Term->UdpSocket1->Sendln(RK_code, '\x00');
      return;
   }
   catch(...){
        return;
   }
};

//----------------------------------------------------------------------------
novosib:: novosib(void)
{

};
novosib:: ~novosib(void)
{

};
int novosib:: read(char Buffer[])
{
    return 0;
};
void novosib:: write(char *RK_code)
{
    return;
};
              
